# coding: utf-8
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.conf import settings
from django.shortcuts import get_object_or_404

from .models import Filter, FilterOption
from ..catalog.models import Product
from ..catalog.utils import filter_product
from ..catalog.utils import get_filters_as_dict

from ..catalog.settings import STANDARD_PRODUCT
from ..catalog.settings import VARIANT

from urllib import urlencode


def get_filter_options(request, filter_id):
    # get saved filters
    set_product_filters = dict(request.GET)
    for key, values in set_product_filters.items():
        for value in values:
            for val in value.split(','):
                if not FilterOption.objects.filter(
                        filter__identificator=key,
                        identificator=val).exists():
                    del set_product_filters[key]
    _filter = Filter.objects.get(pk=filter_id)

    product_filter, show_hide_button = \
        filter_options(_filter, set_product_filters)

    return render_to_response(
        "lfs/filters/filter.html",
        RequestContext(request, {
            'filter': product_filter,
            'show_hide_button': show_hide_button,
        }))


def get_filter_children(request, filter_id):
    set_product_filters = dict(request.GET)
    set_product_filters.pop('start', None)
    option = get_object_or_404(FilterOption, pk=filter_id)
    filters = option.filter.children.all()

    if option.filter.identificator in set_product_filters and \
            option.identificator not in \
            set_product_filters[option.filter.identificator]:
        set_product_filters[option.filter.identificator] \
            .append(option.identificator)
    else:
        set_product_filters[option.filter.identificator] = \
            [option.identificator]

    result = get_filters_as_dict(
        filters,
        [option.filter.category],
        set_product_filters)

    return render_to_response(
        "lfs/filters/filter-children.html",
        RequestContext(request, {
            'filters': result,
        }))


def filter_options(_filter, product_filter):
    SHOW_FILTER_COUNTERS = getattr(settings, 'SHOW_FILTER_COUNTERS', True)
    USE_HR_URLS = getattr(settings, 'USE_HR_URLS', False)
    GENERATE_HR_URLS_FOR_FO = getattr(settings, 'GENERATE_HR_URLS_FOR_FO', False)
    product_filter.pop('start', None)

    params = {}
    for key, value in product_filter.items():
        params[key] = ','.join(value)

    if USE_HR_URLS and GENERATE_HR_URLS_FOR_FO:
        from hr_urls.utils import get_best_template
        base_param = dict(params)
        for key, value in base_param.items():
            if value.count(',') > 0 and key != _filter.identificator:
                del base_param[key]
        if _filter.identificator not in base_param:
            base_param.update({_filter.identificator: ''})
            template1 = template2 = get_best_template(
                _filter.category, base_param)
        elif base_param[_filter.identificator].count(',') < 2:
            template1 = get_best_template(
                _filter.category, base_param)
            del base_param[_filter.identificator]
            template2 = get_best_template(
                _filter.category, base_param)
        elif base_param[_filter.identificator].count(',') >= 2:
            del base_param[_filter.identificator]
            template1 = template2 = get_best_template(
                _filter.category, base_param)

    if product_filter:
        ck_product_filter = ""
        for key, value in product_filter.items():
            ck_product_filter += key + "|"
            ck_product_filter += "|".join(value)
    else:
        ck_product_filter = ""

    categories = [_filter.category]

    products = Product.objects.filter(
        active=True,
        status__is_visible=True,
        categories__in=categories,
        sub_type__in=[STANDARD_PRODUCT, VARIANT])\
        .distinct().select_related()

    products_before_initial = products
    # filtered products
    products = filter_product(categories, products, product_filter)
    ids = [p['id'] for p in products.values('id')]

    options = _filter.filteroption_set.all().order_by('position')
    # mark checked options
    for o in options:
        if _filter.identificator in product_filter:
            o.checked = o.identificator in \
                product_filter[_filter.identificator][0].split(',')

    checked = 0
    show_hide_button = False
    if _filter.type != 20:
        _filters = dict(product_filter)
        _filters.pop(_filter.identificator, None)

        products_with_out_f = filter_product(
            categories, products_before_initial, _filters)

        idents = []
        if SHOW_FILTER_COUNTERS is False:
            idents = [
                x['identificator'] for x in FilterOption.objects.filter(
                    filter__identificator=_filter.identificator,
                    products__in=products_with_out_f
                ).distinct().values('identificator')
            ]

        for o in options:
            if ((hasattr(o, 'checked') and o.checked)) \
                    or not _filter.is_addition:
                checked += 1

                if SHOW_FILTER_COUNTERS:
                    o.quantity = Product.objects.filter(
                        pk__in=ids, filteroption__id=o.id).count()
                else:
                    o.quantity = 1 if o.identificator in idents else 0
            elif _filter.is_addition:
                if SHOW_FILTER_COUNTERS:
                    o.quantity = products_with_out_f.filter(
                        filteroption__id=o.id).count()
                else:
                    o.quantity = 1 if o.identificator in idents else 0

            if o.quantity > 0 and not o.is_popular:
                show_hide_button = True

            # generate links for fitler options
            if USE_HR_URLS and GENERATE_HR_URLS_FOR_FO:
                # import pdb
                # pdb.set_trace()
                template = template1
                get_params = dict(params)
                if _filter.identificator not in get_params:
                    get_params.update(
                        {_filter.identificator: o.identificator})
                elif o.identificator in get_params[_filter.identificator]:
                    if get_params[_filter.identificator] == o.identificator:
                        del get_params[_filter.identificator]
                    else:
                        ps = get_params[_filter.identificator].split(',')
                        if o.identificator in ps:
                            ps.remove(o.identificator)
                        get_params[_filter.identificator] = ','.join(ps)
                    template = template2
                else:
                    ps = get_params[_filter.identificator].split(',')
                    ps += [o.identificator]
                    get_params[_filter.identificator] = ','.join(ps)
                    if len(ps) == 1:
                        template = template1
                    else:
                        template = template2
                get_params = urlencode(get_params).replace('%2C', ',')
                # from hr_urls.utils import get_hr_urls
                # o.link = '/%s' % categories[0].slug + get_hr_urls(
                #     categories[0],
                #     get_params,
                # )

                from hr_urls.utils import get_formatted_url
                link = get_formatted_url(template, get_params)
                o.link = '/%s' % categories[0].slug
                if link != '/':
                    o.link += link
                o.link = o.link.replace('%2C', ',')

    return {
        'id': _filter.id,
        'show_as_tree': _filter.show_as_tree,
        'identificator': _filter.identificator,
        'title': _filter.get_title(),
        'special_filter': _filter.special_filter,
        'template_name': _filter.get_template_name(),
        'type': _filter.type,
        'options': options,
        'is_addition': _filter.is_addition,
        'checked': checked,
        'use_after_choose': _filter.use_after_choose,
        'show_filter_counters': SHOW_FILTER_COUNTERS,
    }, show_hide_button
