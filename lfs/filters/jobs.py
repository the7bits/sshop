# coding: utf-8


def update_filters_job(*category_ids, **kwargs):
    from .utils import update_filters
    for category_id in category_ids:
        update_filters(category_id)

update_filters_job.allow_concatenate_args = True
