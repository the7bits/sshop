# coding: utf-8
import logging

from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save

from ..catalog.models import PropertyType
from ..catalog.settings import PROPERTY_TYPE_FLOAT
from .models import Filter


logger = logging.getLogger('sshop')


def filter_post_save_listener(sender, instance, **kwargs):
    """Automatically add PropertyType with type=PROPERTY_TYPE_FLOAT when
    new slider creates"""
    properties = instance.properties.all()
    if instance.type == 20:  # Slider
        for p in properties:
            try:
                p_type = PropertyType.objects.get(
                    # categories=instance.category,
                    property=p,
                )
                p_type.categories.add(instance.category)
                p_type.save()
            except PropertyType.DoesNotExist:
                p_type = PropertyType.objects.create(
                    property=p, type=PROPERTY_TYPE_FLOAT)
                p_type.categories.add(instance.category)
                p_type.save()
                logger.info(_(
                    u'New PropertyType pk=%(p_type)s with type Float was '
                    u'added when filter "%(filter)s" for category '
                    u'"%(category)s" was added.') % {
                        'p_type': p_type.pk,
                        'filter': instance.get_title(),
                        'category': instance.category.name,
                    })
            except PropertyType.MultipleObjectsReturned:
                logger.error(_(
                    u'More than one PropertyType for category "%(category)s" '
                    u'and property "%(property)s".') % {
                        'category': instance.category.name,
                        'property': p.name,
                    })

post_save.connect(filter_post_save_listener, sender=Filter)
