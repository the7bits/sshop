# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MailTemplate'
        db.create_table('mail_mailtemplate', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(default='', max_length=30)),
            ('comment', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('text_template', self.gf('django.db.models.fields.TextField')(default='')),
            ('html_template', self.gf('django.db.models.fields.TextField')(default='')),
        ))
        db.send_create_signal('mail', ['MailTemplate'])


    def backwards(self, orm):
        # Deleting model 'MailTemplate'
        db.delete_table('mail_mailtemplate')


    models = {
        'mail.mailtemplate': {
            'Meta': {'object_name': 'MailTemplate'},
            'comment': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'html_template': ('django.db.models.fields.TextField', [], {'default': "''"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '30'}),
            'text_template': ('django.db.models.fields.TextField', [], {'default': "''"})
        }
    }

    complete_apps = ['mail']