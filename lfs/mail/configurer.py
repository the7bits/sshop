# coding: utf-8
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

from adminconfig.utils import BaseConfig

# Email options


class EmailConfigForm(forms.Form):
    email_backend = forms.CharField(
        label=_(u'Backend'),
        widget=forms.widgets.TextInput(
            attrs={'class': 'input-xlarge'}))
    email_host = forms.CharField(label=_(u'Host'))
    email_port = forms.IntegerField(label=_(u'Port'))
    email_host_user = forms.CharField(
        label=_(u'Host user'), required=False)
    email_host_password = forms.CharField(
        label=_(u'Host password'),
        required=False,
        widget=forms.PasswordInput(
            attrs={'autocomplete': 'off'}))
    default_format = forms.ChoiceField(
        label=_(u'Default format'), choices=settings.EMAIL_FORMAT_CHOICES)


class EmailConfig(BaseConfig):
    """Configurator for mail options in config.
    """
    form_class = EmailConfigForm
    block_name = 'email'

    def __init__(self):
        super(EmailConfig, self).__init__()

        self.default_data = {
            'EMAIL_BACKEND': settings.EMAIL_BACKEND,
            'EMAIL_HOST': settings.EMAIL_HOST,
            'EMAIL_PORT': settings.EMAIL_PORT,
            'EMAIL_HOST_USER': settings.EMAIL_HOST_USER,
            'EMAIL_HOST_PASSWORD': settings.EMAIL_HOST_PASSWORD,
            'EMAIL_DEFAULT_FORMAT': settings.EMAIL_DEFAULT_FORMAT,
        }

        self.option_translation_table = (
            ('EMAIL_BACKEND', 'email_backend'),
            ('EMAIL_HOST', 'email_host'),
            ('EMAIL_PORT', 'email_port'),
            ('EMAIL_HOST_USER', 'email_host_user'),
            ('EMAIL_HOST_PASSWORD', 'email_host_password'),
            ('EMAIL_DEFAULT_FORMAT', 'default_format'),
        )


class MessageSendingConfigForm(forms.Form):
    send_mail_when_checkout = forms.BooleanField(
        label=_(u'Send order mail when checkout'),
        required=False)


class MessageSendingConfig(BaseConfig):
    """Configurator for message sending options in config.
    """
    form_class = MessageSendingConfigForm
    block_name = 'message_sending'

    def __init__(self):
        super(MessageSendingConfig, self).__init__()

        self.default_data = {
            'LFS_SEND_ORDER_MAIL_ON_CHECKOUT':
            settings.LFS_SEND_ORDER_MAIL_ON_CHECKOUT,
        }

        self.option_translation_table = (
            ('LFS_SEND_ORDER_MAIL_ON_CHECKOUT', 'send_mail_when_checkout'),
        )
