# coding: utf-8
from django.contrib import admin
from .forms import (
    MailTemplateForm,
)

from lfs.mail.models import MailTemplate


class MailTemplateAdmin(admin.ModelAdmin):
    list_display = ('name', 'comment')
    search_fields = ['name']
    form = MailTemplateForm
    change_form_template = 'admin/mail_change_form.html'
    suit_form_includes = (
        ('admin/mail_includes/info.html', 'top',),
    )
    ordering = ('name',)

admin.site.register(MailTemplate, MailTemplateAdmin)
