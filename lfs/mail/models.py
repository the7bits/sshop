# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class MailTemplate(models.Model):
    name = models.CharField(_(u'Internal name'), max_length=30, default='')
    comment = models.CharField(
        _(u'Info'), max_length=255, default='', blank=True, null=True)
    text_template = models.TextField(_(u'Text'), default='')
    html_template = models.TextField(_(u'HTML'), default='')

    class Meta:
        verbose_name = _(u'Mail template')
        verbose_name_plural = _(u'Mail templates')

    def __unicode__(self):
        return self.name
