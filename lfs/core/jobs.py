# coding: utf-8
import datetime
from django.contrib.sessions.models import Session
from django.conf import settings
from django.utils.importlib import import_module


def clear_sessions(*args, **kwargs):
    """Clear expired sessions.
    """
    engine = import_module(settings.SESSION_ENGINE)
    SessionStore = engine.SessionStore

    expired_sessions = Session.objects.filter(
        expire_date__lte=datetime.datetime.now())

    for session in expired_sessions[:5000]:
        store = SessionStore(session.session_key)
        store.delete()


def clear_old_logs(*args, **kwargs):
    import os
    from datetime import timedelta, datetime
    from tasks.models import Job
    from django.conf import settings
    t = datetime.now() - timedelta(days=7)
    if 'soap_1c_api' in settings.INSTALLED_APPS:
        from soap_1c_api.models import ProductImportLog, XMLFile
        vals = True
        while vals:
            vals = zip(*ProductImportLog.objects.filter(
                date__lte=t)[:5000].values_list('id'))
            if vals:
                vals = vals[0]
                ProductImportLog.objects.filter(id__in=vals).delete()

        vals = True
        while vals:
            vals = zip(*XMLFile.objects.filter(
                came_at__lte=t)[:500].values_list('id', 'file_path'))
            if vals:
                ids, paths = vals
                XMLFile.objects.filter(id__in=ids).delete()
                for path in paths:
                    p = settings.MEDIA_ROOT + '/' + path
                    if os.path.exists(p):
                        os.remove(p)

    vals = True
    while vals:
        vals = zip(*Job.objects.filter(
            executed__lte=t)[:500].values_list('id'))
        if vals:
            vals = vals[0]
            Job.objects.filter(id__in=vals).delete()


def resave_product_images_job(*product_ids, **kwargs):
    from lfs.catalog.models import Product
    for product in Product.objects.filter(id__in=product_ids):
        for image in product.images.all():
            image.image.resave()

resave_product_images_job.allow_concatenate_args = True
