# coding: utf-8
import django.dispatch

# Shop
shop_changed = django.dispatch.Signal()

# Catalog
cart_changed = django.dispatch.Signal()
category_changed = django.dispatch.Signal()
product_changed = django.dispatch.Signal()
lfs_sorting_changed = django.dispatch.Signal()

product_page_visited = django.dispatch.Signal()
category_page_visited = django.dispatch.Signal()

project_startup = django.dispatch.Signal()

# Marketing
topseller_changed = django.dispatch.Signal()
featured_changed = django.dispatch.Signal()

# Order

# order_created sends when new order is created. This signal is not listened
# in the P-Cart core.
order_created = django.dispatch.Signal()

order_paid = django.dispatch.Signal()
order_sent = django.dispatch.Signal()
order_submitted = django.dispatch.Signal()

# User
customer_added = django.dispatch.Signal()
customer_change_password = django.dispatch.Signal()
customer_change_email = django.dispatch.Signal()
email_added = django.dispatch.Signal()
