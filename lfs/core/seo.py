# coding: utf-8
from lfs.core import utils as core_utils


class BaseSEOGenerator(object):
    def __init__(self, request, context):
        self.request = request
        self.context = context

    def get_h1(self):
        return None

    def get_title(self):
        return None

    def get_description(self):
        return None

    def get_keywords(self):
        return None

    def get_seo_text(self):
        return None


class CoreSEOGenerator(BaseSEOGenerator):
    def __init__(self, request, context):
        self.request = request
        self.context = context
        self.shop = core_utils.get_default_shop()

    def get_title(self):
        return self.shop.get_meta_title()

    def get_description(self):
        return self.shop.get_meta_description()

    def get_keywords(self):
        return self.shop.get_meta_keywords()

    def get_seo_text(self):
        if 'main_page' in self.context:
            return self.shop.get_meta_seo_text()
        else:
            return None
