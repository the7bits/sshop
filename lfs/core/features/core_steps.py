# -*- coding: utf-8 -*-

from lettuce import step, world, before
from salad.steps.everything import *

from lfs.core.utils import get_default_shop
from django.contrib.auth.models import User
from splinter import Browser
from lettuce.django import django_url, mail
import time


@step('I have the default shop')
def have_the_default_shop(step):
    shop = get_default_shop()
    world.shop = shop

@step(r'I see the shop name "(.*)"')
def i_see_the_shop_name(step, expected):
    assert world.shop.name == expected

@step(r'I see the shop owner "(.*)"')
def i_see_the_shop_owner(step, expected):
    assert world.shop.shop_owner == expected

@step(r'I see "(.*)" in shop description')
def i_see_shop_description(step, expected):
    assert expected in world.shop.description

@step(r'I load a browser "(.*)"')
def set_browser(step, name):
    world.browser = Browser(name)

@step(r'I go to "(.*)"')
def i_go_to(step, link):
    world.browser.visit(django_url(link))

@step(r'should see the link to "(.*)"')
def should_see_the_link_to(step, link):
    assert len(world.browser.find_link_by_href(link)) == 1

@step(r'should see the links to "(.*)"')
def should_see_the_links_to(step, link):
    print len(world.browser.find_link_by_href(link)), world.browser.find_link_by_href(link)
    assert len(world.browser.find_link_by_href(link)) > 1

@step(r'should not see the link to "(.*)"')
def should_not_see_the_link_to(step, link):
    assert world.browser.find_link_by_href(link) == []

@step(r'I fill "(.*)" with "(.*)"')
def i_find_link(step, field, value):
    world.browser.fill(field, value)

@step(r'I sign in as "(.*)" with password "(.*)"')
def i_sign_in(step, login, password):
    world.browser.visit(django_url('/login'))
    world.browser.fill("username", login)
    world.browser.fill("password", password)
    world.browser.find_by_xpath('//*[@id="content"]/div/div[2]/div[1]/div/form/div[4]/button')[0].click()

@step(r'I wait "(.*)" seconds')
def i_wait_seconds(step, seconds):
    time.sleep(int(seconds))

@step(r'I logout')
def i_logout(step):
    world.browser.visit(django_url('/logout'))

@step(r'I close a browser')
def set_browser(step):
    world.browser.quit()

@step(r'fill in the field "(.*)" with "(.*)"')
def fill_in_the_field_with(step, field, value):
    world.browser.fill(field, value)

@step(r'And I click on the button with name "(.*)"')
def click_on_the_button_with_type(step, name):
    world.browser.find_by_name(name).click()

@step(r'I should see "(.*)" somewhere in the page')
def i_should_see_somewhere_in_the_page(step, text):
    assert unicode(text) in world.browser.html

@step(r'should see user email "(.*)" in database')
def should_see_user_email_in_database(self, email):
    assert User.objects.filter(email=email).exists()

@step(u'an email is sent to "([^"]*?)" with subject "([^"]*)"')
def email_sent(step, to, subject):
    message = mail.queue.get(True, timeout=5)
    assert message.subject == subject
    assert to in message.recipients()

@step(r'I click on the button with ID "(.*)"')
def click_on_the_button_with_id(step, name):
    world.browser.find_by_id(name).click()

@step(r'I click on the button with value "(.*)"')
def click_on_the_button_with_value(step, val):
    world.browser.find_by_value(val).click()

@step(r'click on the tab with href "(.*)"')
def click_on_the_tab_with_href(step, link):
    world.browser.click_link_by_href(link)

@step(r'fill in select box "(.*)" with "(.*)"')
def fill_select_box(step, select_box, val):
    world.browser.select(select_box, val)

@step(r'check the field "(.*)"')
def check_the_field(step, name):
    world.browser.check(name)
