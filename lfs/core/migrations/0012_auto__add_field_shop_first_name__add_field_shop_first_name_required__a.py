# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Shop.first_name'
        db.add_column('core_shop', 'first_name',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.first_name_required'
        db.add_column('core_shop', 'first_name_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.last_name'
        db.add_column('core_shop', 'last_name',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.last_name_required'
        db.add_column('core_shop', 'last_name_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.FIO'
        db.add_column('core_shop', 'FIO',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.FIO_required'
        db.add_column('core_shop', 'FIO_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.phone'
        db.add_column('core_shop', 'phone',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.phone_required'
        db.add_column('core_shop', 'phone_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.date_of_birthday'
        db.add_column('core_shop', 'date_of_birthday',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.date_of_birthday_required'
        db.add_column('core_shop', 'date_of_birthday_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.email'
        db.add_column('core_shop', 'email',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.email_required'
        db.add_column('core_shop', 'email_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.ref_code'
        db.add_column('core_shop', 'ref_code',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.ref_code_required'
        db.add_column('core_shop', 'ref_code_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.address_form'
        db.add_column('core_shop', 'address_form',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.address_form_required'
        db.add_column('core_shop', 'address_form_required',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.captcha'
        db.add_column('core_shop', 'captcha',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Shop.address_fields'
        db.add_column('core_shop', 'address_fields',
                      self.gf('django.db.models.fields.IntegerField')(null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.template_of_address'
        db.add_column('core_shop', 'template_of_address',
                      self.gf('django.db.models.fields.TextField')(max_length=255, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Shop.use_for_login'
        db.add_column('core_shop', 'use_for_login',
                      self.gf('django.db.models.fields.IntegerField')(default=1),
                      keep_default=False)

        # Adding field 'Shop.email_confirmation'
        db.add_column('core_shop', 'email_confirmation',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.confirmation_by_sms'
        db.add_column('core_shop', 'confirmation_by_sms',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.send_users_data_to_1c'
        db.add_column('core_shop', 'send_users_data_to_1c',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.template_msg_of_registration'
        db.add_column('core_shop', 'template_msg_of_registration',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'Shop.template_of_discount_code'
        db.add_column('core_shop', 'template_of_discount_code',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=30, blank=True),
                      keep_default=False)

        # Adding field 'Shop.require_accept_rules'
        db.add_column('core_shop', 'require_accept_rules',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Shop.link_to_rules'
        db.add_column('core_shop', 'link_to_rules',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=255, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Shop.first_name'
        db.delete_column('core_shop', 'first_name')

        # Deleting field 'Shop.first_name_required'
        db.delete_column('core_shop', 'first_name_required')

        # Deleting field 'Shop.last_name'
        db.delete_column('core_shop', 'last_name')

        # Deleting field 'Shop.last_name_required'
        db.delete_column('core_shop', 'last_name_required')

        # Deleting field 'Shop.FIO'
        db.delete_column('core_shop', 'FIO')

        # Deleting field 'Shop.FIO_required'
        db.delete_column('core_shop', 'FIO_required')

        # Deleting field 'Shop.phone'
        db.delete_column('core_shop', 'phone')

        # Deleting field 'Shop.phone_required'
        db.delete_column('core_shop', 'phone_required')

        # Deleting field 'Shop.date_of_birthday'
        db.delete_column('core_shop', 'date_of_birthday')

        # Deleting field 'Shop.date_of_birthday_required'
        db.delete_column('core_shop', 'date_of_birthday_required')

        # Deleting field 'Shop.email'
        db.delete_column('core_shop', 'email')

        # Deleting field 'Shop.email_required'
        db.delete_column('core_shop', 'email_required')

        # Deleting field 'Shop.ref_code'
        db.delete_column('core_shop', 'ref_code')

        # Deleting field 'Shop.ref_code_required'
        db.delete_column('core_shop', 'ref_code_required')

        # Deleting field 'Shop.address_form'
        db.delete_column('core_shop', 'address_form')

        # Deleting field 'Shop.address_form_required'
        db.delete_column('core_shop', 'address_form_required')

        # Deleting field 'Shop.captcha'
        db.delete_column('core_shop', 'captcha')

        # Deleting field 'Shop.address_fields'
        db.delete_column('core_shop', 'address_fields')

        # Deleting field 'Shop.template_of_address'
        db.delete_column('core_shop', 'template_of_address')

        # Deleting field 'Shop.use_for_login'
        db.delete_column('core_shop', 'use_for_login')

        # Deleting field 'Shop.email_confirmation'
        db.delete_column('core_shop', 'email_confirmation')

        # Deleting field 'Shop.confirmation_by_sms'
        db.delete_column('core_shop', 'confirmation_by_sms')

        # Deleting field 'Shop.send_users_data_to_1c'
        db.delete_column('core_shop', 'send_users_data_to_1c')

        # Deleting field 'Shop.template_msg_of_registration'
        db.delete_column('core_shop', 'template_msg_of_registration')

        # Deleting field 'Shop.template_of_discount_code'
        db.delete_column('core_shop', 'template_of_discount_code')

        # Deleting field 'Shop.require_accept_rules'
        db.delete_column('core_shop', 'require_accept_rules')

        # Deleting field 'Shop.link_to_rules'
        db.delete_column('core_shop', 'link_to_rules')


    models = {
        'catalog.file': {
            'Meta': {'ordering': "('position',)", 'object_name': 'File'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'files'", 'null': 'True', 'to': "orm['contenttypes.ContentType']"}),
            'description': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '999'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'catalog.staticblock': {
            'Meta': {'ordering': "('position',)", 'object_name': 'StaticBlock'},
            'display_files': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'html': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'position': ('django.db.models.fields.SmallIntegerField', [], {'default': '1000'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.action': {
            'Meta': {'ordering': "('position',)", 'object_name': 'Action'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'group': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'actions'", 'to': "orm['core.ActionGroup']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'level': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'lft': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'link': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'parent': ('mptt.fields.TreeForeignKey', [], {'blank': 'True', 'related_name': "'children'", 'null': 'True', 'to': "orm['core.Action']"}),
            'position': ('django.db.models.fields.IntegerField', [], {'default': '999'}),
            'rght': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'tree_id': ('django.db.models.fields.PositiveIntegerField', [], {'db_index': 'True'})
        },
        'core.actiongroup': {
            'Meta': {'ordering': "('name',)", 'object_name': 'ActionGroup'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100', 'blank': 'True'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'core.application': {
            'Meta': {'object_name': 'Application'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '10', 'blank': 'True'})
        },
        'core.country': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Country'},
            'code': ('django.db.models.fields.CharField', [], {'max_length': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'core.shop': {
            'FIO': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'FIO_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Meta': {'object_name': 'Shop'},
            'address_fields': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'address_form': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'address_form_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'captcha': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'category_cols': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'checkout_type': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'confirm_toc': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'confirmation_by_sms': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_of_birthday': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_of_birthday_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'default_country': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['core.Country']"}),
            'default_currency': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['sshop_currencies.Currency']"}),
            'default_locale': ('django.db.models.fields.CharField', [], {'default': "'en_US.UTF-8'", 'max_length': '20'}),
            'default_search': ('django.db.models.fields.CharField', [], {'default': "'searchapi.searchconfigs.SimpleSearchConfig'", 'max_length': '100'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'email': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email_confirmation': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'email_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'featured_badge': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'first_name_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'from_email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'ga_ecommerce_tracking': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ga_site_tracking': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'google_analytics_id': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'invoice_countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'invoice'", 'symmetrical': 'False', 'to': "orm['core.Country']"}),
            'last_name': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'link_to_rules': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'max_menu_item_count': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'menu_type': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'meta_description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_keywords': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'meta_title': ('django.db.models.fields.CharField', [], {'default': "'<name>'", 'max_length': '80', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'notification_emails': ('django.db.models.fields.TextField', [], {}),
            'phone': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'phone_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prerendered_menu': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'price_calculator': ('django.db.models.fields.CharField', [], {'default': "'lfs.default_price.DefaultPriceCalculator'", 'max_length': '255'}),
            'product_cols': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'product_rows': ('django.db.models.fields.IntegerField', [], {'default': '10'}),
            'ref_code': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ref_code_required': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'require_accept_rules': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'search_field_text': ('django.db.models.fields.CharField', [], {'default': "u'Search products...'", 'max_length': '50', 'blank': 'True'}),
            'send_email_as': ('django.db.models.fields.IntegerField', [], {'default': '5'}),
            'send_users_data_to_1c': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'shipping_countries': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'shipping'", 'symmetrical': 'False', 'to': "orm['core.Country']"}),
            'shop_owner': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'static_block': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'shops'", 'null': 'True', 'to': "orm['catalog.StaticBlock']"}),
            'template_msg_of_registration': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'template_of_address': ('django.db.models.fields.TextField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'template_of_discount_code': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'topseller_badge': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'use_for_login': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'use_international_currency_code': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'watermark_image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'watermark_opacity': ('django.db.models.fields.FloatField', [], {'default': '1'}),
            'watermark_position': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'sshop_currencies.currency': {
            'Meta': {'object_name': 'Currency'},
            'abbr': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            'coeffitient': ('django.db.models.fields.FloatField', [], {'default': '1.0'}),
            'format_str': ('django.db.models.fields.CharField', [], {'default': "u'%(value).2f %(abbr)s'", 'max_length': '50'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '70'})
        }
    }

    complete_apps = ['core']