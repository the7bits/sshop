# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _

from suit_ckeditor.widgets import CKEditorWidget
from suit.widgets import LinkedSelect
from codemirror.widgets import CodeMirrorTextarea

from .models import Shop


class ShopForm(forms.ModelForm):
    class Meta:
        model = Shop
        widgets = {
            'description': CKEditorWidget(
                editor_options={'startupFocus': True}),
            'static_block': LinkedSelect,
            'default_country': LinkedSelect,
            'default_currency': LinkedSelect,
            'notification_emails': CodeMirrorTextarea(
                config={'fixedGutter': True}),
            'prerendered_menu': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_keywords': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_description': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_seo_text': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'template_of_address': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'template_msg_of_registration': CKEditorWidget(),
            'extra': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
        }

    def clean_from_email(self):
        from_email = self.cleaned_data.get('from_email')

        if from_email.endswith('@info.com'):
            raise forms.ValidationError(_(u"info.com unavailible domain"))

        return from_email