# coding: utf-8
from django.contrib.auth import (
    SESSION_KEY,
    BACKEND_SESSION_KEY,
)
from django.utils import timezone
from django.contrib import auth
from ..cart.utils import update_cart_after_login
from ..customer.utils import update_customer_after_login


def pcart_login(request, user):
    """
    Persist a user id and a backend in the request. This way a user doesn't
    have to reauthenticate on every request.
    """
    if user is None:
        user = request.user
    # TODO: It would be nice to support different login methods,
    # like signed cookies.
    user.last_login = timezone.now()
    user.save()

    if SESSION_KEY in request.session:
        if request.session[SESSION_KEY] != user.id:
            # To avoid reusing another user's session, create a new, empty
            # session if the existing session corresponds to a different
            # authenticated user.
            request.session.flush()
    else:
        pass
    request.session[SESSION_KEY] = user.id
    request.session[BACKEND_SESSION_KEY] = user.backend
    if hasattr(request, 'user'):
        request.user = user

    update_cart_after_login(request)
    update_customer_after_login(request)

auth.login = pcart_login
