# coding: utf-8
from django.conf import settings

from django.conf.urls.defaults import *
from django.views.generic.base import TemplateView

from lfs.core.utils import get_default_shop

# Shop
urlpatterns = patterns(
    'lfs.core.views',
    url(r'^$', "shop_view", name="lfs_shop_view"),
    url(r'^top-bar$', "top_bar_view", name="sshop_top_bar_view"),
    url(
        r'^admin-ajax-forbidden/$',
        TemplateView.as_view(template_name='adminextras/ajax-forbidden.html')),
    url(r'^robots.txt$', TemplateView.as_view(template_name='robots.txt')),
    url(
        r'^get-products-prices$',
        "get_products_prices",
        name="get_products_prices"
    )
)

# Cart
urlpatterns += patterns(
    'lfs.cart.views',
    url(
        r'^add-to-cart/(?P<product_id>[-\d]*)/(?P<quantity>.*)/$',
        'add_to_cart_item'),
    url(r'^add-to-cart$', "add_to_cart"),
    url(
        r'^add-accessory-to-cart/(?P<product_id>\d*)/(?P<quantity>.*)$',
        "add_accessory_to_cart",
        name="lfs_add_accessory_to_cart"),
    url(r'^added-to-cart$', "added_to_cart", name="lfs_added_to_cart"),
    url(r'^delete-cart-item/(?P<cart_item_id>\d*)$', "delete_cart_item",
        name="lfs_delete_cart_item"),
    url(r'^refresh-cart$', "refresh_cart"),
    url(r'^cart-block$', "cart_block", name="lfs_cart_block"),
    url(r'^clear-cart$', 'clear_cart', name='lfs_clear_cart'),
)

# Catalog
from lfs.catalog.views import (ProductListView, PropertyListView,)
urlpatterns += patterns(
    'lfs.catalog.views',
    url(r'^product-api/products_json$', ProductListView.as_view(),
        name="product_list_as_json"),
    url(r'^product-api/properties_json$', PropertyListView.as_view(),
        name="property_list_as_json"),
    url(r'^product-question/(?P<product_id>[-\w]*)$', "product_question",
        name="lfs_product_question"),
    url(
        r'^product-variants/(?P<product_id>[-\w]*)$',
        "product_variants_for_category",
        name="lfs_product_variants_for_category"),
    url(r'^product-description/(?P<slug>[-\w]*)$', "product_description_view",
        name="lfs_product_description"),
    url(r'^product-accessories/(?P<slug>[-\w]*)$', "product_accessories_view",
        name="lfs_product_accessories"),
    url(
        r'^product-reviews/(?P<slug>[-\w]*)$',
        "product_reviews_view", name="lfs_product_reviews"),
    url(
        r'^product-form-dispatcher',
        "product_form_dispatcher", name="lfs_product_dispatcher"),
    url(r'^set-sorting', "set_sorting", name="lfs_catalog_set_sorting"),
    url(r'^set-page-mode', "set_page_mode", name="lfs_catalog_set_page_mode"),
    url(r'^modal-cart/$', "modal_cart", name="sshop_modal_cart"),
    url(
        r'^change-modal-cart/$',
        "change_modal_cart", name="sshop_changed_modal_cart"),
)

# Checkout
urlpatterns += patterns(
    'lfs.checkout.views',
    url(
        r'^checkout-dispatcher$',
        "checkout_dispatcher", name="lfs_checkout_dispatcher"),
    url(r'^checkout-login$', "login", name="lfs_checkout_login"),
    url(r'^checkout$', "one_page_checkout", name="lfs_checkout"),
    url(
        r'^checkout/authorization$',
        "one_page_checkout_authorization",
        name="lfs_checkout_authorization"),
    url(r'^thank-you$', "thank_you", name="lfs_thank_you"),
    url(
        r'^changed-checkout/$',
        "changed_checkout", name="lfs_changed_checkout"),
    url(
        r'^update-shipping-form$',
        "update_shipping_form", name="update_shipping_form"),
    url(
        r'^update-payment-form$',
        "update_payment_form", name="update_payment_form"),
    url(
        r'^update-customer-form$',
        "update_customer_form", name="update_customer_form"),
    url(
        r'^update-order-form$',
        "update_order_form", name="update_order_form"),
    url(r'^check-voucher/$', "check_voucher", name="lfs_check_voucher"),
)

# Comparison
urlpatterns += patterns(
    'lfs.comparison.views',
    url(
        r'^comparison/add-product/(?P<product_id>[-\d]*)'
        r'/(?P<portlet_assignment_id>[-\d]*)/$',
        'add_product_view',
        name='lfs_comparison_add_product_ajax'),
    url(
        r'^comparison/add-product/(?P<product_id>[-\d]*)/$',
        'add_product_view',
        name='lfs_comparison_add_product'),
    url(
        r'^comparison/ajax-load/(?P<portlet_assignment_id>[-\d]*)/$',
        'ajax_comparison_render',
        name='lfs_comparison_ajax'),
    url(
        r'^comparison/remove-product/(?P<product_id>[-\d]*)/$',
        'remove_product_view',
        name='lfs_comparison_remove_product'),
    url(r'^comparison/$', 'comparison_view', name='lfs_comparison'),
    url(
        r'^comparison/remove-products/$',
        'remove_all_products_view',
        name='lfs_comparison_remove_all_products'
    ),
    url(
        r'^comparison/get-products$',
        'get_products_ajax',
        name='get_products_ajax'
    )
)

# Notebook
urlpatterns += patterns(
    'lfs.notebook.views',
    url(
        r'^notebook/add-product/(?P<product_id>[-\d]*)'
        r'/(?P<portlet_assignment_id>[-\d]*)/$',
        'add_product_view',
        name='lfs_notebook_add_product_ajax'),
    url(
        r'^notebook/add-product/(?P<product_id>[-\d]*)/$',
        'add_product_view',
        name='lfs_notebook_add_product'),
    url(
        r'^notebook/ajax-load/(?P<portlet_assignment_id>[-\d]*)/$',
        'ajax_notebook_render',
        name='lfs_notebook_add_product'),
    url(
        r'^notebook/remove-products/$',
        'remove_all_products_view',
        name='lfs_notebook_remove_all_products'
    ),
    url(
        r'^notebook/remove-product/(?P<product_id>[-\d]*)/$',
        'remove_product_view',
        name='lfs_notebook_remove_product'),
    url(
        r'^notebook/$',
        'notebook_view',
        name='lfs_notebook'),
    url(
        r'^notebook/ajax-remove-product/(?P<portlet_assignment_id>[-\d]*)/(?P<product_id>[-\d]*)/$',
        'notebook_remove_product_ajax',
        name='lfs_notebook_remove_product_ajax'),
    url(
        r'^notebook/ajax-remove-all-products/(?P<portlet_assignment_id>[-\d]*)$',
        'remove_all_products_ajax',
        name='lfs_notebook_remove_all_products_ajax'
    ),
    url(
        r'^notebook/get-products$',
        'get_products_ajax',
        name='get_products_ajax'
    )
)


# Customer
urlpatterns += patterns(
    'lfs.customer.views',
    url(r'^login$', "login", name="lfs_login"),
    url(r'^logout$', "logout", name="lfs_logout"),
    url(
        r'^update-register-form$',
        "update_register_form", name="update_register_form"),

    url(r'^my-account$', "account", name="lfs_my_account"),
    url(r'^my-account/edit$', "edit_account", name="lfs_edit_my_account"),
    url(
        r'^my-account/update-account-form$',
        "update_account_form", name="update_account_form"),

    url(r'^add-email/$', 'add_email', name='lfs_add_email'),
    url(r'^remove-email/(\d+)/$', 'remove_email', name='lfs_remove_email'),
    url(
        r'^set-default-email/(\d+)/$',
        'set_default_email', name='lfs_set_default_email'),

    url(r'^add-phone/$', 'add_phone', name='lfs_add_phone'),
    url(r'^remove-phone/(\d+)/$', 'remove_phone', name='lfs_remove_phone'),
    url(
        r'^set-default-phone/(\d+)/$',
        'set_default_phone', name='lfs_set_default_phone'),

    url(r'^add-address/$', 'add_address', name='lfs_add_address'),
    url(
        r'^update-address-form$',
        "update_address_form", name="update_address_form"),
    url(
        r'^remove-address/(\d+)/$',
        'remove_address', name='lfs_remove_address'),
    url(
        r'^set-default-address/(\d+)/$',
        'set_default_address', name='lfs_set_default_address'),

    url(r'^my-orders', "orders", name="lfs_my_orders"),
    url(r'^my-order/(?P<id>\d+)', "order", name="lfs_my_order"),
    url(r'^my-password', "password", name="lfs_my_password"),
    url(
        r'^confirm-email/(?P<id>[-\d]*)-(?P<code>[-\w]*)/$',
        'confirm_email', name='lfs_confirm_email'),
)

# Page
urlpatterns += patterns(
    'lfs.page.views',
    url(r'^page/(?P<slug>[-\w]*)$', "page_view", name="lfs_page_view"),
    url(r'^pages/$', "pages_view", name="lfs_pages"),
    url(r'^popup/(?P<slug>[-\w]*)$', "popup_view", name="lfs_popup_view"),
)

# Password reset
urlpatterns += patterns(
    'django.contrib.auth.views',
    url(r'^password-reset/$', "password_reset", {'from_email': get_default_shop().from_email}, name="lfs_password_reset"),
    url(r'^password-reset-done/$', "password_reset_done"),
    url(
        r'^password-reset-confirm/(?P<uidb36>[-\w]*)/(?P<token>[-\w]*)$',
        "password_reset_confirm"),
    url(r'^password-reset-complete/$', "password_reset_complete"),
)

# Search
urlpatterns += patterns(
    'lfs.search.views',
    url(r'^search', "search", name="lfs_search"),
    url(r'^livesearch', "live_search", name="lfs_livesearch"),
)

# FAQ
urlpatterns += patterns(
    'lfs.faq.views',
    url(r'^faq/add-faq', 'add_faq', name='lfs_faq_add_faq'),
    url(r'^faq/faq-view', 'faq_view', name='lfs_faq_view'),
    url(r'^faq/thank-you', 'thank_you', name='faq_thank_you'),
)

# Portlet
urlpatterns += patterns(
    'lfs.portlet.views',
    url(
        r'^portlet/panginator/portlet-assignment/(?P<pa_id>\d+)/',
        'portlet_panginator', name='sshop_portlet_panginator'),
)

urlpatterns += patterns(
    '',
    (r'', include('lfs_contact.urls')),
)

# Callback
urlpatterns += patterns(
    'callback.views',
    url(r'^callback', 'callback', name='sshop_callback'),
)

if getattr(settings, 'ALLOW_REGIONS', False):
    urlpatterns += patterns(
        '',
        (r'^regions/', include('regions.urls')),
    )

if getattr(settings, 'USE_HR_URLS', False):
    proxies = []
    from lfs.catalog.models import Category
    categories = Category.objects.all()
    for category in categories:
        hr_templates = category.filterurltemplate_set.all().order_by('order')
        if hr_templates:
            for t in hr_templates:
                slug = '^' + category.slug + t.template + '$'
                proxies.append(
                    url(
                        slug,
                        'category_hr_view',
                        {
                            'category_slug': category.slug,
                            'template_id': t.pk})
                )
    urlpatterns += patterns('lfs.catalog.views', *proxies)

# Catalog
urlpatterns += patterns(
    'lfs.catalog.views',
    url(
        r'^(?P<slug>[-\w]*)$',
        "category_product_url_proxy",
        name="lfs_category_product_url_proxy"),
    url(r'^(?P<slug>[-\w]*)$', "category_view", name="lfs_category"),
    url(r'^(?P<slug>[-\w]*)$', "product_view", name="lfs_product"),
)

# Filters
urlpatterns += patterns(
    'lfs.filters.views',
    url(
        r'^get-filter-options/(?P<filter_id>\d+)$',
        "get_filter_options", name="sshop_get_filter_options"),
    url(
        r'^get-filter-children/(?P<filter_id>\d+)$',
        "get_filter_children", name="sshop_get_filter_children"),
)

#Criteria
urlpatterns += patterns(
    '',
    (r'', include('lfs.criteria.urls')),
)

urlpatterns += patterns(
    '',
    (r'', include('lfs.portlet.urls')),
)

# Recent products
urlpatterns += patterns(
    'lfs.recent_products.views',
    url(
        r'^recent_products/ajax-load/$',
        'ajax_recent_products_render',
        name = 'lfs_recent_products_ajax'),
)