# coding: utf-8
from django.http import HttpResponseForbidden
from django.utils.safestring import mark_safe
from django.conf import settings
from django.core.cache import cache

from ..catalog.models import SortType


_ERROR_MSG = '''
<!DOCTYPE html><html lang="en"><body><h1>%s</h1><p>%%s</p></body></html>
'''
_403_ERROR = _ERROR_MSG % '403 Forbidden'


def ajax_required(f):
    """
    AJAX request required decorator
    use it in your views:

    @ajax_required
    def my_view(request):
        ....

    """
    def wrap(request, *args, **kwargs):
            if not request.is_ajax():
                return HttpResponseForbidden(
                    mark_safe(_403_ERROR % 'Request must be set via AJAX.'))
            return f(request, *args, **kwargs)
    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap


def sbits_cache_page(time_out=3600):
    def decorator(func):
        def wrapper(request, *args, **kwargs):
            extra_key = ''
            if 'category' in kwargs or 'category_slug' in kwargs:
                page_mode = request.session.get(
                    "page_mode",
                    getattr(settings, 'DEFAULT_CATEGORY_MODE', 'list')
                )
                sorting = request.session.get("sorting")
                if sorting is None:
                    try:
                        sorting = SortType.objects.all(
                        ).order_by('order')[0].sortable_fields
                    except IndexError:
                        sorting = getattr(
                            settings, 'DEFAULT_SORTING', 'effective_price')
                extra_key = '%s.%s' % (
                    page_mode, sorting.replace(
                        ' ', ''
                    ).replace(',', ';')
                )
            path = request.get_full_path()
            if '?' in path:
                path, get_params = path.split('?')
                new_get_params = []

                for get_param in get_params.split('&'):
                    if 'utm_' not in get_param.split('=')[0]:
                        new_get_params.append(get_param)
                path = '%s?%s' % (path, '&'.join(new_get_params))
            # 250 is SERVER_MAX_KEY_LENGTH
            # + 3 because cache.get() third parameter by default ':1:'
            length = 250 - (len('%s.sbits_cache_page..%s' % (
                settings.CACHE_MIDDLEWARE_KEY_PREFIX,
                extra_key,)) + len(settings.CACHE_MIDDLEWARE_KEY_PREFIX) + 3)
            cache_key = '%s.sbits_cache_page.%s.%s' % (
                settings.CACHE_MIDDLEWARE_KEY_PREFIX,
                path[:length],
                extra_key,
            )
            value = cache.get(cache_key)
            if value:
                return value
            else:
                value = func(request, *args, **kwargs)
                cache.set(cache_key, value, time_out)
                return value
        return wrapper
    return decorator
