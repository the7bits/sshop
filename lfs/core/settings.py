# coding: utf-8
from django.utils.translation import gettext_lazy as _
# from django.conf import settings

ACTION_PLACE_TABS = 1
ACTION_PLACE_FOOTER = 2

ACTION_PLACE_CHOICES = [
    (ACTION_PLACE_TABS, _(u"Tabs")),
    (ACTION_PLACE_FOOTER, _(u"Footer")),
]
# POSTAL_ADDRESS_L10N = getattr(settings, 'POSTAL_ADDRESS_L10N', True)

ACTION_TYPE_CHOICES = (
    (1, _(u'Simple')),
    (2, _(u'With sublevels')),
)

# DEPRECATED from 0.3.1
MENU_TYPE_CHOICES = (
    (1, _(u'Simple')),
    (2, _(u'With sublevels')),
    (3, _(u'Infinite sublevels')),
    (4, _(u'Custom')),
)

POSITION_CHOICES = (
    (1, _(u'Top left')),
    (2, _(u'Top right')),
    (3, _(u'Bottom left')),
    (4, _(u'Bottom right')),
    (5, _(u'Center')),
    (6, _(u'Top center')),
    (7, _(u'Bottom center')),
    (8, _(u'Right center')),
    (9, _(u'Left center')),
)

EMAIL_TYPE_CHOICES = (
    (0, _(u'Text')),
    (5, _(u'HTML')),
    (10, _(u'HTML with images')),
)

LOGIN_CHOICES = (
    (1, _(u'Email')),
    (5, _(u'Phone')),
)
