# coding: utf-8
from django.conf import settings
from lfs.core.utils import get_default_shop
# from lfs.checkout.settings import CHECKOUT_TYPE_ANON


def main(request):
    """context processor for lfs
    """
    shop = get_default_shop(request)

    return {
        # 'ANON_ONLY': shop.checkout_type == CHECKOUT_TYPE_ANON,  # DEPRECATED
        # 'LFS_DOCS': getattr(settings, 'LFS_DOCS', ''),  # DEPRECATED

        'SHOP': shop,
        'CUSTOMER_AUTH_ENABLED':
        getattr(settings, 'CUSTOMER_AUTH_ENABLED', True),
        'VERSION': getattr(settings, 'SSHOP_VERSION', ''),
        'DEMO_MODE': getattr(settings, 'DEMO_MODE', False),
        'admin_root_path': '/' + getattr(settings, 'ADMIN_ROOT_PATH', False),
        'EXTRA_CSS': getattr(settings, 'EXTRA_CSS', []),
        'EXTRA_JS': getattr(settings, 'EXTRA_JS', []),
    }
