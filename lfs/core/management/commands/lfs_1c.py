# -*- coding: utf-8 -*-

# python imports
import xml.sax
import pytils
import StringIO
import re
import time
import random
from urllib2 import urlopen
import os
from datetime import datetime

# django imports
from django.core.management.base import BaseCommand
from django.conf import settings
from django import db
from django.core.files.base import ContentFile

# SShop imports
from lfs.catalog.models import Category, Product, Property, PropertyGroup, Image, ProductsPropertiesRelation
from lfs.catalog.models import GroupsPropertiesRelation, PropertyOption, ProductPropertyValue
from lfs.manufacturer.models import Manufacturer
from sshop.settings import DIR_FOR_ERRORS

filters = []

def parse_categories_init(text):
    '''
        This function contain class for parse
        categories from XML file from 1C
    '''

    class XMLCategoryReader(xml.sax.ContentHandler):
        def __init__(self,imported=False,mywin=None):
            xml.sax.ContentHandler.__init__(self)
 
        def startDocument(self):
            print 'Start categories processed'
            # stores the tag to be processed
            self.classifier = False
            self.catalog_level = 0
            self.catalog_id = ''
            self.catalog_name = ''
            self.catalog_id_ = False
            self.catalog_name_ = False
            self.catalog = {}
            self.parents = []
 
        def endDocument(self):
            pass
 
        def characters(self, data):
            if self.catalog_id_:
                self.catalog_id = data
            elif self.catalog_name_:
                self.catalog_name = data
 
        def startElement(self, name, attrs):
            if name == u'Группа':
                self.catalog_level += 1
            elif name == u'Ид':
                self.catalog_id_ = True
            elif name == u'Наименование':
                self.catalog_name_ = True
 
        def create_category(self, name):
            print u'\tProcess category {category}'.format(category=name)
            if self.catalog_level > 1:
                try:
                    parent = self.parents[-1].encode('utf-8')
                    obj, created = Category.objects.get_or_create(
                            name=name.encode('utf-8'),
                            slug=pytils.translit.slugify(name),
                            level=self.catalog_level,
                            parent=Category.objects.get(name=parent),
                            uid=self.catalog_id
                        )
                except Exception, e:
                    print e
                    print 'ERROR: Create category with parents. Error_10'
            else:
                try:
                    obj, created = Category.objects.get_or_create(
                            name=name.encode('utf-8'),
                            slug=pytils.translit.slugify(name),
                            level=1,
                            uid=self.catalog_id

                        )
                except Exception, e:
                    print e
                    print 'ERROR: Create category. Error_11'
 
        def endElement(self, name):
            # print name
            try:
                if name == u'Группа':
                    self.catalog_level -= 1
                    self.parents.pop()
                elif name == u'Ид':
                    self.catalog_id_ = False
                elif name == u'Наименование':
                    self.catalog_name_ = False
 
                    if self.catalog_level > 0:
                        self.catalog[self.catalog_id.encode('utf-8')] = self.catalog_name.encode('utf-8')
                        self.create_category(self.catalog_name)
                   
                    self.parents.append(self.catalog_name)
            except Exception, e:
                print e
                print 'ERROR: Error_12'
                pass
    try:
        xml.sax.parseString(text, XMLCategoryReader())
        db.reset_queries()
    except Exception, e:
        print e
        print 'ERROR'
    print 'End categories processed'

def parse_product_init(text):
    '''
        This function contain class for parse
        products from XML file from 1C
    '''

    class XMLProductReader(xml.sax.ContentHandler):
        def __init__(self, imported=False, mywin=None):
            xml.sax.ContentHandler.__init__(self)

        def startDocument(self):
            # print 'Start products processed'
            # stores the tag to be processed
            self.data = ''
            self.catalog = False
            self.product = False

            self.id = ''
            self.id_ = False
            self.articul = ''
            self.articul_ = False
            self.product_name = ''
            self.product_name_ = False
            self.key_1c = ''
            self.key_1c_ = False
            self.availability = ''
            self.availability_ = False
            self.manufacturer = ''
            self.manufacturer_ = False
            self.basic_unit = ''
            self.basic_unit_ = False
            
            self.znach_recvizitov = ''
            self.znach_recvizitov_ = False 
            self.znach_recvizita = ''
            self.znach_recvizita_ = False 
            self.tax = ''
            self.tax_ = False 

            self.groups_id = ''
            self.groups = []
            self.groups_ = False

            self.content = ''
            self.content_ = False
            self.METATitle = ''
            self.METATitle_ = False
            self.METADescription = ''
            self.METADescription_ = False
            self.METAKeywords = ''
            self.METAKeywords_ = False
            self.accessory = ''
            self.accessory_ = False
            self.is_new = ''
            self.is_new_ = False
            self.popular = ''
            self.popular_ = False
            self.recommended = ''
            self.recommended_ = False
            
            self.short_description = ''
            self.short_description_ = False
            self.description = ''
            self.description_ = False
            self.img = ''
            self.img_ = False
            self.links_product = ''
            self.links_product_ = False
            self.yandex_link = ''
            self.yandex_link_ = False

            self.filter = ''
            self.filter_ = False
            self.option = False
            self.options = []
            self.option_group = ''
            self.option_group_ = False
            self.option_name = ''
            self.option_name_ = False
            self.option_value = ''
            self.option_value_ = False

            self.available = False

        def get_unit_numb(self, text):
            nums = re.findall(r'(\d+.*\d+)',text)
            if len(nums)>0:
                try:
                    return ''.join(str(a) for a in nums)
                except:
                    return ''.join(a for a in nums)
            else:
                return text

        def get_unit_type(self, text):
            nums = re.findall(r'(\d+.*\d+)',text)
            if len(nums)>0:
                for a in nums:
                    text = text.replace(a,'')
                return text.split(' ')[-1]
            else:
                return ''

        def create_product(self):
            # print u'\tProcess product {product}'.format(product=self.product_name)

            try:
                if self.img != '':
                    img_page = urlopen(self.img)
                    image = Image(title=self.articul)
                    image.image.save(self.articul+'.'+self.img.split('.')[-1], ContentFile(img_page.read()))
                    image.save()
                else:
                    image = ''
            except:
                image = ''

            try:
                if u'Есть в Наличии' in self.available:
                    available = True
                else:
                    available = False
                manufacturer, created = Manufacturer.objects.get_or_create(
                        name=self.manufacturer
                    )
                try:
                    product, created = Product.objects.get_or_create(
                        name=self.product_name,
                        slug=pytils.translit.slugify(self.product_name),
                        sku=self.articul,
                        short_description=self.short_description,
                        description=self.description,
                        meta_title=self.METATitle,
                        meta_keywords=self.METAKeywords,
                        meta_description=self.METADescription,
                        active=True,
                        manufacturer=manufacturer,
                        deliverable=available,
                        uid=self.key_1c
                    )
                except:
                    product, created = Product.objects.get_or_create(
                        name=self.product_name,
                        slug=pytils.translit.slugify(self.product_name)+'_'+random.randint(0,9),
                        sku=self.articul,
                        short_description=self.short_description,
                        description=self.description,
                        meta_title=self.METATitle,
                        meta_keywords=self.METAKeywords,
                        meta_description=self.METADescription,
                        active=True,
                        manufacturer=manufacturer,
                        deliverable=available,
                        uid=self.key_1c
                    )
                if image != '':
                    product.images.add(image)
                    product.save()

                for i in self.groups:
                    cat = Category.objects.get(uid=i)
                    cat.products.add(product)
                    cat.save()
                    category_name = cat.name

                for i in self.options:
                    try:
                        # print unicode(self.get_unit_type(i[2])[:15]).encode('utf-8')
                        
                        # name = category_name + '|'  + i[0] + '|' + i[1] + '|'
                        # global filters
                        # name = str(name.encode('utf8'))
                        # unit = [x.split('|') for x in filters if name in x]
                        # if len(unit)>0 and unit[0][-1].replace('\n','').lower()=='true':
                        #     unit = unit[0][-2]
                        #     print unit
                        #     continue

                        property_group, created = PropertyGroup.objects.get_or_create(
                                category=cat,
                                name=i[0],
                            )
                        property_group.products.add(product)
                    
                        _property, created = Property.objects.get_or_create(
                                name=i[1],
                                title=i[1],
                                # unit=unit,
                                filterable=False
                            )
                        # ProductPropertyValue.objects.get_or_create(
                        #         product=product,
                        #         property=opt,
                        #         value=unicode(self.get_unit_numb(i[2])).encode('utf-8'),
                        #         type=0)

                        ProductPropertyValue.objects.get_or_create(
                                product=product,
                                property=_property,
                                value=unicode(i[2]).encode('utf-8'),
                                type=2)

                        property_group_rel, created = GroupsPropertiesRelation.objects.get_or_create(
                                group=property_group,
                                property=_property
                            )
                        prod_prop_relat, created = ProductsPropertiesRelation.objects.get_or_create(
                                product=product,
                                property=_property
                                )
                    except Exception, e:
                        print e
                        try:
                            open(DIR_FOR_ERRORS + 'error_command.txt','a').write(str(e)+str(self.articul)+'\n')
                        except:
                            pass

                self.options = []
            except Exception, e:
                print e
                try:
                    open(DIR_FOR_ERRORS + 'error_command.txt','a').write(str(e)+str(self.articul)+'\n')
                except:
                    pass
                print 'ERROR: Create product. Error_20'

        def endDocument(self):
            pass

        def characters(self, data):
            if self.product:
                if self.id_ and not self.groups_:
                    self.id = data
                if self.articul_:
                    self.articul = data
                if self.product_name_ and not self.znach_recvizitov_ and not self.tax_:
                    self.product_name = data
                if self.tax_:
                    pass
                if self.key_1c_:
                    self.key_1c = data
                if self.availability_:
                    self.availability = data
                if self.manufacturer_:
                    self.manufacturer = data
                if self.basic_unit_:
                    self.basic_unit = data
                if self.groups_ and self.id_:
                    self.groups.append(data)
                if self.img_:
                    self.img = data
                if self.content_:
                    if self.METATitle_:
                        self.METATitle = data
                    if self.METADescription_:
                        self.METADescription = data
                    if self.METAKeywords_:
                        self.METAKeywords = data
                if self.accessory_:
                    self.accessory = data
                if self.is_new_:
                    self.is_new = data
                if self.popular_:
                    self.popular = data
                if self.recommended_:
                    self.recommended = data
                if self.short_description_:
                    self.short_description = data
                if self.description_:
                    self.description += data
                if self.option:
                    if self.option_group_:
                        self.option_group = data
                    if self.option_name_:
                        self.option_name = data
                    if self.option_value_:
                        self.option_value = data
                if self.availability_:
                    self.available = data
            else:
                self.data = ''

        def startElement(self, name, attrs):
            if name == u'Товар':
                self.product = True
            elif name == u'Ид':
                self.id_ = True
            elif name == u'Артикул':
                self.articul_ = True
            elif name == u'Наименование':
                self.product_name_ = True
            elif name == u'ЗначенияРеквизитов':
                self.znach_recvizitov_ = True
            elif name == u'СтавкиНалогов':
                self.tax_ = True
            elif name == u'Код1с':
                self.key_1c_ = True
            elif name == u'Наличие':
                self.availability_ = True
            elif name == u'Производитель':
                self.manufacturer_ = True
            elif name == u'БазоваяЕдиница':
                self.basic_unit_ = True
            elif name == u'Группы':
                self.groups_ = True
            elif name == u'Контент':
                self.content_ = True
            elif name == u'METATitle':
                self.METATitle_ = True
            elif name == u'METADescription':
                self.METADescription_ = True
            elif name == u'METAKeywords':
                self.METAKeywords_ = True
            elif name == u'Аксессуар':
                self.accessory_ = True
            elif name == u'Новый':
                self.is_new_ = True
            elif name == u'Популярный':
                self.popular_ = True
            elif name == u'Рекомендуемый':
                self.recommended_ = True
            elif name == u'ОписаниеКраткое':
                self.short_description_ = True
            elif name == u'Описание':
                self.description_ = True
            elif name == u'URL':
                self.img_ = True
            elif name == u'СсылкиТовар':
                self.links_product_ = True
            elif name == u'СсылкаЯндекс':
                self.yandex_link_ = True
            elif name == u'Опция':
                self.option = True
            elif name == u'ГруппировкаОпции':
                self.option_group_ = True
            elif name == u'НаименованиеОпции':
                self.option_name_ = True
            elif name == u'ЗначениеОпции':
                self.option_value_ = True
            elif name == u'Наличие':
                self.availability_ = True

        def endElement(self, name):
            if self.product:
                if name == u'Товар':
                    self.create_product()
                    self.product = False
                    self.groups = []
                elif name == u'Ид':
                    self.id_ = False
                elif name == u'Артикул':
                    self.articul_ = False
                elif name == u'Наименование':
                    self.product_name_ = False
                elif name == u'ЗначенияРеквизитов':
                    self.znach_recvizitov_ = False
                elif name == u'СтавкиНалогов':
                    self.tax_ = False
                elif name == u'Код1с':
                    self.key_1c_ = False
                elif name == u'Наличие':
                    self.availability_ = False
                elif name == u'Производитель':
                    self.manufacturer_ = False
                elif name == u'БазоваяЕдиница':
                    self.basic_unit_ = False
                elif name == u'Группы':
                    self.groups_ = False
                elif name == u'Контент':
                    self.content_ = False
                elif name == u'METATitle':
                    self.METATitle_ = False
                elif name == u'METADescription':
                    self.METADescription_ = False
                elif name == u'METAKeywords':
                    self.METAKeywords_ = False
                elif name == u'Аксессуар':
                    self.accessory_ = False
                elif name == u'Новый':
                    self.is_new_ = False
                elif name == u'Популярный':
                    self.popular_ = False
                elif name == u'Рекомендуемый':
                    self.recommended_ = False
                elif name == u'ОписаниеКраткое':
                    self.short_description_ = False
                elif name == u'Описание':
                    self.description_ = False
                elif name == u'URL':
                    self.img_ = False
                elif name == u'СсылкиТовар':
                    self.links_product_ = False
                elif name == u'СсылкаЯндекс':
                    self.yandex_link_ = False
                elif name == u'Опция':
                    self.option = False
                    # if u'Прочие' not in self.option_group:
                    self.options.append((self.option_group, self.option_name, self.option_value))
                elif name == u'ГруппировкаОпции':
                    self.option_group_ = False
                elif name == u'НаименованиеОпции':
                    self.option_name_ = False
                elif name == u'ЗначениеОпции':
                    self.option_value_ = False
                elif name == u'Наличие':
                    self.availability_ = False
    try:
        d = xml.sax.parseString(text, XMLProductReader())
        del d
        db.reset_queries()
    except Exception, e:
        print 'ERROR', e



class Command(BaseCommand):
    
    args = ''
    help = 'Import 1C XML data to LFS'

    def handle(self, *args, **options):
        t = datetime.now()
        print 'Import 1C XML categories, products, options to SShop\n'
      
        try:
            path_to_file = args[0]
        except IndexError:
            path_to_file = False
            print 'ERROR: Input path to XML file with data'

        try:
            command =  args[1].strip()
        except:
            command = False

        # global filters
        # filters = open(DIR_FOR_FILTERS + 'filters.csv', 'r').readlines()
        if command == 'init':
            Product.objects.all().delete()
            Category.objects.all().delete()
            ProductPropertyValue.objects.all().delete()
            ProductsPropertiesRelation.objects.all().delete()
            Property.objects.all().delete()
            PropertyGroup.objects.all().delete()
            PropertyOption.objects.all().delete()
            Image.objects.all().delete()
            GroupsPropertiesRelation.objects.all().delete()
            try:
                os.rename('sshop/public/media/images', 'sshop/public/media/images1')
            except:
                pass
            try:
                os.rmdir('sshop/public/media/images')
            except:
                pass

        f = open(path_to_file, 'r')
        categories_f = False
        product_f = False
        text = ''

        for i in f.xreadlines():
            if '<Классификатор>' in i:
                categories_f = True
                text += i
            elif '</Классификатор>' in i:
                categories_f = False
                text += i
            elif categories_f:
                text += i

        f.close()
        if command == 'init' :
            parse_categories_init(text)

        f = open(path_to_file, 'r')
        text = ''
        x = 0
        for i in f.xreadlines():
            if '<Товар>' in i:
                product_f = True

            if product_f:
                text += i

            if '</Товар>' in i:
                x+=1
                print x
                product_f = False
                # if command == 'init':
                parse_product_init(text)
                # else:
                #     parse_product(text)
                text = ''
                # break

        f.close()
        print 'Import Finish'
        print t
        print datetime.now()
