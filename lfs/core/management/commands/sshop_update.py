# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
import os
from django.conf import settings
from django.utils.translation import ugettext as _

class Command(BaseCommand):
    args = ''
    help = ''

    def handle(self, *args, **options):
        import sshop_update

        if 'tire_shop' in args[0]:
            tire_shop()

        try:
            old_version = args[0]
            new_version = getattr(settings, 'SSHOP_VERSION', '0.2.9')

            old = int(filter(lambda x: x.isdigit(), old_version))
            new = int(filter(lambda x: x.isdigit(), new_version))

            for v in xrange(old + 1, new + 1):
                if v < 100:
                    func_name = 'update_to_0%s' % v
                else:
                    func_name = 'update_to_%s' % v

                func = getattr(sshop_update, func_name)
                func()
        except Exception, e:
            print e
            if len(args) > 0:
                print 'Cannot find version ' + args[0]
            else:
                print 'Enter number of old version'


def tire_shop():
    CMDS = [
        './manage.py migrate tire_shop.wheels_auto 0001',
        './manage.py migrate tire_shop.wheels_truck 0001',
        './manage.py migrate tire_shop.wheels_moto 0001',
        './manage.py migrate tire_shop.tires_variants 0001',
    ]
    os.system('\n'.join(CMDS))


def update_to_012():
    CMDS = [
        './manage.py migrate tasks 0001',
    ]
    os.system('\n'.join(CMDS))
    print 'updating to 0.1.2'


def update_to_013():
    CMDS = [
        './manage.py migrate lfs.catalog 0011',
        './manage.py migrate lfs.portlet 0010',
        './manage.py migrate lfs.portlet 0011',
        './manage.py migrate lfs.faq 0001',
        './manage.py migrate lfs.portlet 0012',
        './manage.py migrate lfs.page 0001 --fake',
        './manage.py migrate lfs.page 0002',
    ]
    os.system('\n'.join(CMDS))
    print 'updating to 0.1.3'


def update_to_014():
    CMDS = [
        './manage.py migrate lfs.catalog 0012',
    ]
    os.system('\n'.join(CMDS))
    print 'updating to 0.1.4'


def update_to_015():
    CMDS = [
        './manage.py migrate loginza 0001',
        './manage.py migrate lfs.core 0008',
    ]
    os.system('\n'.join(CMDS))
    print 'updating to 0.1.5'


def change_product_statuses():
    from lfs.catalog.models import ProductStatus, Product

    in_stock, create = ProductStatus.objects.get_or_create(name=u'В наличии', css_class='label-success')
    not_in_stock, create = ProductStatus.objects.get_or_create(name=u"Нет в наличии", show_buy_button=False)
    for product in Product.objects.all():
        if product.is_deliverable():
            product.status = in_stock
        else:
            product.status = not_in_stock
        product.save()


def update_to_016():
    CMDS = [
        './manage.py migrate lfs.catalog 0013',
        './manage.py migrate question 0001',
        './manage.py migrate lfs.portlet 0013',
        './manage.py migrate callback 0001',
        './manage.py migrate sms 0001',
        './manage.py migrate lfs.catalog 0014',
    ]
    os.system('\n'.join(CMDS))
    change_product_statuses()
    print 'Updating to 0.1.6'


def update_to_017():
    CMDS = [
        './manage.py migrate lfs.catalog 0015',
        './manage.py migrate lfs.portlet 0014',
        './manage.py migrate lfs.portlet 0015',
    ]
    os.system('\n'.join(CMDS))

    from lfs.catalog.models import SortType

    SortType.objects.get_or_create(name=u'По возрастанию цены', sortable_fields='price')
    SortType.objects.get_or_create(name=u'По убыванию цены', sortable_fields='-price')
    SortType.objects.get_or_create(name=u'По алфавиту', sortable_fields='name')
    SortType.objects.get_or_create(name=u'Обратно алфавиту', sortable_fields='-name')
    SortType.objects.get_or_create(name=u'По статусу и цене', sortable_fields='status__order, -effective_price')
    print 'Updating to 0.1.7'

def update_to_018():
    CMDS = [
        './manage.py migrate lfs.catalog 0016',
        './manage.py migrate lfs.catalog 0017',
        './manage.py migrate lfs.core 0009',
        './manage.py migrate tasks 0002',
        './manage.py migrate price_navigator 0001',
        './manage.py migrate portlets 0003',
        './manage.py migrate lfs.portlet 0016',
        './manage.py migrate lfs.core 0010',
    ]
    os.system('\n'.join(CMDS))
    print 'Updating to 0.1.8'

def update_to_019():
    CMDS = [
        './manage.py migrate lfs.catalog 0018',
        './manage.py migrate lfs.filters 0010',
    ]
    os.system('\n'.join(CMDS))
    from lfs.catalog.models import SortType
    SortType.objects.filter(name=u'По статусу и цене').update(sortable_fields=u'status__order, -effective_price')

    print 'Updating to 0.1.9'

def update_to_020():
    CMDS = [
        './manage.py migrate lfs.catalog 0019',
        './manage.py migrate lfs.catalog 0020',
        './manage.py migrate lfs.core 0011',
        './manage.py migrate hr_urls 0001',
        # './manage.py create_search_indexes',
        './manage.py migrate form_designer',
        './manage.py migrate lfs.filters 0011',
        './manage.py migrate lfs.catalog 0021',
    ]
    os.system('\n'.join(CMDS))
    from lfs.filters.utils import update_filters
    from lfs.catalog.models import Category
    ids = [cat.id for cat in Category.objects.all()]
    for i in ids:
        update_filters(i)
    print 'Updating to 0.2.0'

def update_to_021():
    CMDS = [
        './manage.py migrate lfs.core 0012',
        './manage.py migrate lfs.core 0013',
        './manage.py migrate lfs.customer 0001 --fake',
        './manage.py migrate lfs.customer 0002',
        './manage.py migrate lfs.customer 0003',
        './manage.py migrate lfs.filters 0012',
        './manage.py migrate lfs.catalog 0022',
        './manage.py migrate lfs.catalog 0023',
        './manage.py migrate lfs.mail 0001',
        './manage.py migrate lfs.order 0007',
        './manage.py migrate lfs.order 0008',
        './manage.py migrate searchapi 0001',
        './manage.py create_search_indexes',
    ]
    os.system('\n'.join(CMDS))
    from lfs.mail.models import MailTemplate
    # Mail templates
    folder = 'basic_theme/templates/lfs/mail/'
    files = [folder + a for a in os.listdir(folder)]
    for f in files:
        name, file_type = f.split('/')[-1].split('.')
        if file_type not in ['txt', 'html']:
            continue
        mail_template, created = MailTemplate.objects.get_or_create(name=name)
        content = open(f, 'r')
        if file_type == 'txt':
            mail_template.text_template = content.read()
        elif file_type == 'html':
            mail_template.html_template = content.read()
        mail_template.save()
        content.close()

    print 'Updating to 0.2.1'


def update_to_022():
    CMDS = [
        './manage.py migrate lfs.filters 0013',
        './manage.py migrate lfs.filters 0014',
    ]
    os.system('\n'.join(CMDS))
    from lfs.filters.models import Filter, FilterOption
    Filter.objects.all().update(template="%s", is_addition=True)
    for f in Filter.objects.all():
        f.fill_identificator()

    for fo in FilterOption.objects.all():
        fo.fill_identificator()

    print 'Updating to 0.2.2'


def update_to_023():
    CMDS = [
        './manage.py migrate lfs.fields 0005',
        './manage.py migrate lfs.catalog 0024',
    ]
    os.system('\n'.join(CMDS))

    from lfs.catalog.models import Product
    Product.objects.filter(packing_unit=None).update(packing_unit=1.)
    templates = ['order_sent_subject', 'order_sent_mail', 'order_paid_subject', 'order_paid_mail',
     'order_received_subject', 'order_received_mail', 'new_user_mail', 'new_user_mail_subject',
      'change_password_mail', 'change_password_mail_subject', 'change_email_mail',
      'change_email_mail_subject', 'review_added_mail', 'answer_mail']


    from lfs.mail.models import MailTemplate
    import re
    include = re.compile(r'{% include "(.+?)" %}')
    for mail in MailTemplate.objects.all():
        if mail.name not in templates:
            mail.delete()
            continue

        current_text = mail.text_template
        files = include.findall(current_text)
        files = [f for f in files if '.txt' in f]

        for f in files:
            name = f.split('/')[-1].split('.')[0]
            text = ''
            try:
                text = MailTemplate.objects.get(name=name).text_template
            except:
                text = open('basic_theme/templates/' + f, 'r').read()

            mail.text_template = current_text.replace('{% include "' + f + '" %}', text)

        current_html = mail.html_template
        files = include.findall(current_html)
        files = [f for f in files if '.html' in f]

        for f in files:
            name = f.split('/')[-1].split('.')[0]
            text = ''
            try:
                text = MailTemplate.objects.get(name=name).html_template
            except:
                text = open('basic_theme/templates/' + f, 'r').read()

            mail.html_template = current_html.replace('{% include "' + f + '" %}', text)

        mail.save()


    print 'Updating to 0.2.3'

def update_to_024():
    CMDS = [
        './manage.py migrate lfs.customer 0004',
        './manage.py migrate callback 0002',
    ]
    os.system('\n'.join(CMDS))

    from lfs.filters.models import Filter, FilterOption
    Filter.objects.all().update(template="%s", is_addition=True)
    for f in Filter.objects.all():
        f.fill_identificator()

    for fo in FilterOption.objects.all():
        fo.fill_identificator()
    print 'Updating to 0.2.4'

def update_for_admin_site():
    CMDS = [
        './manage.py migrate easy_thumbnails',
        './manage.py migrate filer',
    ]
    os.system('\n'.join(CMDS))

    print 'Updating a couple of things in new admin'

def update_for_admin_site_01():
    CMDS = [
        './manage.py migrate lfs.catalog 0025',
        './manage.py migrate lfs.filters 0015',
    ]
    os.system('\n'.join(CMDS))

def update_to_025():
    update_for_admin_site()
    update_for_admin_site_01()

    CMDS = [
        './manage.py migrate lfs.criteria 0001 --fake',
        './manage.py migrate lfs.criteria 0002',
        './manage.py migrate lfs.manufacturer 0001 --fake',
        './manage.py migrate lfs.manufacturer 0002',
        './manage.py migrate lfs.marketing 0003',
        './manage.py migrate lfs.filters 0016',
    ]
    os.system('\n'.join(CMDS))
    from lfs.marketing.models import ProductList
    import pytils
    for p in ProductList.objects.filter(identifier=''):
        p.identifier = pytils.translit.slugify(p.name)
        p.save()

    print 'Updating to 0.2.5'

def update_to_026():

    CMDS = [
        './manage.py migrate lfs.filters 0017',
    ]
    os.system('\n'.join(CMDS))

    print 'Updating to 0.2.6'

def update_to_027():
    CMDS = [
        './manage.py migrate lfs.catalog 0026',
        './manage.py migrate lfs.catalog 0027',
        './manage.py migrate lfs.catalog 0028',
        './manage.py migrate lfs.catalog 0029',
        './manage.py migrate lfs.catalog 0030',
        './manage.py migrate lfs.catalog 0031',
        './manage.py migrate lfs.catalog 0032',
        './manage.py migrate lfs.catalog 0033',
        './manage.py migrate lfs.catalog 0034', 
        './manage.py migrate lfs.core 0014',
        './manage.py migrate lfs.page 0003',                
        './manage.py migrate logtailer',
    ]
    os.system('\n'.join(CMDS))

    from lfs.catalog.models import Product, Property, ProductPropertyValue, PropertyGroup
    if Property.objects.filter(is_group=True).count() == 0:
        Property.objects.all().update(is_group=False)
        for i, product in enumerate(Product.objects.all()):
            print i
            position = 10
            for property_group in product.property_groups.all().order_by('id'):
                groups = Property.objects.filter(name=property_group.name, is_group=True)
                if groups:
                    group = groups[0]
                else:
                    group = Property(name=property_group.name, is_group=True)
                    group.save()
                try:
                    ProductPropertyValue.objects.get_or_create(property=group, product=product, position=position)
                    position += 10

                    pps = []
                    for group in PropertyGroup.objects.filter(products__in=[product], name=property_group.name):
                        properties = [p for p in
                            group.properties.filter(display_on_product=True).order_by("groupspropertiesrelation")]
                        pps.extend(properties)

                    ppvs = ProductPropertyValue.objects.filter(property__in=pps, product=product, type=2)
                    for p in ppvs:
                        p.position = position
                        p.save()
                        position += 10
                except:
                    pass

    if Property.objects.filter(identificator=u''):
        for p in Property.objects.all():
            p.save()
    print 'Updating to 0.2.7'


def update_to_028():
    print 'Updating to 0.2.8'
    CMDS = [
        './manage.py migrate lfs.catalog 0035',
        './manage.py migrate hr_urls 0002',
        './manage.py migrate price_navigator',
    ]
    os.system('\n'.join(CMDS))

    from ...models import Shop
    from django.contrib.auth.models import Permission
    from django.contrib.contenttypes.models import ContentType

    content_type = ContentType.objects.get_for_model(Shop)
    permission, created = Permission.objects.get_or_create(codename='manage_portlets',
                                           name=_(u'Can manage portlets'),
                                           content_type=content_type)
    if created:
        print 'New permission was created:', permission

    permission, created = Permission.objects.get_or_create(codename='manage_criteria',
                                           name=_(u'Can manage criteria'),
                                           content_type=content_type)
    if created:
        print 'New permission was created:', permission

    permission, created = Permission.objects.get_or_create(codename='manage_fields',
                                           name=_(u'Can manage fields'),
                                           content_type=content_type)
    if created:
        print 'New permission was created:', permission

    print 'OK'


def update_to_029():
    from lfs.catalog.models import Category

    print 'Updating to 0.2.9'
    CMDS = [
        './manage.py migrate maintenance 0001',
        './manage.py migrate maintenance 0002',
        './manage.py migrate lfs.catalog 0036',
        './manage.py migrate lfs.catalog 0037',
        './manage.py migrate lfs.catalog 0038',
        './manage.py migrate lfs.catalog 0039',
        './manage.py migrate lfs.catalog 0040',
        './manage.py migrate lfs.catalog 0041',
        './manage.py migrate lfs.catalog 0042',
        './manage.py migrate lfs.catalog 0043',
        './manage.py migrate lfs.catalog 0044',
        './manage.py migrate lfs.catalog 0045',
        './manage.py migrate tasks 0003',
        './manage.py migrate tasks 0004',
        './manage.py migrate tasks 0005',
        './manage.py migrate tasks 0006',
        './manage.py migrate tasks 0007',
        './manage.py migrate lfs.cart 0001 --fake',
        './manage.py migrate lfs.cart 0002',
        './manage.py migrate lfs.payment 0004',
        './manage.py migrate lfs.order 0009',
        './manage.py migrate lfs.order 0010',
        './manage.py migrate lfs.order 0011',
        './manage.py migrate lfs.order 0012',
        './manage.py migrate lfs.shipping 0003',
        './manage.py migrate lfs.criteria 0003',
        './manage.py migrate lfs.customer 0005',
        './manage.py migrate lfs.customer 0006',
        './manage.py migrate lfs.customer 0007',
        './manage.py migrate lfs.customer 0008',
        './manage.py migrate lfs.customer 0009',
        './manage.py migrate lfs.customer 0010',
        './manage.py migrate lfs.customer 0011',
        './manage.py migrate lfs.core 0015',
        './manage.py migrate lfs.core 0016',
        './manage.py migrate lfs.core 0017',
        './manage.py migrate lfs.core 0018',
        './manage.py migrate lfs.core 0019',
        './manage.py migrate lfs.core 0020',
        './manage.py migrate lfs.core 0021',
        './manage.py migrate lfs.core 0022',
        './manage.py migrate lfs.discounts 0001 --fake',
        './manage.py migrate lfs.discounts 0002',
        './manage.py migrate lfs.marketing 0004',
        './manage.py migrate lfs.manufacturer 0003',
        './manage.py migrate lfs.voucher 0001 --fake',
        './manage.py migrate lfs.voucher 0002',
        './manage.py migrate lfs.fields 0006',
        './manage.py migrate lfs.fields 0007',
        './manage.py migrate price_navigator 0002',
        './manage.py migrate price_navigator 0003',
    ]
    os.system('\n'.join(CMDS))

    print 'Fix default seo settings for categories...'
    categories = Category.objects.all()
    for c in categories.iterator():
        meta_title = c.meta_title
        meta_keywords = c.meta_keywords
        meta_description = c.meta_description
        seo = c.seo

        if meta_title == '<name>':
            meta_title = getattr(
                settings, 'DEFAULT_CATEGORY_META_TITLE_TEMPLATE', '')
        meta_title = meta_title.replace(
            '<name>', '{{ name }}').replace('<shop>', '{{ shop_name }}')
        meta_keywords = meta_keywords.replace(
            '<name>', '{{ name }}').replace('<shop>', '{{ shop_name }}')
        meta_description = meta_description.replace(
            '<name>', '{{ name }}').replace('<shop>', '{{ shop_name }}')
        seo = seo.replace(
            '<name>', '{{ name }}').replace('<shop>', '{{ shop_name }}')

        c.meta_title = meta_title
        c.meta_keywords = meta_keywords
        c.meta_description = meta_description
        c.seo = seo
        c.save()
        print c.name
    print '-------------OK'

    print 'Cleaning stolen Address...'
    from lfs.customer.models import Address
    Address.objects.filter(customer=None).update(fields={})
    addrs = Address.objects.filter(customer=None)
    addrs.delete()
    print 'Fill Address hashes...'
    addrs = Address.objects.filter(hash_str=None)
    for a in addrs.iterator():
        a.save()

    print 'Cleaning stolen Phone...'
    from lfs.customer.models import Phone
    phones = Phone.objects.filter(customer=None)
    phones.delete()
    print 'Cleaning stolen EmailAddress...'
    from lfs.customer.models import EmailAddress
    emails = EmailAddress.objects.filter(customer=None)
    emails.delete()

    print 'Fix invalid data for Customers...'
    from lfs.customer.models import Customer
    customers = Customer.objects.all()
    for c in customers.iterator():
        phones = c.phones.filter(default=True)
        if phones.count() > 0:
            print 'Multiple default phones for', c
            for p in phones[1:]:
                p.default = False
                p.save()
            print 'Fixed.'

    print 'OK'
    print 'Don\'t forget to clear stolen ContentType'


def update_to_030():
    print 'Updating to 0.3.0'

    CMDS = [
        './manage.py migrate lfs.catalog 0046',
        './manage.py migrate lfs.catalog 0047',
        './manage.py migrate macroses 0001',
        './manage.py migrate lfs.discounts 0003',
        './manage.py migrate lfs.criteria 0004',
        './manage.py migrate lfs.page 0004',
    ]
    os.system('\n'.join(CMDS))

    print 'OK'


def convert_products_for_031():
    from django.db.models import F
    from lfs.catalog.models import Product
    from lfs.catalog.settings import (
        PRODUCT_WITH_VARIANTS,
        STANDARD_PRODUCT,
        VARIANT,
    )

    print 'Fill denormalized fields for product with variants'
    products_with_variants = Product.objects.filter(
        sub_type=PRODUCT_WITH_VARIANTS)

    count = products_with_variants.count()
    i = 0
    for p in products_with_variants:
        i += 1
        print '%d/%d' % (i, count)
        p.save()

    print 'Fill denormalized fields for standard products'
    standard_products = Product.objects.filter(
        sub_type=STANDARD_PRODUCT)
    result = standard_products.update(
        min_variant_price=F('effective_price'),
        max_variant_price=F('effective_price'),
    )
    print result

    print 'Fill denormalized fields for product variants'
    variants = Product.objects.filter(
        sub_type=VARIANT)
    result = variants.update(
        min_variant_price=F('effective_price'),
        max_variant_price=F('effective_price'),
    )
    print result


def delete_stolen_contetypes():
    from django.contrib.contenttypes.models import ContentType
    for c in ContentType.objects.all():
        if not c.model_class():
            print "deleting %s" % c
            c.delete()


def update_to_031():
    print 'Updating to 0.3.1'

    CMDS = [
        './manage.py migrate lfs.filters 0018',
        './manage.py migrate report_builder 0001',
        './manage.py migrate simple_import 0001',
        './manage.py migrate lfs.filters 0019',
        './manage.py migrate lfs.page 0005',
        './manage.py migrate lfs.core 0023',
        './manage.py migrate lfs.portlet 0017',
        './manage.py migrate lfs.order 0013',
        './manage.py migrate lfs.order 0014',
    ]
    os.system('\n'.join(CMDS))

    print 'Converting DB...'
    convert_products_for_031()

    print 'Delete stolen content types...'
    delete_stolen_contetypes()

    print 'Calculate product sales...'
    from lfs.marketing.utils import calculate_product_sales
    calculate_product_sales()

    print 'Creating default order statuses'
    from lfs.order.models import OrderStatus, Order
    from django.utils import translation
    translation.activate(settings.LANGUAGE_CODE)

    if OrderStatus.objects.count() <= 1:
        OrderStatus.objects.all().delete()
        o, created = OrderStatus.objects.get_or_create(
            name=_(u"Submitted"), identifier="submitted")
        OrderStatus.objects.get_or_create(
            name=_(u"Paid"), identifier="paid")
        OrderStatus.objects.get_or_create(
            name=_(u"Sent"), identifier="sent")
        OrderStatus.objects.get_or_create(
            name=_(u"Closed"), identifier="closed")
        OrderStatus.objects.get_or_create(
            name=_(u"Canceled"), identifier="canceled")
        OrderStatus.objects.get_or_create(
            name=_(u"Payment Failed"), identifier="payment_failed")
        OrderStatus.objects.get_or_create(
            name=_(u"Payment Flagged"), identifier="payment_flagged")
        Order.objects.update(status=o)

    print 'OK'


def update_to_032():
    print 'Updating to 0.3.2'
    CMDS = [
        './manage.py migrate lfs.page 0006',
        './manage.py migrate lfs.marketing 0005',
        './manage.py migrate lfs.portlet 0018'
    ]
    os.system('\n'.join(CMDS))

    from lfs.catalog.models import StaticBlock
    StaticBlock.objects.get_or_create(name="header")
    StaticBlock.objects.get_or_create(name="footer")

    # check orders for empty status
    from lfs.order.models import Order, get_default_status
    orders = Order.objects.filter(status=None)
    if orders:
        o = get_default_status()
        orders.update(status=o)
    # register FilterParametrsPortlet
    from portlets.utils import register_portlet
    from lfs.portlet.models import FilterParametrsPortlet
    register_portlet(FilterParametrsPortlet, u'Выбранные фильтры')
    print 'OK'
