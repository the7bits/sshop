# coding: utf-8
from django.contrib import admin
from lfs.faq.models import Question, Topic


class TopicAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


class QuestionAdmin(admin.ModelAdmin):
    list_display = ['question', 'sort_order', 'created_by', 'creation_date',
                    'answered_by', 'answer_date', 'published']
    list_editable = ['sort_order', 'published']

admin.site.register(Question, QuestionAdmin)
admin.site.register(Topic, TopicAdmin)
