# coding: utf-8
from django.contrib.contenttypes import generic
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from ..payment.models import CriteriaObjects
from ..core.utils import import_symbol
from .settings import DISCOUNT_TYPE_CHOICES
from .settings import DISCOUNT_TYPE_ABSOLUTE


class Discount(models.Model):
    """A discount which is given to the customer if several criteria
    fullfilled.

    **Attributes:**

    name
        The name of the discount. This can be displayed to the customer.

    value
        The value of the discount, can be absolute or percentage dependend on
        the type of the discount.

    type
        The type of the discount. Absolute or percentage.

    sku
        The SKU of the discount.

    criteria_objects
        Criteria which must all valid to make the discount happen.

    discount_calculator
        Calculator that will calculate discounts for products in cart

    """
    name = models.CharField(_(u"Name"), max_length=100)
    value = models.FloatField(_(u"Value"))
    type = models.PositiveSmallIntegerField(
        _(u"Type"),
        choices=DISCOUNT_TYPE_CHOICES,
        default=DISCOUNT_TYPE_ABSOLUTE)
    sku = models.CharField(
        _(u"SKU"), blank=True, max_length=50,
        help_text=_(u'The SKU of the discount.'))
    criteria_objects = generic.GenericRelation(
        CriteriaObjects,
        object_id_field="content_id", content_type_field="content_type")
    discount_calculator = models.CharField(
        _(u"Discount calculator"),
        choices=settings.DISCOUNT_CALCULATORS, max_length=255,
        default=settings.DISCOUNT_CALCULATOR,
    )

    class Meta:
        verbose_name = _(u'Discount')
        verbose_name_plural = _(u'Discounts')

    def __unicode__(self):
        return self.name

    def is_valid(self, request, product=None):
        """The shipping method is valid if it has no criteria or if all
        assigned criteria are true.

        If product is given the product is tested otherwise the whole cart.
        """
        if self.discount_calculator ==\
                'lfs.discounts.api.BaseDiscountCalculator':
            from ..criteria.utils import is_valid as _is_valid
            return _is_valid(request, self, product)
        else:
            class_ = import_symbol(self.discount_calculator)
            return class_().is_valid(request, self)

    def get_price(self, request, product=None):
        """Returns the price of the discount.
        """
        if self.discount_calculator ==\
                'lfs.discounts.api.BaseDiscountCalculator':
            if self.type == DISCOUNT_TYPE_ABSOLUTE:
                return self.value

            from ..cart.utils import get_cart
            cart = get_cart(request)

            if cart is not None:
                return cart.get_price(request) * (self.value / 100)
            elif product is not None:
                return product.get_price(request) * (self.value / 100)

            return 0.0
        else:
            class_ = import_symbol(self.discount_calculator)
            return class_().get_price(request, self)

    def get_value(self, request):
        class_ = import_symbol(self.discount_calculator)
        return class_().get_value(request, self)

    def is_absolute(self, request):
        class_ = import_symbol(self.discount_calculator)
        return class_().is_absolute(request, self)
