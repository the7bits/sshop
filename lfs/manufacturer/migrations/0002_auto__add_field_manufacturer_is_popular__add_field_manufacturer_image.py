# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Manufacturer.is_popular'
        db.add_column('manufacturer_manufacturer', 'is_popular',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'Manufacturer.image'
        db.add_column('manufacturer_manufacturer', 'image',
                      self.gf('lfs.core.fields.thumbs.ImageWithThumbsField')(max_length=100, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Manufacturer.is_popular'
        db.delete_column('manufacturer_manufacturer', 'is_popular')

        # Deleting field 'Manufacturer.image'
        db.delete_column('manufacturer_manufacturer', 'image')


    models = {
        'manufacturer.manufacturer': {
            'Meta': {'ordering': "('name',)", 'object_name': 'Manufacturer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('lfs.core.fields.thumbs.ImageWithThumbsField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'is_popular': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['manufacturer']