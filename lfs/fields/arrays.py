# coding: utf-8


class FieldsDataManager(object):
    def __init__(
            self, data,
            container_object=None,
            extra_data_object=None):
        self.data = data
        self.data_lines = data.splitlines()

    def get_array(self, name):
        if name[0] != '@':
            ind = self.data_lines.index('@' + name)
        else:
            ind = self.data_lines.index(name)
        result = []
        for i in xrange(ind+1, len(self.data_lines)):
            s = self.data_lines[i]
            if s and (s[0] not in ['@', '$', '`']):
                if s[0] != '#':
                    if ':' in s:
                        t = s.split(':')
                        tokens = t[1].split(',')
                        actions = []
                        for x in tokens:
                            if '->' in x:
                                w = x.split('->')
                                if w[0][0] == '@':
                                    actions.append({
                                        'type': 'write_array',
                                        'array': w[0],
                                        'target': w[1],
                                    })
                                elif w[0][0] == '$':
                                    actions.append({
                                        'type': 'write_variable',
                                        'variable': w[0],
                                        'target': w[1],
                                    })
                                elif w[0][0] == '`':
                                    actions.append({
                                        'type': 'write_value',
                                        'value': w[0][1:],
                                        'target': w[1],
                                    })
                            else:
                                actions.append({
                                    'type': 'touch',
                                    'target': x,
                                })
                        result.append({
                            'type': 'action',
                            'value': t[0],
                            'actions': actions,
                        })
                    else:
                        result.append({
                            'type': 'simple',
                            'value': s,
                        })
            elif s and (s[0] in ['@', '$', '`']):
                break
        return result

    def get_not_binded_actions(self):
        result = []
        for s in self.data_lines:
            if s and s[0] == '$':
                w = s.split('->')
                result.append({
                    'type': 'write_variable',
                    'variable': w[0],
                    'target': w[1],
                })
            if s and s[0] == '`':
                w = s.split('->')
                result.append({
                    'type': 'write_value',
                    'value': w[0][1:],
                    'target': w[1],
                })
        return result
