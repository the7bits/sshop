# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ChoiceField'
        db.create_table('fields_choicefield', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('field_name', self.gf('django.db.models.fields.CharField')(default='', max_length=100)),
            ('identificator', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('field_value', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal('fields', ['ChoiceField'])

        # Deleting field 'CharField.default_value'
        db.delete_column('fields_charfield', 'default_value')

        # Adding field 'CharField.field_name'
        db.add_column('fields_charfield', 'field_name',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100),
                      keep_default=False)

        # Adding field 'CharField.field_value'
        db.add_column('fields_charfield', 'field_value',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'ChoiceField'
        db.delete_table('fields_choicefield')

        # Adding field 'CharField.default_value'
        db.add_column('fields_charfield', 'default_value',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True),
                      keep_default=False)

        # Deleting field 'CharField.field_name'
        db.delete_column('fields_charfield', 'field_name')

        # Deleting field 'CharField.field_value'
        db.delete_column('fields_charfield', 'field_value')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'fields.charfield': {
            'Meta': {'object_name': 'CharField'},
            'field_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'field_value': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'fields.choicefield': {
            'Meta': {'object_name': 'ChoiceField'},
            'field_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '100'}),
            'field_value': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identificator': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'})
        },
        'fields.fieldsobjects': {
            'Meta': {'ordering': "['position']", 'object_name': 'FieldsObjects'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fields'", 'to': "orm['contenttypes.ContentType']"}),
            'field_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'field_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'field'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '999'})
        }
    }

    complete_apps = ['fields']