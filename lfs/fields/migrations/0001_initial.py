# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'FieldsObjects'
        db.create_table('fields_fieldsobjects', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('field_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='field', to=orm['contenttypes.ContentType'])),
            ('field_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='fields', to=orm['contenttypes.ContentType'])),
            ('content_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('position', self.gf('django.db.models.fields.PositiveIntegerField')(default=999)),
        ))
        db.send_create_signal('fields', ['FieldsObjects'])


    def backwards(self, orm):
        # Deleting model 'FieldsObjects'
        db.delete_table('fields_fieldsobjects')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'fields.fieldsobjects': {
            'Meta': {'ordering': "['position']", 'object_name': 'FieldsObjects'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'fields'", 'to': "orm['contenttypes.ContentType']"}),
            'field_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'field_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'field'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.PositiveIntegerField', [], {'default': '999'})
        }
    }

    complete_apps = ['fields']