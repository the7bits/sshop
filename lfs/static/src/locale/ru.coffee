window.ru = {
    'Cart': 'Корзина',
    'Checkout': 'Оформить заказ',
    'Continue shopping': 'Продолжить покупки',
    'Show all': 'Показать все',
    'Only popular': 'Только популярные',
    'Locked : form can\'t be submited': 'Заблокирована: форма не может быть подтверждена',
    'Unlocked : form can be submited': 'Разблокирована: форма может быть подтверждена',
}
