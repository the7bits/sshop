# coding: utf-8
from django.conf import settings
from django.utils.translation import ugettext as _
from suit.admin import SortableModelAdmin, SortableTabularInline
from django.core import urlresolvers
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.conf.urls import patterns, url
from django.contrib import admin
from django.contrib import messages
from django.core.exceptions import PermissionDenied

from .models import (
    Topseller,
    FeaturedProduct,
    ProductList,
    ProductListItem,
    ProductSales,
)
from .forms import (
    FeaturedProductForm,
    TopsellerForm,
    ProductListItemForm,
)
from genericadmin.admin import GenericTabularInline
from ..catalog.models import Image


class FeaturedProductAdmin(SortableModelAdmin):
    list_display = ('product', 'active')
    search_fields = ['product__name']
    sortable = 'position'
    form = FeaturedProductForm


class TopsellerAdmin(SortableModelAdmin):
    list_display = ('product',)
    search_fields = ['product__name']
    sortable = 'position'
    form = TopsellerForm


if settings.COMPATIBILITY_USE_TOPSELLER_AND_FEATURED:
    admin.site.register(FeaturedProduct, FeaturedProductAdmin)
    admin.site.register(Topseller, TopsellerAdmin)


class ImageInline(SortableTabularInline, GenericTabularInline):
    model = Image
    ct_field = 'content_type'
    ct_fk_field = 'content_id'
    sortable = 'position'
    extra = 0


# class ProductListItemInline(SortableTabularInline):
#     model = ProductListItem
#     form = ProductListItemInlineForm
#     extra = 0
#     sortable = 'position'

class ProductListItemAdmin(admin.ModelAdmin):
    list_display = ('product', 'product_list', 'position')
    model = ProductListItem
    form = ProductListItemForm
    search_fields = ['product__name']
    list_filter = ('product_list',)
    ordering = ['product_list']


class ProductListAdmin(admin.ModelAdmin):
    list_display = ('name', 'identifier', 'get_product_count')
    search_fields = ['name']
    # inlines = [ImageInline, ProductListItemInline]
    inlines = [ImageInline]

    def get_product_count(self, obj):
        return obj.productlistitem_set.all().count()
    get_product_count.short_description = _(u'Product count')


class ProductSalesAdmin(admin.ModelAdmin):
    change_list_template = 'admin/marketing_includes/sales_change_list.html'
    list_display = ('get_product_name', 'sales')
    search_fields = ('product__name',)
    ordering = ('-sales',)
    readonly_fields = ('product', 'sales')

    def has_add_permission(self, request):
        return False

    def get_product_name(self, obj):
        p = obj.product
        return u'<a href="%s">%s</a>' % (
            urlresolvers.reverse(
                'admin:%s_%s_change' % ('catalog', 'product'), args=[p.pk]),
            p.name,
        )
    get_product_name.short_description = _(u'Product')
    get_product_name.allow_tags = True
    get_product_name.admin_order_field = 'product__name'

    def changelist_view(self, request, extra_context=None):
        """Renders the change view."""
        opts = self.model._meta
        context = {
            "reloadlist_url":
            reverse("%s:%s_%s_reloadlist" % (
                self.admin_site.name, opts.app_label, opts.module_name)),
            "settask_url":
            reverse("%s:%s_%s_settask" % (
                self.admin_site.name, opts.app_label, opts.module_name)),
        }
        context.update(extra_context or {})
        return super(ProductSalesAdmin, self).changelist_view(request, context)

    def get_urls(self):
        """Returns the additional urls used by the admin."""
        urls = super(ProductSalesAdmin, self).get_urls()
        admin_site = self.admin_site
        opts = self.model._meta
        info = opts.app_label, opts.module_name,
        sales_urls = patterns(
            "",
            url(
                "^reload/$",
                admin_site.admin_view(self.reload_view),
                name='%s_%s_reloadlist' % info),
            url(
                "^set-task/$",
                admin_site.admin_view(self.set_task_view),
                name='%s_%s_settask' % info),
        )
        return sales_urls + urls

    def reload_view(self, request, extra_context=None):
        """Reload a sales list."""
        from .utils import calculate_product_sales
        # check if user has change or add permissions for model
        if not self.has_change_permission(request) and \
                not self.has_add_permission(request):
            raise PermissionDenied
        model = self.model
        opts = model._meta
        calculate_product_sales()
        messages.success(
            request, _('Product sales was updated successfully.'))
        return HttpResponseRedirect(
            reverse("%s:%s_%s_changelist" % (
                self.admin_site.name, opts.app_label, opts.module_name)))

    def set_task_view(self, request, extra_context=None):
        """Set task for periodically update a sales list."""
        from tasks.api import TaskQueueManager
        # check if user has change or add permissions for model
        if not self.has_change_permission(request) and \
                not self.has_add_permission(request):
            raise PermissionDenied
        model = self.model
        opts = model._meta

        task_manager = TaskQueueManager()
        task_manager.schedule(
            'lfs.marketing.jobs.update_product_sales_job',
            repeat=24*60)  # Repeat every day

        messages.success(
            request, _('A background job successfully added.'))
        return HttpResponseRedirect(
            reverse("%s:%s_%s_changelist" % (
                self.admin_site.name, opts.app_label, opts.module_name)))


admin.site.register(ProductList, ProductListAdmin)
admin.site.register(ProductListItem, ProductListItemAdmin)
admin.site.register(ProductSales, ProductSalesAdmin)
