# coding: utf-8
from django import forms
import autocomplete_light


class FeaturedProductForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }


class TopsellerForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }


class ProductListItemForm(forms.ModelForm):
    class Meta:
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }
