# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string

from portlets.models import Portlet


class SocNetworkPortlet(Portlet):
    """Portlet to display soc. networks buttons.
    """

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get("request")

        result = render_to_string(
            "lfs/portlets/soc_network.html",
            RequestContext(request, {
                "title": self.title,
                'slot_name': context.get('slot_name'),
            }))

        return result

    def form(self, **kwargs):
        return SocNetworkPortletForm(instance=self, **kwargs)


class SocNetworkPortletForm(forms.ModelForm):
    """Form for CategoriesCarouselPortlet.
    """
    class Meta:
        model = SocNetworkPortlet

    def __init__(self, *args, **kwargs):
        super(SocNetworkPortletForm, self).__init__(*args, **kwargs)
