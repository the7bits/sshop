# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string
from django.db import models
from django.utils.translation import ugettext_lazy as _

from portlets.models import Portlet


class CarouselPortlet(Portlet):
    """Portlet to display carousel.
    """

    slides = models.TextField(
        _(u"Slides"),
        blank=True,
        default=
        "{'image':'static/img/header_01.jpg', 'caption':"
        "'some caption', 'url':'#some_url'}")

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        request = context.get("request")
        try:
            images = [eval(slide) for slide in self.slides.split('\n')]
        except:
            images = False
        return render_to_string(
            "lfs/portlets/carousel.html",
            RequestContext(request, {
                'images': images,
                'id': self.id,
                'slot_name': context.get('slot_name'),
            }))

    def form(self, **kwargs):
        return CarouselForm(instance=self, **kwargs)


class CarouselForm(forms.ModelForm):
    """Form for the CarouselPortlet.
    """
    class Meta:
        model = CarouselPortlet
