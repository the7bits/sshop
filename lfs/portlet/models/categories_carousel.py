# coding: utf-8
from django import forms
from django.db import models
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from portlets.models import Portlet
from ...catalog.models import Category


class CategoriesCarouselPortlet(Portlet):
    """Portlet to display categories as carousel.
    """
    categories = models.ManyToManyField(
        Category, verbose_name=_(u'Categories'))

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get("request")

        result = render_to_string(
            "lfs/portlets/categories_carousel.html",
            RequestContext(request, {
                "title": self.title,
                "categories": self.categories.all(),
                'slot_name': context.get('slot_name'),
            }))

        return result

    def form(self, **kwargs):
        return CategoriesCarouselPortletForm(instance=self, **kwargs)


class CategoriesCarouselPortletForm(forms.ModelForm):
    """Form for CategoriesCarouselPortlet.
    """
    categories = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all(),
        widget=admin.widgets.FilteredSelectMultiple(
            _(u'Categories'), False
        ),
        label=_(u'Categories'),
        required=True
    )

    class Media:
        js = ('/superadmin/jsi18n/',)

    class Meta:
        model = CategoriesCarouselPortlet

    def __init__(self, *args, **kwargs):
        super(CategoriesCarouselPortletForm, self).__init__(*args, **kwargs)
