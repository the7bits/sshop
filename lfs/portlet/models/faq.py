# -coding: utf-8
from django import forms
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.template import RequestContext
from django.template.loader import render_to_string
from django.contrib import admin

from portlets.models import Portlet

from ...faq.models import Question, Topic


class FaqPortlet(Portlet):
    """Portlet to display questions.
    """
    random = models.BooleanField(
        _(u"Use random choice."), default=False, blank=True)
    number = models.IntegerField(
        _(u"Number of questions to show."), default=5, blank=True)
    topic = models.ManyToManyField(
        Topic, verbose_name=_(u'Be sure to select at least one topic'))

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get("request")
        questions = Question.objects.filter(published=True)
        if self.topic:
            questions = questions.filter(topic__in=self.topic.all())
        if not self.random:
            questions = questions.order_by('-creation_date')
        if len(questions) > self.number:
            questions = questions[:self.number]

        return render_to_string(
            "lfs/portlets/faq.html",
            RequestContext(request, {
                'questions': questions,
                'slot_name': context.get('slot_name'),
            }))

    def form(self, **kwargs):
        return FaqPortletForm(instance=self, **kwargs)


class FaqPortletForm(forms.ModelForm):
    """Form for FaqPortlet.
    """
    topic = forms.ModelMultipleChoiceField(
        queryset=Topic.objects.all(),
        widget=admin.widgets.FilteredSelectMultiple(
            _(u'Topics'), False
        ),
        label=_(u'Be sure to select at least one topic'),
        required=True
    )

    class Media:
        js = ('/superadmin/jsi18n/',)

    class Meta:
        model = FaqPortlet
