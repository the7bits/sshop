# coding: utf-8
from django import forms
from django.db import models
from django.template import RequestContext
from django.template.loader import render_to_string
from django.core.paginator import Paginator, PageNotAnInteger

from ...marketing.models import Topseller

from portlets.models import Slot
from portlets.models import Portlet


class TopsellerPortlet(Portlet):
    """Portlet to display recent visited products.
    """
    limit = models.IntegerField(default=5)

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context, page=1):
        """Renders the portlet as html.
        """
        slot = Slot.objects.get(name=context.get('slot_name'))
        request = context.get("request")

        topseller = Topseller.objects.all()
        paginator = Paginator(topseller, self.limit)
        try:
            topseller = paginator.page(page)
        except PageNotAnInteger:
            topseller = paginator.page(1)

        return render_to_string(
            "lfs/portlets/topseller.html",
            RequestContext(request, {
                "title": self.title,
                "topseller": topseller,
                "slot": slot,
                "MEDIA_URL": context.get("MEDIA_URL"),
                "portlet_id": self.id
            }))

    def form(self, **kwargs):
        return TopsellerForm(instance=self, **kwargs)


class TopsellerForm(forms.ModelForm):
    """Form for the TopsellerPortlet.
    """
    class Meta:
        model = TopsellerPortlet
