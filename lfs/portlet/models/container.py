# coding: utf-8
from django import forms
from django.template import RequestContext
from django.template.loader import render_to_string
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin

from portlets.models import Portlet, PortletAssignment


CONTAINER_TYPE_CHOICES = (
    (1, _(u'Tabs')),
    (2, _(u'Collapse')),
)


class ContainerPortlet(Portlet):
    """Portlet to display container of portlets.
    """

    portlets = models.ManyToManyField(
        PortletAssignment, verbose_name=_(u"Portlets"), blank=True)
    type = models.IntegerField(
        _(u'Container type'), default=1, choices=CONTAINER_TYPE_CHOICES)

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        request = context.get("request")
        portlets = [{
            'html': portlet.portlet.render(context),
            'title': portlet.portlet.title,
            'id': portlet.portlet.id} for portlet in self.portlets.all()]

        if self.type == 1:
            template_name = 'lfs/portlets/container_tabs.html'
        elif self.type == 2:
            template_name = 'lfs/portlets/container_collapse.html'

        return render_to_string(template_name, RequestContext(request, {
            'portlets': portlets,
            'slot_name': context.get('slot_name'),
            'main_page': context.get('main_page'),
            'container_id': self.id,
            'page_type': context.get('page_type')
        }))

    def form(self, **kwargs):
        return ContainerForm(instance=self, **kwargs)


class ContainerForm(forms.ModelForm):
    """Form for the ContainerPortlet.
    """
    portlets = forms.ModelMultipleChoiceField(
        queryset=PortletAssignment.objects.all(),
        widget=admin.widgets.FilteredSelectMultiple(
            _(u'Portlets'), False
        ),
        label=_(u'Portlets'),
        required=False
    )

    class Media:
        js = ('/superadmin/jsi18n/',)

    class Meta:
        model = ContainerPortlet
