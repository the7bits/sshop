# coding: utf-8
from django import forms
from django.db import models
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.translation import ugettext_lazy as _
from django.core.paginator import Paginator, PageNotAnInteger
from django.contrib.contenttypes.models import ContentType

from ...catalog.models import Category, SortType, Product
from ...marketing.models import ProductList

from portlets.models import Portlet
from portlets.models import PortletAssignment


class ProductListPortlet(Portlet):
    """A portlet for displaying product list.
    """
    product_list = models.ForeignKey(
        ProductList, verbose_name=_(u'Product list'))
    order_by = models.ForeignKey(
        SortType, verbose_name=_(u'Order by'), null=True)
    limit = models.IntegerField(_(u"Limit"), default=5)
    current_category = models.BooleanField(
        _(u"Use current category"), default=False)
    slideshow = models.BooleanField(_(u"Slideshow"), default=False)

    name = _(u'Product list')

    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    @property
    def rendered_title(self):
        return self.title or self.name

    def render(self, context, page=1, ajax_mode=False):
        """Renders the portlet as html.
        """
        request = context.get("request")
        filters = {}

        if self.current_category:
            obj = context.get("category") or context.get("product")
            if obj:
                if isinstance(obj, Category):
                    category = obj
                else:
                    category = obj.get_current_category(request)
                categories = [category]
                categories.extend(category.get_all_children())

                filters = {'categories__in': categories}

        products = Product.objects.filter(
            productlistitem__product_list=self.product_list,
            active=True,
            status__is_visible=True,
            **filters
        )

        try:
            fields = self.order_by.sortable_fields.replace(' ', '').split(',')
            products = products.order_by(*fields)
        except:
            # WTF?
            pass

        pagination = False
        if products.count() > self.limit:
            pagination = True
            paginator = Paginator(products, self.limit)
            try:
                products = paginator.page(page)
            except PageNotAnInteger:
                products = paginator.page(1)

        pa = PortletAssignment.objects.get(
            portlet_id=self.id,
            portlet_type=ContentType.objects.get_for_model(self))

        # pa_id using in portlet paginations

        return render_to_string(
            'lfs/portlets/product_list.html',
            RequestContext(request, {
                "title": self.rendered_title,
                "slideshow": self.slideshow,
                "products": products,
                'slot_name': context.get('slot_name'),
                'pa_id': pa.id,
                'product_list_id': self.product_list.id,
                "portlet_id": self.id,
                'pagination': pagination,
                "MEDIA_URL": context.get("MEDIA_URL"),
                'ajax_mode': ajax_mode,
            }))

    def form(self, **kwargs):
        """
        """
        return ProductListForm(instance=self, **kwargs)


class ProductListForm(forms.ModelForm):
    """
    """
    class Meta:
        model = ProductListPortlet
