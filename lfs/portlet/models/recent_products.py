# coding: utf-8
from django import forms
# from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.template import RequestContext
from django.template.loader import render_to_string
from portlets.models import Portlet  # , PortletAssignment
import time
import hashlib

# from ...catalog.models import Product
# from ...caching.utils import lfs_get_object


class RecentProductsPortlet(Portlet):
    """Portlet to display recent visited products.
    """
    class Meta:
        app_label = 'portlet'

    def __unicode__(self):
        return "%s" % self.id

    def render(self, context):
        """Renders the portlet as html.
        """
        request = context.get("request")
        # pa = PortletAssignment.objects.get(
        #     portlet_id=self.id,
        #     portlet_type=ContentType.objects.get_for_model(self)
        # )
        object = context.get("product")
        slug_not_to_display = ""
        if object:
            ctype = ContentType.objects.get_for_model(object)
            if ctype.model == u'product':
                slug_not_to_display = object.slug
        portlet_hash = hashlib.sha1()
        portlet_hash.update(str(time.time()))
        return render_to_string(
            "lfs/portlets/recent_products.html",
            RequestContext(
                request,
                {
                    'init': True,
                    # 'portlet_assignment_id': pa.id,
                    'slug_not_to_display': slug_not_to_display,
                    'title': self.title,
                    'slot_name': context.get('slot_name'),
                    'portlet_hash': str(portlet_hash.hexdigest()),
                }
            )
        )

    def form(self, **kwargs):
        return RecentProductsForm(instance=self, **kwargs)


class RecentProductsForm(forms.ModelForm):
    """Form for the RecentProductsPortlet.
    """
    class Meta:
        model = RecentProductsPortlet
