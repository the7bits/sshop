# coding: utf-8
from django.conf.urls.defaults import *

# These url patterns use for admin interface
urlpatterns = patterns(
    'lfs.portlet.admin_views',
    url(
        r'^portlets-for-object/(?P<content_type_id>[-\d]*)/'
        '(?P<object_id>[-\d]*)/$',
        'portlets_for_object', name="admin_portlets_for_object"),
    url(r'^move-portlet/(?P<portlet_assignment_id>[-\d]*)/$',
        'move_portlet', name="admin_move_portlet"),
    url(r'^delete-portlet/(?P<portlet_assignment_id>[-\d]*)$',
        "delete_portlet", name="admin_delete_portlet"),
    url(r'^change-portlet/(?P<portlet_assignment_id>[-\d]*)$',
        "change_portlet", name="admin_change_portlet"),

    url(
        r'^delete-all-portlets/(?P<content_type_id>[-\d]*)/'
        '(?P<object_id>[-\d]*)/$',
        'delete_all_portlets', name="admin_delete_all_portlets"),
    url(
        r'^add-portlet/(?P<content_type_id>[-\d]*)/(?P<object_id>[-\d]*)/'
        '(?P<model_id>[-\d]*)/$',
        "add_portlet", name="admin_portlets_portlet_add"),
    url(r'^blocked_slot/(?P<content_type_id>[-\d]*)/(?P<object_id>[-\d]*)/$',
        'blocked_slot', name="admin_blocked_slot"),
)

urlpatterns += patterns(
    'lfs.portlet.views',
    url(
        r'^load-related-options/$',
        'filter_reload_ajax',
        name='load_related_options'
    ),
    url(
        r'^filter/ajax-load/(?P<portlet_id>[-\d]*)/(?P<category_id>[-\d]*)$',
        'filter_ajax_load',
        name='filter_ajax_load'
    )
)
