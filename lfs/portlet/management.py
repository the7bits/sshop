# coding: utf-8
from django.utils.translation import ugettext as _
from django.db.models.signals import post_syncdb

from .models import AverageRatingPortlet
from .models import CartPortlet
from .models import CarouselPortlet
from .models import CategoriesPortlet
from .models import ContainerPortlet
# from .models import DeliveryTimePortlet
from .models import FilterPortlet
from .models import PagesPortlet
from .models import RecentProductsPortlet
from .models import RelatedProductsPortlet
from .models import TextPortlet
from .models import TopsellerPortlet
from .models import ForsalePortlet
from .models import FeaturedPortlet
from .models import EmptySpacePortlet
from .models import NotebookPortlet
from .models import ProductListPortlet
from .models import CustomHTMLPortlet
from .models import AssociatedPagesPortlet
from .models import FaqPortlet
from .models import CategoriesCarouselPortlet
from .models import SocNetworkPortlet

import portlets
from portlets.utils import register_portlet


def register_lfs_portlets(sender, **kwargs):
    # don't register our portlets until the table has been created by syncdb
    if sender == portlets.models:
        register_portlet(AverageRatingPortlet, _(u"Average Rating"))
        register_portlet(CartPortlet, _(u"Cart"))
        register_portlet(CategoriesPortlet, _(u"Categories"))
        register_portlet(CarouselPortlet, _(u"Carousel"))
        register_portlet(ContainerPortlet, _(u"Container"))
        # register_portlet(DeliveryTimePortlet, _(u"Delivery Time"))
        register_portlet(FilterPortlet, _(u"Filter"))
        register_portlet(PagesPortlet, _(u"Pages"))
        register_portlet(RecentProductsPortlet, _(u"Recent Products"))
        register_portlet(RelatedProductsPortlet, _(u"Related Products"))
        register_portlet(TextPortlet, _(u"Text"))
        register_portlet(TopsellerPortlet, _(u"Topseller"))
        register_portlet(ForsalePortlet, _(u"For sale"))
        register_portlet(FeaturedPortlet, _(u"Featured Products"))
        register_portlet(EmptySpacePortlet, _(u"Empty Space"))
        register_portlet(NotebookPortlet, _(u"Notebook"))
        register_portlet(ProductListPortlet, _(u"Product list"))
        register_portlet(CustomHTMLPortlet, _(u"Custom HTML"))
        register_portlet(AssociatedPagesPortlet, _(u"Associated Pages"))
        register_portlet(FaqPortlet, _(u"Faq"))
        register_portlet(CategoriesCarouselPortlet, _(u"Categories Carousel"))
        register_portlet(SocNetworkPortlet, _(u"SocNetwork"))

post_syncdb.connect(register_lfs_portlets)
