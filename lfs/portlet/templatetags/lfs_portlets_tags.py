# coding: utf-8
from django import template
from django.conf import settings
from django.core.cache import cache
from django.utils.translation import ugettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from portlets.utils import get_portlets, is_blocked, get_registered_portlets
from portlets.models import Slot
from portlets.models import PortletRegistration

from ...core.utils import get_default_shop

from ..models import CartPortlet
from ..models import CategoriesPortlet
from ..models import PagesPortlet
from ..models import RecentProductsPortlet
from ..models import RelatedProductsPortlet
from ..models import TopsellerPortlet


register = template.Library()


# TODO: Make a better reuse of django-portlets portlet slot
@register.inclusion_tag('portlets/portlet_slot.html', takes_context=True)
def lfs_portlet_slot(context, slot_name):
    """Returns the portlets for given slot and instance. If the instance
    implements the ``get_parent_for_portlets`` method the portlets of the
    parent of the instance are also added.
    """
    request = context.get("request")
    user = request.user

    instance = context.get("category") or \
        context.get("product") or \
        context.get("page") or \
        get_default_shop(request)

    cache_key = "%s-lfs-portlet-slot-%s-%s-%s" % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX,
        slot_name, instance.__class__.__name__,
        instance.id)
    temp = cache.get(cache_key)
    if temp is None:
        try:
            slot = Slot.objects.get(name=slot_name)
        except Slot.DoesNotExist:
            return {"portlets": []}

        # Get portlets for given instance
        temp = get_portlets(instance, slot)

        # Get inherited portlets
        try:
            instance.get_parent_for_portlets()
        except AttributeError:
            instance = None

        while instance:
            # If the portlets are blocked no portlets should be added
            if is_blocked(instance, slot):
                break

            # If the instance has no get_parent_for_portlets,
            # there are no portlets
            try:
                instance = instance.get_parent_for_portlets()
            except AttributeError:
                break

            # If there is no parent for portlets, there are no portlets to add
            if instance is None:
                break

            parent_portlets = get_portlets(instance, slot)
            parent_portlets.reverse()

            for p in parent_portlets:
                if p not in temp:
                    if not p.blocked_in_inheritor:
                        temp.insert(0, p)

            cache.set(cache_key, temp)

    rendered_portlets = []
    context['slot_name'] = slot_name
    for portlet in temp:
        try:
            rendered_portlets.append(portlet.render(context))
        except Exception as e:
            if getattr(settings, 'PORTLET_SHOW_ERRORS', False)\
                    and user.is_active and user.is_superuser:
                template = u'''
                <div class="alert alert-danger container" role="alert">
                <span class="sr-only">Error:</span>
                <div style="word-break:break-all">
                %s
                </div>
                </div>
                '''
                rendered_portlets.append(template % (e))
            else:
                rendered_portlets.append('')

    return {"portlets": rendered_portlets}


@register.inclusion_tag('lfs/portlets/portlet.html', takes_context=True)
def insert_portlet(context, portlet_class, slot_name=None, **kwargs):
    """Tag to render any portlet in any position.
    """
    request = context.get('request')
    user = request.user
    portlet_types = get_registered_portlets()
    portlet_type = portlet_types.get(portlet_class)
    if portlet_type:
        try:
            model_class = ContentType.objects.get(model=portlet_class)
        except ContentType.DoesNotExist:
            return {'html': ''}
        model = model_class.model_class()
        portlet = model(**kwargs)

        if slot_name is not None:
            context.update({'slot_name': slot_name})
        try:
            rendered_portlet = portlet.render(context)
        except Exception as e:
            if getattr(settings, 'PORTLET_SHOW_ERRORS', False)\
                    and user.is_active and user.is_superuser:
                template = u'''
                <div class="alert alert-danger container" role="alert">
                <span class="sr-only">Error:</span>
                <div style="word-break:break-all">
                %s
                </div>
                </div>
                '''
                rendered_portlet = template % (e)
            else:
                rendered_portlet = ''
        return {
            "html": rendered_portlet
        }
    else:
        return {'html': ''}


# Inclusion tags to render portlets. This can be used if one wants to display
# portlets without the possibility to manage them via the UI.
@register.inclusion_tag('lfs/portlets/portlet.html', takes_context=True)
def lfs_cart_portlet(context, title=None):
    """Tag to render the cart portlet.
    """
    if title is None:
        title = _(u"Cart")

    portlet = CartPortlet()
    portlet.title = title

    return {
        "html": portlet.render(context)
    }


@register.inclusion_tag('lfs/portlets/portlet.html', takes_context=True)
def lfs_categories_portlet(context, title=None):
    """Tag to render the related products portlet.
    """
    if title is None:
        title = _(u"Categories")

    portlet = CategoriesPortlet()
    portlet.title = title

    return {
        "html": portlet.render(context)
    }


@register.inclusion_tag('lfs/portlets/portlet.html', takes_context=True)
def lfs_pages_portlet(context, title=None):
    """Tag to render the pages portlet.
    """
    if title is None:
        title = _(u"Information")

    portlet = PagesPortlet()
    portlet.title = title

    return {
        "html": portlet.render(context)
    }


@register.inclusion_tag('lfs/portlets/portlet.html', takes_context=True)
def lfs_recent_products_portlet(context, title=None):
    """Tag to render the recent products portlet.
    """
    if title is None:
        title = _(u"Recent Products")

    portlet = RecentProductsPortlet()
    portlet.title = title

    return {
        "html": portlet.render(context)
    }


@register.inclusion_tag('lfs/portlets/portlet.html', takes_context=True)
def lfs_related_products_portlet(context, title=None):
    """Tag to render the related products portlet.
    """
    if title is None:
        title = _(u"Related Products")

    portlet = RelatedProductsPortlet()
    portlet.title = title

    return {
        "html": portlet.render(context)
    }


@register.inclusion_tag('lfs/portlets/portlet.html', takes_context=True)
def lfs_topseller_portlet(context, title=None, limit=5):
    """Tag to render the related products portlet.
    """
    if title is None:
        title = _(u"Topseller")

    portlet = TopsellerPortlet()
    portlet.title = title
    portlet.limit = limit

    return {
        "html": portlet.render(context)
    }


@register.inclusion_tag(
    'admin/portlets_includes/portlets_block.html', takes_context=True)
def admin_portlets_info_block(context):
    content_object = ContentType.objects.get(id=context['content_type_id'])
    object_id = context['object_id']
    # pdb.set_trace()
    object = content_object.get_object_for_this_type(id=object_id)
    portlets_models = PortletRegistration.objects.order_by('name')
    return {
        'content_type': content_object,
        'object': object,
        'portlets_models': portlets_models,
    }


@register.filter
def class_name(portlet):
    pa = PortletRegistration.objects.get(type=portlet._meta.module_name)
    return _(pa.name)
