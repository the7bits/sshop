# coding: utf-8
import json

from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from django.template import RequestContext

from ..caching.utils import lfs_get_object_or_404
from ..catalog.models import Product, ProductPropertyValue

from django.template.loader import render_to_string
from portlets.models import PortletAssignment

from django.utils import simplejson
from django.conf import settings


def add_product_view(request, product_id, portlet_assignment_id=None):
    ''' Add product to comparison
    '''
    try:
        product = lfs_get_object_or_404(Product, pk=product_id)
    except Product.DoesNotExist:
        return HttpResponse(json.dumps('Error'), mimetype="application/json")

    if product.has_variants() and\
            not getattr(settings, 'CATEGORY_HIDE_VARIANTS', False):
        product = product.get_default_variant()

    if request.session.get('comparison', False):
        comparison_list = request.session['comparison']
        if product.id not in comparison_list:
            comparison_list.append(product.id)
            request.session['comparison'] = comparison_list
    else:
        comparison_list = []
        comparison_list.append(product.id)
        request.session['comparison'] = comparison_list

    if portlet_assignment_id is None:
        return HttpResponse(json.dumps('Ok'), mimetype="application/json")
    else:
        return ajax_comparison_render(request, portlet_assignment_id)


def ajax_comparison_render(request, portlet_assignment_id):
    obj = PortletAssignment.objects.get(pk=portlet_assignment_id)

    comparison_list = []
    products = []
    if request.session.get('comparison', False):
        comparison_list = request.session['comparison']

    for product_id in comparison_list:
        try:
            products.append(Product.objects.get(id=product_id))
        except Product.DoesNotExist:
            pass

    html = render_to_string(
        "comparison_list/comparison_list_portlet.html",
        RequestContext(
            request,
            {
                'comparison_list_portlet': obj.portlet,
                'products': products,
                'ajax': True,
                'slot_name': obj.slot.name,
            }
        )
    )
    response = simplejson.dumps({'html': html})
    return HttpResponse(response)


def remove_product_view(request, product_id):
    ''' Remove product from comparison
        (Delete product id from user session)
    '''
    if request.session.get('comparison', False):
        comparison_list = request.session['comparison']
        try:
            comparison_list.remove(int(product_id))
            request.session['comparison'] = comparison_list
        except ValueError:
            pass

    return redirect('/comparison/')


def comparison_view(request, template_name='lfs/comparison/comparison.html'):
    ''' View current select products of comparison
    '''
    from collections import OrderedDict

    properties = OrderedDict()
    if 'comparison' in request.session:
        comparison_list = request.session['comparison']
        products = Product.objects.filter(pk__in=comparison_list)

        t_values = ProductPropertyValue.objects.filter(
            product__in=products,
        ).distinct().select_related().order_by('position')\
            .values(
                'value', 'property__name',
                'property__is_group', 'product_id')
        groups_for_products = OrderedDict()
        for p in products:
            if p.id not in groups_for_products:
                groups_for_products[p.id] = ''

        for v in t_values:
            if v['property__is_group'] and v['property__name'] \
                    not in properties:
                properties[v['property__name']] = OrderedDict()
            if v['property__is_group']:
                groups_for_products[v['product_id']] = v['property__name']

            if not v['property__is_group']:
                last_group = groups_for_products[v['product_id']]
                if last_group not in properties:
                    properties[last_group] = OrderedDict()
                if v['property__name'] not in properties[last_group]:
                    properties[last_group][v['property__name']] = {}
                properties[last_group][v['property__name']][v['product_id']] =\
                    v['value']
    else:
        products = []

    return render_to_response(template_name, RequestContext(request, {
        'comparison_list': products,
        'properties': properties,
    }))


def remove_all_products_view(request):
    if request.session.get('comparison', False):
        request.session['comparison'] = []
    return redirect('/')


def get_products_ajax(request):
    return HttpResponse(simplejson.dumps({
        'products': request.session.get('comparison', [])}))
