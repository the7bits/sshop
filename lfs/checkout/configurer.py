# coding: utf-8
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

from adminconfig.utils import BaseConfig
from lfs.order.models import OrderStatus


class CheckoutConfigForm(forms.Form):
    auth_only = forms.BooleanField(
        label=_(u'Authenticated only checkout'),
        required=False)
    phone_number_region = forms.CharField(
        label=_(u'Default phone number region'),
        help_text=_(
            u'http://www.iso.org/iso/country_codes/'
            u'iso_3166_code_lists/country_names_and_code_elements.htm'))
    phone_help_text = forms.CharField(
        label=_(u'Phone help text'),
        required=False)
    phone_placeholder = forms.CharField(
        label=_(u'Phone placeholder'),
        required=False)
    thank_you_text = forms.CharField(
        label=_(u'Text "thank you"'))
    show_shipping = forms.BooleanField(
        label=_(u'Show shipping select'),
        required=False)
    show_payment = forms.BooleanField(
        label=_(u'Show payment select'),
        required=False)
    show_comment = forms.BooleanField(
        label=_(u'Show comment form'),
        required=False)
    show_voucher = forms.BooleanField(
        label=_(u'Show voucher input'),
        required=False)
    default_shipping_only = forms.BooleanField(
        label=_(u'Default shipping only'),
        required=False)
    default_payment_only = forms.BooleanField(
        label=_(u'Default payment only'),
        required=False)
    require_email = forms.BooleanField(
        label=_(u'Require email'),
        required=False)
    require_phone = forms.BooleanField(
        label=_(u'Require phone'),
        required=False)
    validate_phone = forms.BooleanField(
        label=_(u'Validate phone'),
        required=False)
    default_order_status = forms.ChoiceField(
        label=_(u'Default order status'),
        choices=[],
    )

    def __init__(self, *args, **kwargs):
        super(CheckoutConfigForm, self).__init__(*args, **kwargs)
        self.fields['default_order_status'].choices = [
            (o.id, o) for o in OrderStatus.objects.all()]


class CheckoutConfig(BaseConfig):
    """Configurator for checkout options in config.
    """
    form_class = CheckoutConfigForm
    block_name = 'checkout'

    def __init__(self):
        super(CheckoutConfig, self).__init__()

        self.default_data = {
            'CHECKOUT_AUTH_ONLY': settings.CHECKOUT_AUTH_ONLY,
            'CHECKOUT_PHONE_NUMBER_REGION':
            settings.CHECKOUT_PHONE_NUMBER_REGION,
            'CHECKOUT_PHONE_HELP_TEXT':
            settings.CHECKOUT_PHONE_HELP_TEXT,
            'CHECKOUT_PHONE_PLACEHOLDER':
            settings.CHECKOUT_PHONE_PLACEHOLDER,
            'CHECKOUT_SHOW_SHIPPING': settings.CHECKOUT_SHOW_SHIPPING,
            'CHECKOUT_SHOW_PAYMENT': settings.CHECKOUT_SHOW_PAYMENT,
            'CHECKOUT_SHOW_COMMENT': settings.CHECKOUT_SHOW_COMMENT,
            'CHECKOUT_SHOW_VOUCHER': settings.CHECKOUT_SHOW_VOUCHER,
            'CHECKOUT_USE_DEFAULT_SHIPPING':
            settings.CHECKOUT_USE_DEFAULT_SHIPPING,
            'CHECKOUT_USE_DEFAULT_PAYMENT':
            settings.CHECKOUT_USE_DEFAULT_PAYMENT,
            'CHECKOUT_REQUIRE_EMAIL':
            settings.CHECKOUT_REQUIRE_EMAIL,
            'CHECKOUT_REQUIRE_PHONE':
            settings.CHECKOUT_REQUIRE_PHONE,
            'CHECKOUT_VALIDATE_PHONE':
            settings.CHECKOUT_VALIDATE_PHONE,
            'CHECKOUT_THANK_YOU_TEXT': settings.CHECKOUT_THANK_YOU_TEXT,
            'DEFAULT_ORDER_STATUS': None,
        }

        self.option_translation_table = (
            ('CHECKOUT_AUTH_ONLY', 'auth_only'),
            ('CHECKOUT_PHONE_NUMBER_REGION', 'phone_number_region'),
            ('CHECKOUT_PHONE_HELP_TEXT', 'phone_help_text'),
            ('CHECKOUT_PHONE_PLACEHOLDER', 'phone_placeholder'),
            ('CHECKOUT_SHOW_SHIPPING', 'show_shipping'),
            ('CHECKOUT_SHOW_PAYMENT', 'show_payment'),
            ('CHECKOUT_SHOW_COMMENT', 'show_comment'),
            ('CHECKOUT_SHOW_VOUCHER', 'show_voucher'),
            ('CHECKOUT_USE_DEFAULT_SHIPPING', 'default_shipping_only'),
            ('CHECKOUT_USE_DEFAULT_PAYMENT', 'default_payment_only'),
            ('CHECKOUT_REQUIRE_EMAIL', 'require_email'),
            ('CHECKOUT_REQUIRE_PHONE', 'require_phone'),
            ('CHECKOUT_VALIDATE_PHONE', 'validate_phone'),
            ('CHECKOUT_THANK_YOU_TEXT', 'thank_you_text'),
            ('DEFAULT_ORDER_STATUS', 'default_order_status'),
        )
