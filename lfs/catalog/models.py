# coding: utf-8
import locale
import uuid
import pytils
import logging

from django.db import models
from django.conf import settings
from django.core.cache import cache
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
# from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _

from mptt.models import MPTTModel, TreeForeignKey, TreeManyToManyField
from jsonfield import JSONField

from ..core.fields.thumbs import ImageWithThumbsField
from ..core.managers import ActiveManager
from .settings import CONTENT_CATEGORIES
from .settings import PRODUCT_TYPE_CHOICES
from .settings import STANDARD_PRODUCT
from .settings import VARIANT
from .settings import PRODUCT_WITH_VARIANTS
from .settings import CAT_CATEGORY_PATH
from .settings import CATEGORY_TEMPLATES
from .settings import CONTENT_PRODUCTS
from .settings import DELIVERY_TIME_UNIT_CHOICES
from .settings import DELIVERY_TIME_UNIT_SINGULAR
from .settings import DELIVERY_TIME_UNIT_HOURS
from .settings import DELIVERY_TIME_UNIT_DAYS
from .settings import DELIVERY_TIME_UNIT_WEEKS
from .settings import DELIVERY_TIME_UNIT_MONTHS
from .settings import PRODUCT_TEMPLATES
from .settings import QUANTITY_FIELD_TYPES
from .settings import QUANTITY_FIELD_INTEGER
from .settings import QUANTITY_FIELD_DECIMAL_1
from .settings import THUMBNAIL_SIZES
from .settings import VARIANTS_DISPLAY_TYPE_TEMPLATES
from .settings import (
    PROPERTY_TYPE_CHOICES,
    PROPERTY_TYPE_CHOICE,
    PROPERTY_TYPE_INTEGER,
    PROPERTY_TYPE_FLOAT,
    PROPERTY_TYPE_STRING,
    PROPERTY_TYPE_BOOLEAN,
)
from .settings import DEFAULT_PROPERTY_TYPE_REGEX
from .settings import PROPERTY_TYPE_VALIDATORS
from .utils import get_filtered_products_for_category

from ..manufacturer.models import Manufacturer


logger = logging.getLogger('sshop')


def get_unique_id_str():
    return str(uuid.uuid4())

LFS_UNITS = [[u, u] for u in settings.LFS_UNITS]

LFS_PRICE_UNITS = [[u, u] for u in settings.LFS_PRICE_UNITS]

LFS_BASE_PRICE_UNITS = [[u, u] for u in settings.LFS_BASE_PRICE_UNITS]

SSHOP_IMAGE_SIZES = getattr(
    settings,
    'SSHOP_IMAGE_SIZES',
    dict(
        small=(60, 60),
        medium=(100, 100),
        large=(200, 200),
        huge=(400, 400)
    ),
)


class Category(MPTTModel):
    """A category is used to browse through the shop products. A category can
    have one parent category and several child categories.

    **Attributes:**

    name
        The name of the category.

    slug
        Part of the URL

    parent
        Parent of the category. This is used to create a category tree. If
        it's None the category is a top level category.

    show_all_products
       If True the category displays it's direct products as well as products
       of it's sub categories. If False only direct products will be
       displayed.

    products
        The assigned products of the category.

    short_description
        A short description of the category. This is used in overviews.

    description
        The description of the category. This can be used in details views
        of the category.

    icon
        The category icon

    image
        The image of the category.

    static_block
        A assigned static block to the category.

    active_formats
        If True product_rows, product_cols and category_cols are taken from
        the category otherwise from the parent.

    product_rows, product_cols, category_cols
        Format information for the category views

    meta_title
        Meta title of the category (HTML title)

    meta_keywords
        Meta keywords of the category

    meta_description
       Meta description of the category

    uid
       The unique id of the category

    template
       Sets the template which renders the category view. If left to None,
       default template is used.

    product_count
        Denormalized field for saving the product count in given category

    extras
        Extra json data
    """
    name = models.CharField(_(u"Name"), max_length=300)
    slug = models.SlugField(_(u"Slug"), unique=True, max_length=300)
    parent = TreeForeignKey(
        "self", verbose_name=_(u"Parent"),
        blank=True, null=True, related_name='children')

    show_all_products = models.BooleanField(
        _(u"Show all products"), default=True)

    products = models.ManyToManyField(
        "Product", verbose_name=_(u"Products"),
        blank=True, related_name="categories")
    short_description = models.TextField(_(u"Short description"), blank=True)
    description = models.TextField(_(u"Description"), blank=True)
    icon = models.ImageField(
        _(u"Icon"), upload_to="category-icons", blank=True, null=True)
    image = ImageWithThumbsField(
        _(u"Image"), upload_to="category-images",
        blank=True, null=True, sizes=THUMBNAIL_SIZES)
    exclude_from_navigation = models.BooleanField(
        _(u"Exclude from navigation"), default=False)

    static_block = models.ForeignKey(
        "StaticBlock", verbose_name=_(u"Static block"), blank=True, null=True,
        related_name="categories")
    template = models.PositiveSmallIntegerField(
        _(u"Category template"), max_length=400,
        blank=True, null=True,
        default=CONTENT_PRODUCTS,
        choices=CATEGORY_TEMPLATES)

    active_formats = models.BooleanField(_(u"Active formats"), default=False)
    product_rows = models.IntegerField(_(u"Product rows"), default=3)
    product_cols = models.IntegerField(_(u"Product cols"), default=3)
    category_cols = models.IntegerField(_(u"Category cols"), default=3)

    meta_h1 = models.TextField(
        _(u"H1 header"),
        default=getattr(
            settings, 'DEFAULT_CATEGORY_META_H1_TEMPLATE', '{{name}}'),
    )
    meta_title = models.TextField(
        _(u"Meta title"),
        default=getattr(
            settings, 'DEFAULT_CATEGORY_META_TITLE_TEMPLATE', '{{name}}'),
        help_text=_(
            u'You can use the next variables: {{ name }}, {{ shop_name }}.'))
    meta_keywords = models.TextField(
        _(u"Meta keywords"), blank=True,
        default=getattr(
            settings, 'DEFAULT_CATEGORY_META_KEYWORDS_TEMPLATE', ''),
    )
    meta_description = models.TextField(
        _(u"Meta description"), blank=True,
        default=getattr(
            settings, 'DEFAULT_CATEGORY_META_DESCRIPTION_TEMPLATE', ''),
    )
    seo = models.TextField(
        _(u"SEO text (HTML)"), blank=True,
        default=getattr(settings, 'DEFAULT_CATEGORY_META_TEXT_TEMPLATE', ''),
    )

    uid = models.CharField(
        max_length=50, editable=False, unique=True, default=get_unique_id_str)
    extras = JSONField(default={'sample_data': 'test'}, blank=True)
    product_count = models.IntegerField(default=0, blank=True)

    class Meta:
        verbose_name = _(u'Category')
        verbose_name_plural = _(u'Categories')

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        """
        Overwritten to save seo data.
        """
        if not self.meta_h1:
            self.meta_h1 = getattr(
                settings, 'DEFAULT_CATEGORY_META_H1_TEMPLATE', '')
        if not self.meta_title:
            self.meta_title = getattr(
                settings, 'DEFAULT_CATEGORY_META_TITLE_TEMPLATE', '')
        if not self.meta_keywords:
            self.meta_keywords = getattr(
                settings, 'DEFAULT_CATEGORY_META_KEYWORDS_TEMPLATE', '')
        if not self.meta_description:
            self.meta_description = getattr(
                settings, 'DEFAULT_CATEGORY_META_DESCRIPTION_TEMPLATE', '')
        if not self.seo:
            self.seo = getattr(
                settings, 'DEFAULT_CATEGORY_META_TEXT_TEMPLATE', '')
        super(Category, self).save(*args, **kwargs)

    def recount_products(self, save=False):
        """Init product_count attribute with the number of products
        in given category.
        """
        _product_count = self.products.filter(
            active=True, status__is_visible=True).count()
        childrens = self.get_all_children()
        if childrens:
            children_counts = 0
            for c in childrens:
                children_counts += c.products.filter(
                    active=True, status__is_visible=True).count()
            self.product_count = _product_count + children_counts
        else:
            self.product_count = _product_count
        if save:
            self.save()

    def get_absolute_url(self):
        """
        Returns the absolute_url.
        """
        return ("lfs.catalog.views.category_view", (), {"slug": self.slug})
    get_absolute_url = models.permalink(get_absolute_url)

    @property
    def content_type(self):
        """
        Returns the content type of the category as lower string.
        """
        return u"category"

    def get_all_children(self):
        """
        Returns all child categories of the category.
        """
        cache_key = "%s-category-all-children-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        children = cache.get(cache_key)
        if children is not None:
            return children

        children = self.get_descendants()
        cache.set(cache_key, children)
        return children

    def get_format_info(self):
        """
        Returns format information.
        """
        if self.active_formats:
            return {
                "product_cols": self.product_cols,
                "product_rows": self.product_rows,
                "category_cols": self.category_cols,
            }
        else:
            if self.parent is None:
                from ..core.utils import get_default_shop
                shop = get_default_shop()
                return {
                    "product_cols": shop.product_cols,
                    "product_rows": shop.product_rows,
                    "category_cols": shop.category_cols,
                }
            else:
                return self.parent.get_format_info()

    def _get_data_for_meta_info(self, **kwargs):
        """
        Get common data for meta templates.
        """
        from ..core.utils import get_default_shop
        shop = get_default_shop()
        data = {
            'name': self.name,
            'shop_name': shop.name,
            'category': self,
            'shop': shop,
        }
        data.update(kwargs)
        return data

    def get_meta_h1(self, **kwargs):
        """
        Returns the H1 header of the category.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_h1,
            self._get_data_for_meta_info(**kwargs))

    def get_meta_title(self, **kwargs):
        """
        Returns the meta keywords of the category.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_title,
            self._get_data_for_meta_info(**kwargs))

    def get_meta_keywords(self, **kwargs):
        """
        Returns the meta keywords of the category.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_keywords,
            self._get_data_for_meta_info(**kwargs))

    def get_meta_description(self, **kwargs):
        """
        Returns the meta description of the category.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_description,
            self._get_data_for_meta_info(**kwargs))

    def get_meta_seo_text(self, **kwargs):
        """Returns the seo text of the category"""
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.seo, self._get_data_for_meta_info(**kwargs))

    def get_image(self):
        """
        Returns the image of the category.
        """
        if self.image:
            return self.image
        else:
            if self.parent:
                return self.parent.get_image()
            else:
                return None

    def get_icon(self):
        return self.icon if self.icon else None

    def get_products(self):
        """
        Returns the direct products of the category.
        """
        cache_key = "%s-category-products-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        products = cache.get(cache_key)
        if products is not None:
            return products

        products = self.products.filter(active=True, status__is_visible=True)
        cache.set(cache_key, products)

        return products

    def get_all_products(self):
        """
        Returns the direct products and all products of the sub categories
        """
        cache_key = "%s-category-all-products-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        products = cache.get(cache_key)
        if products is not None:
            return products

        categories = [self]
        categories.extend(self.get_all_children())
        products = Product.objects.distinct().filter(
            active=True,
            status__is_visible=True,
            categories__in=categories).distinct()

        cache.set(cache_key, products)
        return products

    def get_filtered_products(self, filters, sorting):
        """
        Returns products for this category filtered by passed filters
        and sorted by passed sorting.
        """
        return get_filtered_products_for_category(self, filters, sorting)

    def get_static_block(self):
        """
        Returns the static block of the category.
        """
        cache_key = "%s-static-block-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        block = cache.get(cache_key)
        if block is not None:
            return block

        block = self.static_block
        cache.set(cache_key, block)
        return block

    def get_parent_for_portlets(self):
        """
        Returns the parent for portlets.
        """
        from ..core.utils import get_default_shop
        return self.parent or get_default_shop()

    def get_template_name(self):
        """
        Returns the path of the category template.
        """
        if self.template is not None:
            id = int(self.template)
            return CATEGORY_TEMPLATES[id][1]["file"]
        return CATEGORY_TEMPLATES[CONTENT_PRODUCTS][1]["file"]

    def get_content(self):
        """
        Returns the type of content the template is rendering depending on its
        path.
        """
        if self.get_template_name().startswith(CAT_CATEGORY_PATH):
            return CONTENT_CATEGORIES
        return CONTENT_PRODUCTS


class Product(models.Model):
    """
    A product is sold within a shop.

    **Parameters:**

    name
        The name of the product

    slug
        Part of the URL

    sku
        The external unique id of the product

    price
        The gross price of the product

    price_calculator
        Class that implements lfs.price.PriceCalculator for
        calculating product price.

    effective_price:
        Only for internal usage (price filtering).

    unit
        The unit of the product. This is displayed beside the quantity
        field.

    price_unit
        The unit of the product's price. This is displayed beside the price

    short_description
        The short description of the product. This is used within overviews.

    description
        The description of the product. This is used within the detailed view
        of the product.

    images
        The images of the product.

    meta_title
        the meta title of the product (the title of the HTML page).

    meta_keywords
        the meta keywords of the product.

    meta_description
        the meta description of the product.

    related_products
        Related products for this products.

    accessories
        Accessories for this products.

    for_sale
        If True the product is for sale and the for sale price will be
        displayed.

    for_sale_price
        The for sale price for the product. Will be displayed if the product
        is for sale.

    active
        If False the product won't be displayed to shop users.

    creation_date
        The creation date of the product

    deliverable
        If True the product is deliverable. Otherwise not.

    manual_delivery_time
        If True the delivery_time of the product is taken. Otherwise the
        delivery time will be calculate on global delivery times and
        selected shipping method.

    delivery_time
        The delivery time of the product. This is only relevant if
        manual_delivery_time is set to true.

    order_time
        Order time of the product when no product is within the stock. This
        is added to the product's delivery time.

    ordered_at
        The date when the product has been ordered. To calculate the rest of
        the order time since the product has been ordered.

    manage_stock_amount
        If true the stock amount of the product will be decreased when a
        product has been saled.

    weight, height, length, width
        The dimensions of the product relevant for the the stock (IOW the
        dimension of the product's box not the product itself).

    tax
        Tax rate of the product.

    static_block
        A static block which has been assigned to the product.

    sub_type
        Sub type of the product. At the moment that is standard, product with
        variants, variant.

    default_variant
        The default variant of a product with variants. This will be
        displayed at first if the shop customer browses to a product with
        variant.

    variant_category
        The variant of a product with variants which will be displayed within
        the category overview.

    variants_display_type
        This decides howt the variants of a product with variants are
        displayed. This is select box of list.

    parent
        The parent of a variant (only relevant for variants)

    active_xxx
        If set to true the information will be taken from the variant.
        Otherwise from the parent product (only relevant for variants)

    supplier
        The supplier of the product

    template
        Sets the template, which renders the product content. If left
        to None, default template is used.

    active_price_calculation
        If True the price will be calculated by the field price_calculation

    price_calculation
        Formula to calculate price of the product.

    sku_manufacturer
        The product's article ID of the manufacturer (external article id)

    manufacturer
        The manufacturer of the product.

    uid
       The unique id of the product

    type_of_quantity_field
        The type of the quantity field: Integer or Decimal for now.

    id_1c
        1c id

    status
        product status

    guarantee
        The guarantee of the product

    default_value
        The default number of products when buying
    """
    # All products
    name = models.CharField(_(u"Name"), max_length=300, blank=True)
    short_name = models.CharField(
        _(u"Short name"),
        help_text=_(u"Optional short product name."),
        max_length=300, blank=True)
    slug = models.SlugField(
        _(u"Slug"),
        help_text=_(u"The unique last part of the Product's URL."),
        unique=True, max_length=300)
    sku = models.CharField(
        _(u"SKU"), help_text=_(u"Unique article number of the product."),
        blank=True, max_length=100)
    price = models.FloatField(_(u"Price"), default=0.0)
    price_calculator = models.CharField(
        _(u"Price calculator"), null=True, blank=True,
        choices=settings.LFS_PRICE_CALCULATORS,
        max_length=255)
    effective_price = models.FloatField(
        _(u"Price"), default=0.0, blank=True)  # not for manual editing

    price_unit = models.CharField(
        _(u"Price unit"), blank=True,
        max_length=20, choices=LFS_PRICE_UNITS)
    unit = models.CharField(
        _(u"Quanity field unit"), blank=True,
        max_length=20, choices=LFS_UNITS)

    short_description = models.TextField(
        _(u"Short description"), blank=True,
        help_text=_(u'This text using on the category page and product page.'))
    active_short_description = models.BooleanField(
        _(u"Active short description"), default=False)
    description = models.TextField(
        _(u"Description"), blank=True,
        help_text=_(
            u'This text using on the product page at the Description tab.'))
    active_description = models.BooleanField(
        _(u"Active description"), default=False)
    images = generic.GenericRelation(
        "Image", verbose_name=_(u"Images"),
        object_id_field="content_id", content_type_field="content_type")

    # SEO fields
    meta_h1 = models.TextField(
        _(u"H1 header"), blank=True,
        default=getattr(
            settings, 'DEFAULT_PRODUCT_META_H1_TEMPLATE', '{{name}}'),
    )
    meta_title = models.TextField(
        _(u"Meta title"), blank=True,
        default=getattr(
            settings, 'DEFAULT_PRODUCT_META_TITLE_TEMPLATE', '{{name}}'),
        help_text=_(
            u'You can use the next variables: {{ name }}, '
            u'{{ shop_name }}, {{ manufacturer }}, {{ category }}, '
            u'{{ product }}.'))
    meta_keywords = models.TextField(
        _(u"Meta keywords"),
        default=getattr(
            settings, 'DEFAULT_PRODUCT_META_KEYWORDS_TEMPLATE', ''),
        blank=True)
    meta_description = models.TextField(
        _(u"Meta description"),
        default=getattr(
            settings, 'DEFAULT_PRODUCT_META_DESCRIPTION_TEMPLATE', ''),
        blank=True)
    meta_seo_text = models.TextField(
        _(u"Meta SEO text"),
        default=getattr(
            settings, 'DEFAULT_PRODUCT_META_TEXT_TEMPLATE', ''),
        blank=True)

    related_products = models.ManyToManyField(
        "self", verbose_name=_(u"Related products"),
        blank=True, null=True,
        symmetrical=False, related_name="reverse_related_products")

    accessories = models.ManyToManyField(
        "Product", verbose_name=_(u"Acessories"),
        blank=True, null=True,
        symmetrical=False, through="ProductAccessories",
        related_name="reverse_accessories")

    for_sale = models.BooleanField(_(u"For sale"), default=False)
    for_sale_price = models.FloatField(_(u"For sale price"), default=0.0)
    active = models.BooleanField(_(u"Active"), default=False)

    static_block = models.ForeignKey(
        "StaticBlock", verbose_name=_(u"Static block"),
        blank=True, null=True,
        related_name="products",
        help_text=_(
            u'You can use it for example for info '
            u'about shipping or warranty.'))
    active_static_block = models.BooleanField(
        _(u"Active static bock"), default=False,
        help_text=_(
            u'Set this if your want to inherit '
            u'static block from parent product.'))

    sub_type = models.CharField(
        _(u"Subtype"),
        help_text=_(
            u'Use it if you want to create product with variants.'),
        max_length=10, choices=PRODUCT_TYPE_CHOICES, default=STANDARD_PRODUCT)

    variants_display_type = models.IntegerField(
        _(u"Variants display type"),
        default=0, null=True, blank=True,
        help_text=_(u'For product with variants only.'),
        choices=VARIANTS_DISPLAY_TYPE_TEMPLATES)

    # Product Variants
    variant_position = models.IntegerField(
        default=999, blank=True, null=True,
        verbose_name=_(u"Variant position"))
    parent = models.ForeignKey(
        "self", blank=True, null=True,
        verbose_name=_(u"Parent"), related_name="variants")
    template = models.PositiveSmallIntegerField(
        _(u"Product template"), blank=True, null=True,
        default=0,
        help_text=_(
            u'You can use alternative '
            u'template if it is installed.'),
        choices=PRODUCT_TEMPLATES)

    # Manufacturer
    sku_manufacturer = models.CharField(
        _(u"SKU Manufacturer"), blank=True, max_length=100)
    manufacturer = models.ForeignKey(
        Manufacturer, verbose_name=_(u"Manufacturer"),
        blank=True, null=True,
        related_name="products")
    type_of_quantity_field = models.PositiveSmallIntegerField(
        _(u"Type of quantity field"), blank=True, null=True,
        choices=QUANTITY_FIELD_TYPES)

    # uid
    uid = models.CharField(
        _(u'UUID'), max_length=50,
        editable=False, unique=True,
        default=get_unique_id_str)
    id_1c = models.CharField(
        _(u'Code in 1C'), max_length=50,
        default='', blank=True,
        help_text=_(
            u'Internal product\'s code in 1C system (not for all sites).'))

    status = models.ForeignKey(
        'ProductStatus', verbose_name=_(u'Status'),
        blank=True, null=True)

    guarantee = models.IntegerField(
        _(u'Warranty'), default=0)  # TODO: rename to warranty

    default_value = models.IntegerField(
        _(u'Default quantity'), default=1)  # TODO: rename to default_quantity

    #TODO: think about some statistics here
    creation_date = models.DateTimeField(
        _(u'Creation date'), auto_now_add=True)
    last_changes = models.DateTimeField(
        _(u'Creation date'), auto_now=True)

    # Min and max values for variants
    min_variant_price = models.FloatField(
        _(u"Min variant price"), default=0.0)
    max_variant_price = models.FloatField(
        _(u"Max variant price"), default=0.0)
    recommended_position = models.IntegerField(
        _(u'Recommended position'), default=1)

    # Reserved extra fields
    extra_text_1 = models.TextField(
        _(u"Extra text 1"), default='', blank=True)
    extra_text_2 = models.TextField(
        _(u"Extra text 2"), default='', blank=True)
    extra_int_1 = models.IntegerField(
        _(u'Extra int 1'), default=1)
    extra_int_2 = models.IntegerField(
        _(u'Extra int 2'), default=1)
    extra_float_1 = models.FloatField(
        _(u"Extra float 1"), default=0.0)
    extra_float_2 = models.FloatField(
        _(u"Extra float 2"), default=0.0)

    rank_1 = models.FloatField(
        _(u"Rank 1"), default=0.0)
    rank_2 = models.FloatField(
        _(u"Rank 2"), default=0.0)
    rank_3 = models.FloatField(
        _(u"Rank 3"), default=0.0)

    objects = ActiveManager()

    class Meta:
        ordering = ("name", )
        verbose_name = _(u'Product')
        verbose_name_plural = _(u'Products')

    def __unicode__(self):
        return self.name

    def save(self, ignore_parent_save=False, *args, **kwargs):
        """
        Overwritten to save effective_price and seo data.
        """
        if self.for_sale:
            self.effective_price = self.for_sale_price
        else:
            self.effective_price = self.price

        if self.parent is not None:
            self.sub_type = VARIANT

        if self.sub_type == STANDARD_PRODUCT:
            self.min_variant_price = self.effective_price
            self.max_variant_price = self.effective_price
        elif self.sub_type == PRODUCT_WITH_VARIANTS:
            if ignore_parent_save is None or ignore_parent_save is False:
                if not settings.COMPATIBILITY_USE_PARENT_STATUS:
                    max_status = self.get_max_variant_status()
                    if max_status is not None:
                        self.status = max_status
                min_price, max_price = self.get_min_max_variant_prices()
                self.min_variant_price = min_price
                self.max_variant_price = max_price
        elif self.sub_type == VARIANT:
            self.min_variant_price = self.effective_price
            self.max_variant_price = self.effective_price
            if self.parent is not None:
                # Calculate minimum and maximum variant prices.
                # Store to parent when save the variant.
                variants = self.parent.get_variants()
                if len(variants) > 0:
                    min_price, max_price = \
                        self.get_min_max_variant_prices(variants)
                    # Use save instead update for passing the tests
                    # parent_product = Product.objects.filter(
                    #     pk=self.parent_id)
                    # parent_product.update(
                    #     min_variant_price=min_price,
                    #     max_variant_price=max_price)
                    parent = self.parent
                    parent.min_variant_price = min_price
                    parent.max_variant_price = max_price
                    if not settings.COMPATIBILITY_USE_PARENT_STATUS:
                        max_status = parent.get_max_variant_status()
                        if max_status is not None:
                            parent.status = max_status
                    parent.save(ignore_parent_save=True)

        if not self.meta_h1:
            self.meta_h1 = getattr(
                settings, 'DEFAULT_PRODUCT_META_H1_TEMPLATE', '')
        if not self.meta_title:
            self.meta_title = getattr(
                settings, 'DEFAULT_PRODUCT_META_TITLE_TEMPLATE', '')
        if not self.meta_keywords:
            self.meta_keywords = getattr(
                settings, 'DEFAULT_PRODUCT_META_KEYWORDS_TEMPLATE', '')
        if not self.meta_description:
            self.meta_description = getattr(
                settings, 'DEFAULT_PRODUCT_META_DESCRIPTION_TEMPLATE', '')
        if not self.meta_seo_text:
            self.meta_seo_text = getattr(
                settings, 'DEFAULT_PRODUCT_META_TEXT_TEMPLATE', '')
        super(Product, self).save(*args, **kwargs)

    def get_absolute_url(self):
        """
        Returns the absolute url of the product.
        """
        return ("lfs.catalog.views.product_view", (), {"slug": self.slug})

    get_absolute_url = models.permalink(get_absolute_url)

    @property
    def content_type(self):
        """
        Returns the content type of the product as lower string.
        """
        return u"product"

    def get_min_max_variant_prices(self, variants=None):
        from django.db.models import Max, Min
        if variants is None:
            if self.sub_type == VARIANT:
                variants = self.parent.get_variants()
            else:
                variants = self.get_variants()
        variants = variants.exclude(effective_price=0.0)
        if len(variants) > 0:
            min_price = variants.aggregate(
                Min('effective_price'))['effective_price__min']
            if self.sub_type == VARIANT and\
                    0.0 < self.effective_price < min_price:
                min_price = self.effective_price
            max_price = variants.aggregate(
                Max('effective_price'))['effective_price__max']
            if self.sub_type == VARIANT and\
                    0.0 < self.effective_price > max_price:
                max_price = self.effective_price
            return min_price, max_price
        else:
            return 0, 0

    def get_accessories(self):  # TODO: need rewrite
        """
        Returns the ProductAccessories relationship objects - not the accessory
        (Product) objects.

        This is necessary to have also the default quantity of the
        relationship.
        """
        if self.is_variant() and self.accessories.count() == 0:
            product = self.parent
        else:
            product = self

        pas = []
        for pa in ProductAccessories.objects.filter(product=product):
            if pa.accessory.is_active():
                pas.append(pa)

        return pas

    def has_accessories(self):
        """
        Returns True if the product has accessories.
        """
        return len(self.get_accessories()) > 0

    def get_attachments(self):
        """
        Returns the ProductAttachment relationship objects. If no attachments
        are found and the instance is a variant returns the parent's ones.
        """
        attachments = ProductAttachment.objects.filter(product=self)
        if not attachments and self.is_variant():
            attachments = ProductAttachment.objects.filter(product=self.parent)
        return attachments

    def has_attachments(self):
        """
        Returns True if the product has attachments.
        """
        return len(self.get_attachments()) > 0

    def get_categories(self, with_parents=False):  # TODO: need rewrite
        """
        Returns the categories of the product.
        """
        # if categories is not None:
        #     return categories

        if self.is_variant():
            object = self.parent
        else:
            object = self

        if with_parents:
            categories = []
            for category in object.categories.filter(exclude_from_navigation=False)\
                    .order_by('-exclude_from_navigation'):
                while category:
                    categories.append(category)
                    category = category.parent
            categories = categories
        else:
            categories = object.categories.filter(exclude_from_navigation=False)\
                .order_by('-exclude_from_navigation')
        return categories

    def get_category(self):
        """
        Returns the first category of a product.
        """
        if self.is_variant():
            object = self.parent
        else:
            object = self

        try:
            return object.get_categories()[0]
        except IndexError:
            return None

    def get_current_category(self, request):
        """
        Returns product category based on actual categories of the
        given product and the last visited category.

        This is needed if the category has more than one category to display
        breadcrumbs, selected menu points, etc. appropriately.
        """
        last_category = None
        category = None
        product_categories = self.get_categories()
        if len(product_categories) >= 1:
            try:
                if len(product_categories) == 1:
                    category = product_categories[0]
                    return category
                else:
                    last_category = request.session.get("last_category")

                if last_category is None:
                    return product_categories[0]

                category = None
                if last_category in product_categories:
                    category = last_category
                else:
                    children = last_category.get_all_children()
                    for product_category in product_categories:
                        if product_category in children:
                            category = product_category
                            break
                if category is None:
                    category = product_categories[0]
            except IndexError:
                category = None
        request.session["last_category"] = category
        return category

    def get_description(self):
        """
        Returns the description of the product. Takes care whether the product
        is a variant and description is active or not.
        """
        if self.is_variant():
            if self.active_description:
                description = self.description
                # description = description.replace(
                #     "%P",
                #     self.parent.description
                # )
            else:
                description = self.parent.description
        else:
            description = self.description

        return description

    def has_description(self):
        """ Returns True if the product has description.
        """
        return len(self.get_description()) > 0

    def apply_property_set(self, property_set):
        """Apply specified PropertySet for product"""
        p_values = self.property_values.order_by('position')
        ps_rels = property_set.propertysetrelation_set.order_by('position')

        for rel in ps_rels:
            property = rel.property
            position = rel.position

            try:
                pv = p_values.get(property=property)

                pv.position = position
                pv.save(use_validation=False)
            except ProductPropertyValue.DoesNotExist:
                pv = ProductPropertyValue(
                    product=self,
                    property=property,
                    position=position,
                )
                pv.save(use_validation=False)

    def create_property_set_from_product(self, name):
        """Creates the new ProductSet using the properties of
        current product"""
        p_set, created = PropertySet.objects.get_or_create(name=name)
        if created:
            p_values = self.property_values.order_by('position')
            for pv in p_values:
                PropertySetRelation.objects.create(
                    property_set=p_set,
                    property=pv.property,
                    position=pv.position,
                )
        else:
            logger.error(
                _(
                    u'Product "%(product)s" (pk=%(p_pk)d) try to create new '
                    u'PropertySet "%(pset)s" but it is already exists.'
                ) % {
                    'product': self.name,
                    'p_pk': self.pk,
                    'pset': name,
                }
            )
        return p_set

    def get_for_sale(self):
        """
        Returns true if the product is for sale.
        """
        return self.for_sale

    def get_short_description(self):
        """
        Returns the short description of the product. Takes care whether the
        product is a variant and short description is active or not.
        """
        if self.is_variant() and not self.active_short_description:
            return self.parent.short_description
        else:
            return self.short_description

    def get_image(self):
        """
        Returns the first image (the main image) of the product.
        """
        try:
            return self.get_images()[0]
        except IndexError:
            return None

    def get_images(self):
        """
        Returns all images of the product, including the main image.
        """
        cache_key = "%s-product-images-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        images = cache.get(cache_key)

        if images is None:
            if self.is_variant():
                q_images = Image.objects.filter(
                    content_type=ContentType.objects.get_for_model(self),
                    content_id__in=[self.pk, self.parent_id])\
                    .order_by('-product__sub_type', 'position')
                p_images = []
                s_images = []
                for i in q_images:
                    if i.content_id == self.pk:
                        s_images.append(i)
                    elif i.content_id == self.parent_id:
                        p_images.append(i)

                if len(s_images) > 0:
                    return s_images
                else:
                    return p_images
                # TODO: need to think about image positions
                # images = self.images.all() | self.parent.images.all()
                # if self.images.count() > 0:
                #     object = self
                # else:
                #     object = self.parent
            else:
                images = list(self.images.all())
            cache.set(cache_key, images)

        return images

    def get_image_by_type(self, _type):
        """Get url for different types of images.
        _type can be small, medium, large or huge.
        """
        name = 'url_%sx%s' % SSHOP_IMAGE_SIZES[_type]
        _image = self.get_image()
        if _image:
            url = getattr(_image.image, name)
        else:
            url = ''
        return url

    def small_image_url(self):
        """Get small image url."""
        return self.get_image_by_type('small')

    def medium_image_url(self):
        """Get medium image url."""
        return self.get_image_by_type('medium')

    def large_image_url(self):
        """Get large image url."""
        return self.get_image_by_type('large')

    def huge_image_url(self):
        """Get huge image url."""
        return self.get_image_by_type('huge')

    def get_sub_images(self):
        """
        Returns all images of the product, except the main image.
        """
        return self.get_images()[1:]

    def get_manufacturer(self):
        if self.is_variant() and not self.manufacturer:
            return self.parent.manufacturer
        else:
            return self.manufacturer

    def _get_data_for_meta_info(self, **kwargs):
        """
        Get common data for meta templates.
        """
        from ..core.utils import get_default_shop
        shop = get_default_shop()
        data = {
            'name': self.get_name(),
            'shop_name': shop.name,
            'category': self.get_category(),
            'shop': shop,
            'manufacturer': self.get_manufacturer(),
            'product': self,
        }
        data.update(kwargs)
        return data

    def get_meta_h1(self, **kwargs):
        """
        Returns the H1 header of the product.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_h1, self._get_data_for_meta_info(**kwargs))

    def get_meta_title(self, **kwargs):
        """
        Returns the meta title of the product.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_title, self._get_data_for_meta_info(**kwargs))

    def get_meta_keywords(self, **kwargs):
        """
        Returns the meta keywords of the product.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_keywords, self._get_data_for_meta_info(**kwargs))

    def get_meta_description(self, **kwargs):
        """
        Returns the meta seo text of the product.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_description, self._get_data_for_meta_info(**kwargs))

    def get_meta_seo_text(self, **kwargs):
        """
        Returns the seo text of the product. Takes care whether the
        product is a variant and meta description are active or not.
        """
        from ..core.utils import render_meta_info
        return render_meta_info(
            self.meta_seo_text, self._get_data_for_meta_info(**kwargs))

    def get_name(self):
        """
        Returns the name of the product. Takes care whether the product is a
        variant and name is active or not.
        """
        if self.is_variant():
            if self.name:
                name = self.name
                # name = name.replace("%P", self.parent.name)
            else:
                name = self.parent.name
        else:
            name = self.name

        return name

    def get_price_calculator(self, request):
        """
        Returns the price calculator class as defined in LFS_PRICE_CALCULATORS
        in settings.
        """
        from ..core.utils import get_default_shop, import_symbol

        if self.is_variant():
            obj = self.parent
        else:
            obj = self

        if obj.price_calculator is not None:
            price_calculator = obj.price_calculator
        else:
            price_calculator = get_default_shop(request).price_calculator

        price_calculator_class = import_symbol(price_calculator)
        return price_calculator_class(request, self)

    def get_price(self, request=None, use_price_calculator=False):
        """
        See lfs.plugins.PriceCalculator
        """
        if use_price_calculator:
            pc = self.get_price_calculator(request)
            return pc.get_price()
        else:
            return self.effective_price

    def get_standard_price(self, request=None, use_price_calculator=False):
        """
        See lfs.plugins.PriceCalculator
        """
        if use_price_calculator:
            pc = self.get_price_calculator(request)
            return pc.get_standard_price()
        else:
            return self.price

    def get_for_sale_price(self, request=None, use_price_calculator=False):
        """
        See lfs.plugins.PriceCalculator
        """
        if use_price_calculator:
            pc = self.get_price_calculator(request)
            return pc.get_for_sale_price()
        else:
            return self.for_sale_price

    def get_price_unit(self):
        """
        Returns the price_unit of the product. Takes care whether
        the product is a variant or not.
        """
        if self.is_variant():
            return self.parent.price_unit
        else:
            return self.price_unit

    def get_unit(self):
        """
        Returns the unit of the product. Takes care whether the product is a
        variant or not.
        """
        if self.is_variant():
            return self.parent.unit
        else:
            return self.unit

    def get_properties_groups(self):
        """
        Returns list of all property groups for the
        product (ordering by `position`)
        """
        all_properties = self.get_properties()
        groups = []

        for i in all_properties:
            if i.is_group:
                groups.append(i)

        return groups

    def get_properties_for_group(self, group):
        """
        Returns list of all properties from `group` for the
        product (ordering by `position`)
        """
        if group not in self.get_properties_groups():
            return []

        properties = []
        all_properties = list(self.get_properties())
        start = all_properties.index(group)
        for i in range(start+1, len(all_properties)):
            if not all_properties[i].is_group:
                properties.append(all_properties[i])
            else:
                break

        return properties

    def get_property_values(self):
        """Return property values for current product."""
        property_values = self.property_values.order_by('position')
        return property_values

    def get_properties(self):
        """
        Returns list of all properties (with groups) of the product.
        """
        property_values = self.get_property_values()
        properties = []
        for pv in property_values:
            if pv not in properties:
                properties.append(pv.property)
        return properties

    def get_property_value(self, property):
        """
        Return value of property for the product
        """
        try:
            pv = ProductPropertyValue.objects.get(
                product=self, property=property)
            return pv
        except ProductPropertyValue.DoesNotExist:
            return None

    def get_group_of_property(self, property):
        """
        Return group of property or None, if property without group
        """
        if property.is_group:
            return None

        all_properties = list(self.get_properties())
        try:
            property_index = all_properties.index(property)
        except ValueError:
            return None

        for i in range(property_index-1, -1, -1):
            if all_properties[i].is_group:
                return all_properties[i]

        return None

    def get_sku(self):
        """
        Returns the sku of the product. Takes care whether the product is a
        variant and sku is active or not.
        """
        if self.is_variant():
            if not self.sku:
                return self.parent.sku
            else:
                return self.sku
        else:
            return self.sku

    def has_related_products(self):
        """
        Returns True if the product has related products.
        """
        return self.get_related_products().count() > 0

    def get_related_products(self):
        """
        Returns the related products of the product.
        """
        cache_key = "%s-related-products-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        related_products = cache.get(cache_key)

        if related_products is None:
            related_products = self.related_products.all()
            if self.is_variant() and related_products.count() == 0:
                related_products = self.parent.related_products.all()
            cache.set(cache_key, related_products)

        return related_products

    def get_cheapest_variant(self, request, use_price_calculator=False):
        """
        Returns the cheapest variant by price.
        """
        cheapest_variant = None
        min_price = None
        for variant in self.get_variants():
            price = variant.get_price(request, use_price_calculator)
            if price == 0:
                continue
            if (min_price is None) or (price < min_price):
                cheapest_variant = variant
                min_price = price

        return cheapest_variant

    def get_cheapest_for_sale_price(self, request, use_price_calculator=False):
        """
        Returns the min price as dict.
        """
        # if self.is_variant():
        #     product = self.parent
        # else:
        #     product = self
        prices = []
        for variant in self.get_variants():
            price = variant.get_for_sale_price(request, use_price_calculator)
            if price not in prices:
                prices.append(price)

        return {
            "price": min(prices) if prices else 0,
            "starting_from": len(prices) > 1,
        }

    def get_cheapest_standard_price(self, request, use_price_calculator=False):
        """
        Returns the min price as dict.
        """
        prices = []
        for variant in self.get_variants():
            price = variant.get_standard_price(request, use_price_calculator)
            if price not in prices:
                prices.append(price)

        return {
            "price": min(prices) if prices else 0,
            "starting_from": len(prices) > 1,
        }

    def get_most_expensive_standard_price(
            self, request, use_price_calculator=False):
        """
        Returns the max price as dict.
        """
        prices = []
        for variant in self.get_variants():
            price = variant.get_standard_price(request, use_price_calculator)
            if price not in prices:
                prices.append(price)

        return {
            "price": max(prices) if prices else 0,
            "rising_to": len(prices) > 1,
        }

    def get_cheapest_price(self, request, use_price_calculator=False):
        """
        Returns the min price as dict.
        """
        if use_price_calculator:
            prices = []
            for variant in self.get_variants():
                price = variant.get_price(request)
                if price not in prices:
                    prices.append(price)

            return {
                "price": min(prices) if prices else 0,
                "starting_from": len(prices) > 1,
            }
        else:
            # Use extreme optimization with ignoring the price calculation
            variants = self.get_variants()
            if variants:
                return {
                    'price': self.min_variant_price,
                    'starting_from': variants.count() > 1,
                }
            else:
                return {
                    'price': 0,
                    'starting_from': False,
                }

    def get_most_expensive_price(self, request, use_price_calculator=False):
        """
        Returns the max price as dict.
        """
        if use_price_calculator:
            prices = []
            for variant in self.get_variants():
                price = variant.get_price(request)
                if price not in prices:
                    prices.append(price)

            return {
                "price": max(prices) if prices else 0,
                "rising_to": len(prices) > 1,
            }
        else:
            # Use extreme optimization with ignoring the price calculation
            variants = self.get_variants()
            if variants:
                return {
                    'price': self.max_variant_price,
                    'rising_to': variants.count() > 1,
                }
            else:
                return {
                    'price': 0,
                    'rising_to': False,
                }

    def get_static_block(self):
        """
        Returns the static block of the product. Takes care whether the product
        is a variant and meta description are active or not.
        """
        cache_key = "%s-product-static-block-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX, self.id)
        block = cache.get(cache_key)
        if block is not None:
            return block

        if self.is_variant() and not self.active_static_block:
            block = self.parent.static_block
        else:
            block = self.static_block

        cache.set(cache_key, block)

        return block

    def get_variants(self):
        """
        Returns the variants of the product.
        """
        return self.variants.filter(
            active=True,
            status__is_visible=True).order_by("variant_position")

    def get_default_variant(self):
        """
        Return the default (first) variant of the product
        """
        try:
            return self.get_variants()[0]
        except IndexError:
            return None

    def has_variants(self):
        """
        Returns True if the product has variants.
        """
        if self.is_product_with_variants():
            return self.get_variants().count() > 0
        else:
            return False

    def is_standard(self):
        """
        Returns True if product is standard product.
        """
        return self.sub_type == STANDARD_PRODUCT

    def is_product_with_variants(self):
        """
        Returns True if product is product with variants.
        """
        return self.sub_type == PRODUCT_WITH_VARIANTS

    def is_variant(self):
        """
        Returns True if product is variant.
        """
        return self.sub_type == VARIANT

    def is_active(self):
        """
        Returns the activity state of the product.
        """
        return self.active

    def get_clean_quantity(self, quantity=1):
        """
        Returns the correct formatted quantity based on the product's type of
        quantity field.
        """
        try:
            quantity = float(quantity)
        except (TypeError, ValueError):
            quantity = 1.0

        type_of_quantity_field = self.get_type_of_quantity_field()
        if type_of_quantity_field == QUANTITY_FIELD_INTEGER:
            quantity = int(quantity)
        elif type_of_quantity_field == QUANTITY_FIELD_DECIMAL_1:
            quantity = locale.format("%.1f", quantity)
        else:
            quantity = locale.format("%.2f", quantity)

        return quantity

    def get_type_of_quantity_field(self):
        """
        Returns the type of quantity field.
        """
        if self.is_variant():
            obj = self.parent
        else:
            obj = self

        if obj.type_of_quantity_field:
            return obj.type_of_quantity_field
        else:
            return QUANTITY_FIELD_INTEGER

    # 3rd party contracts
    def get_parent_for_portlets(self):
        """
        Returns the current category. This will add the portlets of the current
        category to the product portlets.
        """
        if self.is_variant():
            return self.parent
        else:
            # TODO Return the current category
            try:
                return self.categories.all()[0]
            except:
                from ..core.utils import get_default_shop
                return get_default_shop()

    def get_template_name(self):
        """
        Method to return the path of the product template
        """
        if self.template is not None:
            id = int(self.template)
            return PRODUCT_TEMPLATES[id][1]["file"]
        return PRODUCT_TEMPLATES[0][1]["file"]

    def get_product_lists(self):
        from ..marketing.models import ProductList
        return ProductList.objects.filter(
            productlistitem__product=self).order_by('-position').distinct()

    def get_id(self):
        if settings.SSHOP_USE_1C:
            return self.id_1c
        else:
            return self.id

    def get_max_variant_status(self):
        variants = self.get_variants().order_by('status__order')
        if variants:
            return variants[0].status
        else:
            return None

    def get_min_variant_status(self):
        variants = self.get_variants().order_by('-status__order')
        if variants:
            return variants[0].status
        else:
            return None


class ProductAccessories(models.Model):
    """
    Represents the relationship between products and accessories.

    An accessory is just another product which is displayed
    within a product and which can be added to the cart
    together with it.

    Using an explicit class here to store the position of an accessory within
    a product.

    **Attributes:**

    product
        The product of the relationship.

    accessory
        The accessory of the relationship (which is also a product)

    position
        The position of the accessory within the product.

    quantity
        The proposed amount of accessories for the product.
    """
    product = models.ForeignKey(
        "Product",
        verbose_name=_(u"Product"),
        related_name="productaccessories_product")
    accessory = models.ForeignKey(
        "Product",
        verbose_name=_(u"Accessory"),
        related_name="productaccessories_accessory")
    position = models.IntegerField(_(u"Position"), default=999)
    quantity = models.FloatField(_(u"Quantity"), default=1)

    class Meta:
        ordering = ("position", )
        verbose_name = _(u'Product accessory')
        verbose_name_plural = _(u'Product accessories')

    def __unicode__(self):
        return "%s -> %s" % (self.product.name, self.accessory.name)

    def get_price(self, request):
        """
        Returns the total price of the accessory based on the product price and
        the quantity in which the accessory is offered.
        """
        return self.accessory.get_price(request) * self.quantity


class Property(models.Model):
    """
    Represents a property of a product like color or size.

    **Parameters**:

    name:
        Name of the property.

    identificator:
        Unique identificator for internal use.

    uid
        Unique UID of property.

    is_group
        True for property groups.
    """
    name = models.CharField(_(u'Name'), max_length=300)
    identificator = models.CharField(
        _(u'Identificator'), max_length=300, unique=True,
        help_text=_(
            u'If you leave this field blank it will '
            u'be created automatically.'))
    uid = models.CharField(
        _(u'UID'), max_length=50,
        editable=False, unique=True, default=get_unique_id_str)
    is_group = models.BooleanField(
        _(u'Is group'), default=False,
        help_text=_(u'If checked this property will be rendered as group.'))

    class Meta:
        unique_together = ('name', 'is_group')
        verbose_name = _(u"Property")
        verbose_name_plural = _(u"Properties")
        ordering = ['-is_group', 'name']

    def __unicode__(self):
        if self.is_group:
            return _(u'%s [Group]') % self.name
        else:
            return self.name

    def save(self, *args, **kwargs):
        if self.identificator == '':
            self.identificator = pytils.translit.slugify(self.name)
            if not self.identificator:
                self.identificator = 'empty'
        iden = self.identificator
        count = 1
        while (
            Property.objects.filter(identificator=iden)
            .exclude(pk=self.pk)
            .exclude(identificator=u'').exists()
        ):
            iden = '%s_%s' % (self.identificator, count)
            count += 1

        if self.identificator != iden:
            self.identificator = iden
        super(Property, self).save(*args, **kwargs)


class PropertyOption(models.Model):
    """
    Represents a choosable option of a ``Property`` like red, green, blue.

    **Attributes:**

    property:
        The property to which the option belongs

    identificator:
        Unique identificator for internal use.
    """
    name = models.CharField(_(u'Name'), max_length=255, unique=True)
    identifier = models.CharField(
        _(u'Identifier'), max_length=255, unique=True)

    class Meta:
        ordering = ["name"]
        verbose_name = _(u'Property option')
        verbose_name_plural = _(u'Property options')

    def __unicode__(self):
        return self.name


class ProductPropertyValue(models.Model):
    """
    Stores the property value for product.

    **Attributes:**

    product
        The product for which the value is stored.

    property
        The property for which the value is stored.

    value
        The value for the product/property pair.

    value_as_float
        The numeric representation of the value if type is specified.

    position
        Position of the property in list for current product.

    """
    product = models.ForeignKey(
        Product,
        verbose_name=_(u'Product'),
        related_name='property_values')
    property = models.ForeignKey(
        'Property',
        verbose_name=_(u'Property'),
        related_name='property_values')
    value = models.TextField(_(u'Value'), blank=True)
    value_as_float = models.FloatField(
        _(u'Value as float'), blank=True, null=True)
    position = models.PositiveSmallIntegerField(_(u'Position'), default=999)

    class Meta:
        unique_together = ("product", "property")
        verbose_name = _(u'Property value')
        verbose_name_plural = _(u'Property values')
        ordering = ['position']

    def __unicode__(self):
        return "%s/%s: %s" % (
            self.product.name,
            self.property.name,
            self.value)

    # TODO: is there sence to reimplement this method with background tasks
    def save(self, use_validation=True, *args, **kwargs):
        """
        Automatically synchronize value and value_as_float if needed.
        """
        if use_validation is True:
            self.fill_float()
        super(ProductPropertyValue, self).save(*args, **kwargs)

    def get_type(self):
        p_type = None
        if settings.SMART_DEFINE_PROPERTY_TYPE:
            try:
                p_type = PropertyType.objects.filter(
                    categories__in=self.product.categories.all(),
                    property=self.property,
                ).distinct().annotate(
                    num_categories=models.Count('categories')
                ).order_by('num_categories')[0]
            except:
                pass
        else:
            try:
                p_type = PropertyType.objects.get(
                    categories__in=self.product.categories.all(),
                    property=self.property,
                )
            except PropertyType.DoesNotExist:
                pass
            except PropertyType.MultipleObjectsReturned:
                pass

        return p_type

    def fill_float(self):
        p_type = self.get_type()
        if p_type is None:
            self.value_as_float = 0
            return False

        self.value_as_float = p_type.convert_to_number(self)


class Image(models.Model):
    """
    An image with a title and several sizes. Can be part of a product or
    category.

    **Attributes:**

    content
        The content object it belongs to.

    title
        The title of the image.

    image
        The image file.

    position
        The position of the image within the content object it belongs to.

    """
    content_type = models.ForeignKey(
        ContentType, verbose_name=_(u"Content type"),
        related_name="image", blank=True, null=True)
    content_id = models.PositiveIntegerField(
        _(u"Content id"), blank=True, null=True)
    content = generic.GenericForeignKey(
        ct_field="content_type", fk_field="content_id")

    title = models.CharField(_(u"Title"), blank=True, max_length=100)
    image = ImageWithThumbsField(
        _(u"Image"),
        upload_to="images",
        blank=True,
        null=True,
        sizes=THUMBNAIL_SIZES,
        watermark=True,
        max_length=300)
    position = models.PositiveSmallIntegerField(_(u"Position"), default=999)

    class Meta:
        verbose_name = _(u'Image')
        verbose_name_plural = _(u'Images')
        ordering = ("position", )

    def __unicode__(self):
        return self.title


class StaticBlock(models.Model):
    """
    A block of static HTML which can be assigned to content objects.

    **Attributes**:

    name
        The name of the static block.

    html
        The static HTML of the block.
    """
    name = models.CharField(_(u"Name"), max_length=30)
    html = models.TextField(_(u"HTML"), blank=True)

    class Meta:
        verbose_name = _(u'Static block')
        verbose_name_plural = _(u'Static blocks')
        ordering = ("name", )

    def __unicode__(self):
        return self.name


class DeliveryTime(models.Model):
    """
    Selectable delivery times.

    **Attributes:**

    min
        The minimal lasting of the delivery date.

    max
        The maximal lasting of the delivery date.

    unit
        The unit of the delivery date, e.g. days, months.

    description
        A short description for internal uses.

    """
    min = models.FloatField(_(u"Min"))
    max = models.FloatField(_(u"Max"))
    unit = models.PositiveSmallIntegerField(
        _(u"Unit"), choices=DELIVERY_TIME_UNIT_CHOICES,
        default=DELIVERY_TIME_UNIT_DAYS)
    description = models.TextField(_(u"Description"), blank=True)

    class Meta:
        verbose_name = _(u'Delivery time')
        verbose_name_plural = _(u'Delivery times')
        ordering = ("min", )

    def __unicode__(self):
        return self.round().as_string()

    def __gt__(self, other):
        if self.max > other.max:
            return True
        return False

    def __add__(self, other):
        """
        Adds to delivery times.
        """
        # If necessary we transform both delivery times
        # to the same base (hours)
        if self.unit != other.unit:
            a = self.as_hours()
            b = other.as_hours()
            unit_new = DELIVERY_TIME_UNIT_HOURS
        else:
            a = self
            b = other
            unit_new = self.unit

        # Now we can add both
        min_new = a.min + b.min
        max_new = a.max + b.max
        unit_new = a.unit

        return DeliveryTime(min=min_new, max=max_new, unit=unit_new)

    @property
    def name(self):
        """
        Returns the name of the delivery time
        """
        return self.round().as_string()

    def subtract_days(self, days):
        """
        Substract the given days from delivery time's min and max. Takes the
        unit into account.
        """
        if self.unit == DELIVERY_TIME_UNIT_HOURS:
            max_new = self.max - (24 * days)
            min_new = self.min - (24 * days)
        elif self.unit == DELIVERY_TIME_UNIT_DAYS:
            max_new = self.max - days
            min_new = self.min - days
        elif self.unit == DELIVERY_TIME_UNIT_WEEKS:
            max_new = self.max - (days / 7.0)
            min_new = self.min - (days / 7.0)
        elif self.unit == DELIVERY_TIME_UNIT_MONTHS:
            max_new = self.max - (days / 30.0)
            min_new = self.min - (days / 30.0)

        if min_new < 0:
            min_new = 0
        if max_new < 0:
            max_new = 0

        return DeliveryTime(min=min_new, max=max_new, unit=self.unit)

    def as_hours(self):
        """
        Returns the delivery time in hours.
        """
        if self.unit == DELIVERY_TIME_UNIT_HOURS:
            max = self.max
            min = self.min
        elif self.unit == DELIVERY_TIME_UNIT_DAYS:
            max = self.max * 24
            min = self.min * 24
        elif self.unit == DELIVERY_TIME_UNIT_WEEKS:
            max = self.max * 24 * 7
            min = self.min * 24 * 7
        elif self.unit == DELIVERY_TIME_UNIT_MONTHS:
            max = self.max * 24 * 30
            min = self.min * 24 * 30

        return DeliveryTime(min=min, max=max, unit=DELIVERY_TIME_UNIT_HOURS)

    def as_days(self):
        """
        Returns the delivery time in days.
        """
        if self.unit == DELIVERY_TIME_UNIT_HOURS:
            min = self.min / 24
            max = self.max / 24
        elif self.unit == DELIVERY_TIME_UNIT_DAYS:
            max = self.max
            min = self.min
        elif self.unit == DELIVERY_TIME_UNIT_WEEKS:
            max = self.max * 7
            min = self.min * 7
        elif self.unit == DELIVERY_TIME_UNIT_MONTHS:
            max = self.max * 30
            min = self.min * 30

        return DeliveryTime(min=min, max=max, unit=DELIVERY_TIME_UNIT_DAYS)

    def as_weeks(self):
        """
        Returns the delivery time in weeks.
        """
        if self.unit == DELIVERY_TIME_UNIT_HOURS:
            min = self.min / (24 * 7)
            max = self.max / (24 * 7)
        elif self.unit == DELIVERY_TIME_UNIT_DAYS:
            max = self.max / 7
            min = self.min / 7
        elif self.unit == DELIVERY_TIME_UNIT_WEEKS:
            max = self.max
            min = self.min
        elif self.unit == DELIVERY_TIME_UNIT_MONTHS:
            max = self.max * 4
            min = self.min * 4

        return DeliveryTime(min=min, max=max, unit=DELIVERY_TIME_UNIT_WEEKS)

    def as_months(self):
        """
        Returns the delivery time in months.
        """
        if self.unit == DELIVERY_TIME_UNIT_HOURS:
            min = self.min / (24 * 30)
            max = self.max / (24 * 30)
        elif self.unit == DELIVERY_TIME_UNIT_DAYS:
            max = self.max / 30
            min = self.min / 30
        elif self.unit == DELIVERY_TIME_UNIT_WEEKS:
            max = self.max / 4
            min = self.min / 4
        elif self.unit == DELIVERY_TIME_UNIT_MONTHS:
            max = self.max
            min = self.min

        return DeliveryTime(min=min, max=max, unit=DELIVERY_TIME_UNIT_MONTHS)

    def as_reasonable_unit(self):
        """
        Returns the delivery time as reasonable unit based on the max hours.

        This is used to show the delivery time to the shop customer.
        """
        delivery_time = self.as_hours()

        if delivery_time.max > 1440:               # > 2 months
            return delivery_time.as_months()
        elif delivery_time.max > 168:              # > 1 week
            return delivery_time.as_weeks()
        elif delivery_time.max > 48:               # > 2 days
            return delivery_time.as_days()
        else:
            return delivery_time

    def as_string(self):
        """
        Returns the delivery time as string.
        """
        if self.min == 0:
            self.min = self.max

        if self.min == self.max:
            if self.min == 1:
                unit = DELIVERY_TIME_UNIT_SINGULAR[self.unit]
            else:
                unit = dict(DELIVERY_TIME_UNIT_CHOICES)[self.unit]
            return u"%s %s" % (self.min, unit)
        else:
            return u"%d-%d %s" % (
                self.min,
                self.max,
                dict(DELIVERY_TIME_UNIT_CHOICES)[self.unit])

    def round(self):
        """
        Rounds the min/max of the delivery time to an integer and returns a new
        DeliveryTime object.
        """
        min = int(u"%.0f" % (self.min + 0.001))
        max = int(u"%.0f" % (self.max + 0.001))
        return DeliveryTime(min=min, max=max, unit=self.unit)


class ProductAttachment(models.Model):
    """
    Represents a downloadable attachment of a product.

    **Attributes:**

    title
        The title of the attachment

    description
        The description of the attachment

    file
        The downloadable file of the attachment

    product
        The product the attachment belongs to

    position
        The position of the attachment within a product.
    """
    title = models.CharField(_(u"Title"), max_length=50)
    description = models.TextField(_(u"Description"), blank=True)
    file = models.FileField(upload_to="files")
    product = models.ForeignKey(
        Product,
        verbose_name=_(u"Product"),
        related_name="attachments")
    position = models.IntegerField(_(u"Position"), default=1)

    class Meta:
        verbose_name = _(u'Product attachment')
        verbose_name_plural = _(u'Product attachment')
        ordering = ("position", )

    def get_url(self):
        if self.file.url:
            return self.file.url
        return None


class ProductStatus(models.Model):
    """
    Current status of the product in shop.

    name
        Status name.

    css_class
        Optional CSS class for label.

    description
        Status description.

    order
        Ordering of the status in the list. Using for product sorting.

    show_buy_button
        If True product is available for adding to cart.

    show_ask_button
        If True special Ask button is shown.

    is_visible
        If True product is visible on the site.

    is_searchable
        If True product is searchable.

    delivery_time
        Optional argument for delivery time.
    """
    name = models.CharField(_(u"Name"), max_length=70)
    css_class = models.CharField(_(u"Css class"), max_length=70, blank=True)
    description = models.TextField(_(u"Description"), blank=True)
    order = models.IntegerField(default=10)
    show_buy_button = models.BooleanField(_(u"Show buy button"), default=True)
    show_ask_button = models.BooleanField(_(u"Show ask button"), default=True)
    is_visible = models.BooleanField(_(u"Is visible"), default=True)
    is_searchable = models.BooleanField(_(u"Is searchable"), default=True)
    delivery_time = models.ForeignKey(
        DeliveryTime,
        verbose_name=_(u'Delivery time'),
        null=True, blank=True)

    class Meta:
        verbose_name = _(u'Product status')
        verbose_name_plural = _(u'Product statuses')

    def __unicode__(self):
        return self.name


class ProductImage(models.Model):
    """For internal use. It is an accumulator of external
    links for product images.
    """
    product = models.ForeignKey(Product, verbose_name=_(u"Product"))
    links = models.TextField(_(u"Links"), blank=True)

    class Meta:
        verbose_name = _(u'External image')
        verbose_name_plural = _(u'External images')

    def __unicode__(self):
        return self.product.name


class SortType(models.Model):
    """Sort type for categories and product lists.

    name
        Sort type name.

    order
        Sort type position in dropdown list.

    sortable_fields
        The comma separated list of fields.
    """
    name = models.CharField(_(u"Name"), max_length=70)
    order = models.IntegerField(default=0)
    sortable_fields = models.TextField(_(u"Sortable fields"), default='name')

    class Meta:
        verbose_name = _(u'Sort type')
        verbose_name_plural = _(u'Sort types')

    def __unicode__(self):
        return self.name


class PropertyValueIcon(models.Model):
    """Icon for representation of the property values.

    category
        Category with products.

    properties
        Properties for parsing.

    title
        Text for tooltip and alt tag for icon.

    position
        Ordering of the icons.

    icon
        Icon image.

    products
        This field is initialized when parsing is comlete.

    expression
        Regex for string values or list of comparisons
        for numerical ones.
    """
    category = TreeForeignKey(Category, verbose_name=_(u'Category'))
    properties = models.ManyToManyField(
        Property, verbose_name=_(u'Properties'))
    title = models.CharField(
        max_length=255, default='',
        blank=True, verbose_name=_(u'Icon title'))
    position = models.SmallIntegerField(
        default=999, verbose_name=_(u'Position'))
    icon = models.ImageField(
        verbose_name=_(u'Icon'), upload_to='icons')
    products = models.ManyToManyField(
        Product, blank=True, verbose_name=_(u'Products'))
    expression = models.TextField(verbose_name=_(u'Expression for parsing'))

    class Meta:
        verbose_name = _(u'Property value icon')
        verbose_name_plural = _(u'Property value icons')

    def is_number_property(self):  # TODO: review this later
        p_count = self.properties.count()
        if p_count == 1:
            p = self.properties.all()[0]
            try:
                p_type = PropertyType.objects.get(
                    categories=self.category, property=p)
            except PropertyType.DoesNotExist:
                return False
            except PropertyType.MultipleObjectsReturned:
                logger.error(
                    u'Property %s (pk=%d) for category %s (pk=%d) has '
                    u'more than one specified types.' % (
                        p.name,
                        p.id,
                        self.category,
                        self.category_id,
                    ))
                return False
            return p_type.is_number()
        else:
            pass
        return False

    def parse(self):
        if self.is_number_property():
            command_lines = filter(
                bool,
                map(lambda x: x.strip(), self.expression.split('\n')))
            p_values = ProductPropertyValue.objects.filter(
                property__in=self.properties.all())
            for cl in command_lines:
                t = cl.split()
                if t[0] == '<':
                    p_values = p_values.filter(value_as_float__lt=float(t[1]))
                elif t[0] == '<=':
                    p_values = p_values.filter(value_as_float__lte=float(t[1]))
                elif t[0] == '>':
                    p_values = p_values.filter(value_as_float__gt=float(t[1]))
                elif t[0] == '>=':
                    p_values = p_values.filter(value_as_float__gte=float(t[1]))
                elif t[0] == '==':
                    p_values = p_values.filter(value_as_float=float(t[1]))
                elif t[0] == '!=':
                    p_values = p_values.exclude(value_as_float=float(t[1]))
            products = Product.objects.filter(
                categories=self.category,
                property_values__in=p_values,
                active=True,
                status__is_visible=True,
            )
            self.products = products
        else:
            p_values = ProductPropertyValue.objects.filter(
                property__in=self.properties.all(),
                value__iregex=self.expression.strip(),  # TODO: need fix
            )
            products = Product.objects.filter(
                categories=self.category,
                property_values__in=p_values,
                active=True,
                status__is_visible=True,
            )
            self.products = products


class PropertySet(models.Model):
    """Property set. Usable for manual product management.

    name
        Name of the set.
    """
    name = models.CharField(_(u'Name'), max_length=255)

    class Meta:
        verbose_name = _(u'Property set')
        verbose_name_plural = _(u'Property sets')

    def __unicode__(self):
        return self.name


class PropertySetRelation(models.Model):
    """The relation between PropertySet and Property

    property_set
        Property set.

    property
        Property.

    position
        Ordering of the properties in the property set.
    """
    property_set = models.ForeignKey(
        PropertySet, verbose_name=_(u'Property set'))
    property = models.ForeignKey(Property, verbose_name=_(u'Property'))
    position = models.PositiveSmallIntegerField(_(u'Position'), default=999)

    class Meta:
        verbose_name = _(u'Property set relation')
        verbose_name_plural = _(u'Property set relations')

    def __unicode__(self):
        return '%s: %s' % (self.property_set.name, self.property.name)


class PropertyUnit(MPTTModel):
    """Represents the available units for product properties and
    coefficients for convert from any to each other.

    name
        Name of the unit.

    identifier
        Identifier for internal usage.

    coefficient
        A coefficient for converting this unit to the parent one.

    parent
        Parent unit. Example: "m" is a parent for "mm".
    """
    name = models.CharField(_(u'Name'), max_length=50)
    identifier = models.CharField(
        _(u'Identifier'),
        max_length=50,
        unique=True,
        help_text=_(u'Unique text identifier for property unit'))
    coefficient = models.FloatField(_('Coefficient'), default=1.0)
    parent = TreeForeignKey(
        'self',
        verbose_name=_(u'Parent'),
        blank=True, null=True, related_name='children')

    class Meta:
        verbose_name = _(u'Property unit')
        verbose_name_plural = _(u'Property units')

    def __unicode__(self):
        return self.name


class PropertyType(models.Model):
    """
    Represents a custom type for property at specified category or categories.
    Related to :model:`catalog.Category` and :model:`catalog.Property`.

    categories
        A list of categories where this type is needed to apply.

    property
        Property.

    type
        Type of the property for given categories.

    unit
        A main unit for given property. Only for numerical properties.

    available_units
        Any other available units which can be converte to the main one.

    is_active
        Not implemented yet

    is_hidden
        Not implemented yet

    regex
        See docstring for `do_operation` method.

    validator
        Custom validator function for validate the value.

    actions
        A list of functions which run when value is
        changed. Not implemented yet.

    min_value
        Min value. For numerical properties only.

    max_value
        Max value. For numerical properties only.

    do_sync
        Not implemented yet.

    is_searchable
        Not implemented yet

    true_value
        String representation of True value. For booleans only.

    false_value
        String representation of False value. For booleans only.

    visible_in_list
        Show the list of properties in product lists. Not implemented yet.
    """
    categories = models.ManyToManyField(
        Category,
        verbose_name=_(u'Categories'))
    property = models.ForeignKey(
        Property,
        verbose_name=_(u'Property'))
    type = models.PositiveSmallIntegerField(
        _(u'Type'),
        choices=PROPERTY_TYPE_CHOICES)
    unit = TreeForeignKey(
        PropertyUnit, verbose_name=_(u'Unit'),
        blank=True, null=True, related_name='property_types',
        help_text=_(u'Default unit for values if specified.'))
    available_units = TreeManyToManyField(
        PropertyUnit,
        verbose_name=_(u'Available units'), blank=True,
        null=True, related_name='available_property_types',
        help_text=_(
            u'Available units for the property. Use for unit conversion.'))
    is_active = models.BooleanField(
        _(u'Is active'), default=False,
        help_text=_(
            u'Check this option if you want use call handlers '
            u'when property value is changed.'))
    is_hidden = models.BooleanField(
        _(u'Is hidden'), default=False,
        help_text=_(u'If checked property will not show at property list.'))
    regex = models.TextField(
        _(u'Regexes and rules'), blank=True,
        default=DEFAULT_PROPERTY_TYPE_REGEX,
        help_text=_(
            u'Use different lines for different rules. This option may '
            u'have different sence for custom validators.'))
    validator = models.CharField(
        _('Validator'), max_length=155,
        blank=True, choices=PROPERTY_TYPE_VALIDATORS)
    actions = models.TextField(
        _(u'Actions'), blank=True,
        help_text=_(u'Use separate lines for each function.'))
    min_value = models.FloatField(_(u'Min value'), null=True, blank=True)
    max_value = models.FloatField(_(u'Max value'), null=True, blank=True)
    do_sync = models.BooleanField(
        _(u'Do sync'), default=True,
        help_text=_(u'If checked property will be synced with 1C.'))
    is_searchable = models.BooleanField(
        _(u'Is searchable'), default=False,
        help_text=_(u'Usable for search if search config supports it.'))
    true_value = models.CharField(_(u'True value'), max_length=100, blank=True)
    false_value = models.CharField(
        _(u'False value'), max_length=100, blank=True)
    visible_in_list = models.BooleanField(
        _(u'Visible in list'), default=False,
        help_text=_(u'Show property at category page.'))

    class Meta:
        verbose_name = _(u'Property type')
        verbose_name_plural = _(u'Property type')

    def __unicode__(self):
        return self.property.name

    def is_number(self):
        """Return True if Integer or Float."""
        return self.type in [PROPERTY_TYPE_INTEGER, PROPERTY_TYPE_FLOAT]

    def do_operation(self, value, operation):
        """Use operator for some operations with string.

        **Before regexp**

        @strip
            Strip original string value.

        @replace:str1:str2
            Replace substring str1 by str2. Use \: if you need to insert :.

        @replace:str:
            Delete substring str from original value.

        @lower
            Convert string to lower case.

        @upper
            Convert string to upper case.

        **After regexp**

        Like before but for regexp groups:

        @strip:group

        @replace:group:str1:str2

        @replace:group:str1

        @lower:group

        @upper:group
        """
        t = value
        if len(operation) > 0:
            if operation[0] == '@':
                # Temporary replace for :
                lexes = operation.replace('\:', '~~@~~').split(':')
                for l in lexes:
                    l = l.replace('~~@~~', ':')

                if lexes[0] == '@strip':
                    if type(value) is dict:
                        if len(lexes) == 2:
                            try:
                                t[lexes[1]] = t[lexes[1]].strip()
                            except KeyError, e:
                                logger.error(
                                    u'Invalid group %s for @strip '
                                    u'operation for PropertyType.id=%s' % (
                                        e, self.id))
                            return t
                    else:
                        return value.strip()
                elif lexes[0] == '@replace':
                    if type(value) is dict:
                        if len(lexes) == 3:
                            try:
                                t[lexes[1]] = t[lexes[1]].replace(lexes[2], '')
                            except KeyError, e:
                                logger.error(
                                    u'Invalid group %s for @replace '
                                    u'operation for PropertyType.id=%s' % (
                                        e, self.id))
                            return t
                        elif len(lexes) == 4:
                            try:
                                t[lexes[1]] = t[lexes[1]].replace(
                                    lexes[2], lexes[3])
                            except KeyError, e:
                                logger.error(
                                    u'Invalid group %s for @replace '
                                    u'operation for PropertyType.id=%s' % (
                                        e, self.id))
                            return t
                    else:
                        if len(lexes) == 2:
                            return value.replace(lexes[1], '')
                        elif len(lexes) == 3:
                            return value.replace(lexes[1], lexes[2])
                elif lexes[0] == '@lower':
                    if type(value) is dict:
                        if len(lexes) == 2:
                            try:
                                t[lexes[1]] = t[lexes[1]].lower()
                            except KeyError, e:
                                logger.error(
                                    u'Invalid group %s for @lower '
                                    u'operation for PropertyType.id=%s' % (
                                        e, self.id))
                            return t
                    else:
                        return value.lower()
                elif lexes[0] == '@upper':
                    if type(value) is dict:
                        if len(lexes) == 2:
                            try:
                                t[lexes[1]] = t[lexes[1]].upper()
                            except KeyError, e:
                                logger.error(
                                    u'Invalid group %s for @upper '
                                    u'operation for PropertyType.id=%s' % (
                                        e, self.id))
                            return t
                    else:
                        return value.upper()
        return value

    def validate_regex(self, property_value):
        """Default validator based on regex."""
        import re
        is_ok = False
        if not self.regex:
            return (True, None)

        lines = self.regex.replace('\r', '').split('\n')
        value = property_value.value
        for line in lines:
            if len(line) > 0:
                if line[0] == '@':
                    value = self.do_operation(value, line)
                else:
                    regex = re.compile(line, re.UNICODE)
                    is_ok = regex.match(value)
                    if is_ok:
                        break

        if is_ok:
            return (True, None)
        else:
            try:
                value = unicode(property_value.value)
            except:
                value = unicode(property_value.value.decode('utf-8'))
            return (
                False,
                _(
                    u'Value "%(value)s" for property "%(property)s" '
                    u'(pk=%(property_pk)d) of product "%(product)s" '
                    u'(pk=%(product_pk)d) did not pass regex validator.') % {
                        'value': value,
                        'property': self.property.name,
                        'property_pk': self.property_id,
                        'product': property_value.product.name,
                        'product_pk': property_value.product_id,
                    })

    def validate_choices(self, property_value):
        """Validate CHOICE property."""
        options = PropertyOption.objects.filter(
            name=property_value.value,
            propertyoptionrelation__property_type__property=
            property_value.property)
        o_count = options.count()
        if o_count == 0:
            return (
                False,
                _(
                    u'Value "%(value)s" for property "%(property)s" '
                    u'(pk=%(property_pk)d) of product "%(product)s" '
                    u'(pk=%(product_pk)d) did not pass choice validator.') % {
                        'value': property_value.value.decode('utf-8'),
                        'property': self.property.name,
                        'property_pk': self.property_id,
                        'product': property_value.product.name,
                        'product_pk': property_value.product_id,
                    })
        elif o_count == 1:
            return (True, None)
        else:
            logger.warning(
                _(
                    u'PropertyType pk=%(propertytype_pk)d has '
                    u'duplicate options.') % {
                        'propertytype_pk': self.pk,
                    })
            return (True, None)

    def validate_boolean(self, property_value):
        value = property_value.value
        if value == self.true_value or value == self.false_value:
            return (True, None)
        else:
            return (
                False,
                _(
                    u'Value "%(value)s" for property "%(property)s" '
                    u'(pk=%(property_pk)d) of product "%(product)s" '
                    u'(pk=%(product_pk)d) did not pass boolean validator.') % {
                        'value': property_value.value.decode('utf-8'),
                        'property': self.property.name,
                        'property_pk': self.property_id,
                        'product': property_value.product.name,
                        'product_pk': property_value.product_id,
                    })

    def validate(self, property_value):
        """Return the result of property value validation."""
        if self.validator:
            from ..core.utils import import_symbol
            f = import_symbol(self.validator)
            return f(property_value, self)
        else:
            is_ok = self.validate_regex(property_value)
            if is_ok[0]:
                if self.type == PROPERTY_TYPE_STRING:
                    return is_ok
                elif self.is_number():
                    return is_ok
                elif self.type == PROPERTY_TYPE_CHOICE:
                    return self.validate_choices(property_value)
                elif self.type == PROPERTY_TYPE_BOOLEAN:
                    return self.validate_boolean(property_value)
                else:
                    return (
                        False,
                        _(
                            u'Unknown type for PropertyType '
                            u'pk=%(propertytype_pk)d') % {
                                'propertytype_pk': self.pk,
                            })
            else:
                return is_ok

    def unpack_regex_data(self, property_value):
        """Unpack decimal value and unit from value string using the regex."""
        if self.validator:
            from ..core.utils import import_symbol
            f = import_symbol(self.validator)
            return f(property_value, self, unpack_data=True)
        else:
            import re
            is_ok = False
            lines = self.regex.replace('\r', '').split('\n')
            value = property_value.value
            groupdict = dict()
            for line in lines:
                if len(line) > 0:
                    if line[0] == '@':
                        if not is_ok:
                            value = self.do_operation(value, line)
                        else:
                            groupdict = self.do_operation(groupdict, line)
                    else:
                        if not is_ok:
                            regex = re.compile(line, re.UNICODE)
                            is_ok = regex.match(value)
                            if is_ok:
                                groupdict = is_ok.groupdict()

            if is_ok:
                return groupdict
            else:
                return dict()

    def convert_to_number(self, property_value):
        """Convert string to its number representation
        depends the specified type."""
        value = 0

        is_validate = self.validate(property_value)
        if not is_validate[0]:
            logger.error(is_validate[1])
            return 0

        if self.is_number():
            t = self.unpack_regex_data(property_value)
            value_str = t.get('value', '0')
            unit_str = t.get('unit', None)
            if value_str != '0':
                if self.type == PROPERTY_TYPE_INTEGER:
                    value = int(float(value_str))
                elif self.type == PROPERTY_TYPE_FLOAT:
                    value = float(value_str)

                if self.min_value and value < self.min_value:
                    value = 0
                    logger.error(
                        _(
                            u'Value "%(value)s" for property "%(property)s" '
                            u'(pk=%(property_pk)d) of product "%(product)s" '
                            u'(pk=%(product_pk)d) is less than min value.') % {
                                'value': property_value.value.decode('utf-8'),
                                'property': self.property.name,
                                'property_pk': self.property_id,
                                'product': property_value.product.name,
                                'product_pk': property_value.product_id,
                            })
                if self.max_value and value > self.max_value:
                    value = 0
                    logger.error(
                        _(
                            u'Value "%(value)s" for property "%(property)s" '
                            u'(pk=%(property_pk)d) of product "%(product)s" '
                            u'(pk=%(product_pk)d) is less than max value.') % {
                                'value': property_value.value.decode('utf-8'),
                                'property': self.property.name,
                                'property_pk': self.property_id,
                                'product': property_value.product.name,
                                'product_pk': property_value.product_id,
                            })
            else:
                value = 0

            if unit_str:
                if self.unit:
                    if self.unit.name == unit_str:
                        return value
                    else:
                        try:
                            second_unit = self.available_units.get(
                                name=unit_str)
                        except PropertyUnit.DoesNotExist:
                            logger.error(
                                _(
                                    u'Value "%(value)s" for property '
                                    u'"%(property)s" (pk=%(property_pk)d) of '
                                    u'product "%(product)s" '
                                    u'(pk=%(product_pk)d) has unit which '
                                    u'is not available for it.') % {
                                        'value': property_value.value.decode('utf-8'),
                                        'property': self.property.name,
                                        'property_pk': self.property_id,
                                        'product': property_value.product.name,
                                        'product_pk':
                                        property_value.product_id,
                                    })
                            return 0

                        p_value = second_unit.coefficient * value
                        value = p_value / self.unit.coefficient
                        return value
                else:
                    logger.error(
                        _(
                            u'Value "%(value)s" for property "%(property)s" '
                            u'(pk=%(property_pk)d) of product "%(product)s" '
                            u'(pk=%(product_pk)d) has unit but default unit '
                            u'is not specified.') % {
                                'value': property_value.value.decode('utf-8'),
                                'property': self.property.name,
                                'property_pk': self.property_id,
                                'product': property_value.product.name,
                                'product_pk': property_value.product_id,
                            })
                    return 0

            return value
        else:
            return 0


class PropertyOptionRelation(models.Model):
    """Relation between PropertyType and PropertyOption.

    property_type
        Property type.

    property_option
        Property option.

    position
        Ordering of the options for given property type.
    """
    property_type = models.ForeignKey(
        PropertyType, verbose_name=_(u'Property type'))
    property_option = models.ForeignKey(
        PropertyOption, verbose_name=_(u'Property option'))
    position = models.PositiveSmallIntegerField(_(u'Position'), default=999)

    class Meta:
        ordering = ['position']
        verbose_name = _(u'Property option relation')
        verbose_name_plural = _(u'Property options relations')
