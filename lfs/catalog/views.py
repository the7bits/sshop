# coding: utf-8
import locale

from django.conf import settings
from django.utils.translation import ugettext as _
from django.core.cache import cache
from django.core.paginator import Paginator, EmptyPage, InvalidPage
# from django.core.urlresolvers import reverse
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import (
    require_http_methods,
    require_POST,
    require_GET,
)
from django import forms

from ..caching.utils import lfs_get_object_or_404
from ..core.signals import cart_changed
from .models import Category, Property
from .models import Product
from .models import SortType
# from .settings import VARIANT
from .settings import CONTENT_PRODUCTS
from .settings import VARIANTS_DISPLAY_TYPE_TEMPLATES
from .settings import PRODUCT_WITH_VARIANTS, VARIANT
from ..core.utils import get_default_shop
from question.models import Question
from ..filters.models import Filter, FilterOption
from ..core.utils import import_symbol
from hr_urls.models import FilterUrlTemplate
from hr_urls.utils import get_hr_urls, get_hr_urls_to_client
from ..cart.utils import (
    get_or_create_cart,
)
from ..discounts.utils import get_valid_discounts
from ..core.decorators import sbits_cache_page
from django.views.decorators.csrf import ensure_csrf_cookie
from django.http import HttpResponsePermanentRedirect


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ("email", "question")


class RedirectException(Exception):
    def __init__(self, url):
        self.url = url


@csrf_exempt
@require_POST
def set_sorting(request):
    """Saves the given sortings (by request body) to session.
    """
    sorting = request.POST.get("sorting", "")
    if sorting == "" and "sorting" in request.session:
        del request.session["sorting"]
    else:
        request.session["sorting"] = sorting

    referrer = request.META.get("HTTP_REFERER")
    if referrer is not None:
        return HttpResponseRedirect(referrer)
    else:
        return HttpResponseForbidden(
            _(u'HTTP_REFERER is required.'))


@require_GET
def set_page_mode(request):
    """Saves the given category page mode to session.
    """
    page_mode = request.GET.get("mode")
    if page_mode is None:
        return HttpResponseForbidden(
            _(u'Argument "mode" expected.'))

    request.session["page_mode"] = page_mode

    referrer = request.META.get("HTTP_REFERER")
    if referrer is not None:
        return HttpResponseRedirect(referrer)
    else:
        return HttpResponseForbidden(
            _(u'HTTP_REFERER is required.'))


@sbits_cache_page()
@ensure_csrf_cookie
def category_view(
        request,
        slug,
        direct_request=True,
        category=None,
        template_name="lfs/catalog/category_base.html"):
    """
    """
    from .utils import get_current_top_category
    from ..core.signals import category_page_visited

    if getattr(settings, 'USE_HR_URLS', False) and direct_request:
        from hr_urls.utils import url_validator
        if not url_validator(request, slug):
            raise Http404
    start = request.REQUEST.get("start", 1)
    if category is None:
        category = lfs_get_object_or_404(Category, slug=slug)
    if category.get_content() == CONTENT_PRODUCTS:
        try:
            inline, inline_context = category_products(
                request, slug, start, category=category)
        except RedirectException as e:
            if getattr(
                settings, 'CATEGORY_MAKE_301_REDIRECT_FOR_INVALID_PAGE',
                    False):
                return HttpResponsePermanentRedirect(e.url)
    else:
        inline, inline_context = category_categories(
            request, slug, category=category)
    # Set last visited category for later use, e.g. Display breadcrumbs,
    # selected menu points, etc.
    request.session["last_category"] = category

    # TODO: Factor top_category out to a inclusion tag, so that people can
    # omit if they don't need it.

    # regexp to js format str
    hr_urls_templates, filters_string = \
        get_hr_urls_to_client(request, category)

    category_page_visited.send(category)
    context_data = {
        "category": category,
        "category_inline": inline,
        "top_category": get_current_top_category(request, category),
        "page_type": "category",
        'hr_urls_templates': ';'.join(hr_urls_templates),
        'unpacked_hr_urls': filters_string,
        'category_slug': category.slug,
    }
    context_data.update(inline_context)

    return render_to_response(
        template_name, RequestContext(request, context_data))


def category_categories(
        request, slug, start=0,
        template_name="lfs/catalog/categories/category/default.html",
        category=None):
    """Displays the child categories of the category with passed slug.

    This view is called if the user chooses a template that is situated
    in settings.CATEGORY_PATH ".
    """
    cache_key = "%s-category-categories-%s" % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX, slug)

    result = cache.get(cache_key)
    if result is not None:
        return result

    if not category:
        category = lfs_get_object_or_404(Category, slug=slug)

    format_info = category.get_format_info()
    amount_of_cols = format_info["category_cols"]

    categories = []
    row = []
    for children in category.get_children():
        if not children.exclude_from_navigation:
            row.append(children)
            if len(row) % amount_of_cols == 0:
                categories.append(row)
                row = []

    if len(row) > 0:
        categories.append(row)

    render_template = category.get_template_name()

    if render_template is not None:
        template_name = render_template

    context_data = {
        "category": category,
        "categories": categories,
        "page_type": "category",
    }

    result = (
        render_to_string(
            template_name, RequestContext(request, context_data)),
        context_data,
    )

    cache.set(cache_key, result)
    return result


def category_products(
        request, slug, start=1,
        template_name="lfs/catalog/categories/product/default.html",
        category=None):
    """Displays the products of the category with passed slug.

    This view is called if the user chooses a template that is
    situated in settings.PRODUCT_PATH ".
    """
    from .utils import get_filtered_products_for_category
    from ..core.utils import lfs_pagination

    page_mode = request.session.get(
        "page_mode", getattr(settings, 'DEFAULT_CATEGORY_MODE', 'list'))
    sorting = request.session.get("sorting")
    if sorting is None:
        try:
            sorting = \
                SortType.objects.all().order_by('order')[0].sortable_fields
        except IndexError:
            sorting = \
                getattr(settings, 'DEFAULT_SORTING', 'effective_price')

    product_filter = dict(request.GET)

    # Write known page modes to cache
    page_modes_cache_key = '%s-page-modes' % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX,
    )
    page_modes = cache.get(page_modes_cache_key)
    if page_modes is not None:
        if page_mode not in page_modes:
            page_modes.append(page_mode)
        cache.set(page_modes_cache_key, page_modes)
    else:
        cache.set(page_modes_cache_key, [page_mode])

    cache_key = "%s-new-category-products-%s-mode-%s" % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX, slug, page_mode)
    sub_cache_key = "%s-start-%s-sorting-%s-mode-%s" % (
        settings.CACHE_MIDDLEWARE_KEY_PREFIX, start, sorting, page_mode)

    filter_key = ["%s-%s" % (
        key, ':'.join(value)) for key, value in product_filter.items()]
    if filter_key:
        sub_cache_key += "-%s" % "-".join(filter_key)

    temp = cache.get(cache_key)

    if temp is not None:
        try:
            return temp[sub_cache_key]
        except KeyError:
            pass
    else:
        temp = dict()

    if not category:
        category = lfs_get_object_or_404(Category, slug=slug)

    # Calculates parameters for display.
    try:
        start = int(start)
    except (ValueError, TypeError):
        start = 1

    request.session['filters'] = product_filter
    all_products = get_filtered_products_for_category(
        category=category,
        filters=product_filter,
        sorting=sorting,
        variant_distinct=getattr(
            settings, 'CATEGORY_SHOW_PRODUCT_WITH_VARIANTS', False),
    )

    format_info = category.get_format_info()
    amount_of_rows = format_info["product_rows"]
    amount_of_cols = format_info["product_cols"]

    if page_mode == 'list':
        amount = amount_of_rows
    else:
        amount = amount_of_rows * amount_of_cols

    # prepare paginator
    paginator = Paginator(all_products, amount)
    try:
        current_page = paginator.page(start)
    except (EmptyPage, InvalidPage):
        if getattr(settings, 'CATEGORY_SHOW_404_FOR_INVALID_PAGE', False):
            raise Http404
        elif getattr(
                settings, 'CATEGORY_MAKE_301_REDIRECT_FOR_INVALID_PAGE',
                False):
            if request.GET.get('start'):
                to_replace = 'start=%s' % paginator.num_pages
            raise RedirectException(
                request.build_absolute_uri().replace(
                    'start=%s' % request.GET['start'], to_replace))
        else:
            current_page = paginator.page(paginator.num_pages)
    # Calculate urls
    if getattr(settings, 'USE_HR_URLS', False) and request.GET:
        if 'extra_proxyattrs' in dir(request):
            extras = request.extra_proxyattrs
        else:
            extras = {}
        from urllib import urlencode
        url = category.get_absolute_url() +\
            get_hr_urls(
                category,
                urlencode(request.GET).replace('%2C', ','),
                extras=extras
            )
    else:
        url = category.get_absolute_url()
    pagination_data = lfs_pagination(request, current_page, url=url)

    render_template = category.get_template_name()
    if render_template is not None:
        template_name = render_template

    if len(filter_key) > 0:
        filtered = True
    else:
        filtered = False

    products = current_page.object_list
    if page_mode != 'list':
        t = 0
        for p in products:
            p.left_block = t == 0
            p.right_block = t == (amount_of_cols - 1)
            if t == (amount_of_cols - 1):
                t = 0
            else:
                t += 1

    context_data = {
        "category": category,
        "products": products,
        "pagination": pagination_data,
        'page_mode': page_mode,
        'products_count': paginator.count,
        'filtered': filtered,
        'product_filter': product_filter,
        'page_type': 'category',
        'sortable_fields': sorting.split(','),
    }

    result = (render_to_string(
        template_name, RequestContext(request, context_data)),
        context_data,
    )

    temp[sub_cache_key] = result
    cache.set(cache_key, temp)
    return result


def product_reviews_view(
        request, slug, template_name="lfs/catalog/products/reviews.html"):
    """Main view to display reviews of a product.
    """
    product = lfs_get_object_or_404(Product, slug=slug)
    if not (request.user.is_superuser or product.is_active()):
        raise Http404()

    result = render_to_response(template_name, RequestContext(request, {
        "product": product,
    }))
    return result


@sbits_cache_page()
@ensure_csrf_cookie
def product_view(
        request,
        slug, product=None, template_name="lfs/catalog/product_base.html"):
    """Main view to display a product.
    """
    from ..core.signals import product_page_visited
    if product is None:
        product = lfs_get_object_or_404(Product, slug=slug)
    if getattr(settings, 'CATEGORY_HIDE_VARIANTS', False)\
            and product.sub_type == VARIANT:
        raise Http404()
    if not product.active:
        raise Http404()

    product_page_visited.send(product)
    # Store recent products for later use'id': f.id,
    if product.sub_type != PRODUCT_WITH_VARIANTS:
        recent = request.session.get("RECENT_PRODUCTS", [])
        if slug in recent:
            recent.remove(slug)
        recent.insert(0, slug)
        if len(recent) > settings.LFS_RECENT_PRODUCTS_LIMIT:
            recent = recent[:settings.LFS_RECENT_PRODUCTS_LIMIT + 1]
        request.session["RECENT_PRODUCTS"] = recent

    result = render_to_response(template_name, RequestContext(request, {
        "product": product,
        'product_inline': product_inline(request, product),
        'page_type': 'product',
    }))

    return result


def product_question(
        request, product_id, template_name="question/question.html"):
    if request.method == "POST" and request.is_ajax():
        product = lfs_get_object_or_404(Product, pk=product_id)

        question_form = QuestionForm(data=request.POST)
        if question_form.is_valid():
            question = question_form.save(commit=False)
            question.product = product
            question.save()

            return render_to_response(
                template_name, RequestContext(request, {}))
    else:
        if request.user.is_authenticated():
            question_form = QuestionForm(initial={"email": request.user.email})
        else:
            question_form = QuestionForm()
    return render_to_response(template_name, RequestContext(request, {
        "form": question_form,
        "product_id": product_id,
    }))


def product_inline(
        request, product,
        template_name="lfs/catalog/products/product_inline.html",
        use_cache=True):
    """
    Part of the product view, which displays the actual data of the product.

    This is factored out to be able to better cached and in might
    in future used used to be updated via ajax requests.
    """
    cache_key = None
    if use_cache:
        cache_key = "%s-product-inline-%s-%s" % (
            settings.CACHE_MIDDLEWARE_KEY_PREFIX,
            request.user.is_superuser,
            product.id)
        result = cache.get(cache_key)
        if result is not None:
            return result

    # Switching to default variant
    variants_display_type_template = None
    variants = []

    if product.is_product_with_variants():
        variants_display_type_template = dict(
            VARIANTS_DISPLAY_TYPE_TEMPLATES
        )[product.variants_display_type]['file']
        variants = product.get_variants()
    elif product.is_variant():
        parent = product.parent
        variants_display_type_template = dict(
            VARIANTS_DISPLAY_TYPE_TEMPLATES
        )[parent.variants_display_type]['file']
        variants = parent.get_variants()

    if product.get_template_name() is not None:
        template_name = product.get_template_name()

    attachments = product.get_attachments()

    result = render_to_string(template_name, RequestContext(request, {
        "product": product,
        "variants": variants,
        "product_accessories": product.get_accessories(),
        "global_properties": product_properties_view(request, product),
        "attachments": attachments,
        "quantity": product.get_clean_quantity(1),
        "price_unit": product.get_price_unit(),
        "unit": product.get_unit(),
        "variants_display_type_template": variants_display_type_template,
        "for_sale": product.for_sale,
        "currency": get_default_shop(request).default_currency,
        'page_type': 'product',
    }))

    if use_cache:
        cache.set(cache_key, result)
    return result


def product_variants_for_category(request, product_id):
    from .utils import get_filtered_products_for_category
    product = lfs_get_object_or_404(Product, pk=product_id)
    template_name = dict(
        VARIANTS_DISPLAY_TYPE_TEMPLATES
    )[product.variants_display_type]['file']
    variants = product.get_variants()

    # Now works only for one category
    category = product.get_category()
    filters = request.session.get('filters', {})

    if filters:
        sorting = request.session.get('sorting')
        if sorting is None:
            try:
                sorting = \
                    SortType.objects.all().order_by('order')[0].sortable_fields
            except IndexError:
                sorting = \
                    getattr(settings, 'DEFAULT_SORTING', 'effective_price')

        all_products = get_filtered_products_for_category(
            category=category,
            filters=filters,
            sorting=sorting,
        )
        ids = variants.values_list('id', flat=True)
        allowed_variants = all_products.filter(id__in=ids)
    else:
        allowed_variants = variants

    return render_to_response(template_name, RequestContext(request, {
        "product": product,
        "variants": allowed_variants,
    }))


@csrf_exempt
@require_http_methods(["POST"])
def product_form_dispatcher(request):
    """Dispatches to the added-to-cart view or to the selected variant.
    """
    return modal_cart(request)


@csrf_exempt
@require_http_methods(["POST"])
def change_modal_cart(request):
    cart = get_or_create_cart(request)

    cart_items = []
    for item in cart.get_items():
        try:
            value = request.POST.get("amount-cart-item_%s" % item.id, "0.0")
            if isinstance(value, unicode):
                value = value.encode("utf-8")
            amount = abs(locale.atof(value))
        except (TypeError, ValueError):
            amount = 1.0

        item.amount = amount
        if item.amount == 0:
            item.delete()
        else:
            item.save()
            product = item.product
            quantity = product.get_clean_quantity(item.amount)
            cart_items.append({
                "obj": item,
                "quantity": quantity,
                "product": product,
                "product_price": item.get_price(request),
                "product_price_total": item.get_price_total(request),
            })

    cart_changed.send(cart, request=request)

    # Save the cart to update modification date
    cart.save()

    # cart price
    cart_price = cart.get_price(request)

    # discounts
    discounts = get_valid_discounts(request)
    for discount in discounts:
        cart_price = cart_price - discount["price"]

    return render_to_response(
        "lfs/catalog/modal_cart.html",
        RequestContext(request, {
            'cart_items': cart_items,
            'cart_price': cart_price,
            'discounts': discounts,
        }))


def modal_cart(request):
    product_id = request.REQUEST.get("product_id")
    product = lfs_get_object_or_404(Product, pk=product_id)

    # Only active and deliverable products can be added to the cart.
    if not product.is_active() or not product.status.show_buy_button:
        return render_to_response(
            "lfs/catalog/modal_cart.html",
            RequestContext(request, {
                'message': settings.PRODUCT_CANT_BE_BOUGHT_MESSAGE,
            }))
    try:
        value = request.POST.get("quantity", "1")
        if isinstance(value, unicode):
            value = value.encode("utf-8")
        quantity = abs(locale.atof(value))
    except (TypeError, ValueError):
        quantity = 1.0

    cart = get_or_create_cart(request)

    cart_item = cart.add(product=product, amount=quantity)
    cart_items = [cart_item]

    # Add selected accessories to cart
    for key, value in request.POST.items():
        if key.startswith("accessory"):
            accessory_id = key.split("-")[1]
            try:
                accessory = Product.objects.get(pk=accessory_id)
            except Product.ObjectDoesNotExist:
                continue

            # Get quantity
            quantity = request.POST.get("quantity-%s" % accessory_id, 0)
            try:
                quantity = float(quantity)
            except TypeError:
                quantity = 1

            cart_item = cart.add(product=accessory, amount=quantity)
            cart_items.append(cart_item)

    # Store cart items for retrieval within added_to_cart.
    request.session["cart_items"] = cart_items
    cart_changed.send(cart, request=request)

    # Save the cart to update modification date
    cart.save()

    cart_items = []
    for cart_item in cart.get_items():
        product = cart_item.product
        quantity = product.get_clean_quantity(cart_item.amount)
        cart_items.append({
            "obj": cart_item,
            "quantity": quantity,
            "product": product,
            "product_price": cart_item.get_price(request),
            "product_price_total": cart_item.get_price_total(request),
        })

    # cart price
    cart_price = cart.get_price(request)

    # discounts
    discounts = get_valid_discounts(request)
    for discount in discounts:
        cart_price = cart_price - discount["price"]

    return render_to_response(
        "lfs/catalog/modal_cart.html",
        RequestContext(request, {
            'cart_items': cart_items,
            'cart_price': cart_price,
            'discounts': discounts,
        }))


def product_properties_view(
        request, product,
        template_name="lfs/catalog/products/properties.html"):
    """This view return product properties in html format
    """
    if not (request.user.is_superuser or product.is_active()):
        raise Http404()

    property_values = product.get_property_values()
    html = render_to_string(template_name, RequestContext(request, {
        'property_values': property_values,
    }))
    return html


def product_accessories_view(
        request, slug,
        template_name="lfs/catalog/products/accessories.html"):
    """This view return product accessories in html format
    """
    product = lfs_get_object_or_404(Product, slug=slug)

    if not (request.user.is_superuser or product.is_active()):
        raise Http404()

    result = render_to_response(template_name, RequestContext(request, {
        "accessories": product.get_accessories(),
    }))
    return result


def product_description_view(request, slug):
    """This view return product description in html format
    """
    product = lfs_get_object_or_404(Product, slug=slug)

    return HttpResponse(product.get_description())


def category_product_url_proxy(request, slug):
    try:
        category = lfs_get_object_or_404(Category, slug=slug)
        return category_view(request, slug, category=category)
    except Http404:
        pass

    try:
        product = lfs_get_object_or_404(Product, slug=slug)
        return product_view(request, slug, product=product)
    except Http404:
        pass

    raise Http404


@sbits_cache_page()
@ensure_csrf_cookie
def category_hr_view(request, **kwargs):
    category = lfs_get_object_or_404(
        Category, slug=kwargs.pop('category_slug'))
    url_template = lfs_get_object_or_404(
        FilterUrlTemplate, pk=kwargs.pop('template_id'))

    # if template's parameter contains in request.GET, then it's bad url
    for k in kwargs.keys():
        if k in request.GET:
            raise Http404
    # get parameters to filtered products
    filter_parameters = {}
    extra_proxyattrs = {}
    for k in kwargs.keys():
        try:
            # find filter and option by parameter and value
            f = Filter.objects.get(identificator=k, category=category)
            try:
                FilterOption.objects.get(filter=f, identificator=kwargs[k])
            except FilterOption.DoesNotExist:
                raise Http404
            filter_parameters[k] = kwargs[k]
        except Filter.DoesNotExist:
            # else, try validate this parameter by custom validator
            validator = url_template.get_validator_for_parameter(k)
            if validator:
                v = import_symbol(validator)
                if not v(kwargs[k], category.slug):
                    raise Http404

                request.__setattr__('proxyattr_'+k, kwargs[k])
                extra_proxyattrs[k] = kwargs[k]
            else:
                raise Http404

    request.GET.update(filter_parameters)
    request.__setattr__('extra_proxyattrs', extra_proxyattrs)

    return category_view(request, category.slug, False, category=category)


# -------------------

from django_select2.views import Select2View


class ProductListView(Select2View):
    def get_results(self, request, term, page, context):
        products = Product.objects.filter(
            name__icontains=term).values('id', 'name')
        return ('nil', False, [(p['id'], p['name']) for p in products])


class PropertyListView(Select2View):
    def get_results(self, request, term, page, context):
        properties = Property.objects.filter(
            name__icontains=term).values('id', 'name')
        return ('nil', False, [(p['id'], p['name']) for p in properties])
