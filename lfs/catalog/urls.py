# coding: utf-8
from django.conf.urls.defaults import *

# These url patterns use for admin interface
urlpatterns = patterns(
    'lfs.catalog.admin_views',
    url(
        r'^products-for-category/(?P<category_id>[-\d]*)/$',
        'products_for_category', name="admin_products_for_category"),
    url(
        r'^delete-product-from-category/(?P<category_id>[-\d]*)/'
        r'(?P<product_id>[-\d]*)/$',
        'delete_product_from_category',
        name="admin_delete_product_from_category"),
    url(
        r'^generate-slug$', 'generate_slug', name='admin_generate_slug'),
    url(
        r'^sort-property-options/(?P<property_type_id>[-\d]*)/$',
        'sort_property_options',
        name="admin_sort_property_options"
    ),
    url(
        r'^option-list-for-property-type$',
        'option_list_for_property_type',
        name="option_list_for_property_type"
    ),
    url(
        r'^move-property-option$',
        'move_property_option',
        name="move_property_option"
    ),
    url(
        r'^delete-property-option$',
        'delete_property_option',
        name="delete_property_option"
    )
)
