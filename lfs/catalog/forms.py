# coding: utf-8
import autocomplete_light

from django.utils.translation import ugettext as _
from django import forms
from .models import (
    Category,
    Product,
    Property,
    ProductPropertyValue,
    ProductAccessories,
    PropertyType,
    PropertyOption,
    PropertySet,
    ProductStatus,
    Image,
    Manufacturer,
    PropertyOptionRelation
)
from suit_ckeditor.widgets import CKEditorWidget
from codemirror.widgets import CodeMirrorTextarea

from suit.widgets import LinkedSelect
from .settings import (
    CATEGORY_TEMPLATES,
    PRODUCT_TEMPLATES,
    PRODUCT_TYPE_CHOICES,
    VARIANTS_DISPLAY_TYPE_TEMPLATES
)
from django_select2.widgets import HeavySelect2MultipleWidget
from ..core.utils import check_pattern_exist
from hr_urls.models import FilterUrlTemplate
from ..core.widgets.image import LFSImageInput
from .autocomplete_light_registry import PropertyOptionAutocomplete
from lfs.catalog.settings import CATEGORY_TEMPLATES


CHANGE_PRICE_CHOICES = (
    ('0', _(u'Absolute value')),
    ('5', _(u'Absolute delta')),
    ('10', _(u'Percentage delta')),
)


class ImageInlineForm(forms.ModelForm):

    class Meta:
        model = Image
        widgets = {
            'image': LFSImageInput,
        }


class FilterUrlTemplateForm(forms.ModelForm):

    class Meta:
        model = FilterUrlTemplate
        widgets = {
            'template': forms.widgets.Textarea(
                attrs={'class': 'input-xlarge'}),
            'validators': forms.widgets.Textarea(
                attrs={'class': 'input-xlarge'}),
        }


class AddCategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        fields = ('name', 'slug', 'parent')
        widgets = {
            'parent': LinkedSelect,
        }


class EditCategoryForm(forms.ModelForm):

    class Meta:
        model = Category
        widgets = {
            'short_description': CKEditorWidget(
                editor_options={'startupFocus': True}),
            'description': CKEditorWidget(
                editor_options={'startupFocus': True}),
            'parent': LinkedSelect,
            'products': HeavySelect2MultipleWidget(
                data_url='/product-api/products_json',
                attrs={'class': 'input-xxlarge'},
            ),
            'meta_h1': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_title': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_keywords': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_description': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'seo': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'static_block': LinkedSelect,

        }

    def __init__(self, *args, **kwargs):
        super(EditCategoryForm, self).__init__(*args, **kwargs)
        self.fields['template'] = forms.ChoiceField(
            choices=[(o[0], o[1]['name']) for o in CATEGORY_TEMPLATES],
        )


class ChangeSubtypeForm(forms.Form):
    sub_type = forms.ChoiceField(
        choices=PRODUCT_TYPE_CHOICES,
        label=_(u'Subtype'))


class ChangeStatusForm(forms.Form):
    status = forms.ModelChoiceField(
        queryset=ProductStatus.objects.all(),
        label=_(u'Status'))


class ChangePriceForm(forms.Form):
    change_action = forms.ChoiceField(
        choices=CHANGE_PRICE_CHOICES,
        label=_(u'Change action'))
    price = forms.FloatField(required=True, label=_(u'Price'))


class ChangeManufacturerForm(forms.Form):
    manufacturer = forms.ModelChoiceField(
        queryset=Manufacturer.objects.all(),
        label=_(u'Manufacturer'))


class ChangeCategoriesForm(forms.Form):
    categories = forms.ModelMultipleChoiceField(
        label=_(u'Categories'),
        queryset=Category.objects.all(), required=False,
        widget=autocomplete_light.MultipleChoiceWidget('CategoryAutocomplete'))


class ApplyPropertySetForm(forms.Form):
    property_set = forms.ModelChoiceField(
        queryset=PropertySet.objects.all(),
        label=_(u'Property set'))


class CreatePropertySetFromProductForm(forms.Form):
    name = forms.CharField(max_length=255, label=_(u'Name'))


class AddProductForm(forms.ModelForm):
    propertyset = forms.ModelChoiceField(
        label=_(u'Property set'),
        queryset=PropertySet.objects.all(),
        required=False)
    categories = forms.ModelMultipleChoiceField(
        label=_(u'Categories'),
        queryset=Category.objects.all(), required=False,
        widget=autocomplete_light.MultipleChoiceWidget('CategoryAutocomplete'))

    class Meta:
        model = Product
        fields = ('name', 'slug', 'sub_type')

    def __init__(self, *args, **kwargs):
        super(AddProductForm, self).__init__(*args, **kwargs)

    def clean_slug(self):
        data = self.cleaned_data['slug']
        if check_pattern_exist(data):
            raise forms.ValidationError(
                _(
                    u'%s already used as slug for another object '
                    u'or as url pattern') % data)
        return data


class EditProductForm(forms.ModelForm):
    categories = forms.ModelMultipleChoiceField(
        label=_(u'Categories'),
        queryset=Category.objects.all(), required=False,
        widget=autocomplete_light.MultipleChoiceWidget('CategoryAutocomplete'))

    class Meta:
        from lfs.catalog.widgets import ReSlug
        model = Product
        widgets = {
            'status': LinkedSelect,
            'slug': ReSlug,
            'manufacturer': LinkedSelect,
            'short_description': CKEditorWidget(
                editor_options={'startupFocus': True}),
            'description': CKEditorWidget(
                editor_options={'startupFocus': True}),
            'related_products':
            autocomplete_light.MultipleChoiceWidget('ProductAutocomplete'),
            'meta_h1': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_title': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_keywords': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_description': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_seo_text': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'static_block': LinkedSelect,
        }

    def __init__(self, *args, **kwargs):
        super(EditProductForm, self).__init__(*args, **kwargs)
        self.fields['template'] = forms.ChoiceField(
            choices=[(o[0], o[1]['name']) for o in PRODUCT_TEMPLATES],
            label=_(u'Template')
        )
        self.fields['variants_display_type'] = forms.ChoiceField(
            choices=[(
                o[0],
                o[1]['name']) for o in VARIANTS_DISPLAY_TYPE_TEMPLATES],
            label=_(u'Variants display type')
        )
        if 'instance' in kwargs:
            obj = kwargs['instance']
            self.initial.update({'categories': obj.categories.all()})

            new_choices = []
            categories = Category.objects.filter(
                pk__in=[t[0] for t in self.fields['categories'].choices])
            for c in categories:
                new_choices.append((
                    c.id,
                    '%s %s' % ('---' * c.level, c.name),
                ))
            self.fields['categories'].choices = new_choices

    def clean_slug(self):
        data = self.cleaned_data['slug']
        if check_pattern_exist(data, self.instance):
            raise forms.ValidationError(_(
                u'%s already used as slug for another object '
                u'or as url pattern') % data)
        return data


class EditStandardProductForm(EditProductForm):
    pass


class EditProductWithVariantsForm(EditProductForm):
    pass


class EditVariantForm(EditProductForm):
    parent = forms.ModelChoiceField(
        Product.objects.filter(active=True),
        label=_(u'Parent product'),
        widget=autocomplete_light.ChoiceWidget('ProductAutocomplete'))


class ProductAccessoryForm(forms.ModelForm):
    accessory = forms.ModelChoiceField(
        Product.objects.filter(active=True),
        label=_(u'Accessory'),
        widget=autocomplete_light.ChoiceWidget('ProductAutocomplete'))

    class Meta:
        model = ProductAccessories


class ProductPropertyValueForm(forms.ModelForm):
    property = forms.ModelChoiceField(
        Property.objects.all(),
        label=_(u'Property'),
        widget=autocomplete_light.ChoiceWidget('PropertyAutocomplete'))

    class Meta:
        model = ProductPropertyValue
        fields = ('property', 'value', 'position', 'value_as_float')
        widgets = {
            'value': autocomplete_light.TextWidget(
                'PropertyOptionAutocomplete')
        }

    def get_property_type(self, obj):
        p_type = None
        try:
            p_type = PropertyType.objects.get(
                categories__in=self.product.categories.all(),
                property=obj.property,
            )
        except PropertyType.DoesNotExist:
            pass
        except PropertyType.MultipleObjectsReturned:
            pass
        return p_type

    # def clean_value(self):
    #     property = self.cleaned_data['property']
    #     value = self.cleaned_data['value']

    #     if not hasattr(self, 'product'):
    #         return value

    #     p_type = None
    #     try:
    #         p_type = PropertyType.objects.get(
    #             categories__in=self.product.categories.all(),
    #             property=property,
    #             )
    #     except PropertyType.DoesNotExist:
    #         pass
    #     except PropertyType.MultipleObjectsReturned:
    #         pass

    #     if p_type:
    #         v = self.p_value
    #         v.value = value
    #         t = p_type.validate(v)
    #         if not t[0]:
    #             raise forms.ValidationError(t[1])
    #     return value

    def __init__(self, *args, **kwargs):
        from .settings import (
            PROPERTY_TYPE_CHOICE,
            PROPERTY_TYPE_BOOLEAN,
        )

        super(ProductPropertyValueForm, self).__init__(*args, **kwargs)
        if 'instance' in kwargs:
            obj = kwargs['instance']
            self.p_value = obj
            self.product = obj.product
            if obj.property.is_group:
                self.fields['value'].widget = forms.widgets.TextInput(
                    attrs={'readonly': True})
            else:
                from django.conf import settings
                required = not getattr(
                    settings, 'IGNORE_EMPTY_PROPERTY', False)
                p_type = self.get_property_type(obj)
                if p_type and p_type.type == PROPERTY_TYPE_CHOICE:
                    options = PropertyOption.objects.filter(
                        propertyoptionrelation__property_type=p_type
                    ).order_by(
                        'propertyoptionrelation__position').values('name')
                    option_strs = [('', '')]
                    option_strs += [(s['name'], s['name']) for s in options]
                    self.fields['value'] = forms.ChoiceField(
                        choices=option_strs,
                        required=required,
                    )
                elif p_type and p_type.type == PROPERTY_TYPE_BOOLEAN:
                    option_strs = [
                        (p_type.true_value, p_type.true_value),
                        (p_type.false_value, p_type.false_value),
                    ]
                    self.fields['value'] = forms.ChoiceField(
                        choices=option_strs,
                        required=required)
                else:
                    self.fields['value'] = forms.CharField(
                        widget=forms.widgets.TextInput,
                        required=required)


class GroupsPropertiesRelationInlineForm(forms.ModelForm):

    class Meta:
        widgets = {
            'property': LinkedSelect,
            'group': LinkedSelect,
        }


class PropertyValueIconForm(forms.ModelForm):

    class Meta:
        widgets = {
            'category': LinkedSelect,
            'properties':
            autocomplete_light.MultipleChoiceWidget('PropertyAutocomplete'),
            'expression': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
        }


class StaticBlockForm(forms.ModelForm):

    class Meta:
        widgets = {
            'html': CKEditorWidget(editor_options={'startupFocus': True}),
        }


class PropertySetRelationForm(forms.ModelForm):
    property = forms.ModelChoiceField(
        Property.objects.all(),
        label=_(u'Property'),
        widget=autocomplete_light.ChoiceWidget('PropertyAutocomplete'))


class PropertyTypeForm(forms.ModelForm):

    class Meta:
        model = PropertyType
        widgets = {
            'categories':
            autocomplete_light.MultipleChoiceWidget('CategoryAutocomplete'),
            'property':
            autocomplete_light.ChoiceWidget('PropertyAutocomplete'),
            'unit':
            autocomplete_light.ChoiceWidget('PropertyUnitAutocomplete'),
            'available_units':
            autocomplete_light.MultipleChoiceWidget(
                'PropertyUnitAutocomplete'),
        }


class PropertyOptionRelationForm(forms.ModelForm):

    class Meta:
        model = PropertyOptionRelation
        widgets = {
            'property_option': autocomplete_light.ChoiceWidget(
                'PropertyOptionAutocomplete')
        }

    def __init__(self, *args, **kwargs):
        if 'initial' not in kwargs:
            kwargs['initial'] = {}
        try:
            property_type_id = kwargs['initial']['property_type_id']
            property_type = PropertyType.objects.get(pk=property_type_id)
            kwargs['initial']['property_type'] = property_type
        except:
            pass
        super(PropertyOptionRelationForm, self).__init__(*args, **kwargs)


class CategoryTemplateForm(forms.Form):
    template = forms.ChoiceField(
        label=_(u'Template'),
        choices=[(ct[0], ct[1]['name']) for ct in CATEGORY_TEMPLATES]
    )
