# coding: utf-8
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

from adminconfig.utils import BaseConfig


PRODUCT_MODE_CHOICES = (
    ('list', _(u'List')),
    ('table', _(u'Table')),
)


class CatalogConfigForm(forms.Form):
    default_category_mode = forms.ChoiceField(
        label=_(u'Viewing Mode'),
        widget=forms.Select(),
        choices=PRODUCT_MODE_CHOICES,
    )
    show_products_with_variants = forms.BooleanField(
        label=_(u'Show products with variants'),
        help_text=_(
            u'Use grouping for variants in category view.'),
        required=False)
    hide_variants = forms.BooleanField(
        label=_(u'Hide variants'),
        required=False
    )
    make_301_redirect_for_invalid_page = forms.BooleanField(
        label=_(u'Make 301 redirect for invalid page'),
        help_text=_(
            u'If start parameter value is greater than count of pages.'),
        required=False)
    show_404_for_invalid_page = forms.BooleanField(
        label=_(u'Show 404 error for invalid page'),
        help_text=_(
            u'If start parameter value is greater than count of pages.'),
        required=False)
    show_filter_counters = forms.BooleanField(
        label=_(u'Show counters in filters'),
        required=False)
    smart_define_property_type = forms.BooleanField(
        label=_(u'Smart define property type'),
        required=False
    )
    ignore_empty_property = forms.BooleanField(
        label=_(u'Ignore empty property'),
        required=False
    )
    filter_show_all_products = forms.BooleanField(
        label=_(u'Filter show all products'),
        required=False
    )
    watermark_x_delta = forms.IntegerField(label=_(u'Watermark X delta'))
    watermark_y_delta = forms.IntegerField(label=_(u'Watermark Y delta'))
    product_cant_be_bought_message = forms.CharField(
        label=_(u'Product cant be bought message'),
        help_text=_(u'text in modal cart if product cant be bought'),
        required=False)


class CatalogConfig(BaseConfig):
    """Configurator for catalog options in config.
    """
    form_class = CatalogConfigForm
    block_name = 'catalog'

    def __init__(self):
        super(CatalogConfig, self).__init__()

        self.default_data = {
            'DEFAULT_CATEGORY_MODE': settings.DEFAULT_CATEGORY_MODE,
            'CATEGORY_SHOW_PRODUCT_WITH_VARIANTS':
            settings.CATEGORY_SHOW_PRODUCT_WITH_VARIANTS,
            'CATEGORY_MAKE_301_REDIRECT_FOR_INVALID_PAGE':
            settings.CATEGORY_MAKE_301_REDIRECT_FOR_INVALID_PAGE,
            'CATEGORY_SHOW_404_FOR_INVALID_PAGE':
            settings.CATEGORY_SHOW_404_FOR_INVALID_PAGE,
            'SHOW_FILTER_COUNTERS': settings.SHOW_FILTER_COUNTERS,
            'WATERMARK_X_DELTA': settings.WATERMARK_X_DELTA,
            'WATERMARK_Y_DELTA': settings.WATERMARK_Y_DELTA,
            'CATEGORY_HIDE_VARIANTS': False,
            'SMART_DEFINE_PROPERTY_TYPE': settings.SMART_DEFINE_PROPERTY_TYPE,
            'IGNORE_EMPTY_PROPERTY': settings.IGNORE_EMPTY_PROPERTY,
            'FILTER_SHOW_ALL_PRODUCTS': settings.FILTER_SHOW_ALL_PRODUCTS,
            'PRODUCT_CANT_BE_BOUGHT_MESSAGE':
            settings.PRODUCT_CANT_BE_BOUGHT_MESSAGE,
        }

        self.option_translation_table = (
            ('DEFAULT_CATEGORY_MODE', 'default_category_mode'),
            (
                'CATEGORY_SHOW_PRODUCT_WITH_VARIANTS',
                'show_products_with_variants',
            ),
            (
                'CATEGORY_MAKE_301_REDIRECT_FOR_INVALID_PAGE',
                'make_301_redirect_for_invalid_page',
            ),
            (
                'CATEGORY_SHOW_404_FOR_INVALID_PAGE',
                'show_404_for_invalid_page',
            ),
            (
                'SHOW_FILTER_COUNTERS',
                'show_filter_counters',
            ),
            (
                'WATERMARK_X_DELTA',
                'watermark_x_delta',
            ),
            (
                'WATERMARK_Y_DELTA',
                'watermark_y_delta',
            ),
            ('CATEGORY_HIDE_VARIANTS', 'hide_variants'),
            ('SMART_DEFINE_PROPERTY_TYPE', 'smart_define_property_type'),
            ('IGNORE_EMPTY_PROPERTY', 'ignore_empty_property'),
            ('FILTER_SHOW_ALL_PRODUCTS', 'filter_show_all_products'),
            (
                'PRODUCT_CANT_BE_BOUGHT_MESSAGE',
                'product_cant_be_bought_message',
            ),
        )
