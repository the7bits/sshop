# coding: utf-8


def rebuild_category_counters(*args, **kwargs):
    """Rebuild product counters for all categories.
    """
    from .models import Category
    categories = Category.objects.all()
    for c in categories.iterator():
        c.recount_products(save=True)
