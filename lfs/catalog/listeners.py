# coding: utf-8
from django.conf import settings
from django.db.models.signals import post_save, m2m_changed

from .models import (
    Category,
    PropertyType,
)
from .settings import (
    PROPERTY_TYPE_FLOAT,
    PROPERTY_TYPE_INTEGER,
)


def property_type_post_save_listener(sender, instance, **kwargs):
    if instance.type in [PROPERTY_TYPE_FLOAT, PROPERTY_TYPE_INTEGER]:
        property = instance.property
        p_values = property.property_values.all()
        for pv in p_values:
            pv.save()

post_save.connect(property_type_post_save_listener, sender=PropertyType)


def category_products_changed_listener(sender, instance, action, **kwargs):
    from tasks.api import TaskQueueManager
    tq = TaskQueueManager()
    tq.schedule(
        'lfs.catalog.jobs.rebuild_category_counters',
        priority=getattr(settings, 'JOB_REBUILD_COUNTERS_PRIORITY', 1),
    )

m2m_changed.connect(
    category_products_changed_listener, sender=Category.products.through)
