# python imports
import locale

# django imports
# from django.contrib.auth.models import User
from django.contrib.auth.models import AnonymousUser
from django.contrib.sessions.backends.file import SessionStore
# from django.shortcuts import get_object_or_404
from django.test import TestCase
# from django.test.client import Client

# test imports
from lfs.catalog.models import Product
from lfs.cart.models import Cart
from lfs.cart.models import CartItem
# from lfs.cart.views import add_to_cart
from ..cart.utils import get_cart
# from lfs.core.models import Country
from lfs.customer.models import Address
from lfs.customer.models import Customer
from lfs.order.models import Order
from lfs.order.models import OrderItem
from lfs.order.utils import add_order
from lfs.order.settings import SUBMITTED
from ..payment.models import PaymentMethod
from ..shipping.models import ShippingMethod
# from lfs.tax.modwels import Tax
# from lfs.tests.utils import DummySession
from django.test.client import RequestFactory


class OrderTestCase(TestCase):
    """
    """
    fixtures = ['lfs_shop.xml']

    def setUp(self):
        """
        """
        session = SessionStore()
        session.save()

        rf = RequestFactory()
        self.request = rf.get('/')
        self.request.session = session
        self.request.user = AnonymousUser()

        shipping_method = ShippingMethod.objects.get(pk=1)

        payment_method = PaymentMethod.objects.get(pk=1)

#         us = Country.objects.get(code="us")
#         ie = Country.objects.get(code="ie")

#         address1 = Address.objects.create(
#             firstname="John",
#             lastname="Doe",
#             company_name="Doe Ltd.",
#             line1="Street 42",
#             city="Gotham City",
#             zip_code="2342",
#             country=ie,
#             phone="555-111111",
#             email="john@doe.com",
#         )

#         address2 = Address.objects.create(
#             firstname="Jane",
#             lastname="Doe",
#             company_name="Doe Ltd.",
#             line1="Street 43",
#             city="Smallville",
#             zip_code="2443",
#             country=us,
#             phone="666-111111",
#             email="jane@doe.com",
#         )

        self.customer = Customer.objects.create(
            session=session.session_key,
            selected_shipping_method=shipping_method,
            selected_payment_method=payment_method,
        )

        self.p1 = Product.objects.create(
            name="Product 1",
            slug="product-1",
            sku="sku-1",
            price=1.1,
            active=True,
        )

        self.p2 = Product.objects.create(
            name="Product 2",
            slug="product-2",
            sku="sku-2",
            price=2.2,
            active=True,
        )

        cart = Cart.objects.create(
            session=session.session_key
        )

        item1 = CartItem.objects.create(
            cart=cart,
            product=self.p1,
            amount=2,
        )

        item2 = CartItem.objects.create(
            cart=cart,
            product=self.p2,
            amount=3,
        )

    def test_add_order(self):
        """Tests the general adding of an order via the add_order method
        """
        order = add_order(self.request)

        # self.assertEqual(order.state, SUBMITTED)
        self.assertEqual("%.2f" % order.price, "9.80")

        self.assertEqual(order.shipping_method.name, "Default shipping")
        self.assertEqual(order.shipping_price, 1.0)

        self.assertEqual(order.payment_method.name, "Direct debit")
        self.assertEqual(order.payment_price, 0.0)

#         self.assertEqual(order.shipping_firstname, "John")
#         self.assertEqual(order.shipping_lastname, "Doe")
#         self.assertEqual(order.shipping_line1, "Street 42")
#         self.assertEqual(order.shipping_line2, None)
#         self.assertEqual(order.shipping_city, "Gotham City")
#         self.assertEqual(order.shipping_code, "2342")
#         self.assertEqual(order.shipping_phone, "555-111111")
#         self.assertEqual(order.shipping_company_name, "Doe Ltd.")

#         self.assertEqual(order.invoice_firstname, "Jane")
#         self.assertEqual(order.invoice_lastname, "Doe")
#         self.assertEqual(order.invoice_line1, "Street 43")
#         self.assertEqual(order.invoice_line2, None)
#         self.assertEqual(order.invoice_city, "Smallville")
#         self.assertEqual(order.invoice_code, "2443")
#         self.assertEqual(order.invoice_phone, "666-111111")
#         self.assertEqual(order.invoice_company_name, "Doe Ltd.")

        # Items
        self.assertEqual(len(order.items.all()), 2)

        item = order.items.all().order_by('id')[0]
        self.assertEqual(item.product_amount, 2)
        self.assertEqual(item.product_sku, "sku-1")
        self.assertEqual(item.product_name, "Product 1")
        self.assertEqual("%.2f" % item.product_price, "1.10")

        item = order.items.all().order_by('id')[1]
        self.assertEqual(item.product_amount, 3)
        self.assertEqual(item.product_sku, "sku-2")
        self.assertEqual(item.product_name, "Product 2")
        self.assertEqual("%.2f" % item.product_price, "2.20")

        # The cart should be deleted after the order has been created
        cart = get_cart(self.request)
        self.assertEqual(cart, None)

    def test_pay_link(self):
        """Tests empty pay link.
        """
        locale.setlocale(locale.LC_ALL, 'en_US.UTF-8')
        from lfs.payment.utils import process_payment
        result = process_payment(self.request)

        order = Order.objects.filter()[0]
        self.assertEqual(order.get_pay_link(self.request), "")

#     def test_paypal_link(self):
#         """Tests created paypal link.
#         """
#         payment_method, created = PaymentMethod.objects.get_or_create(
#             id=3,
#             name="PayPal",
#             active=True,
#         )

#         self.customer.selected_payment_method = payment_method
#         self.customer.save()

#         from lfs.payment.utils import process_payment
#         result = process_payment(self.request)

#         order = Order.objects.filter()[0]
#         self.failIf(order.get_pay_link(self.request).find("paypal") == -1)

    def test_delete_product(self):
        """Tests that OrderItems are not deleted when a product is deleted.
        """
        shipping_method = ShippingMethod.objects.get(pk=1)
        payment_method = PaymentMethod.objects.get(pk=1)

        order = Order.objects.create(
            shipping_method=shipping_method,
            payment_method=payment_method)
        order_item_1 = OrderItem.objects.create(
            order=order, product=self.p1)
        self.p1.delete()
        OrderItem.objects.get(pk=order_item_1.id)
