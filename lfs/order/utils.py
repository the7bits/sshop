# coding: utf-8
import string

from django.conf import settings
from django.contrib.auth.models import User

from ..core.signals import order_created
from ..core.utils import import_symbol, get_default_shop
from ..voucher.models import Voucher
from ..mail.utils import send_customer_added
from ..customer.utils import get_customer
from ..cart.utils import get_cart
from ..shipping.utils import (
    get_selected_shipping_method,
    get_shipping_costs,
)
from ..payment.utils import (
    get_selected_payment_method,
    get_payment_costs,
)
from ..discounts.utils import get_valid_discounts
from ..voucher.utils import get_current_voucher_number
from ..customer.models import Customer
from .models import Order, OrderItem


def add_order(
        request,
        checkout_form=None, shipping_form=None, payment_form=None):
    """Adds an order based on current cart for the current customer.

    It assumes that the customer is prepared with all needed information. This
    is within the responsibility of the checkout form.
    """
    from ..fields.utils import get_field_object_list
    shop = get_default_shop(request)
    customer = get_customer(request)
    auth_user = None
    order = None

    cart = get_cart(request)
    if cart is None:
        return order

    shipping_method = get_selected_shipping_method(request)
    shipping_costs = get_shipping_costs(request, shipping_method)

    payment_method = get_selected_payment_method(request)
    payment_costs = get_payment_costs(request, payment_method)

    if request.user.is_authenticated():
        user = request.user
    else:
        user = None

    # Calculate the totals
    price = cart.get_price(request) + shipping_costs["price"] + \
        payment_costs["price"] / payment_costs['ratio']

    # Discounts
    discounts = get_valid_discounts(request)
    for discount in discounts:
        price = price - discount["price"]

    # Add voucher if one exists
    try:
        voucher_number = get_current_voucher_number(request)
        voucher = Voucher.objects.get(number=voucher_number)
    except Voucher.DoesNotExist:
        voucher = None
    else:
        is_voucher_effective, voucher_message = \
            voucher.is_effective(request, cart)
        if is_voucher_effective:
            voucher_number = voucher.number
            voucher_price = voucher.get_price(request, cart)

            price -= voucher_price
        else:
            voucher = None

    customer_email = ''
    if checkout_form is not None:
        if shop.ask_email_when_checkout:
            customer_email_choice = \
                checkout_form.cleaned_data.get('email_choice')
            customer_email = ''

            if customer_email_choice == '!NEW' \
                    or customer_email_choice is None:
                customer_email = checkout_form.cleaned_data.get('email')
            else:
                _email = customer.emails.get(pk=customer_email_choice)
                customer_email = _email.email

            if not customer_email:
                try:
                    _email = customer.emails.get(default=True)
                    customer_email = _email.email
                except:
                    customer_email = ''

    customer_phone = ''
    if checkout_form is not None:
        if shop.ask_phone_when_checkout:
            customer_phone_choice = \
                checkout_form.cleaned_data.get('phone_choice')
            customer_phone = ''

            if customer_phone_choice == '!NEW' \
                    or customer_phone_choice is None:
                customer_phone = checkout_form.cleaned_data.get('phone')
            else:
                _phone = customer.phones.get(pk=customer_phone_choice)
                customer_phone = _phone.number
            if not customer_phone:
                try:
                    _phone = customer.phones.get(default=True)
                    customer_phone = _phone.number
                except:
                    customer_phone = ''

    fields = get_field_object_list(shop, request=request)
    customer_data = {}
    customer_fields = shop.checkout_form_fields.split(',')
    customer_data = customer.get_info()
    if checkout_form is not None:
        for f in fields:
            if f['name'] in customer_fields:
                customer_data[f['name']] = \
                    checkout_form.cleaned_data.get(f['name'])

    order_fields = shop.order_form_fields.split(',')
    order_data = {}

    if checkout_form is not None:
        for f in fields:
            if f['name'] in order_fields:
                order_data[f['name']] = \
                    checkout_form.cleaned_data.get(f['name'])

    shipping_data = {}
    if shipping_form is not None:
        shipping_data = shipping_form.cleaned_data

    payment_data = {}
    if payment_form is not None:
        payment_data = payment_form.cleaned_data

    # If user is anonymous and background registration is active we
    # have to create the new user
    if user is None and shop.make_bg_registration:
        from django.contrib.auth import login, authenticate
        auth_type = getattr(settings, 'CUSTOMER_AUTH_BY', 'email')
        password = User.objects.make_random_password(
            length=8,
            allowed_chars=string.letters+string.digits)

        if auth_type == 'email' and customer_email:
            if User.objects.filter(username=customer_email).count() == 0:
                user = User.objects.create_user(
                    username=customer_email,
                    email=customer_email)
                user.set_password(password)
                user.save()
                # A new customer automatically creates when we create an user,
                # we should delete it before reassign the available customer.
                nc = Customer.objects.filter(user=user)
                nc.delete()
                send_customer_added(user, password)
                # Reassign the available customer to the new user
                customer.user = user
                customer.save()
                auth_user = authenticate(
                    username=customer_email, password=password)
                login(request, auth_user)
        elif auth_type == 'phone' and customer_phone:
            if User.objects.filter(username=customer_phone).count() == 0:
                user = User.objects.create_user(
                    username=customer_phone,
                    email=customer_email)
                user.set_password(password)
                user.save()
                # A new customer automatically creates when we create an user,
                # we should delete it before reassign the available customer.
                nc = Customer.objects.filter(user=user)
                nc.delete()
                # Trying to send email with password if email is specified
                if customer_email:
                    send_customer_added(user, password)
                # Reassign the available customer to the new user
                customer.user = user
                customer.save()
                auth_user = authenticate(
                    username=customer_phone, password=password)
                login(request, auth_user)

                # Send password to customer phone via SMS
                # Works only if special plugin is installed
                from sbits_plugins import mediator
                from django.template.loader import render_to_string
                from django.contrib.sites.models import Site
                site_url = Site.objects.get(pk=settings.SITE_ID)

                mediator.publish(
                    None,
                    'message_sender:sms',
                    '',
                    site_url,
                    render_to_string('lfs/sms/user_register_sms.txt', {
                        'site': site_url,
                        'user': customer_phone,
                        'password': password,
                    }),
                    [customer_phone],
                )

    order = Order.objects.create(
        customer=customer,
        customer_email=customer_email,
        customer_phone=customer_phone,
        customer_data=customer_data,
        extra_data=order_data,
        price=price,
        export_status=10,
        shipping_method=shipping_method,
        shipping_price=shipping_costs["price"],
        shipping_data=shipping_data,

        payment_method=payment_method,
        payment_price=payment_costs["price"]/payment_costs["ratio"],
        payment_data=payment_data,

        message=request.POST.get("message", ""),
        # Maybe will be deprecated soon
        user=user,
        session=request.session.session_key,
    )

    if voucher:
        voucher.mark_as_used()
        order.voucher_number = voucher_number
        order.voucher_price = voucher_price
        order.save()

    order.save()

    # Copy cart items
    for cart_item in cart.get_items():
        OrderItem.objects.create(
            order=order,
            price=cart_item.get_price_total(request),
            product=cart_item.product,
            product_sku=cart_item.product.sku,
            product_name=cart_item.product.get_name()[:100],
            product_amount=cart_item.amount,
            product_price=cart_item.product.get_price(request),
        )

    for discount in discounts:
        OrderItem.objects.create(
            order=order,
            price=-discount["price"],
            product_sku=discount["sku"],
            product_name=discount["name"],
            product_amount=1,
            product_price=-discount["price"],
        )


    # Note: Save order for later use in thank you page. The order will be
    # removed from the session if the thank you page has been called.
    request.session["order"] = order

    ong = import_symbol(settings.LFS_ORDER_NUMBER_GENERATOR)
    try:
        order_numbers = ong.objects.get(id="order_number")
    except ong.DoesNotExist:
        order_numbers = ong.objects.create(id="order_number")

    try:
        order_numbers.init(request, order)
    except AttributeError:
        pass

    order.number = order_numbers.get_next()
    order.save()

    # Send signal before cart is deleted.
    order_created.send(
        {"order": order, "cart": cart, "request": request})
    cart.delete()
    # Edit customer
    if customer_email:
        customer.add_email(customer_email)

    if customer_phone:
        customer.add_phone(customer_phone)

    # If checkout is anonymous than looking for the registered
    # customer with the similar data and reattach order to him.
    if user is None and shop.allow_previous_data:
        customer_finder_class = import_symbol(settings.CUSTOMER_FINDER)
        f = customer_finder_class(customer)
        new_customer = f.find()
        if new_customer is not None:
            order.customer = new_customer
            order.user = new_customer.user
            order.save()
    order.export_status = 0
    order.save()
    return order
