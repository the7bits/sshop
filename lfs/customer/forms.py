# coding: utf-8
from django import forms
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _

from ..core.utils import get_default_shop, import_symbol
from .models import Customer, Phone, EmailAddress
from .fields import AcceptRulesField


class RegisterForm(forms.Form):
    """Form to register a customer.
    """

    def __init__(self, request=None, *args, **kwargs):
        from ..fields.utils import get_field_object_list
        shop = get_default_shop()
        super(RegisterForm, self).__init__(*args, **kwargs)

        if getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'email':
            self.fields['username'] = forms.EmailField(
                label=getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username')),
                widget=forms.TextInput(
                    attrs={
                        'placeholder':
                        getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
                    })
            )
        else:
            self.fields['username'] = forms.CharField(
                label=getattr(settings, 'CUSTOMER_AUTH_LABEL', _(u'Username')),
                widget=forms.TextInput(
                    attrs={
                        'placeholder':
                        getattr(settings, 'CUSTOMER_AUTH_PLACEHOLDER', '')
                    })
            )

        self.fields['password_1'] = forms.CharField(
            label=_(u'Password'),
            widget=forms.PasswordInput())
        self.fields['password_2'] = forms.CharField(
            label=_(u'Confirm password'),
            widget=forms.PasswordInput())

        if shop.ask_email_when_register and \
                getattr(settings, 'CUSTOMER_AUTH_BY', 'email') != 'email':
            self.fields['email'] = forms.EmailField(
                label=_(u'Email'),
                required=False,
            )
        if shop.ask_phone_when_register and \
                getattr(settings, 'CUSTOMER_AUTH_BY', 'email') != 'phone':
            self.fields['phone'] = forms.CharField(
                label=_(u'Phone'),
                required=False,
                help_text=getattr(settings, 'CUSTOMER_PHONE_HELP_TEXT', ''),
                widget=forms.TextInput(
                    attrs={
                        'placeholder':
                        getattr(settings, 'CUSTOMER_PHONE_PLACEHOLDER', '')
                    })
            )

        reg_fields = shop.registration_form_fields.split(',')
        fields = [
            x for x in get_field_object_list(shop, request=request)
            if x['name'] in reg_fields]
        for f in fields:
            self.fields[f['name']] = f['obj']

        active_captcha = getattr(settings, 'ACTIVE_CAPTCHA', '')
        if active_captcha and \
                getattr(settings, 'CAPTCHA_USE_FOR_REGISTER', False):
            captcha_class = import_symbol(active_captcha)
            self.fields['captcha'] = captcha_class()
            self.fields['captcha'].label = _(u'Captcha')

        if shop.require_accept_rules:
            self.fields['require_accept_rules'] = AcceptRulesField(
                label='', custom_text=shop.link_to_rules)

    def clean_password_2(self):
        """Validates that password 1 and password 2 are the same.
        """
        p1 = self.cleaned_data.get('password_1')
        p2 = self.cleaned_data.get('password_2')

        if not (p1 and p2 and p1 == p2):
            raise forms.ValidationError(_(u"The two passwords do not match."))

        return p2

    def clean_email(self):
        """Validates that the entered e-mail is unique.
        """
        email = self.cleaned_data.get("email")

        if email and User.objects.filter(
                Q(email=email) | Q(username=email)).count() > 0:
            raise forms.ValidationError(
                _(u"That email address is already in use."))

        try:
            EmailAddress.objects.get(email=email, default=True)
            raise forms.ValidationError(
                _(u"That email address is already in use."))
        except EmailAddress.DoesNotExist:
            pass

        return email

    def clean_phone(self):
        """ Validates that the entered phone number is unique
        """
        if getattr(settings, 'REGISTRATION_VALIDATE_PHONE', True):
            import phonenumbers
            phone = self.cleaned_data.get('phone')
            if not phone:
                return phone  # This field is not required

            try:
                phone_obj = phonenumbers.parse(
                    phone,
                    getattr(settings, 'CUSTOMER_PHONE_NUMBER_REGION', 'UA'),
                )
            except phonenumbers.NumberParseException:
                raise forms.ValidationError(
                    _(u'The string is not a valid phone number.'))

            phone_str = '%s%s' % (
                phone_obj.country_code,
                phone_obj.national_number,
            )

            phone_type = phonenumbers.number_type(phone_obj)
            if phone_type == 99:
                raise forms.ValidationError(
                    _(u"Invalid phone number."))
        else:
            phone_str = str(phone)

        try:
            User.objects.get(username=phone_str)
            raise forms.ValidationError(_(u"That phone is already in use"))
        except User.DoesNotExist:
            pass

        try:
            Phone.objects.get(number=phone_str, default=True)
            raise forms.ValidationError(
                _(u"That phone is already in use"))
        except Phone.DoesNotExist:
            pass

        return phone

    def clean_username(self):
        """Validates that the entered username is unique
        """
        username = self.cleaned_data.get('username')

        if getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'email':
            try:
                EmailAddress.objects.get(email=username, default=True)
                raise forms.ValidationError(
                    _(u"That email address is already in use."))
            except EmailAddress.DoesNotExist:
                pass
        elif getattr(settings, 'CUSTOMER_AUTH_BY', 'email') == 'phone':
            if getattr(settings, 'REGISTRATION_VALIDATE_PHONE', True):
                import phonenumbers
                try:
                    phone_obj = phonenumbers.parse(
                        username,
                        getattr(settings, 'CUSTOMER_PHONE_NUMBER_REGION', 'UA'),
                    )
                except phonenumbers.NumberParseException:
                    raise forms.ValidationError(
                        _(u'The string is not a valid phone number.'))

                phone_str = '%s%s' % (
                    phone_obj.country_code,
                    phone_obj.national_number,
                )

                phone_type = phonenumbers.number_type(phone_obj)
                if phone_type == 99:
                    raise forms.ValidationError(
                        _(u"Invalid phone number."))
            else:
                phone_str = str(username)

            try:
                Phone.objects.get(number=phone_str, default=True)
                raise forms.ValidationError(
                    _(u"That phone is already in use"))
            except Phone.DoesNotExist:
                pass

            if username != phone_str:
                raise forms.ValidationError(
                    _(
                        u'Canonical form is required for login. '
                        u'For example: 380671111111.'))

        try:
            User.objects.get(username=username)
            raise forms.ValidationError(_(u"That user is already exists"))
        except User.DoesNotExist:
            pass

        return username

    def clean_referral_code(self):
        """Validates the referral code.
        """
        ref_code = self.cleaned_data.get('referral_code')
        if ref_code:
            try:
                Customer.objects.get(ref_code=ref_code)
                return ref_code
            except Customer.DoesNotExist:
                raise forms.ValidationError(
                    _(u'That referral code does not exist.'))
        return ref_code


class PhoneForm(forms.ModelForm):
    """ Form to add new phone number
    """
    class Meta:
        model = Phone
        fields = ('number', 'type')
        widgets = {
            'number': forms.TextInput(
                attrs={
                    'placeholder':
                    getattr(settings, 'CUSTOMER_PHONE_PLACEHOLDER', '')
                }),
        }

    def clean_number(self):
        if getattr(settings, 'REGISTRATION_VALIDATE_PHONE', True):
            import phonenumbers
            number = self.cleaned_data.get('number')
            try:
                phone_obj = phonenumbers.parse(
                    number,
                    getattr(settings, 'CUSTOMER_PHONE_NUMBER_REGION', 'UA'),
                )
            except phonenumbers.NumberParseException:
                raise forms.ValidationError(
                    _(u'The string is not a valid phone number.'))

            phone_str = '%s%s' % (
                phone_obj.country_code,
                phone_obj.national_number,
            )

            phone_type = phonenumbers.number_type(phone_obj)
            if phone_type == 99:
                raise forms.ValidationError(
                    _(u"Invalid phone number."))
        else:
            phone_str = str(number)

        try:
            User.objects.get(username=phone_str)
            raise forms.ValidationError(_(u"That phone is already in use"))
        except User.DoesNotExist:
            pass

        try:
            Phone.objects.get(number=phone_str, default=True)
            raise forms.ValidationError(
                _(u"That phone is already in use"))
        except Phone.DoesNotExist:
            pass

        return phone_str


class AddressForm(forms.Form):
    """Form for edit an address.
    """

    def __init__(self, request=None, *args, **kwargs):
        from ..fields.utils import get_field_object_list
        shop = get_default_shop()
        super(AddressForm, self).__init__(*args, **kwargs)

        address_fields = shop.address_form_fields.split(',')
        fields = [
            x for x in get_field_object_list(shop, request=request)
            if x['name'] in address_fields]
        for f in fields:
            self.fields[f['name']] = f['obj']


class EmailForm(forms.ModelForm):
    """ Form to add new phone number
    """
    class Meta:
        model = EmailAddress
        fields = ('email',)

    def clean_email(self):
        """Validates that the entered e-mail is unique.
        """
        email = self.cleaned_data.get("email")

        if email and User.objects.filter(
                Q(email=email) | Q(username=email)).count() > 0:
            raise forms.ValidationError(
                _(u"That email address is already in use."))

        try:
            EmailAddress.objects.get(email=email, default=True)
            raise forms.ValidationError(
                _(u"That email address is already in use."))
        except EmailAddress.DoesNotExist:
            pass

        return email


class EditAccountForm(forms.Form):
    """A dinamic form for edit customer account.
    """
    def __init__(self, request=None, *args, **kwargs):
        from ..fields.utils import get_field_object_list
        shop = get_default_shop()
        super(EditAccountForm, self).__init__(*args, **kwargs)

        address_fields = shop.address_form_fields.split(',')
        order_fields = shop.order_form_fields.split(',')

        fields = [
            x for x in get_field_object_list(shop, request=request)
            if x['name'] not in address_fields and
            x['name'] not in order_fields and
            x['name'] != 'referral_code']
        for f in fields:
            self.fields[f['name']] = f['obj']
