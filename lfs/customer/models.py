# coding: utf-8
from jsonfield import JSONField

from django.conf import settings
from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _, pgettext

from ..shipping.models import ShippingMethod
from ..payment.models import PaymentMethod
from ..core.utils import get_default_shop
from ..core.signals import email_added


class Customer(models.Model):
    """
    * A customer holds all shop customer related information and is assigned to
      a Django user and/or a session dependend on the login state of
      the current user.
    * A customer is only created when it needs to. Either when:
       - the cart is refreshed (this is because some of the customer related
         information could be changed like shipping/payment method or shipping
         address)
       - the customer browses to the check out page
    """
    user = models.ForeignKey(User, blank=True, null=True)
    session = models.CharField(blank=True, max_length=100)

    selected_shipping_method = models.ForeignKey(
        ShippingMethod,
        verbose_name=_(u"Selected shipping method"),
        blank=True, null=True, related_name="selected_shipping_method")
    selected_payment_method = models.ForeignKey(
        PaymentMethod,
        verbose_name=_(u"Selected payment method"),
        blank=True, null=True, related_name="selected_payment_method")

    first_name = models.CharField(_(u'First name'), max_length=100, blank=True)
    last_name = models.CharField(_(u'Last name'), max_length=100, blank=True)
    full_name = models.CharField(_(u'Full name'), max_length=255, blank=True)
    date_of_birth = models.DateField(
        _(u'Date of birthday'), blank=True, null=True)
    ref_code = models.CharField(
        _(u'Referral code'), max_length=100, blank=True)

    saving_account = models.FloatField(
        _(u'Saving account'), default=0.0,
        help_text=_(u'Internal account for accumulative discounts.'))

    extras = JSONField(_(u'Extra data'), default='{}', blank=True)

    class Meta:
        verbose_name = _(u'Customer')
        verbose_name_plural = _(u'Customers')

    def __unicode__(self):
        return "%s/%s" % (self.user, self.session)

    def save(self, *args, **kwargs):
        """Automatically initialize full name etc.
        """
        if (not self.full_name) and (self.first_name and self.last_name):
            self.full_name = self.first_name + ' ' + self.last_name
        if not (self.first_name and self.last_name):
            names = self.full_name.split()
            try:
                self.first_name = names[0]
                self.last_name = names[-1]
            except IndexError:
                pass
        super(Customer, self).save(*args, **kwargs)

    def is_filled(self):
        t = False
        if self.first_name or self.last_name or self.full_name \
                or self.date_of_birth or self.extras:
            t = True

        if Phone.objects.filter(customer=self).exists() or \
                EmailAddress.objects.filter(customer=self).exists() or \
                Address.objects.filter(customer=self).exists():
            t = True
        return t

    def get_full_name(self):
        return self.full_name

    def set_variable(self, variable, value):
        data = self.extras
        data.update({variable: value})
        self.extras = data

    def get_variable(self, variable, address=None):
        data = self.extras

        if address is None:
            address = self.get_default_address()
        if address:
            data.update(address.fields)
        return data.get(variable, None)

    def set_info(self, data):
        shop = get_default_shop()
        address_fields = shop.address_form_fields.split(',')
        for item in data.keys():
            if item not in address_fields:
                if item == 'first_name':
                    self.first_name = data[item]
                elif item == 'last_name':
                    self.last_name = data[item]
                elif item == 'full_name':
                    self.full_name = data[item]
                elif item == 'date_of_birth':
                    self.date_of_birth = data[item]
                elif item == 'referral_code':
                    self.ref_code = data[item]
                else:
                    self.set_variable(item, data[item])

        if 'first_name' in data and 'last_name' in data \
                and 'full_name' not in data:
            self.full_name = self.first_name + ' ' + self.last_name

    def get_info(self):
        import datetime
        date_of_birth = None
        if self.date_of_birth is not None:
            date_of_birth = datetime.datetime.fromordinal(
                self.date_of_birth.toordinal())

        info = {
            'first_name': self.first_name,
            'last_name': self.last_name,
            'full_name': self.full_name,
            'date_of_birth': date_of_birth,
            'referral_code': self.ref_code,
        }
        info.update(self.extras)
        return info

    def get_default_address(self):
        try:
            return self.addresses.filter(default=True)[0]
        except:
            return None

    def add_phone(self, phone):
        import phonenumbers
        try:
            phone_obj = phonenumbers.parse(
                phone,
                getattr(settings, 'CUSTOMER_PHONE_NUMBER_REGION', 'UA'),
            )
        except phonenumbers.NumberParseException:
            return None

        phone_str = '%s%s' % (
            phone_obj.country_code,
            phone_obj.national_number,
        )

        phone_type = phonenumbers.number_type(phone_obj)
        if phone_type == 99:
            return None
        p_type = 0
        if phone_type == 0:
            p_type = 5  # Landline
        elif phone_type == 1:
            p_type = 0  # Mobile

        try:
            Phone.objects.get(customer=self, number=phone_str)
            return None
        except Phone.DoesNotExist:
            pass

        _default = self.phones.count() == 0

        obj = Phone.objects.create(
            customer=self,
            number=phone_str,
            type=p_type,
            default=_default)
        return obj

    def add_email(self, email):
        try:
            EmailAddress.objects.get(customer=self, email=email)
            return None
        except EmailAddress.DoesNotExist:
            pass

        _default = self.emails.count() == 0

        obj = EmailAddress.objects.create(
            customer=self,
            email=email,
            default=_default)
        return obj


class EmailAddress(models.Model):
    customer = models.ForeignKey(
        Customer,
        verbose_name=_(u"Customer"),
        blank=True, null=True, related_name="emails")
    email = models.EmailField(_(u'Email'))
    default = models.BooleanField(_(u'Default'), default=False)
    verification_code = models.CharField(
        _(u'Verification code'),
        blank=True, max_length=100,
        default='VERIFIED')
    verified = models.BooleanField(_(u'Confirmed'), default=False)

    class Meta:
        ordering = ['-default']
        verbose_name = _(u'Email')
        verbose_name_plural = _(u'Emails')

    def __unicode__(self):
        return self.email

    def save(self, *args, **kwargs):
        """Automatically generate the verification code
        """
        if not self.verified:
            from random import random
            from django.utils.hashcompat import sha_constructor
            salt = sha_constructor(str(random())).hexdigest()[:5]
            confirmation_key = sha_constructor(
                salt + self.email).hexdigest()
            self.verification_code = confirmation_key
        super(EmailAddress, self).save(*args, **kwargs)


def email_added_listener(sender, instance, **kwargs):
    shop = get_default_shop()
    if not instance.verified and shop.make_bg_registration:
        email_added.send(instance)
post_save.connect(email_added_listener, sender=EmailAddress)


class Address(models.Model):
    """An address which can be used as shipping and/or invoice address.
    """
    customer = models.ForeignKey(
        Customer,
        verbose_name=_(u"Customer"),
        blank=True, null=True, related_name="addresses")
    fields = JSONField(default=dict(), blank=True)
    default = models.BooleanField(_(u'Default'), default=False)
    hash_str = models.CharField(
        _(u'Fields hash'),
        max_length=100,
        null=True, blank=True)

    class Meta:
        ordering = ['-default']
        verbose_name = _(u'Address')
        verbose_name_plural = _(u'Addresses')

    def __unicode__(self):
        return self.get_customer_address()

    def save(self, *args, **kwargs):
        """Automatically generate the hash.
        """
        self.hash_str = hash(frozenset(self.fields.items()))
        super(Address, self).save(*args, **kwargs)

    def get_customer_address(self):
        """ Return customer address as string (generate from
            template by Shop -> Registration)
        """
        from django.template import Context, Template
        shop = get_default_shop()
        t = Template(shop.template_of_address)
        c = Context(self.fields)
        return t.render(c)


class Phone(models.Model):
    """ Customer phone number
    """
    PHONE_TYPE = (
        (0, _(u'Mobile')),
        (5, pgettext(u'home phone', u'Home')),
        (10, _(u'Work')),
    )
    customer = models.ForeignKey(
        Customer,
        verbose_name=_(u'Customer'),
        blank=True, null=True, related_name='phones')

    number = models.CharField(
        pgettext(u'phone number', u'Number'),
        max_length=20,
        help_text=getattr(settings, 'CUSTOMER_PHONE_HELP_TEXT', ''),
    )
    type = models.IntegerField(_(u'Type'), default=0, choices=PHONE_TYPE)
    default = models.BooleanField(_(u'Default'), default=False)

    class Meta:
        ordering = ['-default', 'number']
        verbose_name = _(u'Phone')
        verbose_name_plural = _(u'Phones')

    def __unicode__(self):
        return u'%s' % self.number

    def get_type_str(self):
        t = dict(self.PHONE_TYPE)
        return t[self.type]

    def get_type_icon(self):
        file_name = 'icons/phone__home.png'
        if self.type == 0:
            file_name = 'icons/phone__mobile.png'
        if self.type == 5:
            file_name = 'icons/phone__home.png'
        if self.type == 10:
            file_name = 'icons/phone__work.png'
        return file_name

    def get_formatted_number(self):
        """ Get formatted phone number like "+38(055)912-12-34"
        """
        templates = getattr(
            settings,
            'CUSTOMER_PHONE_TEMPLATE',
            '+%s%s(%s%s%s)%s%s%s-%s%s-%s%s'
        ).split(',')

        for template in templates:
            try:
                formatted_phone = template % tuple(self.number)
                return formatted_phone
            except TypeError:
                pass

        return self.number

    def get_formatted_number_for_1c(self):  # Only for old export
        """ Get formatted phone number like "+38(055)912-1234"
        """
        return '+' + self.number[:2] + '(' + \
            self.number[2:5] + ')' + self.number[5:8] + \
            '-' + self.number[8:]
