# coding: utf-8
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _

from adminconfig.utils import BaseConfig


class RegistrationConfigForm(forms.Form):
    auth_enabled = forms.BooleanField(
        label=_(u'Enable auth'),
        required=False)
    validate_phone = forms.BooleanField(
        label=_(u'Validate phone'),
        required=False)
    auth_by = forms.ChoiceField(
        label=_(u'Auth by'), choices=settings.CUSTOMER_AUTH_CHOICES)
    auth_label = forms.CharField(label=_(u'Auth label'))
    auth_placeholder = forms.CharField(
        label=_(u'Auth placeholder'),
        required=False)
    phone_number_region = forms.CharField(
        label=_(u'Default phone number region'),
        help_text=_(
            u'http://www.iso.org/iso/country_codes/'
            u'iso_3166_code_lists/country_names_and_code_elements.htm'))
    phone_help_text = forms.CharField(
        label=_(u'Phone help text'),
        required=False)
    phone_placeholder = forms.CharField(
        label=_(u'Phone placeholder'),
        required=False)
    register_redirect_url = forms.CharField(label=_(u'Register redirect URL'))


class RegistrationConfig(BaseConfig):
    """Configurator for registration options in config.
    """
    form_class = RegistrationConfigForm
    block_name = 'registration'

    def __init__(self):
        super(RegistrationConfig, self).__init__()

        self.default_data = {
            'REGISTRATION_VALIDATE_PHONE': settings.REGISTRATION_VALIDATE_PHONE,
            'CUSTOMER_AUTH_BY': settings.CUSTOMER_AUTH_BY,
            'CUSTOMER_AUTH_LABEL': settings.CUSTOMER_AUTH_LABEL,
            'CUSTOMER_AUTH_PLACEHOLDER': settings.CUSTOMER_AUTH_PLACEHOLDER,
            'CUSTOMER_AUTH_ENABLED': settings.CUSTOMER_AUTH_ENABLED,
            'CUSTOMER_PHONE_NUMBER_REGION':
            settings.CUSTOMER_PHONE_NUMBER_REGION,
            'CUSTOMER_PHONE_HELP_TEXT':
            settings.CUSTOMER_PHONE_HELP_TEXT,
            'CUSTOMER_PHONE_PLACEHOLDER':
            settings.CUSTOMER_PHONE_PLACEHOLDER,
            'REGISTER_REDIRECT_URL':
            settings.REGISTER_REDIRECT_URL,
        }

        self.option_translation_table = (
            ('REGISTRATION_VALIDATE_PHONE', 'validate_phone'),
            ('CUSTOMER_AUTH_BY', 'auth_by'),
            ('CUSTOMER_AUTH_LABEL', 'auth_label'),
            ('CUSTOMER_AUTH_PLACEHOLDER', 'auth_placeholder'),
            ('CUSTOMER_AUTH_ENABLED', 'auth_enabled'),
            ('CUSTOMER_PHONE_NUMBER_REGION', 'phone_number_region'),
            ('CUSTOMER_PHONE_HELP_TEXT', 'phone_help_text'),
            ('CUSTOMER_PHONE_PLACEHOLDER', 'phone_placeholder'),
            ('REGISTER_REDIRECT_URL', 'register_redirect_url'),
        )


class LoginConfigForm(forms.Form):
    login_redirect_url = forms.CharField(label=_(u'Login redirect URL'))
    merge_customers = forms.BooleanField(
        label=_(u'Merge orders after user login'),
        required=False)


class LoginConfig(BaseConfig):
    """Configurator for login options in config.
    """
    form_class = LoginConfigForm
    block_name = 'login'

    def __init__(self):
        super(LoginConfig, self).__init__()

        self.default_data = {
            'LOGIN_REDIRECT_URL': settings.LOGIN_REDIRECT_URL,
            'CUSTOMER_AUTH_MERGE': settings.CUSTOMER_AUTH_MERGE,
        }

        self.option_translation_table = (
            ('LOGIN_REDIRECT_URL', 'login_redirect_url'),
            ('CUSTOMER_AUTH_MERGE', 'merge_customers'),
        )


class CustomerConfigForm(forms.Form):
    phone_template = forms.CharField(
        label=_(u'Phone template'),
        help_text=_(u'Use %s for insert digit. You can specify multiple\
 patterns separated by commas.'),
        widget=forms.widgets.TextInput(
            attrs={'class': 'input-xlarge'}))
    customer_finder = forms.ChoiceField(
        label=_(u'Similar customer finder'),
        help_text=_(u'Using for search customer with identical data.'),
        choices=settings.CUSTOMER_FINDERS)


class CustomerConfig(BaseConfig):
    """Configurator for customer cabinet options in config.
    """
    form_class = CustomerConfigForm
    block_name = 'customer'

    def __init__(self):
        super(CustomerConfig, self).__init__()

        self.default_data = {
            'CUSTOMER_PHONE_TEMPLATE': settings.CUSTOMER_PHONE_TEMPLATE,
            'CUSTOMER_FINDER': settings.CUSTOMER_FINDER,
        }

        self.option_translation_table = (
            ('CUSTOMER_PHONE_TEMPLATE', 'phone_template'),
            ('CUSTOMER_FINDER', 'customer_finder'),
        )
