# coding: utf-8
from lfs.core.seo import BaseSEOGenerator


class PageSEOGenerator(BaseSEOGenerator):
    def __init__(self, request, context):
        self.request = request
        self.context = context
        self.page = None
        if 'page' in self.context:
            self.page = self.context['page']

    def get_h1(self):
        if self.page:
            return self.page.get_meta_h1(
                request=self.request, context=self.context)
        return None

    def get_title(self):
        if self.page:
            return self.page.get_meta_title(
                request=self.request, context=self.context)
        return None

    def get_description(self):
        if self.page:
            return self.page.get_meta_description(
                request=self.request, context=self.context)
        return None

    def get_keywords(self):
        if self.page:
            return self.page.get_meta_keywords(
                request=self.request, context=self.context)
        return None

    def get_seo_text(self):
        if self.page and self.page.slug == self.request.path.split('/')[2]:
            return self.page.get_meta_seo_text(
                request=self.request, context=self.context)
