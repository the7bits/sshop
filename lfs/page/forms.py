# coding: utf-8
from django import forms
from .models import Page
from suit_ckeditor.widgets import CKEditorWidget
from codemirror.widgets import CodeMirrorTextarea

import autocomplete_light
from lfs.catalog.models import Category


class PageForm(forms.ModelForm):
    associated_categories = forms.ModelMultipleChoiceField(
        queryset=Category.objects.all(), required=False,
        widget=autocomplete_light.MultipleChoiceWidget(
            'CategoryAutocomplete'))

    class Meta:
        model = Page
        widgets = {
            'short_text': CKEditorWidget(),
            'body': CKEditorWidget(),
            'associated_products':
            autocomplete_light.MultipleChoiceWidget('ProductAutocomplete'),
            'meta_h1': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_title': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_keywords': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_description': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
            'meta_seo_text': CodeMirrorTextarea(mode='xml', config={
                'fixedGutter': True,
                'lineWrapping': True,
            }),
        }
