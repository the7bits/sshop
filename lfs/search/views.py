# coding: utf-8
from django.http import HttpResponse
from django.utils import simplejson
from lfs.core.utils import get_default_shop

from lfs.catalog.models import SortType


def _search(request, ajax_response=False):
    """
    """
    q = request.GET.get("q", "")

    if q == "" and ajax_response:
        result = simplejson.dumps({
            "state": "failure",
        })
        return HttpResponse(result)
    sorting = request.session.get("sorting", None)

    if not sorting:  # WTF?
        # this should not happen but I experienced it when
        # switching on/off lfs_solr which uses different
        # field specification for sorting but the same session key
        try:
            del request.session["sorting"]
        except KeyError:
            pass

        try:
            sorting = SortType.objects.all().order_by(
                'order')[0].sortable_fields
        except IndexError:
            sorting = None

    try:
        CONFIG_NAME = get_default_shop(request).default_search
    except:
        CONFIG_NAME = 'searchapi.searchconfigs.SimpleSearchConfig'

    #importing config class
    class_reference = '.'.join(CONFIG_NAME.split('.')[:-1])
    class_name = CONFIG_NAME.split('.')[-1]
    config_class = getattr(
        __import__(class_reference, fromlist=[class_name, ]), class_name)
    search_config = config_class(request)

    if ajax_response:
        return search_config.quick_search(query=q)
    else:
        return search_config.search(query=q, order_by=sorting)


def live_search(request):
    """
        Returns the unordered search result in json according
        to given query (via ajax get request)
    """
    return _search(request, ajax_response=True)


def search(request):
    """
        Returns the search result according to given query (via get request)
        ordered by the globally set sorting.
    """
    return _search(request)
