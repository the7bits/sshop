# coding: utf-8
from django import forms
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

from suit.admin import SortableModelAdmin, SortableTabularInline

from codemirror.widgets import CodeMirrorTextarea
from adminextras.admin import LinkedInline
from suit_ckeditor.widgets import CKEditorWidget
from .models import PaymentMethod
from .models import PaymentMethodPrice


class PaymentMethodForm(forms.ModelForm):
    class Meta:
        model = PaymentMethod
        widgets = {
            'description': CKEditorWidget(),
            'note': CKEditorWidget(),
            'extra': CodeMirrorTextarea(config={'fixedGutter': True}),
        }


class PaymentMethodPriceAdmin(admin.ModelAdmin):
    list_display = ('price', 'payment_method', 'priority', 'active')

    fieldsets = [
        (None, {
            'classes': ('suit-tab suit-tab-price',),
            'fields': ['active', 'price', 'payment_method'],
        }),
    ]

    suit_form_tabs = (
        ('price', _(u'Price')),
        ('criteria', _(u'Criteria')),
    )

    suit_form_includes = (
        ('admin/criteria_includes/criteria.html', 'top', 'criteria'),
    )

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )


class PaymentMethodPriceInline(SortableTabularInline, LinkedInline):
    model = PaymentMethodPrice
    suit_classes = 'suit-tab suit-tab-prices'
    sortable = 'priority'
    fields = ('price',)
    extra = 0


class PaymentMethodAdmin(SortableModelAdmin, admin.ModelAdmin):
    form = PaymentMethodForm
    list_display = ('name', 'active', 'note')
    search_fields = ['name']
    sortable = 'priority'
    add_inlines = []
    edit_inlines = [PaymentMethodPriceInline]

    add_suit_form_tabs = None
    edit_suit_form_tabs = (
        ('general', _(u'General')),
        ('criteria', _(u'Criteria')),
        ('fields', _(u'Fields')),
        ('prices', _(u'Prices')),
    )

    add_fieldsets = [(None, {
        'fields': [
            'name', 'active', 'priority', 'description',
            'note', 'image', 'price', 'currency', 'deletable',
            'module']
    })
    ]
    edit_fieldsets = [(None, {
        'classes': ('suit-tab suit-tab-general',),
        'fields': [
            'name', 'active', 'priority', 'description', 'note', 'image',
            'price', 'currency', 'deletable', 'module', 'extra']
    })
    ]

    add_suit_form_includes = None

    edit_suit_form_includes = (
        ('admin/criteria_includes/criteria.html', 'top', 'criteria'),
        ('admin/fields_includes/fields.html', 'top', 'fields'),
    )

    class Media:
        css = {
            "all": ("/static/css/loadover.css",)
        }
        js = (
            '/static/jquery/jquery.cookie.js',
            '/static/js/sco.collapse.js',
            '/static/admin/js/ajax_links.js',
            '/static/js/loadover.js',
        )

    def get_form(self, request, obj=None, **kwargs):
        if obj:
            self.fieldsets = self.edit_fieldsets
            self.suit_form_includes = self.edit_suit_form_includes
            self.suit_form_tabs = self.edit_suit_form_tabs
            self.inlines = self.edit_inlines
        else:
            self.fieldsets = self.add_fieldsets
            self.suit_form_includes = self.add_suit_form_includes
            self.suit_form_tabs = self.add_suit_form_tabs
            self.inlines = self.add_inlines
        return PaymentMethodForm

admin.site.register(PaymentMethod, PaymentMethodAdmin)
admin.site.register(PaymentMethodPrice, PaymentMethodPriceAdmin)
