# coding: utf-8
from django.core.urlresolvers import reverse

from ..core.signals import order_submitted
from .models import PaymentMethod
from .settings import PM_ORDER_IMMEDIATELY
from .settings import PM_ORDER_ACCEPTED
from ..criteria.utils import (
    is_valid,
    get_first_valid,
)
from ..customer.utils import (
    get_customer,
)
from ..core.utils import (
    import_symbol,
)


def update_to_valid_payment_method(request, customer, save=False):
    """
    After this method has been called the given customer has a valid payment
    method.
    """
    valid_sms = get_valid_payment_methods(request)

    if customer.selected_payment_method not in valid_sms:
        customer.selected_payment_method = get_default_payment_method(request)
        if save:
            customer.save()


def get_valid_payment_methods(request):
    """
    Returns all valid payment methods (aka. selectable) for given request as
    list.
    """
    result = []
    for pm in PaymentMethod.objects.filter(active=True):
        if is_valid(request, pm):
            result.append(pm)
    return result


def get_default_payment_method(request):
    """
    Returns the default payment method for given request.
    """
    active_payment_methods = PaymentMethod.objects.filter(active=True)
    return get_first_valid(request, active_payment_methods)


def get_selected_payment_method(request):
    """
    Returns the selected payment method for given request. This could either
    be an explicitly selected payment method of the current user or the default
    payment method.
    """
    customer = get_customer(request)
    if customer:
        update_to_valid_payment_method(request, customer)
    if customer and customer.selected_payment_method:
        return customer.selected_payment_method
    else:
        return get_default_payment_method(request)


def get_payment_costs(request, payment_method):
    """
    Returns the payment price for the given request.
    """
    if payment_method is None:
        return {
            "price": 0.0,
            'ratio': 1.0,
        }

    price = get_first_valid(
        request,
        payment_method.prices.all())

    try:
        ratio = payment_method.currency.coeffitient
    except:
        ratio = 1.0

    if price is None:
        price = payment_method.price
        return {
            "price": price,
            'ratio': ratio,
        }
    else:
        return {
            "price": price.price,
            'ratio': ratio,
        }


def process_payment(
        request, checkout_form=None, shipping_form=None, payment_form=None):
    """
    Processes the payment depending on the selected payment method. Returns a
    dictionary with the success state, the next url and a optional error
    message.
    """
    from ..cart.utils import get_cart
    from ..order.utils import add_order

    form_dispatcher = {
        "accepted": True,
        "next_url": reverse("lfs_checkout_dispatcher"),
    }

    try:
        if get_cart(request).get_amount_of_items() == 0:
            return form_dispatcher
    except AttributeError:
        return form_dispatcher

    payment_method = get_selected_payment_method(request)

    if payment_method.module:
        payment_class = import_symbol(payment_method.module)
        payment_instance = payment_class(request)

        create_order_time = payment_instance.get_create_order_time()
        if create_order_time == PM_ORDER_IMMEDIATELY:
            order = add_order(
                request, checkout_form, shipping_form, payment_form)
            payment_instance.order = order
            result = payment_instance.process()
            # if result.get("order_state"):
            #     order.state = result.get("order_state")
            #     order.save()
            order_submitted.send({"order": order, "request": request})
        else:
            cart = get_cart(request)
            payment_instance.cart = cart
            result = payment_instance.process()

        if result["accepted"]:
            if create_order_time == PM_ORDER_ACCEPTED:
                order = add_order(
                    request,
                    checkout_form, shipping_form, payment_form)
                # if result.get("order_state"):
                #     order.state = result.get("order_state")
                #     order.save()
                order_submitted.send({"order": order, "request": request})
        return result

    else:
        order = add_order(
            request,
            checkout_form, shipping_form, payment_form)
        order_submitted.send({"order": order, "request": request})
        return {
            "accepted": True,
            "next_url": reverse("lfs_thank_you"),
        }


def get_pay_link(request, payment_method, order):
    """
    Creates a pay link for the passed payment_method and order.

    This can be used to display the link within the order mail and/or the
    thank you page after a customer has payed.
    """
    if payment_method.module:
        payment_class = import_symbol(payment_method.module)
        payment_instance = payment_class(request=request, order=order)
        try:
            return payment_instance.get_pay_link()
        except AttributeError:
            return ""
    else:
        return ""
