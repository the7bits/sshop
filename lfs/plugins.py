# coding: utf-8
from django import forms
from django.db import models

# import lfs
# from lfs.payment.settings import PM_ORDER_IMMEDIATELY
# from lfs.payment.settings import PM_ORDER_ACCEPTED
# from lfs.payment.settings import PM_MSG_TOP
# from lfs.payment.settings import PM_MSG_FORM
# from lfs.order.settings import PAID


class OrderNumberGenerator(models.Model):
    """
    Base class from which all order number generators should inherit.

    **Attributes:**

    id
        The unique id of the order number generator.
    """
    id = models.CharField(primary_key=True, max_length=20)

    class Meta:
        abstract = True

    def init(self, request, order):
        """
        Initializes the order number generator. This method is called
        automatically from LFS.
        """
        from .customer.utils import get_customer
        from .cart.utils import get_cart

        self.request = request
        self.order = order
        self.user = request.user
        self.customer = get_customer(request)
        self.cart = get_cart(request)

    def get_next(self, formatted=True):
        """
        Returns the next order number as string. Derived classes must implement
        this method.

        **Parameters:**

        formatted
            If True the number will be returned within the stored format, which
            is based on Python default string formatting operators, e.g.
            ``%04d``.
        """
        raise NotImplementedError

    def exclude_form_fields(self):
        """
        Returns a list of fields, which are excluded from the model form, see
        also ``get_form``.
        """
        return ("id", )

    def get_form(self, **kwargs):
        """
        Returns the form which is used within the shop preferences management
        interface.

        All parameters are passed to the form.
        """
        class OrderNumberGeneratorForm(forms.ModelForm):
            class Meta:
                model = self
                exclude = self.exclude_form_fields()

        return OrderNumberGeneratorForm(**kwargs)


class PaymentMethodProcessor(object):
    """
    Base class from which all 3rd-party payment method
    processors should inherit.

    **Attributes:**

    request
        The current request.

    cart
        The current cart. This is only set, when create order time is ACCEPTED.

    order
        The current order. This is only set, when create order time is
        IMMEDIATELY.
    """
    def __init__(self, request, cart=None, order=None):
        self.request = request
        self.cart = cart
        self.order = order

    def process(self):
        """
        Implements the processing of the payment method. Returns a dictionary
        with several status codes, see below.

        **Return Values:**

        This values are returned within a dictionary.

        accepted (mandatory)
            Indicates whether the payment is accepted or not. if this is
            ``False`` the customer keeps on the checkout page and gets
            ``message`` (if given) below. If this is ``True`` the customer will
            be redirected to next_url (if given).

        message (optional)
            This message is displayed on the checkout page, when the order is
            not accepted.

        message_location (optional)
            The location, where the message is displayed.

        next_url (optional)
            The url to which the user is redirect after the payment has been
            processed. if this is not given the customer is redirected to the
            default thank-you page.

        order_state (optional)
            The state in which the order should be set. It's just PAID. If it's
            not given the state keeps in SUBMITTED.
        """
        raise NotImplementedError

    def get_create_order_time(self):
        """
        Returns the time when the order should be created. It is one of:

        PM_ORDER_IMMEDIATELY
            The order is created immediately before the payment is processed.

        PM_ORDER_ACCEPTED
            The order is created when the payment has been processed and
            accepted.
        """
        raise NotImplementedError

    def get_pay_link(self):
        """
        Returns a link to the payment service to pay the current order, which
        is displayed on the thank-you page and the order confirmation mail. In
        this way the customer can pay the order again if something has gone
        wrong.
        """
        return None


class PriceCalculator(object):
    """
    This is the base class that pricing calculators must inherit from.
    """
    def __init__(self, request, product, **kwargs):
        self.request = request
        self.product = product

    def get_price(self):
        """
        Returns the stored price of the product without any tax calculations.
        It takes variants, properties and sale prices into account, though.

        """
        object = self.product
        price = object.effective_price
        return price

    def get_standard_price(self):
        """
        Returns always the stored standard price for the product. Independent
        whether the product is for sale or not. If you want the real price of
        the product use ``get_price`` instead.

        """
        object = self.product
        price = object.price
        return price

    def get_for_sale_price(self):
        """
        Returns the sale price for the product.
        """
        object = self.product
        price = object.for_sale_price
        return price


class ShippingMethodPriceCalculator(object):
    """
    Base class from which all 3rd-party shipping method prices should inherit.

    **Attributes:**

    request
        The current request.

    shipping_method
        The shipping method for which the price is calculated.
    """
    def __init__(self, request, shipping_method):
        self.shipping_method = shipping_method
        self.request = request

    def get_price(self):
        """
        Returns the stored price without any calculations.
        """
        return self.shipping_method.price
