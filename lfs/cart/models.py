# coding: utf-8
import logging

from django.conf import settings
from django.core.cache import cache
from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from ..catalog.models import Product


logger = logging.getLogger("sshop")


class Cart(models.Model):
    """
    A cart is a container for products which are supposed to be bought by a
    shop customer.

    **Attributes**

    user
       The user to which the cart belongs to

    session
       The session to which the cart belongs to

    creation_date
        The creation date of the cart

    modification_date
        The modification date of the cart

    A cart can be assigned either to the current logged in User (in case
    the shop user is logged in) or to the current session (in case the shop
    user is not logged in).

    A cart is only created if it needs to, i.e. when the shop user adds
    something to the cart.
    """
    user = models.ForeignKey(
        User, verbose_name=_(u"User"), blank=True, null=True)
    session = models.CharField(_(u"Session"), blank=True, max_length=100)
    creation_date = models.DateTimeField(
        _(u"Creation date"), auto_now_add=True)
    modification_date = models.DateTimeField(
        _(u"Modification date"), auto_now=True, auto_now_add=True)

    class Meta:
        verbose_name = _(u'Cart')
        verbose_name_plural = _(u'Carts')

    def __unicode__(self):
        return u"%s, %s" % (self.user, self.session)

    def add(self, product, amount=1):
        """
        Adds passed product to the cart.

        **Parameters**

        product
            The product which is added.

        Returns the newly created cart item.
        """
        try:
            cart_item = CartItem.objects.get(cart=self, product=product)
        except CartItem.DoesNotExist:
            cart_item = CartItem.objects.create(
                cart=self, product=product, amount=amount)
        else:
            cart_item.amount += float(amount)
            cart_item.save()
        return cart_item

    def get_amount_of_items(self):
        """
        Returns the amount of items of the cart.
        """
        amount = 0
        for item in self.get_items():
            amount += item.amount
        return amount

    def get_item(self, product):
        """
        Returns the item for passed product or None if there
        is none.
        """
        return CartItem.objects.filter(cart=self, product=product)

    def get_items(self):
        """
        Returns the items of the cart.
        """
        items = CartItem.objects.select_related().filter(
            cart=self, product__active=True)
        return items

    def get_price(self, request, total=False):
        """
        Returns the total price of all items.
        """
        price = 0
        for item in self.get_items():
            price += item.get_price_total(request)
        return price


class CartItem(models.Model):
    """
    A cart item belongs to a cart. It stores the product and the amount of the
    product which has been taken into the cart.

    **Attributes**

    cart
        The cart the cart item belongs to.

    product
        A reference to a product which is supposed to be bought.

    amount
       Amount of the product which is supposed to be bought.

    creation_date
        The creation date of the cart item.

    modification_date
        The modification date of the cart item.
    """
    cart = models.ForeignKey(Cart, verbose_name=_(u"Cart"))
    product = models.ForeignKey(Product, verbose_name=_(u"Product"))
    amount = models.FloatField(_(u"Quantity"), blank=True, null=True)
    creation_date = models.DateTimeField(
        _(u"Creation date"), auto_now_add=True)
    modification_date = models.DateTimeField(
        _(u"Modification date"), auto_now=True, auto_now_add=True)

    class Meta:
        ordering = ['id']

    def __unicode__(self):
        return u"Product: %(product)s, Quantity: %(amount)f, Cart: %(cart)s" %\
            {
                'product': self.product,
                'amount': self.amount,
                'cart': self.cart
            }

    def get_price(self, request):
        """
        Convenient method to return the gross price of the product.
        """
        return self.get_product_price(request)

    def get_price_total(self, request):
        """
        Returns the total item price.
        """
        return self.get_price(request) * self.amount

    def get_product_price(self, request):
        """
        Returns the product item price. Based on selected properties, etc.
        """
        return self.product.get_price(request)
