# coding: utf-8
from django.contrib import admin
from django.utils.translation import ugettext as _
from django.contrib.admin import SimpleListFilter
from .models import Cart, CartItem
from .forms import CartItemInlineForm


class CartItemInline(admin.TabularInline):
    model = CartItem
    form = CartItemInlineForm
    readonly_fields = ('creation_date', 'modification_date')


class CartFilter(SimpleListFilter):
    title = _(u'Cart types')
    parameter_name = 'cart_type'

    def lookups(self, request, model_admin):
        return [
            ('idle', _(u'Idle carts')),
            ('empty', _(u'Empty carts')),
        ]

    def queryset(self, request, queryset):
        if self.value():
            if self.value() == 'idle':
                queryset = Cart.objects.exclude(cartitem=None)
            elif self.value() == 'empty':
                queryset = Cart.objects.filter(cartitem=None)
            else:
                queryset = Cart.objects.all()
        return queryset


class CartAdmin(admin.ModelAdmin):
    list_display = (
        'user', 'session',
        'creation_date',
        'modification_date',
        'get_amount',
    )
    search_fields = ['user__username', 'session']
    readonly_fields = ('user', 'session', 'creation_date', 'modification_date')
    date_hierarchy = 'creation_date'
    inlines = [CartItemInline]
    list_filter = (CartFilter,)

    def get_amount(self, obj):
        return int(obj.get_amount_of_items())

    get_amount.short_description = _(u'Amount of items')

    def suit_row_attributes(self, obj):
        if obj.get_amount_of_items() > 0:
            return {'class': 'info'}


admin.site.register(Cart, CartAdmin)
