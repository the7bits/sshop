# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.admin import widgets
from django.utils import timezone

from .settings import KIND_OF_CHOICES


class GenerateVouchersForm(forms.Form):
    """Form to add a Vouchers.
    """
    amount = forms.IntegerField(label=_(u"Amount"), required=True, initial=10)
    value = forms.FloatField(label=_(u"Value"), required=True)
    start_date = forms.DateField(
        label=_(u"Start date"), required=True,
        widget=widgets.AdminDateWidget(), initial=timezone.now)
    end_date = forms.DateField(
        label=_(u"End date"),
        required=True, widget=widgets.AdminDateWidget())
    kind_of = forms.ChoiceField(
        label=_(u"Discount type"), choices=KIND_OF_CHOICES, required=True)
    effective_from = forms.FloatField(
        label=_(u"Effective from"), required=True, initial=0.0)
    limit = forms.IntegerField(label=_(u"Limit"), initial=1, required=True)
