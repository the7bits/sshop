# python imports
import datetime

# django imports
from django.contrib.auth.models import User
from django.contrib.sessions.backends.file import SessionStore
from django.test import TestCase

# lfs imports
from lfs.voucher.utils import create_voucher_number
from lfs.cart.models import Cart
from lfs.cart.models import CartItem
from lfs.catalog.models import Product
from django.test.client import RequestFactory
# from lfs.tax.models import Tax
from lfs.voucher.models import Voucher
from lfs.voucher.models import VoucherGroup
# from lfs.voucher.models import VoucherOptions
from lfs.voucher.settings import ABSOLUTE
from lfs.voucher.settings import PERCENTAGE


class VoucherUtilsTestCase(TestCase):
    """
    """
    def test_create_vouchers_1(self):
        """Tests the default voucher options
        """
        number = create_voucher_number()
        self.failUnless(len(number) == 5)

        letters = "ABCDEFGHIJKLMNOPQRSTUVXYZ"

        for letter in number:
            self.failIf(letter not in letters)


class VoucherTestCase(TestCase):
    """
    """
    fixtures = ['lfs_shop.xml']

    def setUp(self):
        """
        """
        self.request = RequestFactory().get("/")
        self.request.session = SessionStore()
        self.request.user = User(id=1)

        self.vg = VoucherGroup.objects.create(
            name="xmas",
            creator=self.request.user,
        )

        self.v1 = Voucher.objects.create(
            number="AAAA",
            group=self.vg,
            creator=self.request.user,
            start_date=datetime.date(2009, 12, 1),
            end_date=datetime.date(2009, 12, 31),
            effective_from=0,
            kind_of=ABSOLUTE,
            value=10.0,
            limit=2,
        )

        self.p1 = Product.objects.create(
            name="Product 1", slug="product-1", price=10.0, active=True)
        self.p2 = Product.objects.create(
            name="Product 2", slug="product-2", price=100.0, active=True)

        self.cart = Cart.objects.create()
        CartItem.objects.create(cart=self.cart, product=self.p1, amount=1)
        CartItem.objects.create(cart=self.cart, product=self.p2, amount=1)

    def test_defaults(self):
        """
        """
        self.assertEqual(self.v1.number, "AAAA")
        self.assertEqual(self.v1.group, self.vg)
        self.assertEqual(self.v1.creator, self.request.user)
        self.assertEqual(self.v1.start_date, datetime.date(2009, 12, 1),)
        self.assertEqual(self.v1.end_date, datetime.date(2009, 12, 31),)
        self.assertEqual(self.v1.effective_from, 0.0)
        self.assertEqual(self.v1.kind_of, ABSOLUTE)
        self.assertEqual(self.v1.active, True)
        self.assertEqual(self.v1.used_amount, 0)
        self.assertEqual(self.v1.last_used_date, None)
        self.assertEqual(self.v1.value, 10.0)

    def test_prices_absolute(self):
        """
        """
        price = self.v1.get_price(self.request)
        self.assertEqual(price, 10)

    def test_prices_percentage(self):
        """
        """
        # 10% discount
        self.v1.kind_of = PERCENTAGE
        self.v1.value = 10.0
        self.v1.save()

        price = self.v1.get_price(self.request, self.cart)
        self.assertEqual(price, 11.0)

    def test_kind_of(self):
        """
        """
        self.assertEqual(self.v1.kind_of, ABSOLUTE)
        self.assertEqual(self.v1.is_absolute(), True)
        self.assertEqual(self.v1.is_percentage(), False)

        self.v1.kind_of = PERCENTAGE
        self.v1.save()

        self.assertEqual(self.v1.kind_of, PERCENTAGE)
        self.assertEqual(self.v1.is_absolute(), False)
        self.assertEqual(self.v1.is_percentage(), True)

    def test_mark_as_used(self):
        """
        """
        self.assertEqual(self.v1.used_amount, 0)
        self.assertEqual(self.v1.last_used_date, None)

        self.v1.mark_as_used()

        self.assertEqual(self.v1.used_amount, 1)
        self.failIf(self.v1.last_used_date is None)

    def test_is_effective(self):
        """
        """
        current_year = datetime.datetime.now().year

        # True
        self.v1.start_date = datetime.date(2000, 1, 1)
        self.v1.end_date = datetime.date(2999, 12, 31)
        self.v1.active = True
        self.v1.used_amount = 1
        self.v1.effective_from = 0
        self.assertEqual(
            self.v1.is_effective(self.request, self.cart)[0], True)

        # start / end
        self.v1.start_date = datetime.date(current_year, 12, 31)
        self.v1.end_date = datetime.date(current_year, 12, 31)
        self.v1.active = True
        self.v1.used_amount = 1
        self.v1.effective_from = 0
        self.assertEqual(
            self.v1.is_effective(self.request, self.cart)[0], False)

        # effective from
        self.v1.start_date = datetime.date(current_year, 1, 1)
        self.v1.end_date = datetime.date(current_year, 12, 31)
        self.v1.active = True
        self.v1.used_amount = 1
        self.v1.effective_from = 1000
        self.assertEqual(
            self.v1.is_effective(self.request, self.cart)[0], False)

        # Used
        self.v1.start_date = datetime.date(current_year, 1, 1)
        self.v1.end_date = datetime.date(current_year, 12, 31)
        self.v1.active = True
        self.v1.used_amount = 1
        self.v1.effective_from = 0
        self.assertEqual(
            self.v1.is_effective(self.request, self.cart)[0], True)

        self.v1.mark_as_used()
        self.assertEqual(
            self.v1.is_effective(self.request, self.cart)[0], False)

        # limited amount
        self.v1.limit = 0
        self.assertEqual(
            self.v1.is_effective(self.request, self.cart)[0], False)

        # Not active
        self.v1.start_date = datetime.date(current_year, 1, 1)
        self.v1.end_date = datetime.date(current_year, 12, 31)
        self.v1.active = False
        self.v1.used_amount = 1
        self.v1.effective_from = 0
        self.assertEqual(
            self.v1.is_effective(self.request, self.cart)[0], False)
