# coding: utf-8
import random

from .settings import (
    VOUCHER_LENGTH,
    VOUCHER_LETTERS,
    VOUCHER_PREFIX,
    VOUCHER_SUFFIX,
)


def create_voucher_number():
    """Create a random voucher number.
    """
    number = ""
    for i in range(0, VOUCHER_LENGTH):
        number += random.choice(VOUCHER_LETTERS)

    return VOUCHER_PREFIX + number + VOUCHER_SUFFIX


def get_current_voucher_number(request):
    """
    """
    return request.POST.get("voucher", request.session.get("voucher", ""))


def set_current_voucher_number(request, number):
    """
    """
    request.session["voucher"] = number
