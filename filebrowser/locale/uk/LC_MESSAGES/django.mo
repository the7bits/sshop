��    P      �  k         �  "   �  .   �  .     &   J     q     �     �     �     �     �  ,   �  *   �     (     .     5     =     E     Q     d  	   i     s     x     ~     �     �     �     �  5   �     	  >   	     T	     `	     i	     p	     w	     z	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     �	  C   �	     (
     5
     A
  $   T
     y
     �
     �
     �
     �
     �
     �
     �
     �
  '   �
       R   2  %   �  '   �  "   �  $   �  	     	   %     /     5     :     A     J     \  
   b     m  �  s  V   ?  V   �  J   �  b   8  "   �  Y   �               2     P  J   i  J   �  
   �  
   
          #     1  #   M     q     x     �     �     �     �  ,   �       -   !  m   O     �  g   �  !   4     V     k  
   �     �     �     �     �     �  !   �             0   
  	   ;     E     Y     l  "   �  o   �          8  "   R  D   u     �     �     �  
          ;   *     f     s     �     �     �  �   �     z     �  .   �  .   �       %   )     O     `     g     ~     �  
   �  +   �     �         .   &       F      J                      P   +   	   C      B   >   -   M   4   !          ?                      "      0      K   N                  1   E      %              D           3   5           6          :       9   H   
       I           $   ,          O       7      ;   /   =   '   @   (           L   <   2                       G      )   A          #       *          8    %(counter)s Item %(counter)s Items %(counter)s Item found %(counter)s Items found %(counter)s Item total %(counter)s Items total %(counter)s result %(counter)s results %(full_result_count)s total 1 result %(counter)s results All Allowed An Error occured Any Date Are you sure you want to delete this Folder? Are you sure you want to delete this file? Audio BROWSE By Date By Type Clear Queue Clear Restrictions Code Completed Date Debug Delete File Delete Folder Do you want to replace the file Document Error creating folder. Error finding Upload-Folder. Maybe it does not exist? Error. Extension %(ext)s is not allowed. Only %(allowed)s is allowed. FileBrowser Filename Filter Folder Go Height Help Home Image Image Version KB MB Max. Filesize Name New Folder New Name No Items No Items Found Only letters, numbers, underscores, spaces and hyphens are allowed. Past 30 days Past 7 days Permission denied. Please correct the following errors. Rename Rename "%s" Renaming was successful. Search Select Select files to upload Size Submit The File already exists. The Folder %s was successfully created. The Folder already exists. The Name will be converted to lowercase. Spaces will be replaced with underscores. The file %s was successfully deleted. The folder %s was successfully deleted. The requested File does not exist. The requested Folder does not exist. This year Thumbnail Today Type Upload Versions Versions for "%s" Video View Image Width Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-01-30 11:04+0200
PO-Revision-Date: 2014-01-30 11:04+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 %(counter)s елемент %(counter)s елементи %(counter)s елементів %(counter)s знайдено %(counter)s знайдено %(counter)s знайдено %(counter)s всього %(counter)s всього %(counter)s всього %(counter)s результат %(counter)s результата %(counter)s результатів %(full_result_count)s всього 1  результат %(counter)s результата %(counter)s результатів Всі Дозволено Виникла помилка Будь-яка дата Ви впевнені, що хочете видалити цю папку? Ви впевнені, що хочете видалити цей файл? Аудіо Огляд По даті По типу Очистити чергу Очистити обмеження Код Завершено Дата Налагодження Видалити файл Видалити папку Ви хочете замінити файл? Документ Помилка створення папки. Помилка пошуку папки для завантаження. Можливо, її не існує? Помилка Тип файлу %(ext)s не дозволяється. Дозволяється лише %(allowed)s. Переглядач файлів Ім’я файлу Фільтрувати Папка Перейти Висота Допомога Головна Зображення Версія зображення КБ МБ Максимальний розмір файлу Ім’я Нова папка Нове ім’я Немає елементів Нічого не знайдено Дозволяються лише літери, цифри, підкреслення, пробіл, дефіс. Останні 30 днів Останні 7 днів Необхідний дозвіл. Буь ласка, виправте наступні помилки. Перейменувати Перейменувати "%s" Перейменовано Пошук Вибрати Виберіть файли для завантаження Розмір Підтвердити Файл вже існує. Папка %s створена. Папка вже існує Ім’я буде конвертоване в нижній регістр. Пробіли будуть замінені підкресленнями. Файл %s видалено. Папка %s видалена. Запрошений файл не існує. Запрошена папка не існує. За цей рік Попередній перегляд Сьогодні Тип Завантажити Версії Версії "%s" Відео Переглянути зображення Ширина 