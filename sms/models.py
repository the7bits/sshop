from django.db import models
from django.utils.translation import ugettext_lazy as _


class SMS(models.Model):
    text = models.CharField(_(u"Message"), max_length=300)
    phone = models.CharField(_(u"Phone"), max_length=20)
    exported = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('SMS')
        verbose_name_plural = _('SMS')

    def __unicode__(self):
        return '%s %s' % (self.text, self.phone)
