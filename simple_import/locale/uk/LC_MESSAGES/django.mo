��          �      \      �     �     �     �     �               3     @  
   N     Y  :   i     �     �  2   �     �     �     �             �  %  %   �  %        =     W  &   o  1   �     �  0   �       !   )  R   K     �  '   �  _   �     >  
   K  
   V  $   a     �                                                       
                               	             Column match Column matches Column name Content type Create New Records Create and Update Records Date Created Default value Field name Header position If cell is blank, clear out the field setting it to blank. Import file Import settings Invalid file type. Must be xls, xlsx, ods, or csv. Model Name Null on empty Only Update Records User Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-02-19 11:52+0200
PO-Revision-Date: 2014-02-19 11:57+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Співпадання колонок Співпадіння колонок Назва колонки Тип контенту Створити нові записи Створити та оновити записи Дата створення Значення за замовчуванням Назва поля Позиція заголовку Якщо колонка пуста, то очищати значення поля. Файл імпорту Налаштування імпорту Недопустимий тип файлц. Повинно бути xls, xlsx, ods, або csv. Модель Назва Пусто Лише оновити записи Користувач 