# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'ImportSetting'
        db.create_table('simple_import_importsetting', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
        ))
        db.send_create_signal('simple_import', ['ImportSetting'])

        # Adding unique constraint on 'ImportSetting', fields ['user', 'content_type']
        db.create_unique('simple_import_importsetting', ['user_id', 'content_type_id'])

        # Adding model 'ColumnMatch'
        db.create_table('simple_import_columnmatch', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('column_name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('field_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
            ('import_setting', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simple_import.ImportSetting'])),
            ('default_value', self.gf('django.db.models.fields.CharField')(max_length=2000, blank=True)),
            ('null_on_empty', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('header_position', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('simple_import', ['ColumnMatch'])

        # Adding unique constraint on 'ColumnMatch', fields ['column_name', 'import_setting']
        db.create_unique('simple_import_columnmatch', ['column_name', 'import_setting_id'])

        # Adding model 'ImportLog'
        db.create_table('simple_import_importlog', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(related_name='simple_import_log', to=orm['auth.User'])),
            ('date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('import_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('error_file', self.gf('django.db.models.fields.files.FileField')(max_length=100, blank=True)),
            ('import_setting', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simple_import.ImportSetting'])),
            ('import_type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('update_key', self.gf('django.db.models.fields.CharField')(max_length=200, blank=True)),
        ))
        db.send_create_signal('simple_import', ['ImportLog'])

        # Adding model 'RelationalMatch'
        db.create_table('simple_import_relationalmatch', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('import_log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simple_import.ImportLog'])),
            ('field_name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('related_field_name', self.gf('django.db.models.fields.CharField')(max_length=255, blank=True)),
        ))
        db.send_create_signal('simple_import', ['RelationalMatch'])

        # Adding model 'ImportedObject'
        db.create_table('simple_import_importedobject', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('import_log', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['simple_import.ImportLog'])),
            ('object_id', self.gf('django.db.models.fields.IntegerField')()),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contenttypes.ContentType'])),
        ))
        db.send_create_signal('simple_import', ['ImportedObject'])


    def backwards(self, orm):
        # Removing unique constraint on 'ColumnMatch', fields ['column_name', 'import_setting']
        db.delete_unique('simple_import_columnmatch', ['column_name', 'import_setting_id'])

        # Removing unique constraint on 'ImportSetting', fields ['user', 'content_type']
        db.delete_unique('simple_import_importsetting', ['user_id', 'content_type_id'])

        # Deleting model 'ImportSetting'
        db.delete_table('simple_import_importsetting')

        # Deleting model 'ColumnMatch'
        db.delete_table('simple_import_columnmatch')

        # Deleting model 'ImportLog'
        db.delete_table('simple_import_importlog')

        # Deleting model 'RelationalMatch'
        db.delete_table('simple_import_relationalmatch')

        # Deleting model 'ImportedObject'
        db.delete_table('simple_import_importedobject')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'simple_import.columnmatch': {
            'Meta': {'unique_together': "(('column_name', 'import_setting'),)", 'object_name': 'ColumnMatch'},
            'column_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'default_value': ('django.db.models.fields.CharField', [], {'max_length': '2000', 'blank': 'True'}),
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'}),
            'header_position': ('django.db.models.fields.IntegerField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'import_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simple_import.ImportSetting']"}),
            'null_on_empty': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        'simple_import.importedobject': {
            'Meta': {'object_name': 'ImportedObject'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'import_log': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simple_import.ImportLog']"}),
            'object_id': ('django.db.models.fields.IntegerField', [], {})
        },
        'simple_import.importlog': {
            'Meta': {'object_name': 'ImportLog'},
            'date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'error_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'import_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'import_setting': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simple_import.ImportSetting']"}),
            'import_type': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'update_key': ('django.db.models.fields.CharField', [], {'max_length': '200', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'simple_import_log'", 'to': "orm['auth.User']"})
        },
        'simple_import.importsetting': {
            'Meta': {'unique_together': "(('user', 'content_type'),)", 'object_name': 'ImportSetting'},
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'simple_import.relationalmatch': {
            'Meta': {'object_name': 'RelationalMatch'},
            'field_name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'import_log': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['simple_import.ImportLog']"}),
            'related_field_name': ('django.db.models.fields.CharField', [], {'max_length': '255', 'blank': 'True'})
        }
    }

    complete_apps = ['simple_import']