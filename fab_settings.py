# -*- coding: utf-8 -*-

# User prefix. Describes the cpmpany.
USER_PREFIX = '7b'

CREATE_USER = False

# Project settings

PROJECT_NAME = 'sshop'

MEDIA_APP = PROJECT_NAME + '_media'
STATIC_APP = PROJECT_NAME + '_static'
VIRTUALENV_NAME = 'env_' + PROJECT_NAME
APP_USERNAME = USER_PREFIX + PROJECT_NAME
COPY_COMMANDS = []

# Settings for different servers
SERVERS = {
    'staging': {
        'KEY_FILENAME': '~/.ssh/id_dsa',
        'USER': 'drom',
        'HOST': '94.75.245.143',
        'BRANCH': 'develop',
        'DOMAIN': 'the7bits.com',
        'SUBDOMAINS': ['sshop'],
        'USE_MAIN_DOMAIN': False,
        'DEBUG': False,
        'APACHE_PORT': 27696,
        'MACHINE_NAME': 'Web210',
        'PROJECT_NAME': PROJECT_NAME,
        'VIRTUALENV_NAME': VIRTUALENV_NAME,
        'APP_USERNAME': APP_USERNAME,
        'MEDIA_APP': MEDIA_APP,
        'STATIC_APP': STATIC_APP,
        'DB_NAME': 'drom_'+PROJECT_NAME,
        'MAILBOX_NAME': 'drom_'+PROJECT_NAME,
        'SUPPORT_EMAIL': 'sshop@the7bits.com',
        'UPGRADE_EMAIL': 'sshop-upgrade@the7bits.com',
        'COPY_COMMANDS': COPY_COMMANDS,
    },
    'sbits': {
        'KEY_FILENAME': '~/.ssh/id_dsa',
        'USER': 'sbits',
        'HOST': '95.211.171.72',
        'BRANCH': 'master',
        'DOMAIN': 'aboutevents.net',    #TODO: replace this domain later
        'SUBDOMAINS': ['sshop'],
        'USE_MAIN_DOMAIN': False,
        'DEBUG': False,
        'APACHE_PORT': 21045,
        'MACHINE_NAME': 'Web332',
        'PROJECT_NAME': PROJECT_NAME,
        'VIRTUALENV_NAME': VIRTUALENV_NAME,
        'APP_USERNAME': APP_USERNAME,
        'MEDIA_APP': MEDIA_APP,
        'STATIC_APP': STATIC_APP,
        'DB_NAME': 'sbits_'+PROJECT_NAME,
        'MAILBOX_NAME': 'sbits_'+PROJECT_NAME,
        'SUPPORT_EMAIL': 'sshop@aboutevents.net',   #TODO: replace domain later
        'UPGRADE_EMAIL': 'sshop-upgrade@aboutevents.net',
        'COPY_COMMANDS': COPY_COMMANDS,
        },
    'aquaweb': {
        'KEY_FILENAME': '~/.ssh/id_dsa',
        'USER': 'aquaweb',
        'HOST': '5.153.9.55',
        'BRANCH': 'master',
        'DOMAIN': 'avto-zap.com.ua',
        'SUBDOMAINS': ['www'],
        'USE_MAIN_DOMAIN': True,
        'DEBUG': False,
        'APACHE_PORT': 54547,
        'MACHINE_NAME': 'Web303',
        'PROJECT_NAME': PROJECT_NAME,
        'VIRTUALENV_NAME': VIRTUALENV_NAME,
        'APP_USERNAME': APP_USERNAME,
        'MEDIA_APP': MEDIA_APP,
        'STATIC_APP': STATIC_APP,
        'DB_NAME': 'aquaweb_avtozap',
        'MAILBOX_NAME': 'aquaweb_avtozap',
        'SUPPORT_EMAIL': 'support@avto-zap.com.ua',
        'UPGRADE_EMAIL': 'autodoctor-upgrade@avto-zap.com.ua',
        'COPY_COMMANDS': COPY_COMMANDS,
        },
}

# List of application for launching migration every update.
MY_APPS = [
    'lfs.filters',
    'lfs.catalog',
    'sshop_currencies',
    'sshop_import',
]

# Remote repository for downloading sources from.
REPOSITORY = 'git@bitbucket.org:the7bits/sshop.git'

# Now we use postgresql
DB_TYPE = 'postgresql'

# Set this in True if you want to see SSH logging
SSH_LOGGING = False

# This template using for adding virtual host if you use the main domain for site access
HTTPD_TEMPLATE_DOMAIN = '''
<VirtualHost *:%(apache_port)d>
    WSGIScriptAlias / /home/%(username)s/webapps/django/%(project_name)s/%(project_name)s/wsgi.py
    ServerName %(domain)s
    ServerAlias %(subdomain)s.%(domain)s
    ErrorLog "logs/%(project_name)s_errors.log"
    RewriteEngine on
    RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ ((/[^/]+)*)//+([^\ ]*)\ HTTP/
    RewriteRule .* http://%(subdomain)s.%(domain)s%1/%3 [R=301,L] 
</VirtualHost>
'''

# This template using for adding virtual host if you use the subdomain for site access
HTTPD_TEMPLATE_SUBDOMAIN = '''
<VirtualHost *:%(apache_port)d>
    WSGIScriptAlias / /home/%(username)s/webapps/django/%(project_name)s/%(project_name)s/wsgi.py
    ServerName %(subdomain)s.%(domain)s
    ErrorLog "logs/%(project_name)s_errors.log"
    RewriteEngine on
    RewriteCond %{THE_REQUEST} ^[A-Z]{3,9}\ ((/[^/]+)*)//+([^\ ]*)\ HTTP/
    RewriteRule .* http://%(subdomain)s.%(domain)s%1/%3 [R=301,L] 
</VirtualHost>
'''



