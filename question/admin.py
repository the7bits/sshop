# coding: utf-8
import autocomplete_light

from django import forms
from django.contrib import admin
from django.contrib import messages
from django.utils.translation import ugettext as _

from .models import Question
from lfs.mail.utils import send_answer


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        widgets = {
            'product': autocomplete_light.ChoiceWidget('ProductAutocomplete'),
        }


class QuestionAdmin(admin.ModelAdmin):
    list_display = (
        'question', 'product', 'email',
        'creation_date', 'answer_date', 'status')
    search_fields = ['question']
    date_hierarchy = 'creation_date'
    list_filter = ['status']
    form = QuestionForm
    fieldsets = [
        (_('Question'), {
            'fields': ['product', 'email', 'question']
        }),
        (_('Answer'), {
            'fields': ['answer', 'status']
        }),
    ]

    actions = [
        'send_answers_action',
    ]

    def send_answers_action(self, request, queryset):
        for q in queryset:
            result = send_answer(q, q.answer)
            if result:
                q.status = 5
                messages.success(
                    request,
                    _(u'Answer to %(email)s was successfully sent.') % {
                        'email': q.email})
            else:
                q.status = 10
                messages.error(
                    request,
                    _(u'Answer to %(email)s did not send because failed.') % {
                        'email': q.email})
            q.save()
    send_answers_action.short_description = _(u'Send answers to customers')

    def suit_row_attributes(self, obj):
        if not obj.answer:
            return {'class': 'info'}


admin.site.register(Question, QuestionAdmin)
