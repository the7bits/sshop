# -*- coding: utf-8 -*-
from lettuce import step, world
from lettuce.django import django_url
from splinter.request_handler.status_code import HttpResponseError

@step(u'sign in as user "(.*)" with password "(.*)"')
def sign_in_as_user(step, username, password):
    world.browser.visit(django_url('/accounts/signin/'))
    world.browser.fill('identification', username)
    world.browser.fill('password', password)
    world.browser.check('remember_me')
    world.browser.find_by_css('button[type=submit]').click()

@step(r'(?:visit|access|open) the site url "(.*)"')
def go_to_the_url(step, url):
    world.response = world.browser.visit('127.0.0.1:8000' + url)
        

