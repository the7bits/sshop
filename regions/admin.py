# coding: utf-8
from django.contrib import admin
from regions.models import Region


class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_default', 'coefficient')
    list_editable = ('is_default', 'coefficient')

admin.site.register(Region, RegionAdmin)
