from regions.models import Region
from django import template

register = template.Library()


@register.inclusion_tag('regions.html', takes_context=True)
def show_regions(context):
    request = context['request']
    regions = Region.objects.all()
    active_region = request.session.get('region', None)
    return {
        'regions': regions,
        'active_region': active_region,
        'request': request,
    }
