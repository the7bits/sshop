CKEDITOR.editorConfig = function( config ) {
    config.language = 'ru';
    config.filebrowserBrowseUrl = '/superadmin/filer/folder/?_popup=1';
    config.toolbarCanCollapse = true;
    config.tabSpaces = 4;
    config.contentsLanguage = 'ru';
    config.forcePasteAsPlainText = true;
};