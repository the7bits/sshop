// Help bar jQuery plugin
// Written for P-Cart admin
// (c) Oleh Korkh


(function ( $ ) {
 
    $.fn.helpbar = function(options) {
        var settings = $.extend({
            ribbon_caption: "Help",
            bar_caption: "P-Cart User Manual",
            doc_url: "about:blank",
        }, options );

        var hlpbr = {
            sidebar_html: '<div id="helpbar">'+
            '<div id="helpbar-handle"><a href="#" id="helpbar-show-button">'+
            '<img src="/static/adminextras/icons/question-frame.png" /> '+settings.ribbon_caption+'</a></div>'+
            '<div id="helpbar-panel">'+
            '<div class="helpbar-header">'+
            '<a href="#" id="helpbar-hide-button"><img src="/static/adminextras/icons/fill.png" title="Hide help" /></a>'+
            '&nbsp;'+settings.bar_caption+
            '</div>'+
            '<div id="helpbar-content"></div>'+
            '</div>'+
            '</div>',
            path: window.location.pathname,

            init: function () {
                $(hlpbr.sidebar_html).appendTo('body');

                $('#helpbar-panel').hide();
                $('#helpbar-show-button').click(hlpbr.showBar);
                $('#helpbar-hide-button').click(hlpbr.hideBar);

                var frame = document.createElement('iframe');
                frame.src = settings.doc_url;
                frame.width = 500;
                $(frame).appendTo('#helpbar-content');
                window.onresize = hlpbr.resizeIframe;

                return true;
            },

            showBar: function () {
                $('#helpbar-handle').hide();
                $('#helpbar-panel').show();                
                hlpbr.resizeIframe();
            },

            hideBar: function () {
                $('#helpbar-panel').hide();
                $('#helpbar-handle').show();
            },

            resizeIframe: function () {
                $('#helpbar-content iframe').height($('#helpbar-panel').height()-22);
            },

        }

        $(document).ready(function() {
            hlpbr.init();
        });
        return hlpbr;
    };
 
}( jQuery ));