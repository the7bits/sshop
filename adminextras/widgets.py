# coding: utf-8
from itertools import chain
from django.utils.safestring import mark_safe
from django import forms
from django.contrib.contenttypes.models import ContentType
from django.contrib.admin.widgets import (
    ManyToManyRawIdWidget,
    ForeignKeyRawIdWidget,
)
from django.core.urlresolvers import reverse
from django.utils.encoding import smart_unicode
from django.utils.html import escape


class ContentTypeSelect(forms.Select):
    fk = 'content'

    def __init__(self, lookup_id,  attrs=None, choices=()):
        self.lookup_id = lookup_id
        if 'fk' in attrs:
            self.fk = attrs['fk']
        super(ContentTypeSelect, self).__init__(attrs, choices)

    def render(self, name, value, attrs=None, choices=()):
        output = super(ContentTypeSelect, self)\
            .render(name, value, attrs, choices)

        choices = chain(self.choices, choices)
        choiceoutput = ' var %s_choice_urls = {' % (
            attrs['id'].replace('-', '_'),)
        for choice in choices:
            try:
                ctype = ContentType.objects.get(pk=int(choice[0]))
                choiceoutput += '    \'%s\' : \'../../../%s/%s?t=%s\',' % (
                    str(choice[0]),
                    ctype.app_label,
                    ctype.model, ctype.model_class()._meta.pk.name)
            except:
                pass
        choiceoutput += '};'

        output += (
            '<script type="text/javascript">'
            '(function($) {'
            '  $(document).ready( function() {'
            '%(choiceoutput)s'
            '    $(\'#%(id)s\').change(function (){'
            '        $(\'#%(fk_id)s\').attr(\'href\',%(id_var)s_'
            'choice_urls[$(this).val()]);'
            '    });'
            '  });'
            '})(django.jQuery);'
            '</script>' % {
                'choiceoutput': choiceoutput,
                'id': attrs['id'],
                'id_var': attrs['id'].replace('-', '_'),
                'fk_id': self.lookup_id+attrs['id'].replace(
                    self.fk+'_type', self.fk+'_id')
            })
        return mark_safe(u''.join(output))


class VerboseForeignKeyRawIdWidget(ForeignKeyRawIdWidget):
    def label_for_value(self, value):
        key = self.rel.get_related_field().name
        try:
            obj = self.rel.to._default_manager.using(self.db)\
                .get(**{key: value})
            change_url = reverse(
                "admin:%s_%s_change" % (
                    obj._meta.app_label,
                    obj._meta.object_name.lower()),
                args=(obj.pk,)
            )
            return '&nbsp;<strong><a href="%s">%s</a></strong>' % (
                change_url, escape(obj))
        except (ValueError, self.rel.to.DoesNotExist):
            return '???'


class VerboseManyToManyRawIdWidget(ManyToManyRawIdWidget):
    def label_for_value(self, value):
        values = value.split(',')
        str_values = []
        key = self.rel.get_related_field().name
        for v in values:
            try:
                obj = self.rel.to._default_manager.using(self.db)\
                    .get(**{key: v})
                x = smart_unicode(obj)
                change_url = reverse(
                    "admin:%s_%s_change" % (
                        obj._meta.app_label,
                        obj._meta.object_name.lower()),
                    args=(obj.pk,)
                )
                str_values += ['<strong><a href="%s">%s</a></strong>' % (
                    change_url, escape(x))]
            except self.rel.to.DoesNotExist:
                str_values += [u'???']
        return u', '.join(str_values)
