# coding: utf-8
from django.conf.urls.defaults import patterns, url

# These url patterns use for admin interface
urlpatterns = patterns(
    'admininfo.views',
    url(r'^system-info/$', 'system_info', name="admin_system_info"),
    url(r'^clear-cache/$', 'clear_cache', name="admin_clear_cache"),
    url(r'^restart-engine/$', 'restart_engine', name="admin_restart_engine"),
    url(r'^collect-garbage/$', 'collect_garbage',
        name="admin_collect_garbage"),
)
