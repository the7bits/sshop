��          �   %   �      `     a     o  !   �     �     �     �     �     �  0        4     9     F     ]     n     }     �     �     �     �     �     �     �     �                 �  :     �     �  /   �  !   #     E     Y     l     y  -   �     �     �     �     �                    6     C     U     u     �     �     �     �     �     �                                                                 
   	                                                        Cache backend Cache was cleared. Cannot find default cache config. Cannot find default database. Clear cache Database driver Django version Engine was restarted. Garbage collector cleared the collected objects. Home Memory usage Memory used by process Operating system P-Cart version PID Process command line Python version Restart engine SSHOP_VERSION is not defined. System info System user Thread number Total memory Virtual environment bytes sys.prefix is not defined. Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-23 16:57+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 Backend mezipaměti Mezipaměť byla vyčištěna. Nelze najít výchozí konfiguraci mezipaměti. Nelze najít výchozí databázi. Vymazat mezipaměť Ovladač databáze Verze Django Pohon byl restartován. Kolektor odpadu očistil sebrané předměty. Úvod Využití paměti Používaná procesem paměť Operační systém Verze P-Cart PID Příkazový řádek procesu Verze Python Restartovat pohon SSHOP_VERSION není defiována. Systémové informace Uživatel systemu Čislo smyčky Celková paměť Virtuální prostředí bajty sys.prefix není defiován. 