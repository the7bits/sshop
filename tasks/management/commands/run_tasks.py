# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    args = ''
    help = 'Import 1C XML data to LFS'

    def handle(self, *args, **options):
        from tasks.models import Task
        from tasks.utils import get_func
        from datetime import datetime
        try:
            if Task.objects.filter(status=3):
                return
            ids = [task.id for task in Task.objects.filter(status=0, run_after__lte=datetime.now())]
            tasks = list(Task.objects.filter(id__in=ids))
            Task.objects.filter(id__in=ids).update(status=3)
            for task in tasks:
                try:
                    func = get_func(task.function_name)
                    result = func(**eval(task.arguments))
                    if result is True or result is None:
                        task.status = 5
                    else:
                        task.error_msg = result
                        task.status = 10
                except Exception, e:
                    task.error_msg = str(e)
                    task.status = 10
                task.finished_at = datetime.now()
                task.save()
        except Exception, e:
            print e
