# coding: utf-8
from optparse import make_option
from django.core.management.base import BaseCommand
from tasks.api import TaskQueueManager

class Command(BaseCommand):
    option_list = BaseCommand.option_list + (
        make_option('--jobs', '-j', dest='jobs', help=u'The maximum number of jobs to run'),
        make_option('--repeat', '-r', dest='repeat', help=u'Time in minutes for repeat flush'),

    )
    help = u'Does a single pass over the asynchronous queue'

    def handle(self, **options):
        import time
        tq = TaskQueueManager()
        sync_plugins()
        try:
            while True:
                result = tq.flush(jobs=int(options['jobs'] or 5))
                if result: print result
                if not options['repeat']:
                    return
                else:
                    time.sleep(int(options['repeat'])*60)
        except KeyboardInterrupt:
            health = tq.health()
            if 'running' in health:
                if health['running'] > 0:
                    print 'There are "freezed" job in queue. You need to resolve this manually.'
            return


def sync_plugins():
    try:
        from sshop import urls
    except Exception, e:
        print 'Cant sync all plugins. Error: %s' % e
