# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Job'
        db.create_table('tasks_job', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('args', self.gf('json_field.fields.JSONField')(default='null', blank=True)),
            ('kwargs', self.gf('json_field.fields.JSONField')(default='null', blank=True)),
            ('storage', self.gf('json_field.fields.JSONField')(default='null', blank=True)),
            ('result', self.gf('json_field.fields.JSONField')(default='null', blank=True)),
            ('priority', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('added', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('scheduled', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('started', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('executed', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('repeat_timeout', self.gf('django.db.models.fields.PositiveIntegerField')(default=0)),
            ('status', self.gf('django.db.models.fields.CharField')(default='pending', max_length=30)),
        ))
        db.send_create_signal('tasks', ['Job'])


    def backwards(self, orm):
        # Deleting model 'Job'
        db.delete_table('tasks_job')


    models = {
        'tasks.job': {
            'Meta': {'object_name': 'Job'},
            'added': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'args': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'}),
            'executed': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kwargs': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'priority': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'repeat_timeout': ('django.db.models.fields.PositiveIntegerField', [], {'default': '0'}),
            'result': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'}),
            'scheduled': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'started': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'pending'", 'max_length': '30'}),
            'storage': ('json_field.fields.JSONField', [], {'default': "'null'", 'blank': 'True'})
        },
        'tasks.task': {
            'Meta': {'object_name': 'Task'},
            'added_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'arguments': ('django.db.models.fields.TextField', [], {'default': "'{}'"}),
            'error_msg': ('django.db.models.fields.TextField', [], {'default': "''", 'blank': 'True'}),
            'finished_at': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'function_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'run_after': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'auto_now_add': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        }
    }

    complete_apps = ['tasks']