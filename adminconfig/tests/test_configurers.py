# coding: utf-8
from django import forms
from django.test import TestCase

from adminconfig.utils import BaseConfig


class TestForm(forms.Form):
    test_field1 = forms.CharField()


class TestConfig(BaseConfig):
    form = TestForm
    block_name = 'test'

    def __init__(self):
        super(TestConfig, self).__init__()

        self.default_data = {
            'TEST_VALUE': 'test_value',
        }

        self.option_translation_table = (
            ('TEST_VALUE', 'test_field1'),
        )


class BaseConfigTest(TestCase):
    def setUp(self):
        self.configurer = TestConfig()
        self.configurer.load_data()

    def test_block_availability(self):
        block = self.configurer.get_block()
        self.assertEqual(block['TEST_VALUE'], 'test_value')
