from price_navigator.models import PriceNavigator
# from tasks.utils import add_task
from datetime import datetime, time
from django.shortcuts import get_object_or_404
from django.template import Context, Template
from lfs.catalog.models import Product
# import lfs.core.utils
from django.conf import settings
import zipfile
from django.contrib.sites.models import Site
from tasks.api import TaskQueueManager

SSHOP_PRICE_DIR = getattr(settings, 'SSHOP_PRICE_DIR', '')


def check_tasks(price_navigator, by_request=False):
    task_manager = TaskQueueManager()
    run_after = None
    if price_navigator.active:
        if price_navigator.update_rate == 0:
            run_after = datetime.combine(datetime.now(), time(23, 59))
        elif price_navigator.update_rate == 5:
            now = datetime.now()
            if now.hour < 12:
                run_after = datetime.combine(now, time(12, 00))
            else:
                run_after = datetime.combine(now, time(23, 59))
        elif price_navigator.update_rate == 10:
            now = datetime.now()
            if now.hour < 8:
                run_after = datetime.combine(now, time(8, 00))
            elif now.hour < 16:
                run_after = datetime.combine(now, time(16, 00))
            else:
                run_after = datetime.combine(now, time(23, 59))

    if by_request:
        run_after = datetime.now()
    if run_after:
        task_manager.schedule(
            'price_navigator.utils.generate',
            kwargs={'price_navigator_id': price_navigator.id},
            run_after=run_after
        )


def generate(price_navigator_id, **kwargs):
    from lfs.core.utils import get_default_shop
    try:
        price_navigator = get_object_or_404(
            PriceNavigator, pk=price_navigator_id)
        if price_navigator.product_from_child_categories:
            price_navigator_categories = price_navigator.categories.all()
            categories = []
            for category in price_navigator_categories:
                categories.extend(category.get_descendants())
                categories.append(category)
        else:
            categories = price_navigator.categories.all()
        products = Product.objects.filter(
            categories__in=categories,
            status__in=price_navigator.product_statuses.all())
        shop = get_default_shop()

        t = Template(price_navigator.template)
        c = Context({
            'SHOP': shop,
            'date': '{:%Y-%m-%d %H:%m}'.format(datetime.now()),
            'categories': categories,
            'products': products,
            'site': 'http://%s' % Site.objects.get(id=settings.SITE_ID),
        })

        path = SSHOP_PRICE_DIR + price_navigator.file_name
        text = t.render(c)  # .encode(price_navigator.get_coding_display())
        for symbol in price_navigator.replacement:
            try:
                replaceable = symbol.decode('string_escape').encode('utf8')
            except:
                replaceable = symbol.decode('string_escape').decode('utf8')
            text = text.replace(
                replaceable,
                price_navigator.replacement[symbol].encode(
                    price_navigator.get_coding_display())
            )

        text = text.encode(price_navigator.get_coding_display())
        if price_navigator.pack_to_zip:
            zf = zipfile.ZipFile(path[:path.rfind('.')] + '.zip', mode='w')
            zf.writestr(price_navigator.file_name, text)
            zf.close()
        else:
            _file = open(path, 'w')
            _file.write(text)
            _file.close()

        price_navigator.last_generation = datetime.now()
        price_navigator.save()
        check_tasks(price_navigator)
    except Exception, e:
        check_tasks(price_navigator)
        return str(e)
    return True
