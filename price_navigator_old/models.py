# coding: utf-8
import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _
from lfs.catalog.models import ProductStatus, Category
from jsonfield import JSONField


class PriceNavigator(models.Model):
    UPDATE_RATES = (
        (0, _(u'Once a day')),
        (5, _(u'Twice a day')),
        (10, _(u'Three times a day')),
        (15, _(u'By request')),
    )

    CODINGS = (
        (0, _(u'utf-8')),
        (5, _(u'windows-1251')),
    )

    active = models.BooleanField(_(u'Active'), default=False)
    name = models.CharField(_(u'Name'), max_length=100)
    file_name = models.CharField(_(u'File name'), max_length=100)
    update_rate = models.IntegerField(
        _(u'Update rate'), default=0, choices=UPDATE_RATES)
    coding = models.IntegerField(_(u'Coding'), default=0, choices=CODINGS)
    product_statuses = models.ManyToManyField(
        ProductStatus, verbose_name=_(u'Statuses'))
    pack_to_zip = models.BooleanField(_(u'Pack to zip'), default=False)
    categories = models.ManyToManyField(
        Category, verbose_name=_(u'Categories'))
    product_from_child_categories = models.BooleanField(
        verbose_name=_(u'Take products from child categories'), default=False)
    template = models.TextField(_(u'Template'))
    replacement = JSONField(
        verbose_name=_(u'Symbol replacement'),
        blank=True, null=True, default='')
    order = models.IntegerField(default=10)
    last_generation = models.DateTimeField(
        _(u"Last generation"),
        auto_now_add=True, default=datetime.datetime.now)

    class Meta:
        verbose_name = _(u'Price navigator')
        verbose_name_plural = _(u'Price navigators')

    def __unicode__(self):
        return self.name
