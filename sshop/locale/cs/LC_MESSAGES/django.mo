��    =        S   �      8     9     B     S     d     l     t     }     �     �  	   �     �     �     �  !   �     
          $     9     M     `     q     w  %        �  
   �     �     �     �     �     �     �     �            	             +     ;     B  
   H     S  
   Y     d     q     �     �     �     �     �     �     �     �  3   �          .     :     J     S     j     y  �  �     b
     s
     �
     �
     �
     �
     �
     �
  	   �
  
   �
     �
          +  &   H  	   o     y     �     �     �     �     �     �  ?         @     P     a     w     }     �     �     �  
   �     �     �  	   �     �     �          %     .     E     M  
   ^     i     }     �     �  W   �  
   �     �  '   �     #  A   *     l  
   �     �     �     �     �     �     .                         -           9   1         %                  /       '      +              *   	               
      ,       $         "                          !       =   <           (   6   0   )   4               8   :      3          ;       5           #   2      &   7              Accounts By default email By default phone Captcha Catalog Checkout Compatibility Configuration Customer Customers Customers and orders DB task queue Default price calculator Default shipping price calculator Diagnose Discount on cart Discount on category Discount on product Do not use captcha Dummy task queue Email English Enter full number with regional code. FAQ questions FAQ topics File management Filter General Groups HTML HTML with images Import data Jobs Login Marketing Message sending Null task queue P-Cart Pages Pagination Phone Plain text Registration Reports and analytics Reviews SEO Service Set task for clear sessions Settings Shop Simple search config Slider Soon you will get an order confirmation via E-Mail. Standard regex and rules System info Task priorities Username Vouchers and discounts Русский Українська Project-Id-Version: LFS
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-12 13:48+0200
PO-Revision-Date: 2014-12-12 21:27+0300
Last-Translator: Antonín <antonin.lampiga@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.7.1
 Emaiolvý účty Přednastavený email Přednastavený telefon Captcha Katalog Sestavení objednávek Kompatibilita Konfigurace Zákaznik Zákaznici Zákaznici a objednávky Fronta úkolů v databázi Standartní kalkulátor ceny Standartní kalkulátor ceny dodání  Diagnóza Sleva na nákupní košík Sleva na kategorii Sleva na zboží Nepoužívat captchu Okmažité provedení Emailová adresa Angličtina Zadejte celé telefonní číslo včetně předčíslí regionu Časté otázky Časté náměty Spravování souborů Filtr Všeobecné Skupiny Obsah HTML s obrázkami Import dat Fonové úkoly Uživatelské jméno Marketing Odeslání zprav Ignorování  fonových úkolů Nákupní košík Stránka Číslování stránek Telefon Jednoduchý text Registrace Zprávy a analitika Kontrola SEO Servis Nastavit úkol pro Установить задачу для очистки сессий Nastavení Obchod Konfigurace obyčejného vyhledávání Slider Brzy obdržíte potvrzení objednávky prostřednictvím e-mailu. Standartní pravidla O systému Priorita úkolů Jméno uživatele Certifikáty a slevy Ruština Ukrajinština 