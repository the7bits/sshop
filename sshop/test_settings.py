# coding: utf-8
TEST_SETTINGS = True
from settings import *

# Make tests faster
SOUTH_TESTS_MIGRATE = False
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'sshop.sqlite',
    },
}

# AVAILABLE_SHOP_THEMES = (
#     ('basic_theme', u'Basic theme'),
# )
# SHOP_THEME = None

# # Extentions
# ALLOW_REGIONS = True

# SSHOP_USE_1C = True
# USE_HR_URLS = True

# PLUGINS_REPOSITORY_DEFAULT_URL = 'http://sitepanel.the7bits.com/'
# PROJECT_IDENTIFIER = 'markendev_sshop'
# PROJECT_SECRET_CODE = '6af7a@Z2'

# from adminconfig.utils import JSONConfigFile
# json_config = JSONConfigFile(GLOBAL_JSON_CONFIG)
# json_config.get_full_config()
# for block_name in json_config.config.keys():
#     block = json_config.config[block_name]
#     for i in block.keys():
#         globals()[i] = block[i]


# if SHOP_THEME is not None and SHOP_THEME != "basic_theme":
#     INSTALLED_APPS = (
#         SHOP_THEME,
#     ) + INSTALLED_APPS
