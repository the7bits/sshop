# coding: utf-8
from django import forms
from django.conf import settings
from django.utils.translation import ugettext as _
from adminconfig.utils import BaseConfig


class CaptchaConfigForm(forms.Form):
    """This form describes the configuration fields
    for captchas options.

    active_captcha
        Active captcha field name.
    """
    active_captcha = forms.ChoiceField(
        label=_(u'Active captcha'), choices=settings.CAPTCHAS)
    use_for_register = forms.BooleanField(
        label=_(u'Use for register'), required=False)
    use_for_anon_checkout = forms.BooleanField(
        label=_(u'Use for anonymous checkout'), required=False)


class CaptchaConfig(BaseConfig):
    """Configurator for captchas options in config.
    """
    form_class = CaptchaConfigForm
    block_name = 'captcha'

    def __init__(self):
        super(CaptchaConfig, self).__init__()

        self.default_data = {
            'ACTIVE_CAPTCHA': settings.ACTIVE_CAPTCHA,
            'CAPTCHA_USE_FOR_REGISTER': settings.CAPTCHA_USE_FOR_REGISTER,
            'CAPTCHA_USE_FOR_ANON_CHECKOUT':
            settings.CAPTCHA_USE_FOR_ANON_CHECKOUT,
        }

        self.option_translation_table = (
            ('ACTIVE_CAPTCHA', 'active_captcha'),
            ('CAPTCHA_USE_FOR_REGISTER', 'use_for_register'),
            ('CAPTCHA_USE_FOR_ANON_CHECKOUT', 'use_for_anon_checkout'),
        )
