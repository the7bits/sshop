# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Callback(models.Model):
    name = models.CharField(_(u'Name'), max_length=50)
    phone = models.CharField(_(u"Phone"), max_length=20)
    time_from = models.TimeField(_(u"From"), blank=True, default='')
    time_to = models.TimeField(_(u"To"), blank=True, default='')
    exported = models.BooleanField(_(u'Exported'), default=False)
    status = models.CharField(
        _(u'Status'), max_length=10, default='', blank=True)

    class Meta:
        verbose_name = _(u'Call request')
        verbose_name_plural = _(u'Call requests')

    def __unicode__(self):
        return '%s %s' % (self.name, self.phone)
