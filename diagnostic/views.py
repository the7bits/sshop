# coding: utf-8
import simplejson
from django.conf import settings
from django.utils.translation import ugettext as _
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import permission_required
from lfs.core.utils import import_symbol, LazyEncoder


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def diagnose(request, template='diagnostic/diagnose.html'):
    test_list = getattr(settings, 'DIAGNOSTIC_TESTS', [])

    diag_tests = []
    for i in test_list:
        diag_tests.append(import_symbol(i)())
    return render_to_response(template, RequestContext(request, {
        'diag_tests': diag_tests,
    }))


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def run_test(request):
    test_id = request.GET.get('test_id')
    test_list = getattr(settings, 'DIAGNOSTIC_TESTS', [])

    t_name = test_list[int(test_id)]
    diag_class = import_symbol(t_name)
    diag_test = diag_class()
    _result = diag_test.check()
    result = simplejson.dumps(
        {
            'result': _result[0],
            'msg': _result[1],
            'how_to_fix': diag_test.how_to_fix(),
            'report': diag_test.get_report(),
            'has_multiple_solutions': diag_test.has_multiple_solutions(),
        },
        cls=LazyEncoder)
    return HttpResponse(result)


@permission_required(
    "core.manage_shop",
    login_url=getattr(
        settings, 'ADMIN_AJAX_FORBIDDEN_URL', '/admin-ajax-forbidden/'))
def fix_test(request):
    test_id = request.GET.get('test_id')
    solution = request.GET.get('solution')
    test_list = getattr(settings, 'DIAGNOSTIC_TESTS', [])

    t_name = test_list[int(test_id)]
    diag_class = import_symbol(t_name)
    diag_test = diag_class()
    if solution:
        _result = diag_test.fix(solution=int(solution))
    else:
        _result = diag_test.fix()
    result = simplejson.dumps(
        {
            'result': _result[0],
            'msg': _result[1],
        },
        cls=LazyEncoder)
    return HttpResponse(result)
