��          �      �       H  �   I     �     �     �     �     �     �                      3     :  
   J  �  U  4  !     V     ]     n  #   �     �  
   �     �  ,   �  *   
     5  $   H     m            	   
                                                  
You can use this interface for start the automatic test of
check the database health. Press &quot;Start tests&quot; for start.
 Actions Comment Diagnose Fix this problem Home Number Processing... Proposed solution Proposed solutions Result Start all tests Start test Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-08-01 13:16+0300
PO-Revision-Date: 2014-08-01 13:18+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 
Ви можете використовувати даний інтерфейс для запуску серії автоматизовнаих тестів
для перевірки коректності даних в БД. Натисніть &quot;Запустити тести&quot; для старту.
 Дії Коментар Діагностика Виправити проблему Домівка Число Виконується... Пропонований розв’язок Пропоновані розв’язки Результат Запустити всі тести Запустити тест 