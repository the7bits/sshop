# coding: utf-8


class BaseDiagnosticTest(object):
    def __init__(self):
        pass

    def get_description(self):
        return ''

    def has_multiple_solutions(self):
        return False

    def how_to_fix(self):
        return ''

    def check(self):
        pass

    def get_report(self):
        return ''

    def fix(self, solution=0):
        pass
