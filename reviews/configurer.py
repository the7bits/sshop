# coding: utf-8
from django import forms
from django.utils.translation import ugettext_lazy as _
from adminconfig.utils import BaseConfig


class ReviewsConfigForm(forms.Form):
    need_moderate = forms.BooleanField(
        label=_(u'Moderate reviews'),
        required=False
    )


class ReviewsConfig(BaseConfig):
    form_class = ReviewsConfigForm
    block_name = 'reviews'

    def __init__(self):
        super(ReviewsConfig, self).__init__()

        self.default_data = {
            'REVIEWS_IS_MODERATED': False,
        }

        self.option_translation_table = (
            ('REVIEWS_IS_MODERATED', 'need_moderate'),
        )
