# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-04-03 15:51+0300\n"
"PO-Revision-Date: 2013-07-17 17:41+0300\n"
"Last-Translator: Natalia <natalia20061990@gmail.com>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 1.5.4\n"

#: admin.py:26
msgid "It has been moderated"
msgstr "Прошел модерацию"

#: configurer.py:9
msgid "Moderate reviews"
msgstr "Модерировать мнения"

#: models.py:17
msgid "Content type"
msgstr "Тип контента"

#: models.py:20
msgid "Content ID"
msgstr "ID контента"

#: models.py:28
msgid "User"
msgstr "Пользователь"

#: models.py:30
msgid "Session ID"
msgstr "ID сессии"

#: models.py:32
msgid "Name"
msgstr "Имя"

#: models.py:33
msgid "E-mail"
msgstr "E-mail"

#: models.py:34
msgid "Comment"
msgstr "Комментарий"

#: models.py:35
msgid "Score"
msgstr "Оценка"

#: models.py:36
msgid "Active"
msgstr "Активно"

#: models.py:39
msgid "Creation date"
msgstr "Дата создания"

#: models.py:41
msgid "IP address"
msgstr "IP адрес"

#: models.py:47
msgid "Review"
msgstr "Мнение"

#: models.py:48
msgid "Reviews"
msgstr "Мнения"

#: settings.py:5
msgid "*"
msgstr "*"

#: settings.py:6
msgid "**"
msgstr "**"

#: settings.py:7
msgid "***"
msgstr "***"

#: settings.py:8
msgid "****"
msgstr "****"

#: settings.py:9
msgid "*****"
msgstr "*****"

#: views.py:39
msgid "This field is required"
msgstr "Поле обязательно для заполнения"

#~ msgid "Content"
#~ msgstr "Контент"
