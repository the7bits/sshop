# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'LogFile'
        db.create_table('logtailer_logfile', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=180)),
            ('path', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal('logtailer', ['LogFile'])

        # Adding model 'Filter'
        db.create_table('logtailer_filter', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=180)),
            ('regex', self.gf('django.db.models.fields.CharField')(max_length=500)),
        ))
        db.send_create_signal('logtailer', ['Filter'])

        # Adding model 'LogsClipboard'
        db.create_table('logtailer_logsclipboard', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=180)),
            ('notes', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('logs', self.gf('django.db.models.fields.TextField')()),
            ('log_file', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['logtailer.LogFile'])),
        ))
        db.send_create_signal('logtailer', ['LogsClipboard'])


    def backwards(self, orm):
        # Deleting model 'LogFile'
        db.delete_table('logtailer_logfile')

        # Deleting model 'Filter'
        db.delete_table('logtailer_filter')

        # Deleting model 'LogsClipboard'
        db.delete_table('logtailer_logsclipboard')


    models = {
        'logtailer.filter': {
            'Meta': {'object_name': 'Filter'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '180'}),
            'regex': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'logtailer.logfile': {
            'Meta': {'object_name': 'LogFile'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '180'}),
            'path': ('django.db.models.fields.CharField', [], {'max_length': '500'})
        },
        'logtailer.logsclipboard': {
            'Meta': {'object_name': 'LogsClipboard'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log_file': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['logtailer.LogFile']"}),
            'logs': ('django.db.models.fields.TextField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '180'}),
            'notes': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['logtailer']