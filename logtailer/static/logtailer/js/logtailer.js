/*
 * Logtailer object
 */

var LogTailer = {
	timeout_id: null,
	timeout: 2000,
	scroll: true,
	file_id: 0
}

LogTailer.getLines = function (){
	LogTailer.currentScrollPosition = $("#log-window").scrollTop();
	$.ajax({
	  url: LOGTAILER_URL_GETLOGLINE,
	  success: function(result){
	  				console.log('getLines');
	  				LogTailer.printLines(result);
	  		   },
	  dataType: "json"
	});

}

LogTailer.getHistory = function ( callback ){
	LogTailer.currentScrollPosition = $("#log-window").scrollTop();
	$.ajax({
	  url: LOGTAILER_URL_GETHISTORY,
	  success: function(result){
	  				console.log('getHistory');
	  				LogTailer.printLines(result);
                    callback && callback();
	  		   },
	  dataType: "json"
	});

}

LogTailer.printLines = function(result){
	if($("#apply-filter").is(':checked')){
		for(var i=0;i<result.length;i++){
			pattern = $("#filter").val();
			if($('#filter-select').val()!="custom"){
				pattern = $('#filter-select').val();
			}
			try {
			    regex = eval(pattern);
			}
			catch(err) {
			    regex = pattern;
			}
			position = result[i].search(regex);
			if(position>-1){
				$("#log-window").append(result[i]);
			}
		}		
	}
	else{
		for(var i=0;i<result.length;i++){
			if(result[i].length>0){
				$("#log-window").append(result[i]);
			}
		}
	}
	if(LogTailer.scroll && result.length){
		$("#log-window").scrollTop($("#log-window")[0].scrollHeight - $("#log-window").height());
	}
	else{
		$("#log-window").scrollTop(LogTailer.currentScrollPosition);
	}
	window.clearTimeout(LogTailer.timeout_id);
	LogTailer.timeout_id = window.setTimeout("LogTailer.getLines("+LogTailer.file_id+")", LogTailer.timeout);
}

LogTailer.startReading = function (){
    if ($('#log-window').is(":empty") ) {
        LogTailer.getHistory( function(){
            LogTailer.timeout_id = window.setTimeout("LogTailer.getLines("+LogTailer.file_id+")", LogTailer.timeout);
        });
    } else {
        alert("set getlines timeout");
        LogTailer.timeout_id = window.setTimeout("LogTailer.getLines("+LogTailer.file_id+")", LogTailer.timeout);
    }
	$("#start-button").hide();
	$("#stop-button").show();
}

LogTailer.stopReading = function (){
	window.clearTimeout(LogTailer.timeout_id);
	$("#stop-button").hide();
	$("#start-button").show();
}


LogTailer.changeAutoScroll = function(){
	if(LogTailer.scroll){
      	LogTailer.scroll = false;
      	$('#auto-scroll').val("OFF");
      	$('#auto-scroll').css('color', 'red');
    }
    else{
      	LogTailer.scroll = true;
      	$('#auto-scroll').val("ON");
      	$('#auto-scroll').css('color', 'green');
    }
}

LogTailer.customFilter = function(){
	if($('#filter-select').val()=="custom"){
	    $('#filter').show();
	}
	else{
		$('#filter').hide();
	}
}
