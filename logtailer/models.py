from django.db import models
from django.utils.translation import ugettext_lazy as _


class LogFile(models.Model):
    name = models.CharField(_('name'), max_length=180)
    path = models.CharField(_('path'), max_length=500)

    class Meta:
        verbose_name = _('Log file')
        verbose_name_plural = _('Log files')

    def __unicode__(self):
        return '%s' % self.name


class Filter(models.Model):
    name = models.CharField(_('name'), max_length=180)
    regex = models.CharField(_('regex'), max_length=500)

    class Meta:
        verbose_name = _('Log filter')
        verbose_name_plural = _('Log filters')

    def __unicode__(self):
        return '%s | %s: %s ' % (self.name, _('pattern'), self.regex)


class LogsClipboard(models.Model):
    name = models.CharField(_('name'), max_length=180)
    notes = models.TextField(_('notes'), blank=True, null=True)
    logs = models.TextField(_('logs'))
    log_file = models.ForeignKey(LogFile, verbose_name=_('log_file'))

    def __unicode__(self):
        return "%s" % self.name

    class Meta:
        verbose_name = _('Log clipboard')
        verbose_name_plural = _('Log clipboards')
