from django.contrib import admin
from logtailer.models import LogFile, Filter, LogsClipboard


class LogFileAdmin(admin.ModelAdmin):
    list_display = ('__unicode__', 'path')
    search_fields = ('name', 'path')

    class Media:
        js = ('/static/logtailer/js/jquery.colorbox-min.js',)
        css = {
            'all': ('/static/logtailer/css/colorbox.css',)
        }


class FilterAdmin(admin.ModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'regex')


class LogsClipboardAdmin(admin.ModelAdmin):
    search_fields = ('name', 'notes', 'log_file')
    list_display = ('name', 'notes', 'log_file')
    readonly_fields = ('name', 'notes', 'logs', 'log_file')


admin.site.register(LogFile, LogFileAdmin)
admin.site.register(Filter, FilterAdmin)
admin.site.register(LogsClipboard, LogsClipboardAdmin)
