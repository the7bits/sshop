from django.contrib import admin
from tire_shop.wheels_auto.models import BoltPattern, Car, Modification, Nut, NutType, ParamTire, ParamType, ParamWheel, Vendor

admin.site.register(Vendor)
admin.site.register(BoltPattern)
admin.site.register(Car)
admin.site.register(Modification)
admin.site.register(Nut)
admin.site.register(NutType)
admin.site.register(ParamTire)
admin.site.register(ParamType)
admin.site.register(ParamWheel)
