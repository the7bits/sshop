# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'AutoMake'
        db.create_table('tires_variants_automake', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('tires_variants', ['AutoMake'])

        # Adding model 'BoltPattern'
        db.create_table('tires_variants_boltpattern', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal('tires_variants', ['BoltPattern'])

        # Adding model 'AutoModel'
        db.create_table('tires_variants_automodel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('auto_make', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tires_variants.AutoMake'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('year', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('x1', self.gf('django.db.models.fields.IntegerField')()),
            ('x2', self.gf('django.db.models.fields.IntegerField')()),
            ('yy', self.gf('django.db.models.fields.IntegerField')()),
            ('inc', self.gf('django.db.models.fields.IntegerField')()),
            ('wst', self.gf('django.db.models.fields.IntegerField')()),
            ('wm', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal('tires_variants', ['AutoModel'])

        # Adding model 'AutoModClar'
        db.create_table('tires_variants_automodclar', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('auto_model', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tires_variants.AutoModel'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('wheel_size1', self.gf('django.db.models.fields.IntegerField')()),
            ('wheel_size2', self.gf('django.db.models.fields.IntegerField')()),
            ('offsets1', self.gf('django.db.models.fields.IntegerField')()),
            ('offsets2', self.gf('django.db.models.fields.IntegerField')()),
            ('bolt_pattern', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['tires_variants.BoltPattern'])),
            ('tyres', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('tires_variants', ['AutoModClar'])


    def backwards(self, orm):
        # Deleting model 'AutoMake'
        db.delete_table('tires_variants_automake')

        # Deleting model 'BoltPattern'
        db.delete_table('tires_variants_boltpattern')

        # Deleting model 'AutoModel'
        db.delete_table('tires_variants_automodel')

        # Deleting model 'AutoModClar'
        db.delete_table('tires_variants_automodclar')


    models = {
        'tires_variants.automake': {
            'Meta': {'object_name': 'AutoMake'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'tires_variants.automodclar': {
            'Meta': {'object_name': 'AutoModClar'},
            'auto_model': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tires_variants.AutoModel']"}),
            'bolt_pattern': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tires_variants.BoltPattern']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'offsets1': ('django.db.models.fields.IntegerField', [], {}),
            'offsets2': ('django.db.models.fields.IntegerField', [], {}),
            'tyres': ('django.db.models.fields.TextField', [], {}),
            'wheel_size1': ('django.db.models.fields.IntegerField', [], {}),
            'wheel_size2': ('django.db.models.fields.IntegerField', [], {})
        },
        'tires_variants.automodel': {
            'Meta': {'object_name': 'AutoModel'},
            'auto_make': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['tires_variants.AutoMake']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'inc': ('django.db.models.fields.IntegerField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'wm': ('django.db.models.fields.IntegerField', [], {}),
            'wst': ('django.db.models.fields.IntegerField', [], {}),
            'x1': ('django.db.models.fields.IntegerField', [], {}),
            'x2': ('django.db.models.fields.IntegerField', [], {}),
            'year': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'yy': ('django.db.models.fields.IntegerField', [], {})
        },
        'tires_variants.boltpattern': {
            'Meta': {'object_name': 'BoltPattern'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['tires_variants']