from django.contrib import admin
from tire_shop.tires_variants.models import AutoMake, BoltPattern, AutoModel, AutoModClar

admin.site.register(AutoMake)
admin.site.register(BoltPattern)
admin.site.register(AutoModel)
admin.site.register(AutoModClar)