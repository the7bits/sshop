from django.contrib import admin
from tire_shop.wheels_moto.models import Vendor, Modification, Car, Param

admin.site.register(Vendor)
admin.site.register(Modification)
admin.site.register(Car)
admin.site.register(Param)