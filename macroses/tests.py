# coding: utf-8
from django.test import TestCase
from .models import Macros


class MacrosTestCase(TestCase):
    """
    """
    def setUp(self):
        self.m1 = Macros.objects.create(
            name='test_macros',
            identifier='test_macros',
        )

    def test_run(self):
        """
        """
        self.m1.run()
        self.assertEqual(self.m1.output, u'Hello!\n')

    def test_write_output(self):
        """
        """
        self.m1.write_output(u'AAA\n')
        self.assertEqual(self.m1.output, u'AAA\n')

        self.m1.write_output(u'BBB\n')
        self.assertEqual(self.m1.output, u'BBB\n')

        self.m1.append_output = True
        self.m1.write_output(u'CCC\n')
        self.assertEqual(self.m1.output, u'BBB\nCCC\n')
