# coding: utf-8
from .models import Macros


def get_macros_listener(macros_id):
    def run_macros_listener(sender=None, instance=None, **kwargs):
        m = Macros.objects.get(id=macros_id)
        m.run(sender=sender, instance=instance, **kwargs)

    return run_macros_listener
