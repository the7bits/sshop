# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Macros'
        db.create_table('macroses_macros', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('identifier', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('code', self.gf('django.db.models.fields.TextField')(default='\n# coding: utf-8\nfrom macroses.default import BaseMacros\n\nclass MainMacros(object):\n    pass\n\n')),
            ('entry_point', self.gf('django.db.models.fields.CharField')(default='MainMacros', max_length=100)),
            ('signal_name', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('signal_sender', self.gf('django.db.models.fields.CharField')(default='', max_length=200, blank=True)),
            ('output', self.gf('django.db.models.fields.TextField')()),
            ('append_output', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('macroses', ['Macros'])


    def backwards(self, orm):
        # Deleting model 'Macros'
        db.delete_table('macroses_macros')


    models = {
        'macroses.macros': {
            'Meta': {'object_name': 'Macros'},
            'append_output': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'code': ('django.db.models.fields.TextField', [], {'default': "'\\n# coding: utf-8\\nfrom macroses.default import BaseMacros\\n\\nclass MainMacros(object):\\n    pass\\n\\n'"}),
            'entry_point': ('django.db.models.fields.CharField', [], {'default': "'MainMacros'", 'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'signal_name': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'}),
            'signal_sender': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '200', 'blank': 'True'})
        }
    }

    complete_apps = ['macroses']