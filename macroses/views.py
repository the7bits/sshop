# coding: utf-8
from django.http import Http404
from .models import Macros


def get_macros_view(
        request, macros_identifier, name, *args, **kwargs):
    """
    """
    ms = Macros.objects.filter(identifier=macros_identifier)
    if len(ms) == 0:
        raise Http404
    else:
        m = ms[0]

    return m.run_method(name, request, *args, **kwargs)
