# coding: utf-8
import logging

from django.db import models
# from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from .settings import MACROS_TEMPLATE, ENTRY_POINT_CLASS
from .utils import importCode


logger = logging.getLogger('sshop')


class Macros(models.Model):
    name = models.CharField(_(u'Name'), max_length=200)
    identifier = models.CharField(_(u'Identifier'), max_length=200)
    code = models.TextField(_(u'Code'), default=MACROS_TEMPLATE)
    entry_point = models.CharField(
        _(u'Entry point'), max_length=100, default=ENTRY_POINT_CLASS)

    signal_name = models.CharField(
        _(u'Signal'), max_length=200, default='', blank=True)
    signal_sender = models.CharField(
        _(u'Sender'), max_length=200, default='', blank=True)

    output = models.TextField(_(u'Output'), default='', blank=True)
    append_output = models.BooleanField(
        _(u'Append output'), default=False)

    class Meta:
        verbose_name = _(u'Macros')
        verbose_name_plural = _(u'Macroses')

    def __unicode__(self):
        return self.name

    def run(self, *args, **kwargs):
        from cStringIO import StringIO
        import sys
        import traceback
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        m = importCode(self.code, self.identifier)
        c = None
        try:
            c = getattr(m, self.entry_point)
        except:
            traceback.print_exc(file=sys.stdout)
        if c is not None:
            try:
                o = c(*args, **kwargs)
                o.run()
            except:
                traceback.print_exc(file=sys.stdout)
        sys.stdout = old_stdout
        self.write_output(mystdout.getvalue())

    def write_output(self, text):
        if self.append_output:
            self.output = self.output + text.decode('utf-8')
        else:
            self.output = text
        self.save()

    def change_urlpatterns(self, urlpatterns):
        from cStringIO import StringIO
        import sys
        import traceback
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        m = importCode(self.code, self.identifier)
        c = None
        try:
            c = getattr(m, self.entry_point)
        except:
            traceback.print_exc(file=sys.stdout)
        if c is not None:
            try:
                o = c()
                sys.stdout = old_stdout
                return o.change_urlpatterns(urlpatterns)
            except:
                traceback.print_exc(file=sys.stdout)
        sys.stdout = old_stdout
        self.write_output(mystdout.getvalue())
        return None

    def run_method(self, name, *argv, **kwargs):
        from cStringIO import StringIO
        import sys
        import traceback
        old_stdout = sys.stdout
        sys.stdout = mystdout = StringIO()
        m = importCode(self.code, self.identifier)
        c = None
        try:
            c = getattr(m, self.entry_point)
        except:
            traceback.print_exc(file=sys.stdout)
        if c is not None:
            try:
                o = c()
                sys.stdout = old_stdout
                return getattr(o, name)(*argv, **kwargs)
            except:
                traceback.print_exc(file=sys.stdout)
        sys.stdout = old_stdout
        self.write_output(mystdout.getvalue())
        return None
