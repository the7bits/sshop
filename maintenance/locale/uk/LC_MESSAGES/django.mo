��          �            x  "   y     �     �     �     �     �     �          
       5   )  9   _  
   �     �  r   �  �     L   �  .   9     h  $   u  -   �  )   �  )   �       8   5     n  j   �  A   �     ;     J  �   c     	                                             
                             Announce both for admins and users Announce for admins End time Full site down Maintenance Mode Maintenance message Maintenance messages Message Message for users Message type Service window is between %(start_time)s  and unknown Service window is between %(start_time)s and %(end_time)s Start time Stop API The application is currently undergoing scheduled maintenance. Please see the messages below for more information: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-16 12:59+0200
PO-Revision-Date: 2014-01-30 11:41+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Повідомлення для адмінів та користувачів Повідомлення для адмінів Кінець Повна зупинка сайту Сервісне обслуговування Сервісне повідомлення Сервісні повідомлення Повідомлення Повідомлення для користувачів Тип повідомлення Сервісні роботи з %(start_time)s  (час завершення поки невідомий) Сервісні роботи з %(start_time)s по %(end_time)s Початок Блокування API Роботу сайту тимчасово призупинено на період проведення сервісних робіт. Зверніть увагу на повідомлення для додаткової інформації: 