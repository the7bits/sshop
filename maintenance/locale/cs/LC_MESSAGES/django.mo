��          �            x  "   y     �     �     �     �     �     �          
       5   )  9   _  
   �     �  r   �  �     (   �     �     �     �               0     B     J     a  0   m  4   �     �     �  e   �     	                                             
                             Announce both for admins and users Announce for admins End time Full site down Maintenance Mode Maintenance message Maintenance messages Message Message for users Message type Service window is between %(start_time)s  and unknown Service window is between %(start_time)s and %(end_time)s Start time Stop API The application is currently undergoing scheduled maintenance. Please see the messages below for more information: Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-12-23 17:03+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2
 Oznámit pro administratory a uživatele Oznámit pro administratory Čas ukončení Full site dolů Režimm údržby Zpráva o údržbě Zprávy o udžbě Zpráva Zpráva pro uživatele Typ zprávy Servisní okno je mezi %(start_time)s a neznámo Servisní okno je mezi %(start_time)s a %(end_time)s Čas začatku Zastavit API Aplikace v současné době prochází pravidelnou údržbu. Pro více informací viz zprávy níže: 