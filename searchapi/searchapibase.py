# coding: utf-8
import abc


class BaseSearchConfig(object):
    __metaclass__ = abc.ABCMeta

    quick_search_template = ''
    search_template = ''
    models = []

    def __init__(self, request):
        self.request = request

    @abc.abstractmethod
    def search(self, query, order_by):
        """
            Return HttpResponse with rendered
            to search_template search results

            :param query: string
            :param order_by: string
            :rtype : HttpResponse
        """
        return

    @abc.abstractmethod
    def quick_search(self, query):
        """
            Return ajax response with rendered
            to quick_search_template search results

            :param query:string
            :rtype : HttpResponse
        """
        return
