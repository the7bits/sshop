from django.db import models
from django.utils.translation import ugettext_lazy as _


class Qaptcha(models.Model):
    key = models.CharField(
        max_length=32, default="",
        primary_key=True, verbose_name=_(u'Qaptcha key'))
    expiration = models.DateTimeField(
        blank=False, verbose_name=_(u'Expiration time'))
    validated = models.BooleanField(
        default=True, verbose_name=_(u'Validated'))

    def __unicode__(self):
        return u'{}'.format(self.key)

    class Meta:
        verbose_name = _(u'Qaptcha')
