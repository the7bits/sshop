# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Qaptcha'
        db.create_table('qaptcha_qaptcha', (
            ('key', self.gf('django.db.models.fields.CharField')(default='', max_length=32, primary_key=True)),
            ('expiration', self.gf('django.db.models.fields.DateTimeField')()),
            ('validated', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('qaptcha', ['Qaptcha'])


    def backwards(self, orm):
        # Deleting model 'Qaptcha'
        db.delete_table('qaptcha_qaptcha')


    models = {
        'qaptcha.qaptcha': {
            'Meta': {'object_name': 'Qaptcha'},
            'expiration': ('django.db.models.fields.DateTimeField', [], {}),
            'key': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '32', 'primary_key': 'True'}),
            'validated': ('django.db.models.fields.BooleanField', [], {'default': 'True'})
        }
    }

    complete_apps = ['qaptcha']