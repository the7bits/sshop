# cpding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _

from lfs.plugins import OrderNumberGenerator as Base


class OrderNumberGenerator(Base):
    """
    Generates order numbers and saves the last one.

    **Attributes:**

    last
        The last stored/returned order number.

    format
        The format of the integer part of the order number.
    """
    last = models.IntegerField(_(u"Last order number"), default=0)
    format = models.CharField(
        _(u'Format'),
        blank=True,
        max_length=20,
        help_text=_(u'For example, AA%s adds the prefix.'))

    class Meta:
        verbose_name = _(u'Order number generator')
        verbose_name_plural = _(u'Order number generator')

    def __unicode__(self):
        return 'format: %s' % self.format

    def get_next(self, formatted=True):
        """Returns the next order number.

        **Parameters:**

        formatted
            If True the number will be returned within the stored format.
        """

        number = OrderNumberGenerator.objects.select_for_update()\
            .get(id=self.id)
        number.last += 1
        number.save()

        if formatted and self.format:
            return self.format % number.last
        else:
            return number.last
