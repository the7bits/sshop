# -*- coding: utf-8 -*-
# SShop imports
import logging

from lfs.catalog.models import Category, Product, Property, Image, ProductAccessories
from lfs.catalog.models import ProductPropertyValue
from lfs.catalog.models import ProductStatus, ProductImage
from lfs.manufacturer.models import Manufacturer
from lfs.marketing.models import FeaturedProduct, Topseller, ProductList, ProductListItem
from lfs.core.utils import make_similar_unique_url
from lfs.core.utils import add_watermark

import xml.sax
import pytils
# import re
import zipfile
# import random
from ftplib import FTP
from StringIO import StringIO

import urllib2
# from django.core.files.base import ContentFile
# from sshop.settings import SSHOP_1C_LOGS_DIR
from django.conf import settings
# from django.core.cache import cache

from django.core.files import File
import os

from sbits_plugins import mediator  # , get_message_result

logger = logging.getLogger('sshop')

# FIX: may be rewrite as method of Product model
# def _update_positions(product):
#     product_properties = ProductPropertyValue.objects.filter(product=product).order_by('position')

#     for i, pv in enumerate(product_properties):
#         pv.position = (i + 1) * 10
#         pv.save()


def parse_product(text):
    '''
        This function contain class for parse
        products from XML file from 1C
    '''

    class XMLProductReader(xml.sax.ContentHandler):
        def __init__(self, imported=False, mywin=None):
            xml.sax.ContentHandler.__init__(self)
            self.success = True
            self.product_uid = ''

        def startDocument(self):
            # print 'Start products processed'
            # stores the tag to be processed
            self.data = ''
            self.catalog = False
            self.product = False

            self.id = ''
            self.id_flag = False
            self.articul = ''
            self.articul_flag = False
            self.product_name = ''
            self.product_name_flag = False
            self.key_1c = ''
            self.key_1c_flag = False
            self.availability = ''
            self.availability_flag = False
            self.manufacturer = ''
            self.manufacturer_flag = False
            self.basic_unit = ''
            self.basic_unit_flag = False

            self.details = '' # Значение реквизитов
            self.details_flag = False
            self.detail = '' # Значение реквизита
            self.detail_flag = False
            self.tax = ''
            self.tax_flag = False

            self.groups_id = ''
            self.groups = []
            self.groups_flag = False

            self.content = ''
            self.content_flag = False
            self.METATitle = ''
            self.METATitle_flag = False
            self.METADescription = []
            self.METADescription_flag = False
            self.METAKeywords = ''
            self.METAKeywords_flag = False
            self.is_new = ''
            self.is_new_flag = False
            self.popular = ''
            self.popular_flag = False
            self.featured = ''
            self.featured_flag = False

            self.accessory = []
            self.accessory_flag = False
            self.recommended = []
            self.recommended_flag = False

            self.short_description = []
            self.short_description_flag = False
            self.description = ''
            self.description_flag = False
            self.img = []
            self.img_flag = False
            self.links_product = ''
            self.links_product_flag = False
            self.yandex_link = ''
            self.yandex_link_flag = False

            self.filter = ''
            self.filter_flag = False
            self.option = False
            self.options = []
            self.option_group = ''
            self.option_group_flag = False
            self.option_name = ''
            self.option_name_flag = False
            self.option_value = ''
            self.option_value_flag = False

            self.available = False

            self.price = ''
            self.price_flag = False
            self.currency = ''
            self.currency_flag = False
            self.status = ''
            self.status_flag = False
            self.is_bid = False

            self.guarantee = '0'
            self.guarantee_flag = False

        def create_product(self):
            # print u'\tProcess product {product}'.format(product=self.product_name)
            if 'update_1c_settings' in getattr(settings, 'INSTALLED_APPS', []):
                from update_1c_settings.models import UpdateSettings
                obj = UpdateSettings.objects.all()[0]
                UPDATE_1C_ACCESSORIES = obj.accessories
                UPDATE_1C_RECOMMENDED = obj.recommended
                UPDATE_1C_FEATURED = obj.featured
                UPDATE_1C_POPULAR = obj.popular
                UPDATE_1C_NEW_PRODUCTS = obj.new_products
                UPDATE_1C_OPTIONS = obj.options
                UPDATE_1C_ONLY_PRICES = obj.only_price
            else:
                UPDATE_1C_ACCESSORIES = getattr(settings, 'UPDATE_1C_ACCESSORIES', True)
                UPDATE_1C_RECOMMENDED = getattr(settings, 'UPDATE_1C_RECOMMENDED', True)
                UPDATE_1C_FEATURED = getattr(settings, 'UPDATE_1C_FEATURED', True)
                UPDATE_1C_POPULAR = getattr(settings, 'UPDATE_1C_POPULAR', True)
                UPDATE_1C_NEW_PRODUCTS = getattr(settings, 'UPDATE_1C_NEW_PRODUCTS', True)
                UPDATE_1C_OPTIONS = getattr(settings, 'UPDATE_1C_OPTIONS', True)
                UPDATE_1C_ONLY_PRICES = getattr(settings, 'UPDATE_1C_ONLY_PRICES', False)

            if UPDATE_1C_ONLY_PRICES:
                try:
                    product = Product.objects.get(uid=self.id)
                    product.price = float(self.price.replace(',','.'))
                    product.save()
                    return
                except Product.DoesNotExist:
                    pass # go to create product

            #logger.info('create_product')
            if self.status != u'Удален':
                image = ''

                try: #B:p:010
                    # logger.warning('T01')
                    try:
                        status = ProductStatus.objects.get(name=self.available)
                    except:
                        status = ProductStatus.objects.create(name=self.available, show_buy_button=True,\
                                show_ask_button=True, is_visible=True, is_searchable=True)
                        #logger.info(u'New status created - %s' % unicode(status))
                    # logger.warning('T02')
                    manufacturer, created = Manufacturer.objects.get_or_create(
                            name=self.manufacturer
                        )
                    self.manufacturer = u''
                    try: #B:p:013
                        guarantee = int(self.guarantee)
                    except Exception, e:
                        #logger.error(u'B:p:013 %s' % unicode(e))
                        guarantee = 0
                    # logger.warning('T06')
                    param = {
                        'name': self.product_name,
                        'sku': self.articul,
                        'short_description': '<br>'.join(a for a in self.short_description if a != '\n'),
                        'description': self.description,
                        'meta_title': self.METATitle,
                        'meta_keywords': self.METAKeywords,
                        'meta_description': ''.join(a for a in self.METADescription),
                        'active': True,
                        'manufacturer': manufacturer,
                        'deliverable': True,
                        'uid': self.id,
                        'id_1c': self.key_1c.replace(' ', ''),
                        'status': status,
                        'guarantee': guarantee,
                        }
                    # logger.warning('T07')
                    try: #B:p:014
                        if Product.objects.filter(uid=param['uid']).update(**param) == 0:
                            param['slug'] = pytils.translit.slugify(param['name']) #TODO:check slug
                            product, created = Product.objects.get_or_create(**param)
                        else:
                            product = Product.objects.get(uid=param['uid'])
                    except Exception, e:
                        param['slug'] = make_similar_unique_url(param['slug'])
                        product, created = Product.objects.get_or_create(**param)
                    product.save()
                    # logger.warning('T08')
                    if created or UPDATE_1C_ACCESSORIES:
                        accessories = []
                        try: #B:p:011
                            accessories = Product.objects.filter(uid__in=self.accessory)
                        except Exception, e:
                            #logger.error(u'B:p:011 %s' % unicode(e))
                            pass

                        try: #B:p:015
                            for acc in accessories:
                                ProductAccessories.objects.get_or_create(product=product,accessory=acc) #TODO:check position
                        except Exception, e:
                            pass
                            #logger.error(u'B:p:015 %s' % unicode(e))
                            # print e, 'accessories'
                    # logger.warning('T09')
                    if created or UPDATE_1C_RECOMMENDED:
                        recommended = []
                        try: #B:p:012
                            recommended = Product.objects.filter(uid__in=self.recommended)
                        except Exception, e:
                            pass
                            #logger.error(u'B:p:012 %s' % unicode(e))

                        try: #B:p:016
                            for p in recommended:
                                product.related_products.add(p)
                            # product.save()
                        except Exception, e:
                            #logger.error(u'B:p:016 %s' % unicode(e))
                            # print e, 'recommended'
                            pass
                    # logger.warning('T10')
                    try: #B:p:017
                        product.price = float(self.price.replace(',','.'))
                        product.save()
                    except Exception, e:
                        pass # Ignore when price is not specified
                        # logger.error(u'B:p:017 %s' % unicode(e))
                    # logger.warning('T11')
                    try: #B:p:018
                        # logger.warning('T12')
                        # clear old images if needed
                        images, _created = ProductImage.objects.get_or_create(product=product)
                        load_all = getattr(settings, 'LOAD_ALL_IMAGES', False)
                        if not load_all:
                            if self.img and self.img[0] in images.links:
                                images.links = '%s;' % self.img[0]
                            else:
                                images.links = ''
                            images.save()

                            if self.img and self.img[0] == '':
                                for im in product.images.all():
                                    im.image.delete() 
                                product.images.clear()
                                product.save()
                        else:
                            # remove images if sent 1+ new image
                            # update = False
                            # for a in self.img:
                            #     if a not in images.links:
                            #         update = True

                            for im in product.images.all():
                                im.image.delete() 
                            product.images.clear()
                            # product.save()

                            images.links = ''
                            images.save()


                        # logger.warning('T13')
                        for url in self.img:
                            try: #B:p:019
                                if url != '' and url not in images.links:
                                    content = ''
                                    if 'http://' in url:
                                        headers = { 'User-Agent' : 'Mozilla/5.0' }
                                        req = urllib2.Request(url, None, headers)
                                        img_page = urllib2.urlopen(req)
                                        content = img_page.read()
                                    elif 'ftp://' in url:
                                        host = url.replace('ftp://', '').split('/')[0]
                                        file_name = url.replace('ftp://%s/' % host, '')

                                        for source in getattr(settings, 'SSHOP_FTP_SOURCES', []):
                                            if source['DOMAIN'] == host:
                                                if source['THIS_SERVER']:
                                                    image = open(source['DIR'] + file_name.split('/')[-1], 'r')
                                                    content = image.read()
                                                    image.close()
                                                else:
                                                    ftp = FTP(user=source['USER'], passwd=source['PASSWORD'], host=host)
                                                    ftp.getwelcome()
                                                    r = StringIO()
                                                    ftp.retrbinary('RETR '+ file_name, r.write)
                                                    content = r.getvalue()
                                                break

                                    if content != '':
                                        image = Image(title=self.product_name[:75])
                                        img_format = url.split('.')[-1]
                                        if not img_format:
                                            img_format = 'jpeg'
                                        path = product.slug + '.' + img_format
                                        path = os.path.join(getattr(settings, 'MEDIA_ROOT', '/'), '1c_import_tmp', path)
                                        open(path, 'wb+').write(content)
                                        file_content = add_watermark(File(open(path, 'rb')))
                                        image.image.save(file_content.name, file_content, save=True)

                                        if not load_all:
                                            # x = [im.image.delete() for im in product.images.all()]
                                            for im in product.images.all():
                                                im.image.delete()
                                            product.images.clear()
                                        product.images.add(image)
                                        product.save()
                                        images.links += '%s;' % url
                                        images.save()
                            except Exception, e:
                                #logger.error(u'B:p:019 %s' % unicode(e))
                                import sys, traceback;print '-'*60;traceback.print_exc(file=sys.stdout);print '-'*60;
                                print e, 'error with image'
                        # logger.warning('T14')
                    except Exception, e:
                        #logger.error(u'B:p:018 %s' % unicode(e))
                        pass
                        # print e, 'images'
                        # pass
                    # logger.warning('T15')
                    if self.featured.lower() == 'true' and (created or UPDATE_1C_FEATURED):
                        try: #B:p:020
                            FeaturedProduct.objects.get_or_create(product_id=product.id)
                        except Exception, e:
                            #logger.error(u'B:p:020 %s' % unicode(e))                            
                            pass
                    elif self.featured.lower() == 'false' and (created or UPDATE_1C_FEATURED):
                        try: #B:p:021
                            featured_product = FeaturedProduct.objects.get(product_id=product.id)
                            featured_product.delete()
                        except FeaturedProduct.DoesNotExist:
                            pass
                    # logger.warning('T16')
                    if self.popular.lower() == 'true' and (created or UPDATE_1C_POPULAR):
                        try: #B:p:022
                            Topseller.objects.get_or_create(product_id=product.id)
                        except Exception, e:
                            #logger.error(u'B:p:022 %s' % unicode(e))
                            pass
                    elif self.popular.lower() == 'false' and (created or UPDATE_1C_POPULAR):
                        try: #B:p:023
                            topseller_product = Topseller.objects.get(product_id=product.id)
                            topseller_product.delete()
                        except Topseller.DoesNotExist:
                            pass
                    # logger.warning('T17')
                    lists = ProductList.objects.filter(identifier="new_products")
                    if self.is_new.lower() == 'true' and lists and (created or UPDATE_1C_NEW_PRODUCTS):
                        ProductListItem.objects.get_or_create(product_list=lists[0], product=product)
                    elif self.is_new.lower() == 'false' and lists and (created or UPDATE_1C_NEW_PRODUCTS):
                        ProductListItem.objects.filter(product_list=lists[0], product=product).delete()
                    # logger.warning('T18')
                    # remove product from all categories
                    try: #B:p:024
                        product.categories = []
                        # product.save()
                    except Exception, e:
                        #logger.error(u'B:p:024 %s' % unicode(e))
                        pass
                    # logger.warning('T19')
                    for i in self.groups:
                        cat = Category.objects.get(uid=i)
                        cat.products.add(product)
                        # cat.save() #TODO: check this
                    # logger.warning('T20')
                    # logger.warning(u'Before init options: %s' % unicode(product))
                    if created or UPDATE_1C_OPTIONS:
                        ProductPropertyValue.objects.filter(product=product).delete()                    
                        # self.options = sorted(self.options, key=lambda x:x[0])
                        i = 0
                        active_group = None
                        # print self.options
                        for option in self.options:
                            i += 1
                            group_name = option[0]
                            property_name = option[1]
                            value = option[2]
                            # logger.warning(u'init option: %s | %s | %s' % (option[0], option[1], option[2]))

                            try:
                                group = Property.objects.get(name=group_name, is_group=True)
                            except Property.DoesNotExist:
                                group = Property(name=group_name, is_group=True)
                                group.save()

                            if group is not active_group:
                                active_group = group
                                try:
                                    ProductPropertyValue.objects.get(product=product, property=group)
                                    # logger.error(u'Import from 1C: Dublicate property group "%(group)s" for product "%(product)s". Second one is ignored.' % {
                                    # 'group': group.name,
                                    # 'product': product.name,
                                    # })
                                except ProductPropertyValue.DoesNotExist:
                                    p_value = ProductPropertyValue(
                                        product=product,
                                        property=group,
                                        position=i,
                                        )
                                    p_value.save()
                                    i += 1

                            try:
                                property = Property.objects.get(name=property_name, is_group=False)
                            except Property.DoesNotExist:
                                property = Property(name=property_name, is_group=False)
                                property.save()
                            except Property.MultipleObjectsReturned:
                                properties = Property.objects.filter(name=property_name, is_group=False)
                                property = properties[0]
                                Property.objects.filter(id__in=[p.id for p in properties[1:]]).delete()
                                logger.error(u'Import from 1C: Dublicate properties "%(property_name)s". Dublicates deleted(except first).' % {
                                'property_name': property_name,
                                })

                            try:
                                ProductPropertyValue.objects.get(product=product, property=property)
                                # logger.error(u'Import from 1C: Dublicate property value "%(property)s" for product "%(product)s". Second value is ignored.' % {
                                #     'property': property.name,
                                #     'product': product.name,
                                #     })
                            except ProductPropertyValue.DoesNotExist:
                                p_value = ProductPropertyValue(
                                    product=product, 
                                    property=property,
                                    value=value.encode('utf-8', 'replace'),
                                    position=i,
                                    )
                                # logger.warning('WTF')
                                p_value.save()
                                # logger.warning('LOL')
                    # for i in self.options:
                    #     group_position = None
                    #     group_name = i[0]
                    #     property_name = i[1]
                    #     value = i[2]

                    #     # get or create group
                    #     group, created = Property.objects.get_or_create(
                    #             name=group_name,
                    #             is_group=True
                    #         )

                    #     if created:
                    #         # if new group was created - add new empty record in ProductPropertyValue and set position
                    #         product_properties = ProductPropertyValue.objects.filter(product=product).order_by('-position')
                    #         if product_properties:
                    #             t = product_properties[0]
                    #             position = t.position + 1
                    #         else:
                    #             position = 1

                    #         product_group_property_value = ProductPropertyValue.objects.create(
                    #                 product=product,
                    #                 property=group,
                    #                 position=position
                    #             )

                    #     product_group_property_value = ProductPropertyValue.objects.get(
                    #             product=product,
                    #             property=group
                    #         )
                    #     group_position = product_group_property_value.position

                    #     property_, created = Property.objects.get_or_create(
                    #             name=property_name,
                    #         )

                    #     product_property_value, created = ProductPropertyValue.objects.get_or_create(
                    #             product=product,
                    #             property=property_
                    #         )

                    #     product_property_value.position = group_position + 3

                    #     # update property value position
                    #     _update_positions(product)

                    #     product_property_value.value = unicode(value[:255]).encode('utf-8')
                    #     product_property_value.save()

                    self.product_uid = self.id
                    self.options = []
                    self.accessory = []
                    self.recommended = []
                    self.product_name = ''
                except Exception, e:
                    # logger.error(u'B:p:010 %s' % unicode(e))
                    self.success = False
                    # print 'ERROR: Create product. Error_20'
                # logger.warning('T24')
            else:
                try: #B:p:025
                    product = Product.objects.get(uid=self.id)
                    product.active = False
                    product.save()
                except Exception, e:
                    # logger.error(u'B:p:025 %s' % unicode(e))
                    self.success = False

            # try: #B:p:026
            #     from lfs.caching.listeners import update_product_cache
            #     update_product_cache(product)
            #     # cache.delete("%s-product-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-product-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.slug))
            #     # cache.delete("%s-product-inline-True-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-product-inline-False-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-product-images-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-related-products-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-manage-properties-variants-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-product-categories-%s-False" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-product-categories-%s-True" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.id))
            #     # cache.delete("%s-product-navigation-%s" % (settings.CACHE_MIDDLEWARE_KEY_PREFIX, product.slug))
            # except Exception, e:
            #     logger.error(u'B:p:026 %s' % unicode(e))



        def endDocument(self):
            pass

        def characters(self, data):
            if self.product:
                if self.id_flag and not self.groups_flag:
                    self.id = data
                if self.articul_flag:
                    self.articul = data
                if self.product_name_flag:
                    self.product_name += data
                if self.key_1c_flag:
                    self.key_1c = data
                if self.availability_flag:
                    self.availability = data
                if self.manufacturer_flag:
                    self.manufacturer += data
                if self.basic_unit_flag:
                    self.basic_unit = data
                if self.groups_flag and self.id_flag:
                    self.groups.append(data)
                if self.img_flag:
                    load_all = getattr(settings, 'LOAD_ALL_IMAGES', False)
                    if len(self.img) > 0 and ('&' == data or self.img[-1][-1] == '&'):
                        self.img[-1] += data
                    else:
                        if load_all:
                            self.img.append(data)
                        else:
                            self.img = [data, ]
                if self.content_flag:
                    if self.METATitle_flag:
                        self.METATitle = data
                    if self.METADescription_flag:
                        self.METADescription.append(data)
                    if self.METAKeywords_flag:
                        self.METAKeywords = data
                    if self.featured_flag:
                        self.featured = data
                if self.accessory_flag:
                    self.accessory.append(data)
                if self.is_new_flag:
                    self.is_new = data
                if self.popular_flag:
                    self.popular = data
                if self.recommended_flag:
                    self.recommended.append(data)
                if self.short_description_flag:
                    self.short_description.append(data)
                if self.description_flag:
                    self.description += data
                if self.option:
                    if self.option_group_flag:
                        self.option_group = data
                    if self.option_name_flag:
                        self.option_name = data
                    if self.option_value_flag:
                        self.option_value += data
                if self.availability_flag:
                    self.available = data
                if self.currency_flag:
                    self.currency = data
                if self.price_flag:
                    self.price = data
                if self.status_flag:
                    self.status = data
                if self.guarantee_flag:
                    self.guarantee = data
            else:
                self.data = ''

        def startElement(self, name, attrs):
            if name == u'Товар':
                self.product = True
            elif name == u'Ид':
                self.id_flag = True
            elif name == u'Артикул':
                self.articul_flag = True
            elif name == u'Наименование' and not self.is_bid:
                self.product_name_flag = True
            elif name == u'ЗначенияРеквизитов':
                self.details_flag = True
            elif name == u'СтавкиНалогов':
                self.tax_flag = True
            elif name == u'Код1с':
                self.key_1c_flag = True
            elif name == u'Наличие':
                self.availability_flag = True
            elif name == u'Производитель':
                self.manufacturer_flag = True
            elif name == u'БазоваяЕдиница':
                self.basic_unit_flag = True
            elif name == u'Группы':
                self.groups_flag = True
            elif name == u'Контент':
                self.content_flag = True
            elif name == u'METATitle':
                self.METATitle_flag = True
            elif name == u'METADescription':
                self.METADescription_flag = True
            elif name == u'METAKeywords':
                self.METAKeywords_flag = True
            elif name == u'Аксессуар':
                self.accessory_flag = True
            elif name == u'Новый':
                self.is_new_flag = True
            elif name == u'Популярный':
                self.popular_flag = True
            elif name == u'СвязанныйТовар':
                self.recommended_flag = True
            elif name == u'Рекомендуемый':
                self.featured_flag = True
            elif name == u'ОписаниеКраткое':
                self.short_description_flag = True
            elif name == u'Описание':
                self.description_flag = True
            elif name == u'СсылкаКартинки':
                self.img_flag = True
            elif name == u'СсылкиТовар':
                self.links_product_flag = True
            elif name == u'СсылкаЯндекс':
                self.yandex_link_flag = True
            elif name == u'Опция':
                self.option = True
            elif name == u'ГруппировкаОпции':
                self.option_group_flag = True
            elif name == u'НаименованиеОпции':
                self.option_name_flag = True
            elif name == u'ЗначениеОпции':
                self.option_value_flag = True
            elif name == u'Наличие':
                self.availability_flag = True
            elif name == u'ЦенаЗаЕдиницу':
                self.price_flag = True
            elif name == u'Валюта':
                self.currency_flag = True
            elif name == u'Статус':
                self.status_flag = True
            elif name == u'СрокГарантии':
                self.guarantee_flag = True
            elif name == u'СтавкаНалога' or name == u'ЗначениеРеквизита':
                self.is_bid = True

        def endElement(self, name):
            if self.product and self.success:
                if name == u'Товар':
                    self.create_product()
                    self.product = False
                    self.groups = []
                    self.img = []
                elif name == u'Ид':
                    self.id_flag = False
                elif name == u'Артикул':
                    self.articul_flag = False
                elif name == u'Наименование':
                    self.product_name_flag = False
                elif name == u'ЗначенияРеквизитов':
                    self.details_flag = False
                elif name == u'СтавкиНалогов':
                    self.tax_flag = False
                elif name == u'Код1с':
                    self.key_1c_flag = False
                elif name == u'Наличие':
                    self.availability_flag = False
                elif name == u'Производитель':
                    self.manufacturer_flag = False
                elif name == u'БазоваяЕдиница':
                    self.basic_unit_flag = False
                elif name == u'Группы':
                    self.groups_flag = False
                elif name == u'Контент':
                    self.content_flag = False
                elif name == u'METATitle':
                    self.METATitle_flag = False
                elif name == u'METADescription':
                    self.METADescription_flag = False
                elif name == u'METAKeywords':
                    self.METAKeywords_flag = False
                elif name == u'Аксессуар':
                    self.accessory_flag = False
                elif name == u'Новый':
                    self.is_new_flag = False
                elif name == u'Популярный':
                    self.popular_flag = False
                elif name == u'СвязанныйТовар':
                    self.recommended_flag = False
                elif name == u'Рекомендуемый':
                    self.featured_flag = False
                elif name == u'ОписаниеКраткое':
                    self.short_description_flag = False
                elif name == u'Описание':
                    self.description_flag = False
                elif name == u'СсылкаКартинки':
                    self.img_flag = False
                elif name == u'СсылкиТовар':
                    self.links_product_flag = False
                elif name == u'СсылкаЯндекс':
                    self.yandex_link_flag = False
                elif name == u'Опция':
                    self.option = False
                    # if u'Прочие' not in self.option_group:
                    self.options.append((self.option_group, self.option_name, self.option_value))
                    self.option_value = ''
                elif name == u'ГруппировкаОпции':
                    self.option_group_flag = False
                elif name == u'НаименованиеОпции':
                    self.option_name_flag = False
                elif name == u'ЗначениеОпции':
                    self.option_value_flag = False
                elif name == u'Наличие':
                    self.availability_flag = False
                elif name == u'ЦенаЗаЕдиницу':
                    self.price_flag = False
                elif name == u'Валюта':
                    self.currency_flag = False
                elif name == u'Статус':
                    self.status_flag = False
                elif name == u'СрокГарантии':
                    self.guarantee_flag = False
                elif name == u'СтавкаНалога' or name == u'ЗначениеРеквизита':
                    self.is_bid = False
    try: #B:p:007
        parser = XMLProductReader()
        d = xml.sax.parseString(text, parser)
        del d
        return parser.product_uid
    except Exception, e:
        # logger.error(u'B:p:007 %s' % unicode(e))
        pass
        # print 'ERROR', e

def parse_categories(text):
    '''
        This function contain class for parse
        categories from XML file from 1C
    '''

    class XMLCategoryReader(xml.sax.ContentHandler):
        def __init__(self,imported=False,mywin=None):
            xml.sax.ContentHandler.__init__(self)
            self.success = True

        def startDocument(self):
            # print 'Start categories processed'
            # stores the tag to be processed
            self.classifier = False
            self.catalog_level = 0
            self.catalog_id = ''
            self.catalog_name = ''
            self.catalog_id_flag = False
            self.catalog_name_flag = False
            self.catalog = {}
            self.parents = []
            self.categories_for_delete = [cat.uid for cat in Category.objects.all()]

        def endDocument(self):
            try: #B:e:003
                # print self.categories_for_delete
                Category.objects.filter(uid__in=self.categories_for_delete).delete()
            except Exception, e:
                logger.error('B:e:003 %s' % unicode(e))

        def characters(self, data):
            if self.catalog_id_flag:
                self.catalog_id = data
            elif self.catalog_name_flag:
                self.catalog_name = data

        def startElement(self, name, attrs):
            if name == u'Группа':
                self.catalog_level += 1
            elif name == u'Ид':
                self.catalog_id_flag = True
            elif name == u'Наименование':
                self.catalog_name_flag = True

        def create_category(self, name):
            # print u'\tProcess category {category}'.format(category=name)
            try: #B:e:004
                param = {
                    'name': name,
                    'level': self.catalog_level - 1,
                    'uid': self.catalog_id,
                }

                if param['level'] > 0:
                    param['parent'] = Category.objects.get(uid=self.parents[-1])

                if Category.objects.filter(uid=param['uid']).update(**param) == 0:
                    param['slug'] = pytils.translit.slugify(name)
                    obj, created = Category.objects.get_or_create(**param)
                    # logger.warning(u'New category created %s' % unicode(obj))
                else:
                    obj = Category.objects.get(uid=param['uid'])
                    # logger.warning(u'Found existing category %s' % unicode(obj))

            except Exception, e:
                # logger.warning('B:c:004 uid=%s; %s' % (param['uid'], unicode(e)))
                param['slug'] = pytils.translit.slugify(name)
                try: #B:e:005
                    if Category.objects.filter(uid=self.catalog_id).count() == 0:
                        param['slug'] = make_similar_unique_url(param['slug'])
                        obj, created = Category.objects.get_or_create(**param)
                except Exception, e:
                    # logger.error('B:e:005 %s' % unicode(e))
                    self.success = False

            try: #B:e:006
                self.categories_for_delete.remove(self.catalog_id)
            except Exception, e:
                #logger.warning('B:e:006 %s' % unicode(e))
                pass

            try: #B:e:007
                from lfs.caching.listeners import update_category_cache
                update_category_cache(obj)
            except Exception, e:
                #logger.error('B:e:007 %s' % unicode(e))
                pass

        def endElement(self, name):
            # # print name
            try: #B:c:002
                if name == u'Группа':
                    self.catalog_level -= 1
                    self.parents.pop()
                elif name == u'Ид':
                    self.catalog_id_flag = False
                elif name == u'Наименование':
                    self.catalog_name_flag = False

                    if self.catalog_level > 0:
                        self.catalog[self.catalog_id.encode('utf-8')] = self.catalog_name.encode('utf-8')
                        if self.success:
                            self.create_category(self.catalog_name)

                    self.parents.append(self.catalog_id)
            except Exception, e:
                # print e
                #logger.error('B:c:002 %s' % unicode(e))
                self.success = False
    try: #B:c:001
        parser = XMLCategoryReader()
        xml.sax.parseString(text, parser)
        return parser.success
    except Exception, e:
        # print e
        # logger.error('B:c:001 %s' % unicode(e))
        return False
        # print e
        # print 'ERROR'
    # print 'End categories processed'


def parse_products(path):
    try: #B:p:001
        path_to_file = path.replace('\r','')

        product_f = False
        text = ''
        categories_f = False

        if '.zip' in path_to_file:
            zf = zipfile.ZipFile(path_to_file, 'r')
            f = f2 = zf.open(zf.namelist()[0]).readlines()
            zf.close()
        else:
            _file = open(path_to_file, 'r')
            f = f2 = _file.readlines()
            _file.close()

        # logger.warning('Before catalog')
        for i in f:
            if '<Классификатор>' in i:
                categories_f = True
                text += i
            elif '</Классификатор>' in i:
                categories_f = False
                text += i
                break
            elif categories_f:
                text += i

        if 'update_1c_settings' in getattr(settings, 'INSTALLED_APPS', []):
            from update_1c_settings.models import UpdateSettings
            obj = UpdateSettings.objects.all()[0]
            QUICK_CHECK_1C_CATALOG = obj.quick_check_catalog
        else:
            QUICK_CHECK_1C_CATALOG = getattr(settings, 'QUICK_CHECK_1C_CATALOG', False)

        if QUICK_CHECK_1C_CATALOG:
            if Category.objects.count() != text.count('<Группа>'):
                if not parse_categories(text):
                    return False
        elif not parse_categories(text):
            return False
        text = ''
        uids = []
        # logger.warning('Before products')
        for i in f2:
            if '<Товар>' in i:
                product_f = True

            if product_f:
                text += i

            if '</Товар>' in i:
                product_f = False
                uid = parse_product(text)
                if uid:
                    uids.append(uid)
                text = ''

        # logger.warning('Before similar_products:make, uids_count=%d' % len(uids))
        mediator.publish(None, 'similar_products:make', uids)
        # logger.warning('After similar_products:make')

        # print 'Import Finish'
        #try:
        #    Category.objects.rebuild()
        #except Exception, e:
        #    print e
        # Rebuild main menu
        try:
            from lfs.core.utils import get_default_shop
            shop = get_default_shop()
            shop.prerender_menu()
        except:
            pass

        # rebuild category counters and add task for updating filters
        # from tasks.utils import add_task
        from tasks.api import TaskQueueManager
        try:
            categories = Category.objects.all()
            for category in categories.iterator():
                children = category.get_descendants(include_self=True)
                counts = [int(t.products.filter(active=True, status__is_visible=True).count()) for t in children]
                category.product_count = sum(counts)
                category.save()

                task_manager = TaskQueueManager()
                task_manager.schedule(
                    'lfs.filters.jobs.update_filters_job',
                    args=[category.id],
                    priority=getattr(
                        settings, 'JOB_UPDATE_FILTERS_PRIORITY', 1)
                )
                # add_task('lfs.filters.utils.update_filters', {'category_id':category.id})
        except Exception, e:
            # print e
            pass

    except Exception, e:
        # logger.error('B:p:001 %s' % unicode(e))
        return False
        # print 'ERROR: Input path to XML file with data'
    return True

def parse_price(path):
    pass
    # print 'Import Finish'