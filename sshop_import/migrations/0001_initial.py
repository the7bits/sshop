# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Task'
        db.create_table('sshop_import_task', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('filename', self.gf('django.db.models.fields.CharField')(default='', max_length=150)),
            ('path', self.gf('django.db.models.fields.CharField')(default='', max_length=500)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2012, 12, 2, 0, 0))),
            ('performed', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('finish', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal('sshop_import', ['Task'])


    def backwards(self, orm):
        # Deleting model 'Task'
        db.delete_table('sshop_import_task')


    models = {
        'sshop_import.task': {
            'Meta': {'object_name': 'Task'},
            'filename': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '150'}),
            'finish': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'path': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '500'}),
            'performed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2012, 12, 2, 0, 0)'})
        }
    }

    complete_apps = ['sshop_import']