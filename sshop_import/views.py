# -*- coding: utf-8 -*-
import logging

from django.shortcuts import render_to_response, render, redirect
from django.contrib.auth.decorators import login_required
from sshop_import.forms import ImportProductForm, ImportPriceForm
from django.core.files.uploadhandler import FileUploadHandler, TemporaryUploadedFile
from django.http import HttpResponse, HttpResponseServerError, Http404
from django.views.decorators.csrf import csrf_exempt
import os
from django.core import serializers
from scripts.parse import parse_product, parse_price
from django.conf import settings
from sshop_import.models import Task

from django.template.loader import render_to_string
from django.template import RequestContext

from django.template.response import TemplateResponse
from lfs.core.utils import get_default_shop

logger = logging.getLogger('sshop')

SSHOP_1C_IMPORT_DIR = getattr(settings, 'SSHOP_1C_IMPORT_DIR', '')
SSHOP_1C_LOGS_DIR = getattr(settings, 'SSHOP_1C_LOGS_DIR', '')
SSHOP_1C_ERROR_TIMEOUT = getattr(settings, 'SSHOP_1C_ERROR_TIMEOUT', 120)
SSHOP_1C_ERROR_LIMIT = getattr(settings, 'SSHOP_1C_ERROR_LIMIT', 5)
SSHOP_1C_MAX_FILE_SIZE = getattr(settings, 'SSHOP_1C_MAX_FILE_SIZE', 104857600)
SSHOP_1C_ENABLE_ZIP = getattr(settings, 'SSHOP_1C_ENABLE_ZIP', True)

def index(request):
    return render_to_response('sshop_import/index.html',{})

@csrf_exempt
def exchange_1c(request):
    from datetime import datetime, timedelta
    from lfs.order.models import Order
    from callback.models import Callback
    from sms.models import SMS
    try:
        open(SSHOP_1C_LOGS_DIR + 'exchange_1c.txt','a').write(str(datetime.now())+'\n'+str(request) + '\n')
    except Exception, e:
        pass
    f = False
    try: #B:001
        if request.method == 'GET':
            f = True #TODO:WTF?
            try: #B:002
                if request.GET['type'] == 'sale' and request.GET['mode'] == 'query':
                    date = "{:%Y-%m-%d}".format(datetime.now())
                    orders = list(Order.objects.filter(export_status=0))
                    Order.objects.filter(export_status=0).update(**{'export_status':5})

                    callbacks = list(Callback.objects.filter(exported=False))
                    Callback.objects.filter(exported=False).update(exported=True)
                    
                    sms = list(SMS.objects.filter(exported=False))
                    SMS.objects.filter(exported=False).update(exported=True)
                    return TemplateResponse(request, 'example_export.xml', 
                        {
                        'orders': orders,
                        'callbacks': callbacks,
                        'sms': sms,
                        'date':date, 
                        'currency':get_default_shop(request).default_currency,
                        },
                        content_type='application/xml')
            except Exception, e:
                logger.error('B:002 %s' % unicode(e))
                return HttpResponse('failure')

            try:  #B:003
                if request.GET['type'] == 'sale' and request.GET['mode'] == 'success':
                    Order.objects.filter(export_status=5).update(**{'export_status':10})
                    return HttpResponse('success')
            except Exception, e:
                logger.error('B:003 %s' % unicode(e))
                return HttpResponse('failure')

            try: # B:004
                if request.GET['type'] == 'sale' and request.GET['mode'] == 'failure':
                    Order.objects.filter(export_status=5).update(**{'export_status':0})
                    return HttpResponse('success')
            except Exception, e:
                logger.error('B:004 %s' % unicode(e))
                return HttpResponse('failure')

            try: #B:005
                if request.GET['type'] == 'catalog' and request.GET['mode'] == 'checkauth':
                    time = datetime.now() - timedelta(minutes=SSHOP_1C_ERROR_TIMEOUT)
                    if Task.objects.filter(finished_at__gt=time, status=25).count() > SSHOP_1C_ERROR_LIMIT: #if failed
                        return HttpResponse( "failure")

                    task = Task.objects.create()
                    return HttpResponse( "success\nsessionid\n%s" % task.id)
            except Exception, e:
                logger.error('B:005 %s' % unicode(e))
                return HttpResponse('failure')


            try: #B:006
                if request.GET['type'] == 'catalog' and request.GET['mode'] == 'init':
                    task_id = int(request.COOKIES.get('sessionid'))
                    Task.objects.filter(id=task_id).update(**{'status':5}) #init
                    if SSHOP_1C_ENABLE_ZIP:
                        _zip = 'yes'
                    else:
                        _zip = 'no'
                    return HttpResponse(
                        'zip={zip}\nfile_limit={file_limit}'\
                        .format(
                            zip=_zip,
                            file_limit=SSHOP_1C_MAX_FILE_SIZE
                            )
                        )
            except Exception, e:
                logger.error('B:006 %s' % unicode(e))
                return HttpResponse('failure')

            try: #B:007
                if request.GET['type'] == 'catalog' and request.GET['mode'] == 'import':
                    task_id = int(request.COOKIES.get('sessionid'))
                    task = Task.objects.get(id=task_id)
                    file_name = request.GET['filename']
                    if file_name not in task.files:
                        task.files += file_name
                        task.status = 15 #Ready to process
                        task.save()
                        return HttpResponse('progress')
                    else:
                        if task.status == 20: #Done successfully
                            return HttpResponse('success')
                        elif task.status == 25: #Failed
                            logger.error('B:007 return failure, task_id=%s' % task.id)
                            return HttpResponse('failure')
                        else:
                            return HttpResponse('progress')
            except Exception, e:
                logger.error('B:007 %s' % unicode(e))
                return HttpResponse('failure')
    except Exception, e:
        logger.error('B:001 %s' % unicode(e))

    try: #B:008
        if request.method == 'POST':
            try: #B:009
                if request.GET['type'] == 'catalog' and request.GET['mode'] == 'file':
                    task_id = int(request.COOKIES.get('sessionid'))

                    _time = str(datetime.now()).split()[-1].replace(':','_').replace('.','_')
                    filename = "{time}_{name}".format(time=_time,name=request.GET['filename'].replace('/',''))
                    path = SSHOP_1C_IMPORT_DIR + filename
                    _file = open(path, 'w')
                    _file.write(request.raw_post_data)
                    _file.close()

                    task = Task.objects.get(id=task_id)
                    task.files += '%s\n' % path
                    task.status = 10 #Saving files
                    task.save()
                    
                    return HttpResponse('success')
            except Exception, e:
                logger.error('B:009 %s' % unicode(e))
                return HttpResponse('failure')
    except Exception, e:
        logger.error('B:008 %s' % unicode(e))

    if f:
        return HttpResponse('success')
    else:
        return HttpResponse('failure')


class SshopFileUploadHandler(FileUploadHandler):
    """
    Upload handler that streams data into a temporary file.
    """
    def __init__(self, *args, **kwargs):
        super(SshopFileUploadHandler, self).__init__(*args, **kwargs)

    def new_file(self, file_name, *args, **kwargs):
        """
        Create the file object to append to as data is coming in.
        """
        print 'new_file'
        super(SshopFileUploadHandler, self).new_file(file_name, *args, **kwargs)
        self.file = TemporaryUploadedFile(self.file_name, self.content_type, 0, self.charset)

    def receive_data_chunk(self, raw_data, start):
        # print 'receive_data_chunk'
        self.file.write(raw_data)

    def file_complete(self, file_size):
        print 'file_complete'
        self.file.seek(0)
        self.file.size = file_size
        handle_uploaded_file(self.file)
        # return self.file

@csrf_exempt
def ajax_upload( request ):
    # request.META['HTTP_REFERER']
    try:
        file_name = eval(request.POST.keys()[0]).values()[0]
        if 'product' in request.META['HTTP_REFERER']:
            parse_product(file_name)
        else:
            parse_price(file_name)
    except:
        pass

    return HttpResponse( "{success:true}" )



def handle_uploaded_file(f):
    path = os.path.join(os.path.dirname(__file__).replace('/sshop_import',''), "sshop/tmp/")
    filename = path + f.name
    destination = open(filename, 'w')
    k = 0
    for chunk in f.chunks():
        destination.write(chunk)
    destination.close()

@login_required
def import_product(request):
    if request.method == 'POST':
        form = ImportProductForm(request.POST, request.FILES)
        if form.is_valid():
            f = request.FILES['file']
            handle_uploaded_file(f)

            return redirect('/import/')
    else:
        form = ImportProductForm()

    return render(request, 'sshop_import/import_product.html',
            {
            'form': form,
            }
        )

@login_required
def import_price(request):
    if request.method == 'POST':
        form = ImportPriceForm(request.POST, request.FILES)
        if form.is_valid():
            f = request.FILES['file']
            print f.temporary_file_path()

            return redirect('/import/')
    else:
        form = ImportPriceForm()

    return render(request, 'sshop_import/import_price.html',
            {
            'form': form,
            }
        )


