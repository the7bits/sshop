# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'PortletAssignment.blocked_in_inheritor'
        db.add_column('portlets_portletassignment', 'blocked_in_inheritor',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'PortletAssignment.blocked_in_inheritor'
        db.delete_column('portlets_portletassignment', 'blocked_in_inheritor')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'portlets.portletassignment': {
            'Meta': {'ordering': "['position']", 'object_name': 'PortletAssignment'},
            'blocked_in_inheritor': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pa_content'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'portlet_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'portlet_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pa_portlets'", 'to': "orm['contenttypes.ContentType']"}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'slot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portlets.Slot']"})
        },
        'portlets.portletblocking': {
            'Meta': {'unique_together': "(['slot', 'content_id', 'content_type'],)", 'object_name': 'PortletBlocking'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pb_content'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portlets.Slot']"})
        },
        'portlets.portletregistration': {
            'Meta': {'ordering': "('name',)", 'object_name': 'PortletRegistration'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'type': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'portlets.slot': {
            'Meta': {'object_name': 'Slot'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'type_of_slot': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        }
    }

    complete_apps = ['portlets']