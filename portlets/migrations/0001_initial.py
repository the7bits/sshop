# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PortletAssignment'
        db.create_table('portlets_portletassignment', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slot', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portlets.Slot'])),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='pa_content', to=orm['contenttypes.ContentType'])),
            ('content_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('portlet_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='pa_portlets', to=orm['contenttypes.ContentType'])),
            ('portlet_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')(default=999)),
        ))
        db.send_create_signal('portlets', ['PortletAssignment'])

        # Adding model 'PortletBlocking'
        db.create_table('portlets_portletblocking', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('slot', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['portlets.Slot'])),
            ('content_type', self.gf('django.db.models.fields.related.ForeignKey')(related_name='pb_content', to=orm['contenttypes.ContentType'])),
            ('content_id', self.gf('django.db.models.fields.PositiveIntegerField')()),
        ))
        db.send_create_signal('portlets', ['PortletBlocking'])

        # Adding unique constraint on 'PortletBlocking', fields ['slot', 'content_id', 'content_type']
        db.create_unique('portlets_portletblocking', ['slot_id', 'content_id', 'content_type_id'])

        # Adding model 'PortletRegistration'
        db.create_table('portlets_portletregistration', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('type', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=50)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('portlets', ['PortletRegistration'])

        # Adding model 'Slot'
        db.create_table('portlets_slot', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('portlets', ['Slot'])


    def backwards(self, orm):
        # Removing unique constraint on 'PortletBlocking', fields ['slot', 'content_id', 'content_type']
        db.delete_unique('portlets_portletblocking', ['slot_id', 'content_id', 'content_type_id'])

        # Deleting model 'PortletAssignment'
        db.delete_table('portlets_portletassignment')

        # Deleting model 'PortletBlocking'
        db.delete_table('portlets_portletblocking')

        # Deleting model 'PortletRegistration'
        db.delete_table('portlets_portletregistration')

        # Deleting model 'Slot'
        db.delete_table('portlets_slot')


    models = {
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'portlets.portletassignment': {
            'Meta': {'ordering': "['position']", 'object_name': 'PortletAssignment'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pa_content'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'portlet_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'portlet_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pa_portlets'", 'to': "orm['contenttypes.ContentType']"}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '999'}),
            'slot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portlets.Slot']"})
        },
        'portlets.portletblocking': {
            'Meta': {'unique_together': "(['slot', 'content_id', 'content_type'],)", 'object_name': 'PortletBlocking'},
            'content_id': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'pb_content'", 'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'slot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['portlets.Slot']"})
        },
        'portlets.portletregistration': {
            'Meta': {'ordering': "('name',)", 'object_name': 'PortletRegistration'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'type': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'portlets.slot': {
            'Meta': {'object_name': 'Slot'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['portlets']