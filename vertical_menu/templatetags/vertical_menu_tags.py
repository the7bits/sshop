# coding: utf-8
from django import template

from lfs.catalog.models import Category

register = template.Library()


@register.inclusion_tag('vertical_menu/vertical_menu.html', takes_context=True)
def vertical_menu(context):
    request = context['request']
    if request.path == u'/':
        homepage_f = True
    else:
        homepage_f = False

    categories = Category.objects.filter(level=0)

    return {
        'categories': categories,
        'request': request,
        'homepage_f': homepage_f,
    }
