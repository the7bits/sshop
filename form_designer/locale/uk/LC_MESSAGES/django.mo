��    e      D  �   l      �     �     �     �     �     �     �     �     �     �     �     �  .   �     !	     3	     :	     ?	     D	     R	  )   X	     �	  :   �	  *   �	  	   �	     �	     
     
     #
     9
     I
     P
     X
     k
     ~
     �
     �
  .   �
  (   �
     �
       	          	     /   !  2   Q  "   �  /   �     �     �                    &     C     Q     Z     p     �  &   �     �  
   �     �     �     �     �            
        #     *     /  	   =     G     Y     g     m     t     �     �  
   �     �     �     �  
   �     �     �     �     �                      0     Q     `     t  
   �     �     �     �     �     �  �  �     y  
   �  
   �     �     �     �     �     �     �       .   #  F   R      �     �     �  
   �     �  
   �  @   �     =  �   Y  M   �  #   +     O     h     �  *   �     �  
   �  
   �  +   �  )   #     M     Z     r  ]   �  [   �     L     e     z  
   �     �  D   �  d   �  F   X  F   �  +   �  ;        N     U     i  W   u     �     �  0   �  '   $     L  F   U  %   �     �  !   �     �       .   0     _     h     z     �  
   �     �     �  *   �  #        )     E     [     v  %   �  '   �  %   �  
     #     %   3  
   Y     d  *   i     �     �     �  2   �  P     #   ^  /   �  *   �     �     �               '     4     *   4   )      .   [      5       Y   ]          F   /       '   %   T   (                 G      7   `      1   L                  ?   #   B      Q       >   !   H           &            
       M   0      9   3   ,   X           R               U   2      	                 ;   @                 Z          -   C   _                  V       K   c      W          a   <       e   $   I      =   6   J         N   \           D   O             A              S   "       +   b           E   d   :   P       8   ^    Basic Choice Choices Created Data Date Date & time Decimal number Default Display E-mail address Export selected %%(verbose_name_plural)s as %s Export view as %s Fields File Form Form Designer Forms HTTP redirect after successful submission Hidden input If enabled, the form can only be reached via a secret URL. Logs all form submissions to the database. Mail form Messages Model Choice Model Choices Model Multiple Choice Multiple Choice Number Numbers One label per line One value per line Password input Radio button Regex Saves all uploaded files using server storage. Send uploaded files as email attachments Settings Submit Templates Text Text area Thank you, the data was submitted successfully. The data could not be submitted, please try again. This field class requires a model. This field class requires a regular expression. This field is required. This file type is not allowed. Time Web address Yes/No allow initial values via URL as paragraphs as table as table (horizontal) as unordered list body clear form after successful submission custom implementation data model decimal places email subject empty label error message field field class field name fields form form template help text include in result initial value label labels log form data max. digits max. length max. value message template method min. length min. value name no obfuscate URL to this form position regular Expression required save uploaded files send form data to e-mail address sender address submit button label success message target URL title value values widget yes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-12-16 12:57+0200
PO-Revision-Date: 2014-01-30 12:42+0300
Last-Translator: Oleh Korkh <korkholeh@gmail.com>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: Poedit 1.5.4
 Основні Вибір Вибір Створено Дані Дата Дата й час Десяткове число За замовчуванням Показати Адреса електронної пошти Експортувати обрані %%(verbose_name_plural)s як %s Експортувати як %s Поля Файл Форма Дизайнер форм Форми HTTP редирект після успішної обробки Приховане поле Якщо обрано, форма може бути переглянута лише за секрентним посиланням. Вести логи відправлених форм в базі даних. Налаштування пошти Повідомлення Вибір моделі Вибір моделі Множинний вибір моделі Множинний вибір Число Числа Один заголовок на рядок Одне значення на рядок Пароль Радіо-кнопка Регулярний вираз Зберегти всі завантажені файли на сховище сервера. Відсилати завантажені файли як поштові вкладення Налаштування Відправити Шаблони Текст Текстове поле Дякуємо, ваші дані успішно збережено. Дані не можуть бути збережені, будь-ласка спробуйте ще. Клас поля потребує регулярного модель Клас поля потребує регулярного виразу Це поле є обов’язковим. Даний тип файлу не допускається. Час Веб адреса Так/Ні Дозволити ініціалізацію через параметри в урлі Як абзаци Як таблиця Як таблиця (горизонтальна) Ненумерований список тіло Очистити форму після успішної обробки Кастомна реалізація Модель даних Десяткові розряди тема листа Пустий заголовок повідомлення про помилку поле Клас поля Назва поля поля форма Шаблон форми Довідковий текст Включити до результату Початкове значення Заголовок поля назви полів лог даних форм Максимум цифр Максимальна довжина Максимальне значення Шаблон повідомлення Метод Мінімальна довжина Мінімальне значення назва ні приховати URL цієї форми Позиція Регулярний вираз Вимагається зберегти завантажені файли відсилати дані з форми на електронні адреси адреса відправника напис на кнопці відправки повідомлення про успіх цільовий URL заголовок Значення значення Віджет так 