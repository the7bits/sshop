__author__ = 'lex'
from django import template
from form_designer.views import process_form
from form_designer.models import FormDefinition
from form_designer.views import detail
register = template.Library()

@register.simple_tag(name='render_form', takes_context=True)
def render_form(context, form_name):
    #form_definition = FormDefinition.objects.get(name=form_name)
    #return process_form(context['request'], form_definition, context, disable_redirection=True)
    return detail(context['request'], form_name)
