# -*- coding: utf-8 -*-
LOCAL_SETTINGS = True
from settings import *

DEBUG = False

SENTRY_DSN = 'https://4d24c79725a443e09eb31a9e1e6c05fa:d0b6d1789f584055916328a959fdb1ed@app.getsentry.com/3425'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'drom_sshop',
        'USER': 'drom_sshop',
        'PASSWORD': 'ybhirp3o',
        'HOST': '',
        'PORT': '',
    }
}

MEDIA_ROOT = '/home/drom/webapps/sshop_media/'
MEDIA_URL = '/media/'
STATIC_ROOT = '/home/drom/webapps/sshop_static/'
STATIC_URL = '/static/'
LFS_LOG_FILE = '/home/drom/logs/sshop.log'

EMAIL_DEBUG = DEBUG
CONTACT_EMAIL = 'sshop@the7bits.com'
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'drom_sshop'
EMAIL_HOST_PASSWORD = '32de61ce'
EMAIL_PORT = 25
DEFAULT_FROM_EMAIL = 'no-reply@the7bits.com'

MIDDLEWARE_CLASSES = (
    'raven.contrib.django.middleware.SentryResponseErrorIdMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'sshop.middleware.TimezoneMiddleware',
    'sshop.middleware.Http403Middleware',        
    'lfs.utils.middleware.RedirectFallbackMiddleware',
    'pagination.middleware.PaginationMiddleware',
    'raven.contrib.django.middleware.Sentry404CatchMiddleware',
)

TEMPLATE_LOADERS = (
    ('django.template.loaders.cached.Loader', (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    )),
)

INSTALLED_APPS = (
    'raven.contrib.django',
) + INSTALLED_APPS

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.handlers.SentryHandler',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': 'unix:/home/drom/memcached.sock',
        'TIMEOUT': 2592000,
        'KEY_PREFIX': 'sshop',
    }
}

COMPRESS_ENABLED = True

DEFAULT_CURRENCY_CODE = 'RUB'
